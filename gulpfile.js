const gulp = require('gulp'),
		exec = require('child_process').exec,
		rename = require('gulp-rename'),
		concat = require('gulp-concat'),
		order = require("gulp-order"),
		mainBowerFiles = require('main-bower-files'),
		babel = require('gulp-babel'),
		sourcemaps = require('gulp-sourcemaps'),
		jshint = require('gulp-jshint'),
		uglify = require('gulp-uglify'),
		gutil = require('gulp-util'),
		jsdoc = require('gulp-jsdoc3');


const paths = {
	jsLocalSrc: 'mfisheries/static/js/**/*.js',
	cssLocalSrc: 'mfisheries/static/css/**/*.css',
	jsDeployPath: 'mfisheries/static/dist/js/',
	jsVendorDeployPath: 'mfisheries/static/dist/vendor/',
	bowerPath: 'mfisheries/static/bower_components',
	vendorPath: 'mfisheries/static/dist/vendor'
};
const babel_ignore = [
	'mfisheries/static/js/caputils/config.js',
	'mfisheries/static/js/caputils/caplib.js',
	'mfisheries/static/js/caputils/OpenLayers.js'
];

// The external libraries in the order required
const js_libraries = [
	//	Independent Libraries
	"mfisheries/static/bower_components/underscore/underscore-min.js",
	// "mfisheries/static/bower_components/outdated-browser/outdatedbrowser/outdatedbrowser.min.js",
	"mfisheries/static/bower_components/moment/min/moment.min.js",
	"mfisheries/static/bower_components/sweetalert2/dist/sweetalert2.all.min.js",
	"mfisheries/static/bower_components/geolocator/dist/geolocator.min.js",
	// jQuery Related Libraries
	"mfisheries/static/bower_components/jquery/dist/jquery.min.js",
	"mfisheries/static/bower_components/fullcalendar/dist/fullcalendar.min.js",
	'mfisheries/static/bower_components/datatables/media/js/jquery.dataTables.min.js',
	// Bootstrap related libraries
	'mfisheries/static/bower_components/bootstrap/dist/js/bootstrap.min.js',
	'mfisheries/static/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js',
	// Angular related libraries
	'mfisheries/static/bower_components/angular/angular.min.js',
	'mfisheries/static/bower_components/angular-route/angular-route.min.js',
	// 'mfisheries/static/bower_components/angular-spinners/dist/angular-spinners.min.js',
	// 'mfisheries/static/bower_components/angular-ui-sortable/sortable.min.js',
	// 'mfisheries/static/bower_components/angular-sanitize/angular-sanitize.min.js',
	// 'mfisheries/static/bower_components/angular-animate/angular-animate.min.js',
	'mfisheries/static/bower_components/tg-angular-validator/dist/angular-validator.min.js',
	
	'mfisheries/static/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js', // Needed for bootstrap-lightbox
	'mfisheries/static/bower_components/angular-bootstrap-lightbox/dist/angular-bootstrap-lightbox.min.js',
	'mfisheries/static/bower_components/angulartics/dist/angulartics.min.js',
	'mfisheries/static/bower_components/angulartics-google-analytics/dist/angulartics-ga.min.js',
	// 'mfisheries/static/bower_components/angularjs-table/dist/ap-mesa.min.js',
	'mfisheries/static/bower_components/angular-datatables/dist/angular-datatables.min.js',
	// Firebase related libraries
	'mfisheries/static/bower_components/firebase/firebase.js',
	'mfisheries/static/bower_components/angularfire/dist/angularfire.min.js',
	//	Temporarily removed push library to keep from conflict with Web system
	// 'node_modules/push.js/bin/push.min.js',
	// 'node_modules/push-fcm-plugin/bin/push.fcm.min.js',
];

/**
 * Development Tasks
 */

gulp.task('lint', function () {
	return gulp.src(paths.jsLocalSrc)
			.pipe(jshint(".jshintrc"))
			.pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('pack-vendor-js', function () {
	return gulp.src(js_libraries)
			.pipe(order(js_libraries, {base: './'}))
			.pipe(sourcemaps.init())
			.pipe(concat('bundle.js'))
			.pipe(gulp.dest(paths.jsVendorDeployPath))
			.pipe(rename('bundle.min.js'))
			.pipe(uglify({mangle: false}))
			.pipe(sourcemaps.write('.'))
			.pipe(gulp.dest(paths.jsVendorDeployPath));
});

gulp.task('pack-js', function () {
	return gulp.src(paths.jsLocalSrc)
			.pipe(order([
				'mfisheries/static/js/polyfills.js',
				'mfisheries/static/js/caputils/config.js',
				'mfisheries/static/js/caputils/caplib.js',
				'mfisheries/static/js/caputils/OpenLayers.js',
				'mfisheries/static/js/ga/firebase_config.js',
				'mfisheries/static/js/ga/google_analytics.js',
				'mfisheries/static/js/ga/push_notify.js',
				'mfisheries/static/js/main.js',
				'mfisheries/static/js/controllers/core/libs.js',
				paths.jsLocalSrc
			], {base: './'}))
			.pipe(sourcemaps.init())
			.pipe(babel({
				presets: ['es2015'],
				ignore: babel_ignore
			}))
			.pipe(concat('app.js'))
			.pipe(gulp.dest(paths.jsDeployPath))
			.pipe(rename('app.min.js'))
			.pipe(uglify({mangle: false})) // TODO: Determine why setting Mangle to true crashes the app
			.pipe(sourcemaps.write('.'))
			.pipe(gulp.dest(paths.jsDeployPath));
});

gulp.task('watch-js', function () {
	gulp.watch(paths.jsLocalSrc, ['lint', 'pack-js']);
});

gulp.task('pack-js-deploy', function () {
	return gulp.src(paths.jsLocalSrc)
			.pipe(sourcemaps.init())
			.pipe(babel({
				presets: ['es2015'],
				ignore: babel_ignore
			}))
			.pipe(concat('app.js'))
			.pipe(gulp.dest(paths.jsDeployPath))
			.pipe(rename('app.min.js'))
			.pipe(uglify({mangle: false})) // TODO: Determine why setting Mangle to true crashes the app
			.pipe(sourcemaps.write('.'))
			.pipe(gulp.dest(paths.jsDeployPath));
});

/**
 * Library Packaging Tasks
 */

gulp.task('pack-libs', function () {
	return gulp.src(mainBowerFiles({
		paths: {
			bowerDirectory: paths.bowerPath,
			bowerJson: 'mfisheries/static/bower.json',
			debugging: true
		}}))
		.pipe(concat('bundle.js'))
		.pipe(gulp.dest(paths.vendorPath));
});

// gulp.task('pack-libs', function () {
// 	return gulp.src([
// 		'assets/js/vendor/*.js'
// 	])
// 	.pipe(order([
// 		'assets/js/vendor/jquery-2.2.4.min.js',
// 		'assets/js/vendor/*.js'
// 	], {base: './'}))
// 	.pipe(concat('bundle.js'))
// 	.pipe(gulp.dest('public/build/js'));
// });


gulp.task('pack-css', function () {
	return gulp.src(['assets/css/vendor/*.css', 'assets/css/main.css'])
			.pipe(concat('stylesheet.css'))
			.pipe(gulp.dest('public/build/css'));
});

gulp.task('watch-css', function () {
	gulp.watch(paths.cssLocalSrc, ['pack-css']);
});

gulp.task('serve', function (cb) {
	exec('pserve development.ini --reload', function (err, stdout, stderr) {
		console.log(stdout);
		console.log(stderr);
		cb(err);
	});
});

gulp.task('deploy', ['pack-js-deploy', 'service-worker']);

gulp.task('watch', ['watch-js','service-worker', 'watch-css']);
// gulp.task('watch', ['watch-js', 'watch-css']);

//https://github.com/mlucool/gulp-jsdoc3
gulp.task('docs', function (cb) {
	gulp.src([paths.jsLocalSrc], {read: false})
			.pipe(jsdoc(cb));
});

// Adding the process for generating the list of pre-caching files
const workbox_config = require("./workbox-config");
const workboxBuild = require('workbox-build');
gulp.task('service-worker', function(){
	console.log("Generating the Service worker to update version of files");
	return workboxBuild.injectManifest(workbox_config);
});

// create a default task and just log a message
// gulp.task('default', ['pack-js', 'pack-libs', 'pack-css']);
gulp.task('default', ['pack-js', 'serve', 'watch', 'docs']);