#!/usr/bin/env bash

echo "Save Country"
curl -v -H "Accept: application/json" -H "Content-type: application/json" -X POST -d ' {"name":"bahamas", "path":""}' http://localhost:8080/api/add/country
curl -v -H "Accept: application/json" -H "Content-type: application/json" -X POST -d ' {"name":"belize", "path":""}' http://localhost:8080/api/countries

echo "Update Country"
curl -v -H "Accept: application/json" -H "Content-type: application/json" -X POST -d ' {"name":"bahamas", "path":"bahamas", "id":7}' http://localhost:8080/api/update/country
curl -v -H "Accept: application/json" -H "Content-type: application/json" -X PUT -d ' {"name":"belize", "path":"belize", "id":8}' http://localhost:8080/api/countries

echo "Retrieve Countries"
curl http://localhost:8080/api/get/country/list
curl http://localhost:8080/api/countries

echo "Delete Countries"
curl http://localhost:8080/api/delete/country/7
curl -X DELETE http://localhost:8080/api/countries/8 # Not working