#!/bin/bash
# This shell file installs the neccessary dependencies for the mFisheries server

#Development tools 
sudo yum groupinstall 'Development Tools' -y
#Python Devel
sudo yum install mysql-devel.x86_64 -y
#Mod Wsgi
sudo yum install mod_wsgi -y
#Python argparse
sudo yum install python-argparse.noarch
#MySQL-Python
sudo pip install MySQL-python
#Python-prb
sudo pip install pbr
#Pyramid
sudo pip install pyramid==1.5.7

# Executing Pyramid's setup file
python setup.py develop 


