import json
import os
import sys

from pyramid.config import Configurator
from pyramid.paster import get_appsettings, setup_logging
from sqlalchemy import engine_from_config

from mfisheries import load_system_modules, load_local_modules
from mfisheries.models import initialize_sql


def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri> [var=value](example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)


def install_database_tables(settings):
    print("Creating Tables within intializedb.py")
    print(settings)
    engine = engine_from_config(settings, 'sqlalchemy.')
    try:
        # Set configuration to scan modules so we can install
        config = Configurator(settings=settings)
        config = load_system_modules(config)
        load_local_modules(config)

        # Run the installation for the System Database
        engine = initialize_sql(engine)
        return engine, True, None
    except Exception as e:
        print(e)
        return engine, False, e


def main(argv=sys.argv):
    print("Attempting to load initialise db")
    # Load configuration from the system
    if len(argv) < 2:
        usage(argv)
    config_uri = argv[1]
    print("Config uri" + str(config_uri))
    setup_logging(config_uri)
    settings = get_appsettings(config_uri)
    print(settings)
    # Run the process of installing the database
    engine, res, e = install_database_tables(settings)
    if not res:  # If error occurred raise exception
        raise Exception(e)
