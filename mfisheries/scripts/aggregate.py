import json
import re
import xml.etree.ElementTree as ET

import requests


def aggregate():
    sources = getSources()
    for source in sources:
        print(source)
        fetchAlerts(source)


# get alerts from CAP server
def fetchAlerts(source):
    r = requests.get(source["url"])
    xml = r.text

    root = ET.fromstring(xml)

    ns = getNamespace(root)
    i = 0

    print("Last Alert = " + source["lastAlert"])
    for entry in root.findall("{http://www.w3.org/2005/Atom}entry"):
        if entry.find(ns + "id").text == source["lastAlert"]:
            print("All new alerts saved")
            return
        if i == 0:
            updateSources(source, entry.find(ns + "id").text)
        save(entry)
        i += 1


# get CAP sources from server
def getSources():
    r = requests.get("http://mfisheries.herokuapp.com/api/capsources")
    return r.json()


# update lastAlertId in CAP source
def updateSources(source, alertId):
    print("Updating sources " + alertId)
    source["lastAlert"] = alertId
    r = requests.put("http://mfisheries.herokuapp.com/api/capsources/" + str(source["id"]), data=json.dumps(source))
    return r.text


def getNamespace(element):
    m = re.match('\{.*\}', element.tag)
    return m.group(0) if m else ''


# save to database
def save(entry):
    alert = entryToAlert(entry)

    data = {
        'uid': "root",
        'password': "pass",
        'xml': ET.tostring(alert, method='xml')
    }

    print(ET.tostring(alert, method='xml'))
    r = requests.post("http://fewercapdemo.herokuapp.com/post/", data)


# print r.text

def entryToAlert(entry):
    namespace = getNamespace(entry)

    alert = ET.Element('alert')
    alert.set("xmlns", "urn:oasis:names:tc:emergency:cap:1.2")

    id = ET.SubElement(alert, 'identifier')
    id.text = entry.find(namespace + "id").text

    sender = ET.SubElement(alert, 'sender')
    sender.text = entry.find(namespace + "author").find(namespace + "name").text

    sent = ET.SubElement(alert, 'sent')
    sent.text = entry.find(namespace + "updated").text

    capNamespace = getNamespace(entry.find("{urn:oasis:names:tc:emergency:cap:1.1}status"))

    status = ET.SubElement(alert, 'status')
    status.text = entry.find(capNamespace + "status").text

    msgType = ET.SubElement(alert, 'msgType')
    msgType.text = entry.find(capNamespace + "msgType").text

    scope = ET.SubElement(alert, 'scope')
    scope.text = "Public"

    info = ET.SubElement(alert, 'info')

    language = ET.SubElement(info, 'language')
    language.text = "en-us"

    category = ET.SubElement(info, 'category')
    category.text = entry.find(capNamespace + "category").text

    event = ET.SubElement(info, 'event')
    event.text = entry.find(capNamespace + "event").text

    if entry.find(capNamespace + "responseType") is not None:
        responseType = ET.SubElement(info, 'responseType')
        responseType.text = entry.find(capNamespace + "responseType").text
    else:
        responseType = ET.SubElement(info, 'responseType')
        responseType.text = "Shelter"

    urgency = ET.SubElement(info, 'urgency')
    urgency.text = entry.find(capNamespace + "urgency").text

    severity = ET.SubElement(info, 'severity')
    severity.text = entry.find(capNamespace + "severity").text

    certainty = ET.SubElement(info, 'certainty')
    certainty.text = entry.find(capNamespace + "certainty").text

    expires = ET.SubElement(info, 'expires')
    expires.text = entry.find(capNamespace + "expires").text

    senderName = ET.SubElement(info, 'senderName')
    senderName.text = entry.find(namespace + "author").find(namespace + "name").text

    headline = ET.SubElement(info, 'headline')
    headline.text = entry.find(namespace + "title").text

    desc = ET.SubElement(info, 'description')
    desc.text = entry.find(namespace + "summary").text

    if entry.find(namespace + "link") is not None:
        web = ET.SubElement(info, 'web')
        web.text = entry.find(namespace + "link").text
    else:
        web = ET.SubElement(info, 'web')
        web.text = "none"

    for param in entry.findall(capNamespace + "parameter"):
        parameter = ET.SubElement(info, 'parameter')
        vname = ET.SubElement(parameter, 'valueName')
        vname.text = param.find(namespace + "valueName").text
        val = ET.SubElement(parameter, 'value')
        val.text = param.find(namespace + "value").text

    area = ET.SubElement(info, 'area')

    areaDesc = ET.SubElement(area, 'areaDesc')
    areaDesc.text = entry.find(capNamespace + "areaDesc").text

    for poly in entry.findall(capNamespace + "polygon"):
        polygon = ET.SubElement(area, 'polygon')
        polygon.text = poly.text

    for circ in entry.findall(capNamespace + "circle"):
        circle = ET.SubElement(area, 'circle')
        circle.text = circ.text

    for geo in entry.findall(capNamespace + "geocode"):
        geocode = ET.SubElement(area, 'geocode')
        vname = ET.SubElement(geocode, 'valueName')
        vname.text = geo.find(namespace + "valueName").text
        val = ET.SubElement(geocode, 'value')
        val.text = geo.find(namespace + "value").text

    return alert


if __name__ == "__main__":
    print("Running aggregate")
    aggregate()
