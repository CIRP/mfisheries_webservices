# Based on http://pyramid-cookbook.readthedocs.io/en/latest/deployment/heroku.html
#!/bin/bash
set -e
# Setup Pyramid and dependencies
#python setup.py develop
pip install -e .

# Initialize Database
initialize_mfisheries_db development.ini
# Run bower
cd ./mfisheries/static/
python init_modules.py
#../../node_modules/bower/bin/bower install bower.json --force-latest
cd ../../
# Run App
python ./mfisheries/scripts/runapp.py
