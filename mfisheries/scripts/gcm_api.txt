GCM API


Description: Get all user groups
Type : GET
URL : /api/get/all/groups/{userid}
Response:
		success : {'status' : 200, "data" : [{"id" : "val", "groupname" : "val"}]}
		error: {'status' : 500, "data" : []}


#######################################################################################################3333

Description : Subscribe to group
Type : GET
URL : /api/add/user/to/group/{userid}/{groupid}/{gcm}
Response:
		success : {'status' : 200}
		error : {'status' : 500}


#######################################################################################################3333		


Description: Unsubscribe group
Type : GET
URL	: /api/delete/user/from/group/{userid}/{groupid}
Response:
		success: {'status' : 200}
		
		error: {'status' : 500}	


#######################################################################################################3333


Description : Send message to group
Type : POST
URL : /api/add/message/to/group

Request : 

	{
		"userid" 	: "val",
		"groupid" 	: "val",
		"msg"		: {
						"user" : "Jane doe",
						"msg" : "Hello world",
						"time" : "10:42",
						"lat" : "16.090",
						"lng":"-61.987"
					}
	}	



Response :	
	success : {'status' : 200, "response" : "messaages saved and sent"}	
	partial success : {'status' : 200, "response" : "sent but not saved"}	
	error 1 : {'status' : 501, "response" : "messages not sent"}	
	error 2 : {'status' : 501, "response" : "group does not exists"}	