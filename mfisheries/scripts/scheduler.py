#!/home/kyle/mfisheries_webservices/venv/bin/python
import os, site, sys, subprocess

## #!/usr/bin/env python

PYTHON_VERSION = "python2.7"
# MFISH_PATH="/home/mfisheries/public_html/mfisheries/"
M_FISH_PATH = "/home/kyle/mfisheries_webservices/"
M_FISH_VENV = os.path.join(M_FISH_PATH, "venv")
M_FISH_BIN_PATH = os.path.join(M_FISH_VENV, "bin")
M_FISH_INI_PATH = os.path.join(M_FISH_PATH, "development.ini")
M_FISH_SITE_PATH = os.path.join(M_FISH_VENV, 'lib', PYTHON_VERSION, 'site-packages')

print("M_FISH_PATH {0}".format(M_FISH_PATH))
print("M_FISH_VENV {0}".format(M_FISH_VENV))
print("M_FISH_INI_PATH {0}".format(M_FISH_INI_PATH))
print("PYTHON_VERSION {0}".format(PYTHON_VERSION))
print("M_FISH_SITE_PATH {0}".format(M_FISH_SITE_PATH))

# Add the site-packages of the chosen virtualenv to work with
site.addsitedir(M_FISH_SITE_PATH)
# Add the app's directory to the PYTHONPATH
sys.path.append(M_FISH_PATH)

# Activate your virtual env
activate_env=os.path.expanduser(M_FISH_BIN_PATH + "/activate_this.py")
# print("Activating the project virtual environment")
execfile(activate_env, dict(__file__=activate_env))
# print("Environment was successfully activated")

from pyramid.paster import setup_logging
setup_logging(M_FISH_INI_PATH)

# Handling the Weather Updates
# try:
# 	# print("Starting the environment at the path: {0}".format(M_FISH_INI_PATH))
# 	command_str = os.path.join(M_FISH_BIN_PATH, "fetch_weather")
# 	# print("Executing command line command: {0}".format(command_str))
# 	subprocess.call([command_str, M_FISH_INI_PATH])
# except Exception as e:
# 	print(e)

# Handling the Alert Updates
command_str = os.path.join(M_FISH_BIN_PATH, "run_alert_broadcast")
subprocess.call([command_str, M_FISH_INI_PATH])


# Clear up old alerts (Community)
