#!/usr/bin/env bash
#python -m mfisheries.tests
source ./venv/bin/activate
pip install -e ".[testing]"
py.test --pyargs mfisheries --cov=mfisheries --ignore=mfisheries/static/bower_components/