from mfisheries.util import get_ini_path
from mfisheries.events import NotifyError
import logging
log = logging.getLogger(__name__)


def trigger_notification(use_command_line=True):
	log.debug("Displaying notification", "info", module_name=__name__)
	from pyramid.config import Configurator
	from pyramid.paster import bootstrap
	config = None
	if use_command_line:
		ini_path = get_ini_path()
		if ini_path:
			with bootstrap(ini_path) as env:
				settings = env['registry'].settings
				config = Configurator(settings=settings)
	else:
		config = Configurator()
	config.registry.notify(NotifyError("Test", "This is a test for notification"))

