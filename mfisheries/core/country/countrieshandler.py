import json
import logging
log = logging.getLogger(__name__)

from cornice import Service
from sqlalchemy import func, asc

from country import Country
from mfisheries.models import DBSession
from mfisheries.core.user import User

from cornice.resource import resource
from mfisheries.core import BaseHandler



@resource(collection_path='/api/countries',path='/api/countries/{id}',description="Countries")
class CountryHandler(BaseHandler):
    def _get_order_field(self):
        return Country.name

    def __init__(self, request, context=None):
        BaseHandler.__init__(self, request, context)

    def _create_empty_model(self):
        return Country("", "")

    def _get_fields(self):
        return self._create_empty_model().getRequiredFields()

    def _get_target_class(self):
        return Country

    def _dict_to_model(self, data, rec_id=0):
        # If id is 0, we want to create a new entry
        if rec_id == 0:
            src = self._create_empty_model()
        else:
            src = DBSession.query(self._target_class).get(rec_id)

        # Assign value to the fields of the model
        src.name = data['name']
        src.areacode = data['areacode']
        src.code = data['code']

        if "path" in data['path'] or data['path'] is not "":
            src.path = data['path']
        else:
            src.path = "static/country_modules/{0}".format(data['code'])

        return src

    def collection_get(self):
        res = super(CountryHandler, self).collection_get()
        if "auths" in self.request.GET or "auth" in self.request.GET:
            recs = []
            try:
                for row in res:
                    user = DBSession.query(User).filter(User.countryid == row['id'], User._class == 1).first()
                    if user is not None:
                        row['authority'] = user.toJSON()
                    recs.append(row)
                res = recs
            except Exception, e:
                log.error("Error occured when attempting to retrieve authorities: {0}".format(e))
        return res

    def _handle_conditional_get(self, query):
        if "countryid" in self.request.GET:
            cid = self.request.GET['countryid']
            try:
                if cid != 0 and cid != "0":
                    query = query.filter(Country.id == cid)
            except Exception, e:
                log.error("Error occured when attempting to retrieve authorities: {0}".format(e))
        return query