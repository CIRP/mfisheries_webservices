import logging

from cornice.resource import resource
from sqlalchemy import desc

from mfisheries.core import BaseHandler
from mfisheries.models import DBSession
from .countryloc import CountryLocation

log = logging.getLogger(__name__)


# http://cornice.readthedocs.io/en/latest/resources.html
@resource(collection_path='/api/countrylocs',path='/api/countrylocs/{id}',description="Country Locations")
class CountryLocationHandler(BaseHandler):
	def _get_order_field(self):
		return CountryLocation.countryid

	def __init__(self, request, context=None):
		BaseHandler.__init__(self, request, context)
		self.order_func = desc
	
	def _create_empty_model(self):
		return CountryLocation("", "", "")
	
	def _get_fields(self):
		return self._create_empty_model().getRequiredFields()
	
	def _get_target_class(self):
		return CountryLocation
	
	def _dict_to_model(self, data, rec_id=0):
		# If id is 0, we want to create a new entry
		if rec_id == 0:
			src = self._create_empty_model()
		else:
			src = DBSession.query(self._target_class).get(rec_id)
		# Assign value to the fields of the model
		src.countryid = data["countryid"]
		src.latitude = data["latitude"]
		src.longitude = data["longitude"]
		if "zoom" in data:
			src.zoom = data['zoom']
		if "createdby" in data:
			src.createdby = data["createdby"]
		return src
	
	