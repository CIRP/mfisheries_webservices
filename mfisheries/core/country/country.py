import transaction
from sqlalchemy import (
	Column,
	Integer,
	String
)
from sqlalchemy import event
from sqlalchemy.orm import relationship

from mfisheries.models import Base, DBSession


class Country(Base):
	"""The SQLAlchemy declarative model class for a Country object"""
	__tablename__ = 'country'
	id = Column(Integer, primary_key=True)
	path = Column(String(100))
	name = Column(String(100), unique=True, nullable=False)
	areacode = Column(String(5))
	code = Column(String(4))

	# Relationships
	occupations = relationship("Occupation")

	def __init__(self, path, name):
		self.path = path
		self.name = name

	def toJSON(self):
		return {
			'id': self.id,
			'path': self.path,
			'name': self.name,
			'areacode': self.areacode,
			'code': self.code
		}

	def getRequiredFields(self):
		return ["name", "areacode", "code"]

	def getCode(self):
		return self.code


# Function to represent country based on 3 letter codes derived from: http://www.nationsonline.org/oneworld/country_code_list.htm
def create_default_countries(target, connection_rec, **kw):
	print("Creating Default Countries")
	countries = [
		["static/country_modules/trinidad", "Trinidad", "868", "tri"],
		["static/country_modules/tobago", "Tobago" , "868", "tob"],
		["static/country_modules/grenada", "Grenada", "473", "grd"],
		["static/country_modules/dominica", "Dominica", "767", "dma"],
		["static/country_modules/slu", "St Lucia", "768", "lca"],
		["static/country_modules/skn", "St Kitts and Nevis", "869", "kna"],
		["static/country_modules/svg", "St Vincent and the Grenadines", "784", "vct"],
		["static/country_modules/atg", "Antigua and Barbuda", "268", "atg"],
		["static/country_modules/belize", "Belize", "501", "blz"],
		["static/country_modules/brb", "Barbados", "246", "brb" ],
		["static/country_modules/bhs","Bahamas", "242", "bhs"],
		["static/country_modules/guy","Guyana", "592", "guy"],
		["static/country_modules/aia","Anguilla", "264", "aia"]
	]
	try:
		for c in countries:
			country = Country(c[0], c[1])
			country.areacode = c[2]
			country.code = c[3]
			DBSession.add(country)
		DBSession.flush()
	except Exception as e:
		print("Error: Inserting Countries Failed: %s" % e.message)
		DBSession.rollback()
	transaction.commit()


event.listen(Country.__table__, 'after_create', create_default_countries)
