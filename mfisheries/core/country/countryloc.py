from sqlalchemy import (
	Column,
	Integer,
	String,
	TIMESTAMP,
	ForeignKey,
)
from sqlalchemy import event
from sqlalchemy.orm import relationship

from country import Country
from mfisheries.models import Base, DBSession
from datetime import datetime
import transaction


class CountryLocation(Base):
	"""SQLAlchemy declarative model class for a Country Location object"""
	__tablename__ = "countrylocations"
	id = Column(Integer, primary_key=True)
	countryid = Column(Integer, ForeignKey('country.id'), nullable=False, unique=True)
	latitude = Column(String(20), nullable=False)
	longitude = Column(String(20), nullable=False)
	zoom = Column(String(2), nullable=False, default="10")
	createdby = Column(Integer, ForeignKey('user.id'))
	timecreated = Column(TIMESTAMP, default=datetime.utcnow)
	
	country = relationship(Country, foreign_keys=[countryid], lazy='subquery')
	
	def __init__(self, cid, lat, long, zoom=10):
		self.countryid= cid
		self.latitude = lat
		self.longitude = long
		self.zoom = zoom
	
	def getRequiredFields(self):
		return ['countryid', 'latitude', 'longitude']
	
	def toJSON(self):
		rec = {
			"id": self.id,
			"countryid": self.countryid,
			"latitude": self.latitude,
			"longitude": self.longitude,
			"zoom" : self.zoom,
			"createdby": self.createdby,
			"timecreated": str(self.timecreated)
		}
		if self.country:
			rec['country'] = self.country.name
		return rec
	
	
def create_default_country_loc(target, connection_rec, **kw):
	from mfisheries.core import Country
	print("Creating default country locations")
	# Extracted from https://developers.google.com/public-data/docs/canonical/countries_csv
	locations = [
		["Trinidad", "10.691803", "-61.222503", 10],
		["Tobago", "11.2849228", "-60.8219893", 12],
		["Grenada", "12.262776", "-61.604171", 10],
		["Dominica", "15.414999", "-61.370976", 10],
		["St Lucia", "13.909444", "-60.978893", 10],
		["St Kitts and Nevis", "17.357822", "-62.782998", 10],
		["St Vincent and the Grenadines", "12.984305", "-61.287228", 10],
	]
	try:
		for loc in locations:
			c = DBSession.query(Country).filter(Country.name == loc[0]).first()
			if c:
				l_model = CountryLocation(c.id, loc[1], loc[2], loc[3])
				DBSession.add(l_model)
		DBSession.flush()
	except Exception as e:
		print("Error: Inserting Countries Failed: %s" % e.message)
		DBSession.rollback()
	transaction.commit()

event.listen(Country.__table__, 'after_create', create_default_country_loc)