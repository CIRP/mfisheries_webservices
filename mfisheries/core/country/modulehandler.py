# import json
import logging
import os
import shutil
import transaction  # transaction manager
from cornice import Service
# from sqlalchemy import func, text, asc

from country import Country
from countrymodule import CountryModule
from mfisheries import APP_ROOT
from mfisheries.core.module import Module
from mfisheries.models import DBSession
from mfisheries.util.secure import validate
# Using the BaseHandler
from mfisheries.core import BaseHandler
from cornice.resource import resource

log = logging.getLogger(__name__)

ab_module_path = os.path.join(APP_ROOT, "")
rel_module_path = os.path.join("", "")


# http://cornice.readthedocs.io/en/latest/resources.html
@resource(collection_path='/api/modules', path='/api/modules/{id}', description='mFisheries Modules')
class ModuleHandler(BaseHandler):
    
    def __init__(self, request, context=None):
        BaseHandler.__init__(self, request, context)
    
    def _get_fields(self):
        return ['name']
    
    def _create_empty_model(self):
        return Module("", "", "")
    
    def _get_target_class(self):
        return Module
    
    def _get_order_field(self):
        return Module.name
    
    def _dict_to_model(self, data, rec_id=0):
        # If id is 0, we want to create a new entry
        if rec_id == 0:
            src = self._create_empty_model()
        else:
            src = DBSession.query(self._target_class).get(rec_id)
        # Assign value to the fields of the model
        src.name = data['name']
        if 'description' in data:
            src.description = data['description']
        if 'hasDownload' in data:
            src.hasDownload = data['hasDownload']
        return src


@resource(collection_path='/api/country/modules', path='/api/country/modules/{id}', description='mFisheries Country Modules')
class CountryModuleHandler(BaseHandler):
    
    def __init__(self, request, context=None):
        BaseHandler.__init__(self, request, context)
    
    def _get_fields(self):
        return ['countryid', 'moduleid']
    
    def _create_empty_model(self):
        return CountryModule("", "", "")
    
    def _get_target_class(self):
        return CountryModule
    
    def _get_order_field(self):
        return CountryModule.countryid
    
    def _dict_to_model(self, data, rec_id=0):
        # If id is 0, we want to create a new entry
        if rec_id == 0:
            src = self._create_empty_model()
        else:
            src = DBSession.query(self._target_class).get(rec_id)
        # Assign value to the fields of the model
        src.countryid = data['countryid']
        src.moduleid = data['moduleid']
        if "path" in data:
            src.path = data['path']
        
        return src


# # API Operations for Country Module Files
addModuleFile = Service(name="addModuleFile", path="/api/add/modulefile", description="adds a new module file")


@addModuleFile.post()
# @moduleFileService.post()
def upload_module_file(request):
    if not validate(request):
        return {'status': 401, 'data': []}
    try:
        # check parameters
        filename = request.POST['upload'].filename
        upfile = request.POST['upload'].file
        countryid = request.POST['countryid']
        moduleid = request.POST['moduleid']
    except Exception as e:
        return {'status': 400, "response": "Invalid request"}

    # All parameters received
    extension = filename.split(".")
    print(extension[-1])
    if len(extension) > 1:
        if extension[-1] == "zip":
            try:
                country = DBSession.query(Country).get(countryid)
                module = DBSession.query(Module).get(moduleid)
                if country is not None and module is not None:

                    country_path = os.path.join(ab_module_path, country.path)
                    local_path = os.path.join(rel_module_path, country.path)
                    # Ensure country path exists
                    if not os.path.exists(country_path):
                        os.makedirs(country_path)
                    # Set full path for file
                    file_path = os.path.join(country_path, filename)
                    local_path = os.path.join(local_path, filename)
                    temp_file_path = file_path + '~'
                    upfile.seek(0)
                    # Upload file to temp location
                    with open(temp_file_path, 'wb') as output_file:
                        shutil.copyfileobj(upfile, output_file)
                    # Once upload complete then copy to desired location
                    os.rename(temp_file_path, file_path)

                    log.info("Saved data at: " + file_path)
                    cm = save_module_file(countryid, moduleid, local_path)
                    if cm is not None:
                        return {'status': 200, "response": "correct file type"}
                    else:
                        return {'status': 500, "response": "Error occurred while saving file"}
                else:
                    return {'status': 500, "response": "Country or Module specified was invalid"}
            except Exception as e:
                print(e.message)
                log.error("Error while uploading module file: " + str(e))
                return {'status': 500, "response": "Error occurred while saving file"}
    return {'status': 400, "response": "Incorrect file type"}


def save_module_file(countryid, moduleid, file_path):
    try:
        cm = DBSession.query(CountryModule).filter(CountryModule.countryid == countryid,
                                                   CountryModule.moduleid == moduleid).first()
        if cm is None:
            cm = CountryModule(countryid, file_path, moduleid)
        else:
            cm.path = file_path
        # Will perform update if exists and create otherwise
        DBSession.add(cm)
        # Update Module
        module = DBSession.query(Module).filter(Module.id == moduleid).one()
        module.hasDownload = "yes"
        DBSession.add(module)

        # Persist Changes
        DBSession.flush()
        transaction.commit()
        return cm
    except Exception as e:
        print(e.message)
        DBSession.rollback()
        return None
