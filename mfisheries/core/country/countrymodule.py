from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey,
    UniqueConstraint
)
from sqlalchemy.orm import relationship

from mfisheries.models import Base


class CountryModule(Base):
    """The SQLAlchemy declarative model class for a Module object"""
    __tablename__ = 'countrymodule'
    id = Column(Integer, primary_key=True)
    countryid = Column(Integer, ForeignKey('country.id'), nullable=False)
    moduleid = Column(Integer, ForeignKey('module.id'), nullable=False)
    path = Column(String(100))

    # http://docs.sqlalchemy.org/en/latest/core/constraints.html#sqlalchemy.schema.UniqueConstraint
    __table_args__ = (UniqueConstraint('countryid', 'moduleid', name='uixcm_1'),)

    country = relationship("Country", foreign_keys=[countryid])
    module = relationship("Module", foreign_keys=[moduleid])

    def __init__(self, countryid, path, moduleid):
        self.countryid = countryid
        self.path = path
        self.moduleid = moduleid

    def toJSON(self):
        rec = {
            'id': self.id,
            'countryid': self.countryid,
            'path': self.path,
            'moduleid': self.moduleid
        }

        if self.module:
            rec['module'] = self.module.name
        if self.country:
            rec['country'] = self.country.name

        return rec
