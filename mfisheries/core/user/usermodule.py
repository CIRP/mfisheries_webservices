from sqlalchemy import (
    Column,
    Integer,
    ForeignKey
)
from sqlalchemy.orm import relationship

from mfisheries.models import Base


class UserModule(Base):
    """The SQLAlchemy declarative model class for a UserModule object"""
    __tablename__ = 'usermodule'
    id = Column(Integer, primary_key=True)
    userid = Column(Integer, ForeignKey('user.id'), nullable=False)
    moduleid = Column(Integer, ForeignKey('module.id'), nullable=False)

    module = relationship("Module", foreign_keys=[moduleid])
    user = relationship("User", foreign_keys=[userid])

    def __init__(self, userid, moduleid):
        self.userid = userid
        self.moduleid = moduleid

    def toJSON(self):
        return {
            'id': self.id,
            'userid': self.userid,
            'moduleid': self.moduleid,
            'user': self.user.fname + " " + self.user.lname,
            'module': self.module.name
        }
