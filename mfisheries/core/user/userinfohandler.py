import json

import transaction  # transaction manager
from cornice import Service  # allows the services to be exposed via a RESTful service
from sqlalchemy import text  # enables the execution of raw sql

from mfisheries.models import DBSession
from mfisheries.util.secure import validate

getnewinfo = Service(name='getnewinfo', path='/api/get/new/user/info/{userid}',
                     description='returns the new user information by userid')
addnewuserinfo = Service(name='addnewuserinfo', path='/api/add/new/user/info/', description='add new users information')

userService = Service(name='loginService', path='/api/users/info', description='login service')


@getnewinfo.get()
@userService.get()
def get_userinfo_by_userid(request):  # .get() indicates to the framework that this method is of type GET
    if not validate(request):
        return {'status': 401, 'data': []}
    result = get_new_info_by_userid(request.matchdict['userid'])
    if not result:
        return {'status': 204, 'data': 'No user info found'}
    else:
        return {'status': 200, 'data': result}


@addnewuserinfo.post()
@userService.post()
def add_new_info(request):
    if not validate(request):
        return {'status': 401, 'data': []}
    userinfo = json.loads(request.body)

    if userinfo is not None:
        value = None
        # info = NewUserInfo(userinfo['userid'],
        # 			userinfo['userinfo'])
        # value = add_user_info(info)

        if value:
            return {'status': 201}
        else:
            request.errors.add('url', 'create', 'user not registered')
    else:
        request.errors.add('url', 'create', 'recieved empty user data')


def add_user_info(data):  # the data model parameter is passed
    try:
        DBSession.add(data)  # the database session adds the new data model to the database
        DBSession.flush()
        transaction.commit()
        return True
    except Exception, e:
        reason = e.message
        print(e.message)
        if "Duplicate entry" in reason:
            print("%s already in table." % e.params[0])
            DBSession.rollback()
        return False


def get_new_info_by_userid(userid):
    try:
        stmt = text(
            "SELECT id, userid, newinfo, DATE_FORMAT(timestamp, '%Y-%m-%d hh:mm:ss') as timestamp from newuserinfo where userid = :userid")  # raw sql statement
        result = DBSession.execute(stmt, {'userid': userid})  # binding parameters to statement
        """
            For each result convert the row into a python dictionary and retrn 
            the result set in a python list
        """
        arr = []
        print(result)
        for model in result:
            arr.append(dict(model))
        print(arr)
        return arr
    except Exception, e:
        print(e.message)
        return None
