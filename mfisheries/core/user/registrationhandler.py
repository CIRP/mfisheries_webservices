import hashlib
import json
import logging
import urllib
import json

import transaction
from sqlalchemy import text
from firebase_admin import auth


from mfisheries.core.user.alternativecontact import AlternativeContact
from mfisheries.core.user.occupations import Occupations
from mfisheries.core.user.userhandler import get_user_by_id
from mfisheries.core.user.users import User
from mfisheries.core.user.vessels import Vessels
from mfisheries.models import DBSession
from mfisheries.util.secure import validate

log = logging.getLogger(__name__)

from cornice import Service

googleregister = Service(name='googleregister', path='/api/add/google/user',description='returns a list of all countries')
userlogin = Service(name="userlogin", path='/api/user/login',description="authenticates coastguard or administrator users")
updateUserProfile = Service("updateUserProfile", path="/api/update/user/profile", description="updates user's profile")
getFullProfileInfo = Service('getFullProfileInfo', path='/api/get/user/full/profile/{userid}',description='returns full user profile')


userService = Service(name='userService', path='/api/user', description='user service')
loginService = Service(name='loginService', path='/api/user/login', description='login service')

userByName = Service(name='userServiceUsername', path='/api/user/username/{username}', description='user service by username')

@getFullProfileInfo.get()
@userService.get()
def get_full_profile(request):
	if not validate(request):
		return {'status': 401, 'data': []}
	profile = getfullprofileinfo(request.matchdict['userid'])
	if profile is not None:
		return {"status": 200, "data": profile}
	else:
		return {"status": 404, "data": []}


@updateUserProfile.post()
@userService.put()
def update_user_profile(request):
	if not validate(request):
		return {'status': 401, 'data': []}
	profiledetails = json.loads(request.body)
	log.info("Received from the server:")
	log.info(profiledetails)
	if profiledetails is not None and profiledetails is not False:
		userdata = updateuserprofile(profiledetails)
		DBSession.flush()
		log.info("Completed attempting update user profile function and produced: {0}".format(userdata))
		if userdata is not None or userdata is not False:
			return {"status": 200, "data": userdata}
	
	return {"status": 400, "data": "invalid operation requested"}


@userlogin.post()
@loginService.post()
def login_user(request):
	log.info("Attempting to log-in user")
	if not validate(request):
		log.info("Unauthorized request received")
		request.response.status = 401
		return {'status': 401, 'data': []}

	user_data = request.json_body

	if len(user_data) > 0:
		password = hashlib.sha256(user_data['password']).hexdigest()
		data = check_user(user_data['username'], password)
		
		if data is None:
			return request.errors.add('url', 'login', 'not found')
		else:
			print("received {0}, \nstored {1}".format(password, data.password))
			if password != data.password:
				request.response.status = 401
				return request.errors.add('url', 'login', 'invalid')
			result = data.toJSON()
			# Authentication was successful. Now we try to generate an firebase authentication token as specified: https://firebase.google.com/docs/auth/admin/create-custom-tokens#create_custom_tokens_using_the_firebase_admin_sdk
			try:
				dev_claims = {
					"displayName" : "{0} {1}".format(result['fname'], result['lname']),
					"email": result['email'],
					"country": result['country'],
					"countryid": result['countryid'],
					"mfish_id": result['id']
				}
				custom_token = auth.create_custom_token(uid="{0}_{1}".format(data.id, data.username), developer_claims=dev_claims)
				result['fb_token'] = custom_token
			except Exception as e:
				# Unable to create token for the user ... however, the log-in process does not have to fail
				log.error("Unable to load Firebase Data: {0}".format(e) )

			return {"status": 200, "data": result}
	else:
		log.error("Received no data from the client")
		request.response.status = 400
		return request.errors.add('url', 'create', 'received empty user data')


@googleregister.post()
@userService.post()
def add_user(request):
	if not validate(request):
		request.response.status = 401
		return {'status': 401, 'data': []}
	try:
		# Retrieve data from the request object
		user_data = json.loads(request.body)['data']
		# if there are no additional modules then set to empty string
		mod = json.loads(request.body)['modules'] if len(json.loads(request.body)['modules']) > 0 else [""]

		# modulesREquireAddInfo = ["photo diary", "alerts", "sos", "navigation", "lek"] # TODO Check which models require additional info
		m = {'modules[]': mod } # store the modules within the composed object
		modules = urllib.urlencode(m, True)
		log.info(modules)

		if "username" in user_data and "password" in user_data:
			password = hashlib.sha256(user_data['password']).hexdigest()
			data = check_user(user_data['username'], password)

			if data:
				log.info("User already exists with an id of: " + str(data.id))
				data.gcmId = user_data['firebaseToken'] # TODO - User - Does not accommodate if user has more than one device, consider splitting
				data.countryid = user_data['countryid']
				message = "User already exists. Records updated"
				user = register_user(data)
				status = 200
			else:
				user = User(
					user_data['username'],
					password,
					user_data['fname'],
					user_data['lname'],
					user_data['email'])
				user.countryid = user_data['countryid']

				if 'firebaseToken' in user_data:
					user.gcmId = user_data['firebaseToken']

				user = register_user(user)  # get newly created user from database
				status = 201
				message = "Successfully created user"
				request.response.status = 201

			if user:
				mod.append("country")
				#  add userid to module list
				mod.append("userid_" + str(user.id))
				#  convert list of modules and userid to url string
				m = {'modules[]': mod}
				modules = urllib.urlencode(m, True)
				log.debug(modules)

				return {
					"status": status,
					"modules": modules,
					"userid": str(user.id),
					"data": user.toJSON(),
					"message": message
				}
			else:
				log.error("Unable to create user in database")
				request.response.status = 400
				return request.errors.add('url', 'create', 'user not registered')
		else:
			request.response.status = 400
			return request.errors.add('url', 'create', 'received empty user data')
	except Exception as e:
		DBSession.rollback()
		log.error("Unable to register user: {0}".format(e.message))
		request.response.status = 500
		return request.errors.add('url', 'create', "system error occurred: {0}".format(e.message))



@userByName.get()
def get_user_by_username(request):
	user_name = request.matchdict['username']
	user = DBSession.query(User).filter(User.username == user_name).one_or_none()
	if user:
		return user.toJSON()
	else:
		request.response.status = 404
		return None

def check_user(username, password):
	# log.info("Attempting to Check credentials of user {0}".format(username))
	# res = DBSession.query(User).filter(User.username == username, User.password == password).one_or_none()
	res = DBSession.query(User).filter(User.username == username).one_or_none()
	return res


def register_user(data):
	try:
		DBSession.add(data)
		DBSession.flush()
		# transaction.commit()
		return data
	except Exception as e:
		log.error("Unable to register user: {0}".format(e.message))
		DBSession.rollback()
		return None


def updateuserprofile(data):
	user_id = ""
	res = True
	if 'initial' in data and res:
		res = updateinitialprofile(data['initial'])
		user_id = data['initial']['userid']
	if 'basic' in data and res:
		res = updatebasicprofile(data['basic'])
		user_id = data['basic']['userid']
	if 'standard' in data and res:
		res = updateStandardProfile(data['standard'])
		user_id = data['standard']['userid']
	if 'full' in data and res:
		res = updateFullProfile(data['full'])
		user_id = data['full']['userid']
	
	userdata = get_user_by_id(user_id)

	if res is False:  # Error Occurred when trying to save the data
		return False
	elif userdata is not None:
		return userdata
	else:
		return {}


def updateinitialprofile(data):
	try:
		user = DBSession.query(User).filter(User.id == data['userid']).one()
		if user is not None:
			user.countryid = data['countryid']
			user.mobileNum = data['mobileNum']
			DBSession.add(user)
			DBSession.flush()
			# transaction.commit()
			return True
	except Exception as e:
		log.error("Error Occurred: " + e.message)
	return False


def updatebasicprofile(data):
	try:
		user = DBSession.query(User).filter(User.id == data['userid']).one()
		user.age = data['age']
		user._class = 3
		log.info("Attempting to update: {0}".format(user))
		# Persist to database
		DBSession.add(user)
		DBSession.flush()
		# transaction.commit()
		# Add the occupation of the user to db
		adduseroccupations(data['userid'], data['occupations'])
		
		return True
	except Exception as e:
		log.error("Failed Inserting user profile:" + e.message)
		return False


def adduseroccupations(userid, data):
	try:
		# Create an user-occupation object for each
		for occupation in data:
			o = Occupations(userid, occupation)
			DBSession.add(o)
		# After Saving Items Attempt to Commit to the database
		DBSession.flush()
		# transaction.commit()
	except Exception as e:
		reason = e.message
		log.error("Failed Inserting user occupations: " + reason)
		DBSession.rollback()


def updateStandardProfile(data):
	try:
		log.debug(data)
		user = DBSession.query(User).filter(User.id == data['userid']).one()
		user.hstreet = data['hstreet']
		user.villageid = data['village']
		DBSession.add(user)
		if data['hasvessel'] == "yes":
			log.info("Has Vessel was passed as true")
			vessel = Vessels(data['userid'],
			                 data['vessel']['regnumber'],
			                 data['vessel']['description'],
			                 data['vessel']['length'],
			                 data['vessel']['hullcolor'],
			                 data['vessel']['insidecolor'],
			                 data['vessel']['hulltype'])
			
			DBSession.add(vessel)
		DBSession.flush()
		# transaction.commit()
		log.info("Added Standard Profile information for User")
		return True
	except Exception as e:
		log.error("Unable to update standard profile: " + e.message)
		DBSession.rollback()
	
	return False


def updateFullProfile(data):
	try:
		user = DBSession.query(User).filter(User.id == data['userid']).one()
		user.nat_id = data['nat_id']
		user.nat_dp = data['nat_dp']
		user.passport = data['passport']
		DBSession.add(user)
		if "emg" in data:
			altcontact = AlternativeContact(
				data['userid'],
				data['emg']['village'],
				data['emg']['fname'],
				data['emg']['lname'],
				data['emg']['street'],
				data['emg']['cellnum'],
				data['emg']['homenum'],
				data['emg']['email']
			)
			DBSession.add(altcontact)
		
		DBSession.flush()
		# transaction.commit()
		log.info("Saved full Profile successful")
		return True
	except Exception as e:
		log.error("Unable to save full profile: " + e.message)
		DBSession.rollback()
	return False


def getfullprofileinfo(userid):
	res = []
	try:
		stmt = text(
			"SELECT u.id, u.fname, u.lname, u.email, u.username,u.nat_dp, u.nat_id, u.passport, u.hstreet, v.name as village, u.age, c.name as country, u.mobileNum, u.countryid, u.villageid as villageid from user u, village v, country c where (u.villageid = v.id or u.villageid is null) and u.countryid = c.id and u.id = :userid")
		result = DBSession.execute(stmt, {'userid': userid})
		arr = []
		for model in result:
			arr.append(dict(model))
		
		if len(arr) > 0:
			res = arr[0]
			# get emergency contact
			stmt = text("select a.id as emergencyid, a.villageid, a.fname, a.lname, a.street, a.cellnum, a.homenum, a.email, v.name as village\
			 from alternativecontact a, village v where a.villageid = v.id and a.userid = :userid")
			result = DBSession.execute(stmt, {'userid': userid})
			for model in result:
				if not res.has_key("emg"):
					res['emg'] = []
				res["emg"].append(dict(model))
			
			# get vessel information
			stmt = text("select v.id as vesselid, v.regnumber, v.description, v.length, v.hullcolor, v.insidecolor, v.hulltype\
						from vessels v where v.userid = :userid")
			result = DBSession.execute(stmt, {'userid': userid})
			for model in result:
				if not res.has_key("vessel"):
					res['vessel'] = []
				res["vessel"].append(dict(model))
			
			# get occupation
			stmt = text(
				"SELECT ocs.userid, ocs.occupationid, o.type FROM `occupations` ocs, occupation o where o.id = ocs.occupationid and ocs.userid = :userid")
			result = DBSession.execute(stmt, {'userid': userid})
			for model in result:
				if not res.has_key("occupations"):
					res['occupations'] = []
				res["occupations"].append(dict(model))
		return res
	except Exception as e:
		log.error("Unable to get profile information: {0}".format(e.message))
		return None
		
		
		# used this section to test internal functions written before testing connection through rest
		# print getoccupations()
		# print deleteVillageRec(3)
		# print deleteOccupationRec(3)
