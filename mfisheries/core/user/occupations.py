from sqlalchemy import (
    Column,
    Integer,
    ForeignKey
)
from sqlalchemy.orm import relationship

from mfisheries.models import Base


class Occupations(Base):
    """The SQLAlchemy declarative model class for a Occupations object"""
    __tablename__ = 'occupations'
    id = Column(Integer, primary_key=True)
    userid = Column(Integer, ForeignKey('user.id'), nullable=False)
    occupationid = Column(Integer, ForeignKey('occupation.id'), nullable=False)

    occupation = relationship("Occupation", foreign_keys=[occupationid])

    def __init__(self, userid, occupationid):
        self.userid = userid
        self.occupationid = occupationid

    def toJSON(self):
        rec = {
            'id': self.id,
            'userid': self.userid,
            'occupationid': self.occupationid
        }
        if self.occupation:
            rec['occupation'] = self.occupation.type
        return rec
