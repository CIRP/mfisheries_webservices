from abc import ABCMeta, abstractmethod

import transaction

from mfisheries.models import DBSession, Base
from sqlalchemy import asc

import logging

log = logging.getLogger(__name__)


class BaseHandler(object):
	__metaclass__ = ABCMeta

	def __init__(self, request, context=None):
		self.request = request
		self.context = context
		self.fields = self._get_fields()
		self._target_class = self._get_target_class()
		self.order_func = asc
		self.enable_debug = True
		self.module_name = __name__
		self.session = DBSession
	# based on https://docs.sqlalchemy.org/en/13/orm/contextual.html#using-thread-local-scope-with-web-applications

	def _isvalid(self, values):
		for f in self.fields:
			if f not in values:
				return False
		return True

	@abstractmethod
	def _create_empty_model(self):
		pass

	@abstractmethod
	def _dict_to_model(self, data, rec_id=0):
		pass

	@abstractmethod
	def _get_fields(self):
		return []

	@abstractmethod
	def _get_order_field(self):
		pass

	# https://stackoverflow.com/questions/553784/can-you-use-a-string-to-instantiate-a-class-in-python
	@abstractmethod
	def _get_target_class(self):
		return None

	def _save(self, data, rec_id=0):
		try:
			src = self._dict_to_model(data, rec_id)
			self.session.add(src)
			self.session.flush()
			# transaction.commit()
			return src
		except Exception as e:
			self.session.rollback()
			# transaction.abort()
			log.error("Database Error while saving:" + str(e.message))
		return None

	def _process_results(self, res):
		return map(lambda m: m.toJSON(), res)  # convert each record received to JSON format

	def _handle_conditional_get(self, query):
		try:
			if "countryid" in self.request.GET:
				cid = self.request.GET['countryid']
				query = query.filter(self._target_class.countryid == cid)
		except Exception as e:
			log.error(str(e))
		return query

	def _order_results(self, query):
		try:
			query = query.order_by(self.order_func(self._get_order_field()))
		except Exception as e:
			log.error(e)
		return query

	def collection_get(self):
		res = []
		try:
			if self._target_class:
				transaction.commit()
				query = self.session.query(self._target_class)
				query = self._order_results(query)
				query = self._handle_conditional_get(query)

				res = query.all()
				if len(res) > 0:
					res = self._process_results(res)
		except Exception as e:
			self.session.rollback()
			log.error(e)

		return res

	def get(self):
		# Extract id passed as a parameter within the URL
		try:
			transaction.commit()  # Attempting to commit to ensure that results is consistent
			ws_id = int(self.request.matchdict['id'])
			if self._target_class:
				res = self.session.query(self._target_class).get(ws_id)
				if res:
					return res.toJSON()
		except Exception as e:
			log.error(str(e))

		self.request.response.status = 404
		return False

	def retrieve_data(self):
		try:
			data = self.request.json_body
		except Exception as e:
			data = self.request.POST
			if self.enable_debug:
				log.info("Data not submitted as json. Attempting to retrieve via POST: " + str(e))
		return data

	def collection_post(self):
		try:
			# Extract data to be updated from the body of the post request
			data = self.retrieve_data()
			# Ensure required fields are specified
			if data and self._isvalid(data):
				# Attempt to save the new source
				src = self._save(data)
				# If source was successfully saved
				if src:
					if isinstance(src, Base):
						# Notify the client using the appropriate HTTP status code
						self.request.response.status = 201
						# Send the record
						result = src.toJSON()
						transaction.commit()
						return result
				else:
					log.error("Data was not saved")
			else:
				log.error("Invalid request, not all required fields specified")
			# If we got to this point the request did not complete so send appropriate status message
			log.error(data)
			self.request.response.status = 400
		except Exception as e:
			self.session.rollback()
			log.error(e)
			self.request.response.status = 500
		return False

	def put(self):
		try:
			# Extract data to be updated from the body of the put request
			data = self.retrieve_data()
			# Extract id passed as a parameter within the URL
			rec_id = int(self.request.matchdict['id'])
			if self.enable_debug:
				log.info("Attempting to update the following record with the id: {0}".format(rec_id))
			if data and self._isvalid(data):
				src = self._save(data, rec_id)
				if src:
					result = src.toJSON()
					transaction.commit()
					return result
			# If we got to this point the request did not complete so send appropriate status message
			self.request.response.status = 400
		except Exception as e:
			self.session.rollback()
			log.error(e)
			self.request.response.status = 500

		return False

	def delete(self):
		# Extract id passed as a parameter within the URL
		rec_id = int(self.request.matchdict['id'])
		result = False
		try:
			# Delete the record
			self.session.query(self._target_class).filter(self._target_class.id == rec_id).delete()
			transaction.commit()
			result = True
		except Exception as e:
			log.error(e)
			self.session.rollback()
			self.request.response.status = 500

		return result
