import logging

log = logging.getLogger(__name__)

from cornice.resource import resource
from mfisheries.core.user.users import User
from mfisheries.modules.alerts.subscribers import Subscribers
from mfisheries.models import DBSession
from mfisheries.util.secure import validate


# noinspection SpellCheckingInspection
@resource(collection_path='/api/group/subscribe', path='/api/group/subscribe/{userid}/{groupid}',
          description='User groups')
class SubscriberHandler(object):
    def __init__(self, request, context=None):
        self.request = request
        self.context = context
        self.fields = ['userid', 'groupid', 'gcm']
        log.debug("Instantiating user group handler")

    def __isvalid(self, values):
        for f in self.fields:
            if f not in values:
                return False
        return True

    def __save_subscribe(self, v):
        subscriber = Subscribers(v['userid'], v['groupid'])
        user = DBSession.query(User).filter(User.id == v['userid']).one()
        user.gcmId = v['gcm']
        try:
            DBSession.add(subscriber)  # the database session adds the new data model to the database
            DBSession.add(user)
            DBSession.flush()
            return subscriber
        except Exception as e:
            log.error("Unable to add Source: " + e.message)
            return False

    def collection_post(self):

        if not validate(self.request):
            return {'status': 401, 'data': []}
        data = self.request.json_body
        try:
            if self.__isvalid(data):
                # Check if record exists before attempting to save
                rec = DBSession.query(Subscribers).filter(Subscribers.userid == data['userid']).filter(Subscribers.usergroupid == data['groupid']).one_or_none()
                log.debug(rec)
                if rec is None:
                    subscribe = self.__save_subscribe(data)
                    if subscribe:
                        self.request.response.status = 201
                        return subscribe.toJSON()
                else:
                    self.request.response.status = 200
                    return rec.toJSON()
        except Exception as e:
            log.error(e.message)

        self.request.response.status = 400
        error = {'error': 'Invalid Request Data'}
        return error

    def delete(self):
        if not validate(self.request):
            return {'status': 401, 'data': []}
        userid = int(self.request.matchdict['userid'])
        groupid = int(self.request.matchdict['groupid'])
        try:
            DBSession.query(Subscribers).filter(Subscribers.userid == userid).filter(
                Subscribers.usergroupid == groupid).delete()
            self.request.response.status = 200
            return {'message': 'Unsubscribed Successfully'}
        except Exception as e:
            log.error(e.message)
            self.request.response.status = 400
            error = {'error': 'Delete Failed'}
            return error
