import logging
log = logging.getLogger(__name__)

from cornice import Service

from mfisheries.models import DBSession
from mfisheries.modules.alerts.usergroups import UserGroups
from mfisheries.modules.alerts.subscribers import Subscribers

groupMembers = Service(name='groupMembers', path='api/groups/{groupid}/members', description='Operations for members of the group')
groupMember = Service(name='groupMember Service', path='api/groups/{groupid}/members/{userid}', description='Operations for individual member in the group')

@groupMembers.get()
def retrieveMembers(request):
	try:
		groupid = int(request.matchdict['groupid'])
		userGroup = DBSession.query(UserGroups).get(groupid)
		if userGroup:
			subscriber = [sub.toJSON() for sub in userGroup.subscribers]
			return subscriber
	except Exception as e:
		log.error(e)
	request.response.status = 404
	return False

@groupMember.delete()
def removeFromGroup(request):
	try:
		groupid = int(request.matchdict['groupid'])
		userid = int(request.matchdict['userid'])
		log.info("Attempting to remove {0} from group {1}".format(userid, groupid))
		res = DBSession.query(Subscribers).filter(Subscribers.usergroupid == groupid).filter(Subscribers.userid == userid).delete()
		if res:
			request.response.status = 200
			return True
	except Exception as e:
		log.error(e)
	request.response.status = 404
	return False