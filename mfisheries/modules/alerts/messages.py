import datetime

from sqlalchemy import (
    Column,
    Integer,
    String,
    TIMESTAMP,
    ForeignKey,
    Boolean,
    TEXT
)
from sqlalchemy.orm import relationship
from mfisheries.models import Base

import logging
log = logging.getLogger(__name__)


# noinspection SpellCheckingInspection,PyPep8Naming
class Messages(Base):
    """The SQLAlchemy model class for a Messages object"""
    __tablename__ = 'messages'
    id = Column(Integer, primary_key=True)
    userid = Column(Integer, ForeignKey('user.id'))
    groupid = Column(Integer, ForeignKey('usergroups.id'))
    messagecontent = Column(TEXT)
    description = Column(TEXT, nullable=True)
    severity = Column(String(100), default="Unknown")
    deviceTimestamp = Column(TIMESTAMP)
    latitude = Column(String(20))
    longitude = Column(String(20))
    likes = Column(Integer, default=0)
    dislikes = Column(Integer, default=0)
    timestamp = Column(TIMESTAMP,default=datetime.datetime.now())
    isPublic = Column(Boolean, default=False)
    isVerified = Column(Boolean, default=False)
    expires = Column(Integer, default=360) #default of 6 hours
    expired = Column(Integer, default=0)
    approvedById = Column(Integer, ForeignKey('user.id'), nullable=True)

    group = relationship("UserGroups", foreign_keys=[groupid])
    user = relationship("User", foreign_keys=[userid], lazy='subquery')
    approvedBy = relationship("User", foreign_keys=[approvedById], lazy='subquery')

    def __init__(self, userid, groupid, messagecontent, deviceTimestamp, latitude, longitude, isPublic):
        self.userid = userid
        self.groupid = groupid
        self.messagecontent = messagecontent
        self.deviceTimestamp = deviceTimestamp
        self.latitude = latitude
        self.longitude = longitude
        self.isPublic = isPublic
        self.likes = 0
        self.dislikes = 0

    def toJSON(self):
        rec = {
            'id': self.id,
            'userid': self.userid,
            'groupid': self.groupid,
            'messagecontent': self.messagecontent,
            'deviceTimestamp': str(self.deviceTimestamp),
            'latitude': self.latitude,
            'longitude': self.longitude,
            'likes': self.likes,
            'dislikes': self.dislikes,
            'timestamp': str(self.timestamp),
            'isPublic': self.isPublic,
            'isVerified': self.isVerified,
            'severity': self.severity,
            'messageSeverity': self.severity,
            'messageDescription': self.description,
            "expires" : self.expires,
            "expired" : self.expired
        }

        if self.group:
            rec['country'] = self.group.country.name
            rec['countryid'] = self.group.country.id
            rec['group'] =  self.group.groupname

        if self.user:
            rec["username"] = self.user.username
            rec["user"]= self.user.username
            rec["fullname"]= "{0} {1}".format(self.user.fname, self.user.lname)

        try: # note every message will have an approver... try catch to ensure it doesn't break if try to read non existant values
            if self.approvedBy:
                rec['approvedBy'] = self.approvedBy.username
                rec['approver'] =  "{0} {1}".format(self.approvedBy.fname, self.approvedBy.lname)
        except:
            log.error("Error retrieving approver")
        return rec

    def __str__(self):
        return str(self.toJSON())