import datetime

from sqlalchemy import (
    Column,
    Integer,
    TIMESTAMP,
    ForeignKey,
    UniqueConstraint
)
from sqlalchemy.orm import relationship

from mfisheries.models import Base


class Subscribers(Base):
    """The SQLAlchemy model class for a Subscribers object"""
    __tablename__ = 'subscribers'
    id = Column(Integer, primary_key=True)
    userid = Column(Integer, ForeignKey('user.id'), nullable=False)
    usergroupid = Column(Integer, ForeignKey('usergroups.id'), nullable=False)
    timestamp = Column(TIMESTAMP, default=datetime.datetime.utcnow)
    __table_args__ = (UniqueConstraint('userid', 'usergroupid', name='unique_user'),)

    group = relationship("UserGroups", foreign_keys=[usergroupid], lazy='subquery')
    user = relationship("User", foreign_keys=[userid], lazy='subquery')

    def __init__(self, userid, usergroupid):
        self.userid = userid
        self.usergroupid = usergroupid

    def toJSON(self):
        return{
            'id': self.id,
            'userid': self.userid,
            'usergroupid': self.usergroupid,
            'timestamp': str(self.timestamp),
            'user': self.user.username,
            'fullname': "{0} {1}".format(self.user.fname, self.user.lname),
            'group': self.group.groupname
        }