from datetime import datetime

from cornice.resource import resource
from sqlalchemy import text
from sqlalchemy import desc
from mfisheries.core import BaseHandler
from mfisheries.models import DBSession
from mfisheries.modules.alerts import Messages, Subscribers
from mfisheries.util import gcmclient

import logging
log = logging.getLogger(__name__)

from . import UserGroups


# Implemented Based on the BaseHandler and Structure in DamageReportHandler
# noinspection SpellCheckingInspection
@resource(collection_path='api/alerts', path='/api/alerts/{id}', description='Send Alerts')
class AlertHandler(BaseHandler):
    def _get_order_field(self):
        return Messages.timestamp

    def __init__(self, request, context=None):
        BaseHandler.__init__(self, request, context)
        self.order_func = desc

    def _get_fields(self):
        return [
            'userid',
            'groupid',
            'messagecontent',
            'deviceTimestamp',
            'latitude',
            'longitude',
            'isPublic'
        ]

    def _create_empty_model(self):
        return Messages("", "", "", "", "", "", "")

    def _get_target_class(self):
        return Messages

    def _handle_conditional_get(self, query):
        # query = super(AlertHandler, self)._handle_conditional_get(query)
        try:
            if "countryid" in self.request.GET:
                cid = self.request.GET['countryid']
                query = query.join(Messages.group).filter(UserGroups.countryid == cid)

            if "all" not in self.request.params:
                query = query.filter(Messages.expired == 0)

            if 'type' in self.request.params:
                is_public = self.request.params['type']
                if is_public == 'public':
                    query = query.filter(Messages.isPublic == True)

            if 'group' in self.request.params:
                groupid = self.request.params['group']
                query = query.filter(Messages.groupid == groupid)

        except Exception as e:
            log.error(e.message)
        return query

    def _dict_to_model(self, data, rec_id=0):
        # If id is 0, we want to create a new entry
        if rec_id == 0:
            src = self._create_empty_model()
        else:
            src = DBSession.query(self._target_class).get(rec_id)

        src.userid = data['userid']
        src.groupid = data['groupid']
        src.messagecontent = data['messagecontent']

        src.deviceTimestamp = datetime.strptime(data['deviceTimestamp'], '%Y-%m-%d %H:%M:%S')

        src.latitude = data['latitude']
        src.longitude = data['longitude']
        src.isPublic = data['isPublic'] if "isPublic" in data else "0"

        # Handling optional parameters by checking if exist and using default if not
        src.likes = data['likes'] if "likes" in data else "0"
        src.dislikes = data['dislikes'] if "dislikes" in data else "0"
        src.description = data['messageDescription'] if "messageDescription" in data else ""
        src.severity = data['messageSeverity'] if "messageSeverity" in data else "Unknown"

        return src

    def __getgcmids(self,groupid):
        try:
            query = DBSession.query(Subscribers)
            result = query.filter(Subscribers.usergroupid == groupid)
            
            arr = []
            for model in result:
                arr.append(model.user.gcmId)
            return arr
        except Exception as e:
            log.error(e.message)
            return None

    # Overiding the POST to support notification via Cloud Messaging
    def collection_post(self):
        alert = super(AlertHandler, self).collection_post()
        if alert:
            try:
                # Add the details that will be used by the clients for managing notifications
                alert['title'] = "New Alert" + "-" + alert['messagecontent']
                alert['message'] = alert['messagecontent']
                alert['type'] = "alert"
                # If first name is not available then add the name as Admin
                if "fullname" not in alert:
                    alert['fullname'] = "FEWER Admin"
                elif alert['username'] is None:
                    alert['fullname'] = "FEWER Admin"
                # Send using Google Cloud Messaging (more accurately Firebase)
                gcmclient.notifyAlertGroup(alert['groupid'], alert)
            except Exception as e:
                log.error("Error Occurred when trying to send alert " + str(e))

        # If Not successful, the 400 would have already be set
        return alert
