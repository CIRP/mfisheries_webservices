from apscheduler.schedulers.background import BackgroundScheduler

from mfisheries.models import DBSession
import transaction

from mfisheries.modules.alerts import Messages
from mfisheries.modules.alerts import UserGroups
from mfisheries.modules.alerts.alertshandler import AlertHandler
from mfisheries.util import display_debug
from pyramid.paster import bootstrap

from mfisheries.core.config import Config

from datetime import timedelta
from datetime import datetime

FREQUENCY = {
    "Every 6 Hours": 6,
    "Every Hour": 1,
    "Once a day": 24
}


class AlertMessageManager(object):

    def __init__(self):
        self.enable_debug = True

    def updateExpiredMessage(self):
        active_messages = DBSession.query(Messages).filter(Messages.expired == 0).all()
        if active_messages:
            if self.enable_debug:
                display_debug("Retrieved {0} active messages".format(len(active_messages)),module_name=__name__)
            for message in active_messages:
                try:
                    if message.timestamp + timedelta(minutes=message.expires) < datetime.now():
                        display_debug("{0} message is old and should be updated".format(message.messagecontent),module_name=__name__)
                        message.expired = 1
                        DBSession.add(message)
                        DBSession.flush()
                    else:
                        display_debug("Message is still relevant",module_name=__name__)
                except Exception as e:
                    display_debug("Database Error:" + str(e), "error",module_name=__name__)

            transaction.commit()

    def ensureAlertBroadcasted(self):
        # TODO Ensure that all alerts to be broadcasted are available
        pass


def run_alert_scheduled_task(path=None, use_bootstrap=False):
    display_debug("Executing Broadcast Manager", "info")
    # Expected to be executed either from the main or source script
    ini_full_path = "./development.ini"
    # Ensures that System Modules are loaded even if executed from the command line or a service
    if use_bootstrap:
        if path:
            bootstrap(path)
        else:
            bootstrap(ini_full_path)

    alertManager = AlertMessageManager()
    alertManager.updateExpiredMessage()
    alertManager.ensureAlertBroadcasted()


if __name__ == "__main__":
    # Run this main using the command:  python -m mfisheries.modules.alerts.scheduler
    display_debug("Running alert schedule task via command line")
    run_alert_scheduled_task(use_bootstrap=True)