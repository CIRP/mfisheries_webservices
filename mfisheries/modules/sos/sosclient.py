import logging
from decimal import Decimal

from sqlalchemy import text

from mfisheries.models import DBSession
from mfisheries.util.emailutil import send_email

log = logging.getLogger(__name__)


def get_coastguard_details(country_id):
    stmt = text("SELECT u.id, u.username, u.countryid, u.email\
	       FROM user u\
	       WHERE u.countryid = :cid AND u._class = 1")
    try:
        result = DBSession.execute(stmt, {"cid": country_id})
        arr = []
        for model in result:
            arr.append(dict(model))
        return arr
    except Exception as e:
        print(("Error Occurred: " + str(e.message)))
        log.error(e.message)
        return None


def send_sos_notification(trackid, userid):
    print(("Attempting to find information for track {0} and userid {1}".format(trackid, userid)))
    details = getDetails(trackid, userid)
    print(details)
    if len(details) > 0:
        print(("Retrieved country {0} with the notification".format(details[0]['countryid'])))
        cg_details = get_coastguard_details(details[0]['countryid'])

        message = ""
        message += "Name: " + details[0]['fname'] + " " + details[0]['lname']
        message += "\nMobile: " + str(details[0]['mobileNum']) + ", LandLine: " + str(details[0]['homeNum'])
        message += "\nLatitude: " + decimalDegrees2DMS(Decimal(details[0]['latitude']), 'latitude')
        message += "\nLongitude: " + decimalDegrees2DMS(Decimal(details[0]['longitude']), 'longitude')
        message += "\nSpeed: " + str(details[0]['speed']) + "m/s"
        message += "\nBearing: " + str(details[0]['bearing'])
        message += "\nTime: " + details[0]['time'] + "\n"

        message += "\n <a target='_blank' href='http://maps.google.com/?q=" + str(details[0]['latitude']) + "," + str(
            details[0]['longitude']) + "'>Open in Maps</a> \n"
        try:
            # If we find emails of coast guards then send to the specified emails
            if len(cg_details) > 0:
                emails = []
                for cg in cg_details:
                    emails.append(cg['email'])
                send_email(message, emails)
            else:
                # send without specifying emails which will use the default email
                send_email(message)
            return True
        except Exception as e:
            log.error("Error occurred when attempting to send sos notification" + str(e))
            print(e)
            return False
    else:
        print("Empty")
        return False


def decimalDegrees2DMS(value, type):
    """
        Converts a Decimal Degree Value into
        Degrees Minute Seconds Notation.
        
        Pass value as double
        type = {Latitude or Longitude} as string
        
        returns a string as D:M:S:Direction
        created by: anothergisblog.blogspot.com 
    """
    degrees = abs(int(value))
    submin = abs((value - int(value)) * 60)
    minutes = int(submin)
    subseconds = abs((submin - int(submin)) * 60)
    direction = ""
    if type == "longitude":
        if degrees < 0:
            direction = "W"
        elif degrees > 0:
            direction = "E"
        else:
            direction = ""
    elif type == "latitude":
        if degrees < 0:
            direction = "S"
        elif degrees > 0:
            direction = "N"
        else:
            direction = ""
    notation = str(degrees) + "\xcb " + str(minutes) + "' " + str(subseconds)[0:5] + '" ' + direction
    return notation


def getDetails(trackid, userid):
    stmt = text("SELECT u.id, u.fname, u.lname, u.hstreet, u.mobileNum, u.homeNum, u.countryid,\
			t.latitude, t.longitude, t.bearing, t.speed, DATE_FORMAT( t.deviceTimestamp, '%b %d %Y %h:%i %p') as time\
			from user u, trackpoints t where u.id = t.userid\
			and u.id = :userid\
			and t.id = :trackid")

    try:
        result = DBSession.execute(stmt, {"trackid": trackid, "userid": userid})
        arr = []
        for model in result:
            arr.append(dict(model))
        return arr
    except Exception as e:
        print(("Error in Reading track from database occurred: {0}".format(e.message)))
        return None


# send_sos_notification(205, 8)

if __name__ == '__main__':
    log.info("Sending Email")
    send_email("Testing Multiple emails")
# send_sos_notification(205, 8)
