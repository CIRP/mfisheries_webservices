import json
import logging

import transaction
from cornice import Service
from sqlalchemy import text
from sqlalchemy import desc


from mfisheries import DBSession
from mfisheries.modules.sos import ResolvedSos
from mfisheries.modules.tracks import TrackPoints
from mfisheries.core.user import User
from mfisheries.util.secure import validate
from mfisheries.modules.sos.sosclient import send_sos_notification
from mfisheries.util.fileutil import (
	add_data,
	read,
)

log = logging.getLogger(__name__)

addTracks = Service(name='addTracks', path='api/add/user/tracks', description='store user track information')
getTrackDates = Service(name="getTrackDates", path="api/get/track/dates/{cgid}",
                        description="get all unique dates from track table")
getTracksByDay = Service(name="getTracksByDay", path='api/get/track/by/day/{day}/{month}/{year}/{cgid}',
                         description='gets tracks for a given day')
getUserTracks = Service(name="getUserTracks",
                        path="api/get/user/tracks/{day}/{month}/{year}/{userid}/{starttrackid}/{cgid}",
                        description="returns all tracks for a user")
resolveSOS = Service(name="resolveSOS", path="/api/update/sos/status",
                     description="updates the status of sos to resolved")

trackService = Service(name='trackService', path='api/tracks', description='track service')
trackDateService = Service(name="trackDateService", path="api/tracks/dates/{cgid}",
                           description="get all unique dates from track table")
tracksByDayService = Service(name="tracksByDayService", path='api/tracks/day/{day}/{month}/{year}/{cgid}',
                             description='gets tracks for a given day')
userTrackService = Service(name="userTrackService",
                           path="api/tracks/user/{day}/{month}/{year}/{userid}/{starttrackid}/{cgid}",
                           description="returns all tracks for a user")
SOSService = Service(name="SOSService", path="/api/sos",
                     description="updates the status of sos to resolved")


@addTracks.post()
@trackService.post()
def add_user_tracks(request):
	session = DBSession
	result = None
	try:
		if not validate(request):
			result = {'status': 401, 'data': []}
		else:
			tracks = json.loads(request.body)
			tracks_models = []
			if tracks is not None:
				# Convert from webservice (JSON) to system model format
				for track in tracks:
					trackpoint = TrackPoints(
						track['uId'],
						# track['starttrackid'],
						track['lat'],
						track['lng'],
						track['rssi'],
						track['acc'],
						track['brg'],
						track['spd'],
						track['prov'],
						track['time'],
						track['type'].lower()
					)
					# Add Each track to a list of tracks
					tracks_models.append(trackpoint)
				# Store the
				value = store_tracks(tracks_models, session)
				if value:
					# log.info("Successfully processed {0} tracks from the server".format(len(tracks_models)))
					result = {'status': 201}
				else:
					# log.error("Unable to save  records to the system")
					result = request.errors.add('url', 'create', 'tracks not saved')
			else:
				# log.error("Bad request received, no records saved")
				result = request.errors.add('url', 'create', 'received empty list')
	except Exception as e:
		log.error(e)

	return result



@getTracksByDay.get()
@tracksByDayService.get()
def get_tracks_by_day(request):
	session = DBSession
	result = None
	try:
		if not validate(request):
			result = {'status': 401, 'data': []}
		else:
			req = {
				"day": request.matchdict['day'],
				"month": request.matchdict['month'],
				"year": request.matchdict['year'],
				"cgid": request.matchdict['cgid']
			}

			records = gettrackbyday(req, session)
			if not records:
				result = {'status': 204, 'data': []}
			else:
				result = {'status': 200, 'data': records}
	except Exception as e:
		log.error(e)

	return result


@getUserTracks.get()
@userTrackService.get()
def get_user_tracks(request):
	session = DBSession
	result = None
	try:
		if not validate(request):
			result = {'status': 401, 'data': []}
		else:
			req = {
				"day": request.matchdict['day'],
				"month": request.matchdict['month'],
				"year": request.matchdict['year'],
				"userid": request.matchdict['userid'],
				"starttrackid": request.matchdict['starttrackid'],
				"cgid": request.matchdict['cgid']
			}

			records = getusertracks(req, session)
			if not records:
				result = {'status': 204, 'data': []}
			else:
				result = {'status': 200, 'data': records}
	except Exception as e:
		log.error(e)

	return result


@getTrackDates.get()
@trackDateService.get()
def get_track_dates(request):
	session = DBSession
	result = None
	try:
		if not validate(request):
			result = {'status': 401, 'data': []}
		else:
			dates = get_dates(request.matchdict['cgid'], session)
			if not dates:
				result = {'status': 204, 'data': []}
			else:
				result = {'status': 200, 'data': dates}
	except Exception as e:
		log.error(e)

	return result


@resolveSOS.post()
@SOSService.put()
def resolve_sos(request):
	session = DBSession
	result = None
	try:
		if not validate(request):
			result = {'status': 401, 'data': []}
		else:
			data = json.loads(request.body)
			resolve = ResolvedSos(data['caseno'], data['details'], None, data['trackid'], data['badgeno'])
			if resolvesos(resolve, session):
				result = {'status': 200}
			else:
				result = request.errors.add('url', 'update', 'unable to resolve sos')
	except Exception as e:
		log.error(e)

	return result



# TODO - Translate to Alchemy syntax - current implementation is mysql specific
# api/get/user/tracks/27/10/2015/42/254
def getusertracks(data, session):
	stmt = text("SELECT t1.id,t1.userid, t1.latitude, t1.longitude, DAY( t1.deviceTimestamp ) AS day, t1.starttrackid, \
					MONTH( t1.deviceTimestamp ) AS month, \
					t1.type, t1.bearing, u.fname, u.lname, t1.speed, DATE_FORMAT( t1.deviceTimestamp, '%Y-%m-%d %H:%i:%s') as time \
					FROM trackpoints t1, user u \
					inner JOIN(select u1.countryid from user u1 where u1.id = :cgid) as c\
					ON c.countryid = u.countryid\
					WHERE u.id = t1.userid \
					AND t1.userid = :userid \
					AND DAY( t1.deviceTimestamp ) <= :day \
					AND MONTH(t1.deviceTimestamp) <= :month \
					AND YEAR(t1.deviceTimestamp) <= :year \
					AND (t1.starttrackid = :starttrackid \
					OR t1.id = :starttrackid) \
					order by time asc")
	try:
		result = session.execute(stmt, data)
		arr = []
		for model in result:
			arr.append(dict(model))
		return arr
	except Exception as e:
		session.rollback()
		log.error(e)
		return None


def gettrackbyday(data, session):
	stmt = text("SELECT distinct t1.userid, t1.latitude, t1.longitude, DAY( t1.deviceTimestamp ) AS day, \
					t1.starttrackid, MONTH( t1.deviceTimestamp ) AS month, YEAR( t1.deviceTimestamp ) AS year,\
					t1.type, t1.bearing, u.fname, u.lname, u.mobileNum, u.homeNum, t1.speed, DATE_FORMAT( t1.deviceTimestamp, '%Y-%m-%d %H:%i:%s') as time \
					FROM trackpoints t1 \
					inner JOIN user u \
					ON u.id = t1.userid \
					inner JOIN (SELECT max(t2.deviceTimestamp) max_time, t2.starttrackid \
					      FROM trackpoints t2 \
					      where day(t2.deviceTimeStamp) = :day \
						  and month(t2.deviceTimeStamp) = :month \
					      group by t2.starttrackid) AS tr \
					ON tr.starttrackid = t1.starttrackid \
					AND tr.max_time = t1.deviceTimeStamp \
					inner JOIN(select u1.countryid from user u1 where u1.id = :cgid) as c\
					ON c.countryid = u.countryid")
	try:
		result = session.execute(stmt, data)
		arr = []
		for model in result:
			arr.append(dict(model))
		# print arr
		sos_tracks = getsostracks(data['cgid'], session)
		if sos_tracks is not None:
			arr += sos_tracks  # Add Both list into one list stored in the variable arr
		return arr
	except Exception as e:
		session.rollback()
		log.error(e)
		return None


def getsostracks(cgid, session):
	stmt = text("SELECT distinct t1.id, t1.userid, t1.latitude, t1.longitude, DAY( t1.deviceTimestamp ) AS day, t1.starttrackid, \
					MONTH( t1.deviceTimestamp ) AS month, YEAR( t1.deviceTimestamp ) AS year, \
					t1.type, t1.bearing, u.fname, u.lname, u.mobileNum, u.homeNum,  t1.speed, DATE_FORMAT( t1.deviceTimestamp, '%Y-%m-%d %H:%i:%s') as time \
					FROM trackpoints t1, user u\
					inner JOIN(select u1.countryid from user u1 where u1.id = :cgid) as c\
					ON c.countryid = u.countryid\
					WHERE u.id = t1.userid \
					AND t1.type = 'sos'")
	try:
		result = session.execute(stmt, {"cgid": cgid})
		arr = []
		for model in result:
			arr.append(dict(model))
		return arr
	except Exception as e:
		session.rollback()
		log.error(e)
		return None


def updateSos(trackid, session):
	try:
		track = session.query(TrackPoints).filter(TrackPoints.id == trackid).one()
		track.type = 'resolved'
		session.add(track)
		session.flush()
		transaction.commit()
		return True
	except Exception as e:
		session.rollback()
		log.error(e)
		return False


def resolvesos(data, session):
	try:
		if updateSos(data.trackid, session):
			session.add(data)
			session.flush()
			transaction.commit()
			return True
		else:
			return False
	except Exception as e:
		session.rollback()
		log.error(e)
		return False


def get_dates(cgid, session):
	# Check user id to determine the type of user
	# if coast guard (i.e. type == 1) then provide all users
	# otherwise provide tracks for only specific user
	try:
		usr = session.query(User).filter(
			User.id == cgid).one()  # http://docs.sqlalchemy.org/en/latest/orm/query.html#sqlalchemy.orm.query.Query.one
		if usr._class == 1:
			log.info("Received request from the coast guard with id " + cgid)
			stmt = text("SELECT distinct DATE_FORMAT(deviceTimestamp, '%Y-%m-%d') as start\
 				FROM trackpoints, user u\
                inner join(select u.countryid from user u where u.id = :cgid)\
			 	as c on u.countryid = c.countryid where u.id = trackpoints.userid")
		else:
			log.info("retrieve request from general user with id " + cgid)
			stmt = text("SELECT distinct DATE_FORMAT(deviceTimestamp, '%Y-%m-%d') as start\
			FROM trackpoints t, user u\
			WHERE u.id = t.userid\
			AND u.id = :cgid")

		result = session.execute(stmt, {"cgid": cgid})
		arr = []
		for model in result:
			obj = dict(model)
			obj['title'] = "Activity"
			arr.append(obj)
		# log.info("Found {0} tracks for the user {1}".format(len(arr), cgid))
		return arr
	except Exception as e:
		session.rollback()
		log.error(e)
		return None


def store_track(data, session):
	try:
		# log.info("Received id {0} of {1} track for user {2} with value of ({3},{4})".format(data.id, data.type, data.userid, data.latitude, data.longitude))
		# Attempt to save track to the database

		session.add(data)
		session.flush()  # flushing required to commit pending transactions


		if data.type == 'start':
			data.starttrackid = data.id
		else: # attempt to link the record to previous track
			track_id = get_last_start_track_id(data.userid, session)
			if track_id is not None:
				data.starttrackid = track_id
			else:  # if we are unable to link to previous track, then start new track
				data.type = 'start'
				data.starttrackid = data.id

		if data.type == 'sos':
			send_sos_notification(data.id, data.userid)

		# Update the track to the database
		session.add(data)  # Attempt to save track to the database
		session.flush()  # flushing required to commit pending transactions
		return data

	except Exception as e:
		session.rollback()
		try:
			reason = str(e)
			if "Duplicate entry" in reason:
				# Write the record to the duplicates.json file
				add_data({
					"userid": data.userid,
					"latitude": round(float(data.latitude), 6),
					"longitude": round(float(data.longitude), 6),
					"deviceTimestamp": data.deviceTimestamp
				})
			else:
				log.error("Failed to save {0} track because of {1}".format(data.type, reason))
		except Exception as e2:
			log.error("Error Occurred when handling track storage error: {0}".format(e2))
			session.rollback()
	return False


def store_tracks(data, session):
	try:
		# log.info("Retrieved {0} tracks".format(len(data)))
		for track in data:
			reviewed = store_track(track, session)
			if not reviewed:
				log.error("Did not save track for " + str(track.toJSON()))
				return False
			else:
				return True
		transaction.commit()
	except Exception as e:
		session.rollback()
		log.error("Unable to store tracks: " + str(e))
	return False


def update_duplicate_tracks(session):
	try:
		data = read()
		for rec in data:
			r = session.query(TrackPoints).filter(TrackPoints.userid == rec['userid'],
			                                        TrackPoints.latitude == rec['latitude'],
			                                        TrackPoints.longitude == rec['longitude'],
			                                        TrackPoints.deviceTimestamp == rec['deviceTimestamp']).first()
			r.duplicated = "yes"
			r.duplicatecount = r.duplicatecount + 1
			session.flush
		transaction.commit()
		return True
	except Exception as e:
		session.rollback()
		log.error(e.message)
	return False


def get_last_start_track_id(userid, session):
	try:
		transaction.commit()
		# stmt = text("SELECT t.starttrackid from trackpoints t inner join \
		# 		(select max(t1.deviceTimestamp) as m,t1.starttrackid, t1.userid \
		# 		from trackpoints t1 where t1.userid = :userid and t1.starttrackid is not null and t1.type = 'start') as tem on \
		# 		t.userid = tem.userid and tem.m = t.deviceTimestamp")
		# rec = session.execute(stmt, {"userid": userid})
		rec = session.query(TrackPoints).filter(TrackPoints.userid == userid).order_by(
			desc(TrackPoints.deviceTimestamp)).first()
		if rec is not None:
			return rec.starttrackid
		return None
	except Exception as e:
		session.rollback()
		log.error(e.message)
		return None
