from sqlalchemy import (
    Column,
    Integer,
    String,
    TIMESTAMP,
    ForeignKey
)

from mfisheries.models import Base


class UserImages(Base):
    """The SQLAlchemy model class for a UserImages object"""
    __tablename__ = 'userimages'
    id = Column(Integer, primary_key=True)
    userid = Column(Integer, ForeignKey('user.id'), nullable=False)
    deviceTimestamp = Column(TIMESTAMP)
    latitude = Column(String(20))
    longitude = Column(String(20))
    imglocation = Column(String(100))
    imgname = Column(String(100))
    hasgps = Column(String(100))

    def __init__(self, userid, deviceTimestamp, latitude, longitude, imglocation, imgname, hasgps):
        self.userid = userid
        self.deviceTimestamp = deviceTimestamp
        self.latitude = latitude
        self.longitude = longitude
        self.imglocation = imglocation
        self.imgname = imgname
        self.hasgps = hasgps

    def toJSON(self):
        return {
            'id': self.id,
            'userid': self.userid,
            'deviceTimestamp': str(self.deviceTimestamp),
            'latitude': self.latitude,
            'longitude': self.longitude,
            'imglocation': self.imglocation,
            'imgname': self.imgname,
            'hasgps': self.hasgps
        }
