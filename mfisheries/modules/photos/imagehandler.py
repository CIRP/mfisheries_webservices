import json
import logging
log = logging.getLogger(__name__)

import transaction
from mfisheries.models import DBSession
from mfisheries.modules.photos.userimages import UserImages
from mfisheries.util.fileutil import store_image
from mfisheries.util.secure import validate
from cornice import Service
from sqlalchemy import text

imageDates = Service(name="imageDates", path="/api/get/image/dates/{userid}", description="userimage dates")
userImage = Service(name='userImage', path='api/add/user/image', description='user image services')
getLastImage = Service(name="getLastImage", path='api/get/last/images/{userid}/{day}/{month}/{year}',description="gets the user image by date in decending order")
getUserImages = Service(name="getUserImages", path="api/get/user/images/{userid}",description="returns the images based on userid")

addImageService = Service(name='imageService', path='api/images', description='image services')
imageDateService = Service(name="imageDateService", path="api/images/dates/{userid}", description="userimage dates")
lastImageService = Service(name="lastImageService", path='api/images/last/{userid}/{day}/{month}/{year}', description="gets the user image by date in decending order")
userImageService = Service(name="userImageService", path="api/images/user/{userid}",description="returns the images based on userid")


@getUserImages.get()
@userImageService.get()
def get_user_images(request):
    if not validate(request):
        return {'status': 401, 'data': []}
    result = getuserimages(request.matchdict['userid'])
    if not result:
        return {'status': 204, 'data': []}
    else:
        return {'status': 200, 'data': result}


@imageDates.get()
@imageDateService.get()
def get_image_dates(request):
    if not validate(request):
        return {'status': 401, 'data': []}
    result = getuserimagedates(request.matchdict['userid'])
    if not result:
        return {'status': 204, 'data': []}
    else:
        return {'status': 200, 'data': result}


@getLastImage.get()
@lastImageService.get()
def get_last_image(request):
    if not validate(request):
        return {'status': 401, 'data': []}
    data = {
        "userid": request.matchdict['userid'],
        "day": request.matchdict['day'],
        'month': request.matchdict['month'],
        'year': request.matchdict['year']
    }
    result = getlastimage(data)
    if not result:
        return {'status': 204, "data": []}
    else:
        return {'status': 200, 'data': result}


@userImage.post()
@addImageService.post()
def add_new_user_image(request):
    # TODO - Photo Diary - convert to use store media function in fileutil.py
    if not validate(request):
        return {'status': 401, 'data': []}
    req = json.loads(request.body)
    image_path = store_image(req['userid'], req['imgname'], req['data'])
    if image_path is not None:
        img_path = getImagePath(image_path)
        image = UserImages(
            req['userid'],
            req['deviceTimestamp'],
            req['latitude'],
            req['longitude'],
            img_path,
            req['imgname'],
            req['hasgps']
        )
        value = adduserimage(image)
        if value:
            return {'status': 201}
        else:
            request.errors.add('url', 'create', 'image not saved to db')
    else:
        request.errors.add('url', 'create', 'image not saved')


def getImagePath(image_path):
    for i in range(1, 4):
        img_path = image_path.split("mfisheries")[i]
        if img_path[1:7] == "static":
            return img_path
    return "/"


def adduserimage(data):
    try:
        DBSession.add(data)
        DBSession.flush()
        transaction.commit()
        return True
    except Exception as e:
        log.error(e.message)
        return False


def getuserimagedates(userid):
    stmt = text(
        "SELECT distinct DATE_FORMAT(deviceTimestamp, '%Y-%m-%d') as date from userimages where userid = :userid")
    try:
        result = DBSession.execute(stmt, {"userid": userid})
        arr = []
        for model in result:
            obj = dict(model)
            obj['title'] = "Activity"
            arr.append(obj)
        return arr
    except Exception as e:
        log.error(e.message)
        return None


def getuserimagesbylocation(data):
    stmt = text("SELECT DATE_FORMAT(i.deviceTimestamp, '%Y-%m-%d') AS 'date', \
					i.latitude, i.longitude, i.imglocation from userimages i \
					where i.userid = :userid and date(i.deviceTimestamp) = date(:date)\
					and i.latitude = :latitude and i.longitude = :longitude and i.hasgps = 'yes'\
					order by i.deviceTimestamp desc")
    try:
        result = DBSession.execute(stmt, data)
        arr = []
        for model in result:
            obj = dict(model)
            arr.append(obj)
        return arr
    except Exception as e:
        log.error(e.message)
        return None


def getlastimage(data):
    stmt = text("SELECT id, DATE_FORMAT(i.deviceTimestamp, '%Y-%m-%d') AS 'date', \
					i.latitude, i.longitude, i.imglocation from userimages i \
					where i.userid = :userid and DAY(i.deviceTimestamp) = :day and\
					MONTH(i.deviceTimestamp) = :month and YEAR(i.deviceTimestamp) = :year and i.hasgps = 'yes'\
					group by i.latitude, i.longitude order by i.deviceTimestamp desc")
    location = {}
    arr = []
    images = {}
    try:
        result = DBSession.execute(stmt, data)
        arr = []
        for model in result:
            obj = dict(model)
            images[obj['id']] = getuserimagesbylocation({"userid": data['userid'],
                                                         "date": obj['date'],
                                                         "latitude": str(obj['latitude']),
                                                         "longitude": str(obj['longitude'])})
            arr.append(obj)
        location['location'] = arr
        location['images'] = images
        return location
    except Exception as e:
        log.error(e.message)
        return None


def getuserimages(userid):
    stmt = text(
        "SELECT DATE_FORMAT(deviceTimestamp, '%Y-%m-%d') as date, imglocation as url from userimages where userid = :userid order by deviceTimestamp desc")
    try:
        result = DBSession.execute(stmt, {"userid": userid})
        arr = []
        for model in result:
            obj = dict(model)
            obj['thumbUrl'] = obj['url']
            obj['caption'] = obj['date']
            arr.append(obj)
        return arr
    except Exception as e:
        log.error(e.message)
        return None
