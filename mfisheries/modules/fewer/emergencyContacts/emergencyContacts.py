import datetime

from sqlalchemy import (
    Column,
    Integer,
    String,
    Text,
    TIMESTAMP,
    ForeignKey,
    UniqueConstraint
)
from sqlalchemy.orm import relationship

from mfisheries.models import Base


class EmergencyContact(Base):
    __tablename__ = 'emergencycontacts'
    id = Column(Integer, primary_key=True)
    name = Column(String(200), nullable=False)
    phone = Column(String(20), nullable=False)
    email = Column(String(100))
    type = Column(String(20), default="organization", nullable=False)
    countryid = Column(Integer, ForeignKey('country.id'), nullable=False)
    additional = Column(Text)
    createdby = Column(Integer, ForeignKey('user.id'))
    timecreated = Column(TIMESTAMP,  default=datetime.datetime.utcnow)
    image_url = Column(String(200))

    country = relationship("Country", foreign_keys=[countryid], lazy='subquery')
    user = relationship("User", foreign_keys=[createdby], lazy='subquery')

    # http://docs.sqlalchemy.org/en/latest/core/constraints.html#sqlalchemy.schema.UniqueConstraint
    __table_args__ = (UniqueConstraint('name', 'countryid', name='_customer_location_uc'),)

    def __init__(self, name, phone, email, country, additional=None):
        self.name = name
        self.phone = phone
        self.email = email
        self.countryid = country

        if additional:
            self.additional = additional

    def getRequiredFields(self):
        return ['name', 'phone', 'countryid']

    def toJSON(self):
        rec = {
            'id': self.id,
            'name': self.name,
            'phone': self.phone,
            'email': self.email,
            'type': self.type,
            'image_url': self.image_url,
            'additional': self.additional,
            'countryid': self.countryid,
            'timecreated': str(self.timecreated),
            'createdby': self.createdby
        }

        if self.country:
            rec["country"] = self.country.name,
        if self.user:
            rec['fullname'] = "{0} {1}".format(self.user.fname, self.user.lname)

        return rec
