from cornice.resource import resource
from mfisheries.core import BaseHandler
from mfisheries.models import DBSession
from sqlalchemy import or_, desc

from mfisheries.util.fileutil import store_media
from .damagereport import DamageReport
from .damagereportcategory import DamageReportCategory
from mfisheries.util.notificationManager import NotificationManager
import logging
from datetime import datetime
log = logging.getLogger(__name__)


# http://cornice.readthedocs.io/en/latest/resources.html
@resource(collection_path='/api/damagereport', path='/api/damagereport/{id}', description="Damage Reporting")
class DamageReportHandler(BaseHandler):
	def __init__(self, request, context=None):
		BaseHandler.__init__(self, request, context)
		self.order_func = desc
	
	def _get_order_field(self):
		return DamageReport.timecreated
	
	def _create_empty_model(self):
		return DamageReport(0, 0, "")

	def _get_fields(self):
		return self._create_empty_model().getRequiredFields()

	def _get_target_class(self):
		return DamageReport

	def _dict_to_model(self, data, rec_id=0):
		# If id is 0, we want to create a new entry
		if rec_id == 0:
			src = self._create_empty_model()
		else:
			src = DBSession.query(self._target_class).get(rec_id)
		# Assign value to the fields of the model
		src.userid = data["userid"]
		src.countryid = data["countryid"]
		src.name = data["name"]
		src.description = data["description"]
		src.filetype = data["filetype"]
		src.category = data['category']
		src.latitude = data["latitude"]
		src.longitude = data["longitude"]

		if "filepath" in data:
			src.filepath = data["filepath"]

		# Try to process date
		try:
			src.aDate = datetime.fromtimestamp(int(data["aDate"]))
		except Exception as e1:
			log.info("First Invalid date format " + str(e1))
			try:
				log.info("Attempting alternative format for " + str(data['aDate']))
				src.aDate = datetime.fromtimestamp(int(data["aDate"])/1000)
			except Exception as e:
				log.error("Second Invalid date format " + str(e))
			# print("Failed to convert date from:" + str(data['aDate']))

		# Ensure we can accommodate for both numbers and words else default to private
		if "isPublic" in data:
			try:
				# If numerical submitted (1 is public [True] or 0 is private [False])
				src.isPublic = int(data['isPublic'])
			except:
				try:
					# If text then convert to integer
					if data['isPublic'].lower() is "public":
						src.isPublic = 1
					elif data['isPublic'].lower() is "private":
						src.isPublic = 0
				except:
					# If failed, then set default to public
					src.isPublic = 1
		else:
			log.info("Audience not set, making public")
			src.isPublic = 1  # public by default

		if "cost" in data:
			try:
				src.cost = float(data['cost'])
			except Exception as e:
				src.cost = 0
				log.error("Invalid cost: {0}".format(e))
				
		# If user report is about is different than person submitting
		if "createdby" in data:
			src.createdby = data["createdby"]
		else:
			src.createdby = data['userid']
			
		log.info("Completed the conversion of the Damage Report model")
	
		return src

	def _handle_conditional_get(self, query):
		query = super(DamageReportHandler, self)._handle_conditional_get(query)
		
		# Handle individual user reports
		if 'userid' in self.request.params:
			userid = self.request.params['userid']
			query = query.filter(DamageReport.userid == userid)
		
		# Handle visibility
		if self.request.params.has_key('type'):
			is_public = self.request.params['type']
			if is_public == 'public':
				query = query.filter(DamageReport.isPublic == 1)
			elif is_public == 'all':
				query = query.filter(or_(DamageReport.isPublic == 1, DamageReport.isPublic == 0))
			elif is_public == 'private':
				query = query.filter(DamageReport.isPublic == 0)
		else:
			query = query.filter(DamageReport.isPublic == 1)
			
		return query
	
	def collection_post(self):
		log.info("Attempting to store Damage report record")
		res = super(DamageReportHandler, self).collection_post()
		log.info(res)
		if res:
			if "file" in self.request.POST:
				file_data = {
					"filename": self.request.POST['file'].filename,
					"file": self.request.POST['file'].value,
					"type": self.request.POST['filetype'],
					"userid": self.request.POST['userid']
				}
				location = store_media(file_data)
				log.debug("Location of stored file was {0}".format(location))

				if location is not None or location is not "":  # If file successfully saved
					location_str = "static" + location.split('static')[1]
					res['filepath'] = location_str
					src = self._save(res, res['id'])
					if src:  # We have successfully saved all the information about a report
						# Check if the report is public then notify that a new record was created
						if src.isPublic == 1:
							self.notify_of_report(src)
						return src.toJSON()
				self.request.response.status = 400
				res['message'] = "Unable to save file resource"
				# TODO Should the record be rollback or deleted at this point
		return res

	def notify_of_report(self, res):
		msg = "Damage Report Created"
		try:
			msg = "{0} was reported as damaged".format(res.name)
			# Check if we have the person, then put who uploaded the message
			if res.user and res.user.username:
				msg = " by {0}.".format(res.user.username)
		except Exception as e :
			log.error("Unable to format notification report for damage report:{0}".format(e))

		try:
			rec = {
				"title": "Damage Report",
				"message": msg,
				"type": "damage_report",
				"alertId": res.id
			}
			# Send using Google Cloud Messaging (more accurately Firebase)
			notifier = NotificationManager()
			notify_res = notifier.notifyByTopic("damage_report_{0}".format(res.countryid), rec)
			if notify_res:
				log.info("Notification for damage report sent correctly")
			else:
				log.error("Unable to send damage report notification")
		except Exception as e:
			log.error("Unable to send notification:{0}".format(e))


@resource(collection_path='/api/damagereport/category', path='/api/damagereport/category/{id}', description="Damage Reporting")
class DamageReportCategoryHandler(BaseHandler):
	def __init__(self, request, context=None):
		BaseHandler.__init__(self, request, context)
	
	def _get_order_field(self):
		return DamageReportCategory.name

	def _create_empty_model(self):
		return DamageReportCategory(0, "")

	def _get_fields(self):
		return self._create_empty_model().getRequiredFields()

	def _get_target_class(self):
		return DamageReportCategory

	def _dict_to_model(self, data, rec_id=0):
		# If id is 0, we want to create a new entry
		if rec_id == 0:
			src = self._create_empty_model()
		else:
			src = DBSession.query(self._target_class).get(rec_id)
		# Assign value to the fields of the model
		src.countryid = data["countryid"]
		src.name = data['name']

		if "relatedCategoryId" in data and data['relatedCategoryId'] is not "":
			src.relatedCategoryId = data['relatedCategoryId']
		
		if "createdby" in data:
			src.createdby = data['createdby']

		return src


@resource(collection_path='/api/damagereport/attachment', path='/api/damagereport/attachment/{id}', description="Damage Reporting")
class DamageReportAttachmentHandler:

	def __init__(self, request, context=None):
		self.request = request
		self.context = context

	def collection_get(self):
		return []

	def collection_post(self):
		try:
			data = self.request.json_body
		except:
			data = self.request.POST

		try:
			log.info(str(data))
			file_data = {
				"filename": self.request.POST['file'].filename,
				"file": self.request.POST['file'].value,
				"type": self.request.POST['filetype'],
				"userid": self.request.POST['userid']
			}
			location = store_media(file_data)
			if location is not "":
				location_str = "static" + location.split('static')[1]
				return location_str
		except Exception as e:
			print(e.message)

		self.request.response.status = 400
		return { "response": "Invalid request. Ensure all required fields are submitted"}