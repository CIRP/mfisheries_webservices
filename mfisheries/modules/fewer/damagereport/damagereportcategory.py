import transaction
from sqlalchemy import (
    Column,
    Integer,
    String,
    TIMESTAMP,
    ForeignKey,
    TEXT,
    REAL, event,
    DateTime, func,
    UniqueConstraint
)
from sqlalchemy.orm import relationship
from mfisheries.models import Base, DBSession
from mfisheries.core import Country, User


class DamageReportCategory(Base):
    """The SQLAlchemy declarative model class for a Module object"""
    __tablename__ = 'damagecategories'
    id = Column(Integer, primary_key=True)
    countryid = Column(Integer, ForeignKey('country.id'), nullable=False)
    name = Column(String(100))
    timecreated = Column(DateTime, nullable=False, default=func.now())
    timemodified = Column(DateTime, onupdate=func.now())
    createdby = Column(Integer, ForeignKey(User.id), nullable=True)
    relatedCategoryId = Column(Integer, ForeignKey('damagecategories.id'), nullable=True)

    country = relationship("Country", foreign_keys=[countryid])
    relatedCategory = relationship("DamageReportCategory", remote_side=[id], uselist=False)

    # http://docs.sqlalchemy.org/en/latest/core/constraints.html#sqlalchemy.schema.UniqueConstraint
    __table_args__ = (UniqueConstraint('countryid', 'name', name='dmg_cat_uixcm_1'),)

    def __init__(self, countryid, name):
        self.countryid = countryid
        self.name = name

    def getRequiredFields(self):
        return ['countryid', 'name']

    def toJSON(self):
        rec = {
            'id': self.id,
            'countryid': self.countryid,
            'name': self.name,
            "country": self.country.name,
            "timecreated": str(self.timecreated),
            "timemodified": str(self.timemodified),
            "relatedCategoryId" : str(self.relatedCategoryId),
            "createdby": str(self.createdby)
        }

        if self.relatedCategory:
            rec['relatedCategory'] = self.relatedCategory.name
        else:
            rec['relatedCategory'] = "None"

        return rec


def create_default_categories(target, connection_rec, **kw):
    print("Creating Default Damage Reporting Categories")

    categories = [
        "Boats",
        "Buildings",
        "Environment",
        "Equipment",
        "Other"
    ]
    try:
        countries = DBSession.query(Country).all()
        for country in countries:
            for cat_str in categories:
                category = DamageReportCategory(country.id, cat_str)
                DBSession.add(category)
        DBSession.flush()
        transaction.commit()
    except Exception as e:
        print("Error: Inserting LEK Categories Failed: %s" % e.message)
        DBSession.rollback()


event.listen(Country.__table__, 'after_create', create_default_categories)