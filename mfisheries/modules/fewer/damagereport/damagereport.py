from sqlalchemy import (
	Column,
	Integer,
	String,
	TIMESTAMP,
	ForeignKey,
	TEXT,
	REAL
)
from sqlalchemy.orm import relationship

from mfisheries.models import Base
from datetime import datetime


class DamageReport(Base):
	"""SQLAlchemy declarative model class for a DamageReport object"""
	__tablename__ = "damagereports"
	id = Column(Integer, primary_key=True)
	name = Column(String(50), nullable=False)
	description = Column(TEXT, default="")
	userid = Column(Integer, ForeignKey('user.id'), nullable=False)
	countryid = Column(Integer, ForeignKey('country.id'), nullable=False)
	latitude = Column(String(20))
	longitude = Column(String(20))
	filetype = Column(String(10), default="text")
	filepath = Column(String(200))
	aDate = Column(TIMESTAMP, default=datetime.now)
	category = Column(String(30), default="Environment")
	isPublic = Column(Integer, default=1)
	timecreated = Column(TIMESTAMP, default=datetime.now)
	
	createdby = Column(Integer, ForeignKey('user.id'))
	cost = Column(REAL)
	
	country = relationship("Country", foreign_keys=[countryid], lazy='subquery')
	user = relationship("User", foreign_keys=[userid], lazy="subquery")
	
	def __init__(self, uid, cid, n):
		self.userid = uid
		self.countryid= cid
		self.name = n
	
	def getRequiredFields(self):
		return [
			"userid",
			"name",
			'countryid',
			'filetype',
			'category',
			'latitude',
			'longitude',
			'aDate',
			'isPublic'
		]
	
	def toJSON(self):
		rec = {
			"id": self.id,
			"name": self.name ,
			"description": self.description ,
			"userid": self.userid ,
			"countryid": self.countryid ,
			"latitude": self.latitude ,
			"longitude": self.longitude ,
			"timecreated": str(self.timecreated) ,
			"createdby": self.createdby ,
			"filetype": self.filetype,
			"filepath": self.filepath,
			"aDate": str(self.aDate),
			"cost": self.cost,
			"category": self.category,
			"isPublic": self.isPublic
		}
		
		if self.country:
			rec['country'] = self.country.name
		if self.user:
			rec["user"]= self.user.username
			rec["fullname"]= "{0} {1}".format(self.user.fname, self.user.lname)
		return rec
