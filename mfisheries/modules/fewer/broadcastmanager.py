import time
import logging
log = logging.getLogger(__name__)
import requests
import sys
# Firebase related imports
from firebase import firebase
from pyfcm import FCMNotification
from pyramid.paster import bootstrap,  get_appsettings, setup_logging
from sqlalchemy import engine_from_config

from mfisheries.core.config import Config
from mfisheries.core.country import Country
from mfisheries.models import DBSession, Base
from mfisheries.modules.fewer.cap.capsource import CAPSource
from mfisheries.modules.alerts.usergroups import UserGroups
from mfisheries.modules.alerts.messages import Messages
from mfisheries.events import NotifyError
from pyramid.config import Configurator
from mfisheries.modules.fewer.sms.smsSubscriptions import  SmsSubscription

# Expected to be executed either from the main or source script
ini_full_path = "./development.ini"


class BroadcastManager(object):

	def __init__(self):
		self.fb = None
		self.api_key = None
		self.enable_debug = True
		self.config = Configurator()

	def getFBApp(self):
		"""
		This method will utilize the system configured firebase connection string to setup the connection
		between the mFisheries system and Firebase
		"""
		if not self.fb:  # If Firebase not previously initialized
			c = DBSession.query(Config).filter(Config.key == "firebase_connection_string").one()
			fb_conn_str = c.value
			if len(fb_conn_str) > 1:
				self.fb = firebase.FirebaseApplication(fb_conn_str)
				DBSession.rollback()
		return self.fb

	def getPushService(self):
		"""
		This method will return an instantiated FCM Notification Object.
		It will fetch the api key from the database and perform the necessary setup for enabling FB push notification services
		"""
		if not self.api_key:
			c = DBSession.query(Config).filter(Config.key == "firebase_api_key").one()
			self.api_key = c.value
			DBSession.rollback()
		return FCMNotification(self.api_key)

	def notify(self, alert, source):
		push_service = self.getPushService()
		# log.debug(alert)
		log.debug("Notifying of alert = " + alert["alert_id"])
		data = {
			"title": "FEWER Alert!",
			"message": alert["title"],
			"type": "cap",
			"alertId": alert["alert_id"]
		}
		topic = "cap-" + alert['audience'].lower() #TODO - Alerts Ensure that it is consistent with mobile
		log.debug("Sending to " + topic)

		result = push_service.notify_topic_subscribers(topic_name=topic, data_message=data)
		log.debug(str(result))

		SmsSubscription("", "").broadcastTo(alert["alert_id"],alert['title'], source.countryid)

		# Retrieve Public Alert Group IDs to send notification to Subscribers
		try:
			log.debug("Attempting to store alert within the public advisory group")
			res = DBSession.query(UserGroups).filter(UserGroups.countryid == source.countryid).filter(UserGroups.groupname.like("%Advisories%")).all()
			if res:
				log.debug("Found {0} public advisory groups to send alert to".format(len(res)))
				for group in res:
					result = push_service.notify_topic_subscribers(topic_name="alert-group-{0}".format(group.id), data_message=data)
					# TODO Alert -  Provide filter to determine if notification was previously sent
					log.debug("Result of Send to group {0} was {1}".format(group.id,str(result)))
					message = self.convertCAPToMessage(alert, group)
					message.isVerified = True
					try:
						DBSession.add(message)
						DBSession.flush()
						log.debug("Saved record to group: {0}".format(group.id))
					except Exception, dex:
						log.error(str(dex))
			else:
				log.debug("Did not find any public alert groups")
			DBSession.rollback()
		except Exception, e:
			log.error(str(e))
		self.saveStatus(alert["alert_id"], alert["title"], source.countryid)

	def debug(self, message, type="debug"):
		if self.enable_debug:
			if type == "debug":
				log.debug(message)
			if type == "error":
				log.error(message)
			else:
				log.info(message)

	def convertCAPToMessage(self, alert, group):
		# Create a Message with minimal configuration
		message = Messages("", group.id, "", "","","", 1)
		userid_updated = False
		log.debug("Alert Message is:")
		log.debug(str(alert))
		log.debug(str(group))
		# Attempt to find the User id from the parameters
		if alert['parameters'] and len(alert['parameters']) > 1:
			for param in alert['parameters']:
				if param['name'] == 'userid':
					message.userid = param['value']
					userid_updated = True

		# If not specified in the parameters, then use system id
		if not userid_updated:
			system_id = DBSession.query(Config).filter(Config.key == "system_account").one().value()
			message.userid = system_id

		# Translate CAP Severity terms to FEWER
		if alert['severity'] == "Extreme":
			message.severity = "4-extreme"
		elif alert['severity'] == "Severe":
			message.severity = "3-severe"
		elif alert['severity'] == "Moderate":
			message.messagecontent = "2-bad"
		elif alert['severity'] == "Minor":
			message.messagecontent = "1-fair"
		else:
			message.messagecontent = "unspecified"

		# Translate CAP Alert category terms to FEWER
		if alert['category'] == "Met":
			message.messagecontent = "Bad Weather"
		elif alert['category'] == "Env":
			message.messagecontent = "Rough Seas"
		else:
			message.messagecontent = alert['category']

		# Translate CAP Response Action to human readable terms
		if alert['response_type'] == "Shelter":
			action = "Take Shelter"
		elif alert['response_type'] == "Evacuate":
			action = "Evacuate"
		elif alert['response_type'] == "Prepare":
			action = "Make Preparations"
		elif alert['response_type'] == "Execute":
			action = "Execute Pre-Planned Action"
		elif alert['response_type'] == "Avoid":
			action = "Avoid the Area"
		elif alert['response_type'] == "Monitor":
			action = "Monitor Conditions"
		elif alert['response_type'] == "AllClear":
			action = "Resume normal activities"
		elif alert['response_type'] == "None":
			action = "Take no action"
		else:
			action = "Unknown"


		description = "A {0} alert with a severity of {1} was created by {2}.".format(message.messagecontent, alert['severity'], alert['sender_name'])
		message.description = "{0} {1}. {2}-{3}".format(description, action, alert['urgency'], alert['certainty'])

		# TODO - Alerts - Translate the exipres time to mobile

		return message

	def checkAllSourcesForAlerts(self):
		"""
		Runs through the process for determine if external CAP sources have alerts and send broadcasts to subscribed clients
		Steps:
		1. Extract CAP sources from the database
		2. Check the Feed.xml for each record
		3. Compare the Alert(s) received at the source to determine if a new alert was issued
		4. If new alert issued (by source) the system adds alert info to Firebase and Broadcast alert to country clients
		"""
		# step 1
		log.debug("Attempting to Check All Sources for CAP Alerts")
		fb = self.getFBApp()
		if fb:
			sources = self.retrieveSources()  # Retrieve Alert-source records from the database
			if sources:
				# step 2
				for src in sources:
					try: # The exception handling is by source .. so if one source fail it should move the the next
						# Build the path of alerts in the Firebase database for this source's country
						country_path = src.country.name.lower().replace(" ", "_")
						log.debug("Retrieving CAP alerts for: {0}".format(country_path))
						fb_last_alert_path = "/capalerts/last_alerts/{0}/".format(country_path)
						# Retrieve from Firebase the last alert from this source (based on its id)
						fb_alert_name = str(src.id)
						last_alert = fb.get(fb_last_alert_path, fb_alert_name) # /capalerts/last_alerts/trinidad/123 (where 123 is the source id)

						if self.enable_debug:
							log.debug("Received {0} from the Firebase path {1}".format(last_alert, fb_last_alert_path))
							log.debug("Name: {0} with feed from {1}".format(src.name, src.url))

						feed_url = src.url
						if len(feed_url) > 5:
							if self.enable_debug: log.debug("Retrieving information from: {0}".format(feed_url))

							r = requests.get(feed_url)
							if r.status_code == 200:

								if r.headers['Content-Type'] == "application/json":
									alerts = r.json()
									log.debug("CAP alert for {0} fetched successfully as {1}".format(src.name, alerts))

									# step 3
									for alert in alerts: # TODO Alert - This logic does not cover all cases, need further analysis
										log.debug("Current alert id: {0}".format(alert['alert_id']))
										if alert['alert_id'] == last_alert: # TODO Alert - Not the best way to determine if alert previously sent
											log.debug("Already notified")
											break
										else:
											log.debug("Attempting to send Alert: {0} to {1}".format(alert, src.url))
											self.notify(alert, src)  # step 4
									# TODO Alert - This implementation needs to be evaluated to determine what limitations exists.
									# TODO Alert - These limitations should be clearly documented
									if len(alerts) > 0:
										if alerts[0]['alert_id'] != last_alert:
											log.debug("Updating last alert to " + alerts[0]['alert_id'])
											result = fb.put(fb_last_alert_path, fb_alert_name, alerts[0]['alert_id'])
											log.debug(str(result))
									else:
										log.debug("No alerts found at: {0}".format(src.name))
								elif r.headers['Content-Type'] == "application/xml":
									responseText = r.content
									# TODO Alert - Process XML context for alerts
								else:
									log.error("Data was retrieved from the source as {0}".format(r.headers['Content-Type']))
							else:
								log.error("Unable to retrieve data. Status code was: {0}".format(r.status_code))
						else:
							log.error("Source {0} has no URL".format(src.id))
							self.config.registry.notify(NotifyError("BroadcastManager", "Unable to retrieve alerts for source {0}. No URL configured for record".format(src.id)))
					except Exception, e:
						self.config.registry.notify(NotifyError("BroadcastManager", "Error Occurred when processing alert for {0}".format(src.name)))
						log.error(str(e))
			else:
				log.debug("Did not retrieve records from the retrieve sources request")
			log.debug("Completed Checking all sources for alerts")
			return True
		else:
			log.error("Unable to configure Firebase for Sending Alerts")
			self.config.registry.notify(NotifyError("BroadcastManager", "Unable to configure Firebase for Sending Alerts"))
		return None

	def retrieveSources(self, countryid=0):
		"""
		Attempts to retrieve the records of the configured alert sources. Currently (16-Jun-18) attempts to retrieve only JSON
		configured CAP sources. XML is not yet supported.
		:param countryid: The Country ID to retrieve alert sources for. If no country specified. The system will retrieve all records for all countries
		:return: returns an array of CAPSource (modules.fewer.cap.capsource) records from the identified country
		"""
		if self.enable_debug: log.info("Attempting to retrieve information for country {0}".format(countryid))
		query = DBSession.query(CAPSource).filter(CAPSource.category == "JSON") # TODO Change to be compatible with XML CAP alerts as well
		if countryid is not 0:
			query = query.filter(CAPSource.countryid == countryid)
		res = query.all()
		DBSession.rollback()
		return res

	def saveStatus(self, alert_fb_id, message, countryid):
		"""

		:param alert_fb_id:
		:param message:
		:param countryid:
		:return:
		"""
		fb = self.getFBApp()
		fb_notify_base = '/capalerts/notif-status/' + alert_fb_id
		log.debug("Saving notification status")
		users = fb.get('/users', None)
		for user in users:
			if 'countryId' in users[user]:
				if users[user]['countryId'] == countryid:
					result = fb.put(fb_notify_base + '/users/' + user, False)
					log.debug(result)
			else:
				log.debug("Unable to retrieve country of account")
		millis = int(round(time.time() * 1000))
		fb.put(fb_notify_base + '/sent', millis)
		fb.put(fb_notify_base + '/message', message)


def run_bcast_mgmtr_default(argv=sys.argv, path=None, use_bootstrap=False):
	print("Executing Broadcast Manager")
	config_uri = argv[1] if len(argv) > 1 else ini_full_path
	config_uri = path if path is not None else config_uri

	# Use bootstrap if loaded as a module via the command line
	if use_bootstrap:
		bootstrap(config_uri)
	else:
		# Used as a similar strategy as the weather scheduler (fewer.weather.parser.scheduler)
		setup_logging(config_uri)
		log = logging.getLogger(__name__)
		settings = get_appsettings(config_uri)
		engine = engine_from_config(settings, 'sqlalchemy.')
		DBSession.configure(bind=engine)
		Base.metadata.bind = engine

	bc = BroadcastManager()
	bc.checkAllSourcesForAlerts()


def test_get_public_group():
	"""
	View the public advisory records to ensure we getting back the groups needed to send CAP alerts
	:return:
	"""
	countries = DBSession.query(Country).all()
	for country in countries:
		res = DBSession.query(UserGroups).filter(UserGroups.countryid == country.id).filter(UserGroups.groupname.like("%Advisories%")).all()
		if res:
			log.info("Found {0} public advisory groups to send alert to".format(len(res)))
			for group in res:
				log.info("Result of Send to group {0} was with name {1}".format(group.id, group.groupname))
		else:
			log.info("Did not find any public alert groups")

def test_send_subscribers():
	"""
	A function to provide a localised testing of the notification features based on expected subscribers
	:return:
	"""
	bc = BroadcastManager()

	countries = DBSession.query(Country).all()
	for country in countries:
		countryid = country.id
		res = DBSession.query(UserGroups).filter(UserGroups.countryid == countryid).filter(UserGroups.groupname.like("%Advisories%")).all()
		log.info("Found {0} advisory groups for country {1} with id {2}".format(len(res), country.name, countryid))
		push_service = bc.getPushService()
		if res is not None and len(res) > 0:
			group = res[0]
			msg = {
				"title": "FEWER",
				"message": "System Test",
				"type": "test",
				"alertId": -1
			}
			result = push_service.notify_topic_subscribers(topic_name="alert-group-{0}".format(group.id), data_message=msg)
			log.info(str(result))
	# bc = BroadcastManager()
	# push_service = bc.getPushService()
	# msg = {
	# 	"title": "FEWER",
	# 	"message": "System Test",
	# 	"type": "test",
	# 	"alertId": -1
	# }
	# user_str="euMnhM8wsjM:APA91bEedJah2b18McMxwRsuZv5P7bof14U9JBRGDdiLvgyBGZWMgxUqfq58Zu4YdyWuxP9V3U_dldbkP7hCID9Xv2lx6xGsuUT4L5E7eaMJ-QftjNz06TLDestp872b8uFk3HWXmpnU"
	# res = push_service.notify_topic_subscribers(topic_name="alert-group-2",data_message=msg )
	# res = push_service.notify_single_device(user_str, data_message=msg)
	# display_debug(str(res))


if __name__ == "__main__":
	print("Running Broadcast Manager via command line")
	# Run this main using the command:  python -m mfisheries.modules.fewer.broadcastmanager
	#TODO This not working as expected through invocation above. However, the function runs as a mfisheries script command
	# run_bcast_mgmtr_default(use_bootstrap=True)