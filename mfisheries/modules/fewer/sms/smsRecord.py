from sqlalchemy import Column, Integer, String, TIMESTAMP, ForeignKey
import datetime
from mfisheries import Base
from mfisheries.models import DBSession
import transaction
from sqlalchemy.orm import relationship
import logging

log = logging.getLogger(__name__)


class SmsRecord(Base):
    __tablename__='smsrecords'
    id = Column(Integer, primary_key=True)

    sentfrom = Column(String(20),nullable=True)
    smsid = Column(String(40),nullable=False)
    accid = Column(String(40),nullable=True)
    msgstat = Column(String(20),nullable=True)
    alertid = Column(String(30),nullable=True)
    sentto =Column(String(20),nullable=True)
    apiver =Column(String(20),nullable=True)
    smsstat = Column(String(20),nullable=True)
    msgid =Column(String(40),   nullable=True)
    timecreated = Column(TIMESTAMP, default=datetime.datetime.utcnow)
    timesent = Column(TIMESTAMP, nullable=True)
    timedelivered = Column(TIMESTAMP, nullable=True)
    recordtype = Column(String(40), nullable=False)
    body = Column(String(400),nullable=True)
    countryid = Column(Integer, ForeignKey('country.id'))

    country = relationship("Country", foreign_keys=[countryid], lazy='subquery')

    def __init__(self, smsid, body, recordtype, countryid, alertid="-1"):
        self.smsid=smsid
        self.body=body
        self.recordtype=recordtype
        self.countryid=countryid
        self.alertid = alertid

    def toJSON(self):
        rec = {
            "id": self.id,
            "countryid": self.countryid,
            "sentfrom": self.sentfrom,
            "smsid": self.smsid,
            "accid": self.accid,
            "msgstatus": self.msgstat,
            "sentto": self.sentto,
            "apiver": self.apiver,
            "smsstat": self.smsstat,
            "alertid": self.alertid,
            "msgid": self.msgid,
            "timecreated": unicode(self.timecreated),
            "timesent": unicode(self.timesent),
            "timedelivered": unicode(self.timedelivered),
            "recordtype": self.recordtype,
            "body": self.body
        }

        if self.country:
            rec['country'] = self.country.name

        return rec

    def getRequiredFields(self):
        return [
            "smsid"
        ]

    # Adds a blank record to the database
    def add(self):
        try:
            DBSession.add(self)
            DBSession.flush()
            transaction.commit()
        except Exception as e:
            DBSession.rollback()
            log.info("There was error adding a SMS record")

    def fetchCountrySms(self, countryid):
        res = DBSession.query(SmsRecord).filter(SmsRecord.countryid == countryid).all()
        res = map(lambda m: m.toJSON(), res)
        return {'status': 200, 'data': res}