from sqlalchemy import Column, Integer, String, ForeignKey, Boolean
from mfisheries import Base
from mfisheries.models import DBSession
import transaction
from sqlalchemy.orm import relationship
import logging

log = logging.getLogger(__name__)


class TwilioCountrySmsConfig(Base):
    __tablename__='smscountryconfig'
    id = Column(Integer, primary_key=True)

    twilioenabled = Column(Boolean,nullable=False)
    countryid = Column(Integer, ForeignKey('country.id'))
    accid = Column(String(40),nullable=True)
    token = Column(String(40),nullable=True)
    num = Column(String(40),nullable=True)

    country = relationship("Country", foreign_keys=[countryid], lazy='subquery')

    def __init__(self, twilioenabled=None, countryid=None, accid=None, token=None, num=None):
        if twilioenabled:
            self.twilioenabled = twilioenabled
        if accid:
            self.accid=accid
        if token:
            self.token=token
        if countryid:
            self.countryid=countryid
        if num:
            self.num = num

    def toJSON(self):
        rec = {
            "id": self.id,
            "twilioenabled": self.twilioenabled,
            "countryid": self.countryid,
            "accid": self.accid,
            "token": self.token,
            "num": self.num
        }

        if self.country:
            rec['country'] = self.country.name

        return rec

    def getRequiredFields(self):
        return [
            "smsid"
        ]

    # Adds a blank record to the database
    def add(self):
        try:
            DBSession.add(self)
            DBSession.flush()
            transaction.commit()
            return {'status':200, 'data':'Success'}
        except Exception as e:
            DBSession.rollback()
            log.info("There was error adding a SMS country configuration record")
            return {'status': 400, 'data': 'Failure'}

    def update(self):
        try:
            rec = DBSession.query(TwilioCountrySmsConfig).filter(TwilioCountrySmsConfig.countryid == self.countryid).one()
            rec.twilioenabled = self.twilioenabled
            rec.countryid = self.countryid
            rec.accid = self.accid
            rec.token = self.token
            rec.num = self.num

            DBSession.flush()
            transaction.commit()
            return {'status': 200, 'data': 'Success'}
        except Exception as e:
            DBSession.rollback()
            log.info("There was error adding a SMS country configuration record")
            return {'status': 400, 'data': 'Failure'}

    def getConfig(self, countryid):
        res = DBSession.query(TwilioCountrySmsConfig).filter(TwilioCountrySmsConfig.countryid == countryid).all()
        return res

    def getAllConfigs(self):
        res = DBSession.query(TwilioCountrySmsConfig).all()
        res = map(lambda m: m.toJSON(), res)
        return {'status': 200, 'data': res}

    def delete(self):
        try:
            DBSession.query(TwilioCountrySmsConfig).filter(TwilioCountrySmsConfig.countryid == self.countryid).delete()
            DBSession.flush()
            transaction.commit()
            return {'status': 200, 'data': 'Success'}
        except Exception as e:
            DBSession.rollback()
            log.info("There was error adding a SMS country configuration record")
            return {'status': 400, 'data': 'Failure'}




