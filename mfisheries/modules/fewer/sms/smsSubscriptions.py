from sqlalchemy import Column, Integer, String, TIMESTAMP, ForeignKey
from datetime import datetime
from mfisheries import Base
from mfisheries.models import DBSession
import transaction
from sqlalchemy.orm import relationship
from smsRecord import SmsRecord
import logging
from .twilioSms import TwilioSms
log = logging.getLogger(__name__)


class SmsSubscription(Base):
	__tablename__ = 'smssubscriptions'
	id = Column(Integer, primary_key=True)
	phonenum = Column(String(20), nullable=False)
	contact_name = Column(String(200), nullable=True)
	countryid = Column(Integer, ForeignKey('country.id'))
	timecreated = Column(TIMESTAMP, default=datetime.now)
	createdby = Column(Integer, ForeignKey('user.id'), nullable=True)
	time_last_notice_received = Column(TIMESTAMP, nullable=True)
	id_last_notice_received = Column(String(200), nullable=True)

	country = relationship("Country", foreign_keys=[countryid], lazy='subquery')
	user = relationship("User", foreign_keys=[createdby], lazy='subquery')

	def __init__(self, phonenum, countryid):
		self.phonenum = phonenum
		self.countryid = countryid

	def toJSON(self):
		rec = {
			"id": self.id,
			"phonenum": self.phonenum,
			"countryid": self.countryid,
			'timecreated': str(self.timecreated)
		}

		if self.contact_name:
			rec['contact_name'] = self.contact_name

		if self.country:
			rec['country'] = self.country.name

		if self.createdby:
			rec['createdby'] = self.createdby

		if self.user:
			rec['createdByUser'] = "{0} {1}".format(self.user.fname, self.user.lname)

		return rec

	def add(self):
		#Check to see if the user already exists in the database for that country
		query = DBSession.query(SmsSubscription).filter(SmsSubscription.countryid == self.countryid).filter(SmsSubscription.phonenum == self.phonenum).all()

		print("The length of the query for ADD is " + str(len(query)))

		if len(query)> 0:
			#print("Length of ADD is morethan 0 so we exit sending the statement")
			return {'status': 500, 'response': 'User is already subscribed'}

		try:
			#print("Len is 0 so we add new user and send sms")

			#Add record to database
			DBSession.add(self)
			#Commit changes to database
			DBSession.flush()
			transaction.commit()

			message = "You have been subscribed to alerts for FEWER / mFisheries"

			#Create an instance of twilio sms and send the message
			messageID = TwilioSms(self.countryid).send_sms(self.phonenum,message)
			if (messageID):
				# Save the message in the database
				SmsRecord(messageID, message, "ADD",self.countryid).add()
				return {'status': 200, 'response': 'success'}
			else:
				print("Error: SMS cannot be sent for your country.")
				return {'status': 401, 'response': 'SMS cannot be sent for your country.'}

		except Exception as e:
			log.info("There was an error in committing the number ")
			log.error("Error: " + e.message)
			return {'status': 500, 'response': 'An error has occurred, please try again'}

	def broadcastTo(self, alertid, message, country):
		# Get a list of all numbers denoted by 'country'
		query = DBSession.query(SmsSubscription).filter(SmsSubscription.country == country).all()

		for item in query:
			# We want to send the message denoted by 'message' to all numbers for 'country'
			messageID = TwilioSms(self.countryid).send_sms(item[SmsSubscription.phonenum], message)
			if messageID:
				# Save the message in the database
				SmsRecord(messageID, message, "BROADCAST", self.countryid, alertid).add()

	def remove(self):
		#run query to see if user exists
		try:
			query = DBSession.query(SmsSubscription).filter(SmsSubscription.phonenum == self.phonenum).one()
		except Exception as e:
			# This happens when user doesn't exist in table. So we return a message saying that they aren't subscribed
			print("Error: " + e.message)
			return {'status': 500, 'response': 'Cannot remove user as they are not subscribed to alerts'}

		if query:
			# There is a user to delete, so we delete them
			try:
				print("THERE IS USER SO WE DELETE")
				DBSession.delete(query)
				#Save changes to the database
				DBSession.flush()
				transaction.commit()
				message ='You have been unsubscribed from alerts for FEWER / mFisheries'
				# Create an instance of twilio sms and send the message

				messageID = TwilioSms(self.countryid).send_sms(self.phonenum, message)
				if (messageID):
					# Save the message in the database
					SmsRecord(messageID, message, "REMOVE", self.countryid).add()

				return {'status': 200, 'response': 'success'}

			except Exception as e:
				print("Error: " + e.message)
				return {'status': 500, 'response': 'This number does not exist in the database'}

	def getRequiredFields(self):
		return [
			"phonenum",
			"countryid"
		]
