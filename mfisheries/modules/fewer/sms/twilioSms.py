from smsInterface import SmsInterface
from twilio.rest import Client
from twilioCountrySmsConfig import TwilioCountrySmsConfig
import logging
log = logging.getLogger(__name__)


class TwilioSms(SmsInterface):

    def __init__(self, countryid=None):
        """Allows the configuration of the Twilio SMS Object via its parameters.
        This will facilitate the develop of a Twilio Configuration per country.
        The twilio class will be instantiated with the contryidID of the sender. The configurations will be pulled
        from the database and then that will be used to send the sms with the respective configurations"""

        if countryid:
            config = TwilioCountrySmsConfig().getConfig(countryid)
            account_sid = config.account
            auth_token = config.token
            self.sender = config.sender
            self.client = Client(account_sid, auth_token)
            self.callback = "http://test.mfisheries.cirp.org.tt/api/messagedelivery"

    def send_sms(self, recipient, message):
        try:
            # Send the SMS message using the Twilio API
            msg = self.client.api.account.messages.create(
                to=recipient,
                from_=self.sender,
                body=message,
                status_callback=self.callback)
            return msg.sid
        except Exception as e:
            print("Error: " + e.message)
            print("SELF" + self)

    def sms_received(self, sender, message):

        return

    def sms_status_update(self, sms_id, status):

        return
