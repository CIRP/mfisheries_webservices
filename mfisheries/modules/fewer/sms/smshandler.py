
from cornice.resource import resource

from mfisheries.core import BaseHandler
from mfisheries.models import DBSession
from .smsSubscriptions import SmsSubscription


@resource(collection_path='/api/sms', path='/api/sms/{id}', description="SMS for receiving alerts")
class SMSHandler(BaseHandler):
	def __init__(self, request, context=None):
		BaseHandler.__init__(self, request, context)
		self.module_name = __name__

	def _create_empty_model(self):
		return SmsSubscription("", "")

	def _get_fields(self):
		return self._create_empty_model().getRequiredFields()

	def _get_target_class(self):
		return SmsSubscription

	def _get_order_field(self):
		return SmsSubscription.timecreated

	def _dict_to_model(self, data, rec_id=0):
		if rec_id == 0:
			src = self._create_empty_model()
		else:
			src = DBSession.query(self._target_class).get(rec_id)

		src.phonenum = data['phonenum']
		src.countryid = data['countryid']

		if "createdby" in data:
			src.createdby = data['createdby']

		if "last_notice_received" in data:
			src.last_notice_received = data['last_notice_received']

		return src
