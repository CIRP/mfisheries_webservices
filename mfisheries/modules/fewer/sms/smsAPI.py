import datetime
import logging
log = logging.getLogger(__name__)

from cornice import Service

from .smsSubscriptions import SmsSubscription
from mfisheries.models import DBSession
from mfisheries.util.secure import validate
from twilioCountrySmsConfig import TwilioCountrySmsConfig
from mfisheries.modules.fewer.sms.smsRecord import SmsRecord
import json

addAlertService = Service(name='addAlertService', path='api/addalertnumber', description='Allows adding of a user to alert groups for a country')
removeAlertService = Service(name='removeAlertService', path='api/removealertnumber', description='Allows removing of a user to alert groups for a country')
deliveryService = Service(name='delService', path='api/messagedelivery', description='Message delivery service')
infoService = Service(name='infoService', path='api/getsms', description='Service for fetching sms records')
twilioService = Service(name='twilioService', path='/api/addtwilio', description='Service for getting twilio related records')


# Post method that subscribes user from receiving FEWER / mFisheries alerts
@addAlertService.post()
def subscribe_user_to_alert(request):
    # validate request... Exit if not a valid request
    if not validate(request):
        return {'status': 401, 'data': []}

    #Get the parameters from the request
    try:
        phoneNumber = request.POST['number']
        simCountryCode = request.POST['simCountry']
    except Exception as e:
        return {'status': 400, 'response': 'Invalid Request Params'}

    #Add the phone number denoted my the 'number' parameter to the country denoted bt the 'simcountry' parameter
    return SmsSubscription(phoneNumber, simCountryCode).add()


@removeAlertService.post()
def unsubscribe_user_from_alert(request):
    """
    Post method that unsubscribes user from receiving FEWER / mFisheries alerts
    :param request:
    :return:
    """
    if not validate(request):  # validate request... Exit if not a valid request
        return {'status': 401, 'data': []}
    try:  # Get the parameters from the request
        phoneNumber = request.POST['number']
        simCountryCode = request.POST['simCountry']
    except Exception as e:
        return {'status': 400, 'response': 'Invalid Request Params'}

    # Remove the phone number denoted my the 'number' parameter to the country denoted bt the 'simcountry' parameter
    return SmsSubscription(phoneNumber, simCountryCode).remove()

@deliveryService.post()
def incoming_sms(request):
    sentFrom = request.POST['From']
    smsId = request.POST['SmsSid']
    accId = request.POST['AccountSid']
    msgStat = request.POST['MessageStatus']
    sentTo = request.POST['To']
    apiVer = request.POST['ApiVersion']
    smsStat = request.POST['SmsStatus']
    msgId = request.POST['MessageSid']

    item = DBSession.query(SmsRecord).filter(SmsRecord.smsid == smsId).one()
    item.sentfrom = sentFrom
    item.accid =accId
    item.msgstat =msgStat
    item.sentto = sentTo
    item.apiver = apiVer
    item.smsstat = smsStat
    item.msgid = msgId

    if msgStat=="delivered":
        item.timedelivered=datetime.datetime.utcnow()

    if msgStat=="sent":
        item.timesent=datetime.datetime.utcnow()

    try:
        DBSession.flush()
    except Exception as e:
        log.error("Unable to report delivery: " + str(e))
        DBSession.rollback()


@infoService.get()
def fetchCountrySms(request):
    if not validate(request):
        return {'status': 401, 'data': []}

    countryid = request.GET['countryid']
    return SmsRecord("", "", "", "").fetchCountrySms(countryid)


@twilioService.get()
def fetchCountriesConfigs(request):
    if not validate(request):
        return {'status': 401, 'data': []}
    return TwilioCountrySmsConfig().getAllConfigs()


@twilioService.post()
def addCountryConfig(request):
    data = json.loads(request.body)
    if 'delete' in data:
        if data['delete']:
            return delCountryConfig(request)
    accountid = data['accid']
    countryid = data['countryid']
    number = data['num']
    token = data['token']

    newConf = TwilioCountrySmsConfig(1,countryid,accountid,token,number)
    return newConf.add()


@twilioService.put()
def updateCountryConfig(request):
    data = json.loads(request.body)
    accountid = data['accid']
    countryid = data['countryid']
    number = data['num']
    token = data['token']

    newConf = TwilioCountrySmsConfig(1, countryid, accountid, token, number)
    return newConf.update()


@twilioService.delete()
def delCountryConfig(request):
    data = json.loads(request.body)
    accountid = data['accid']
    countryid = data['countryid']
    number = data['num']
    token = data['token']

    newConf = TwilioCountrySmsConfig(1, countryid, accountid, token, number)
    return newConf.delete()