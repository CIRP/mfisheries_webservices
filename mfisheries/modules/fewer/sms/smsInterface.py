from zope.interface import Interface


class SmsInterface(Interface):
    """ This Interface outlines the functionality that's required by all clients implementing this interface."""

    def send_sms(self, recipient, message):
        """ This function will be used to send sms messages to the client.

            Depending on the implementation of the service provider, additional segmentation of SMS messages may be
            required within the code as there is a character limit for SMS messages
            Args:
                sender (String): This parameter specifies the E.164 representation of the sender's phone number.
                This may be provided by the local telephone service provider

                recipient (String): This parameter specifies the E.164 representation of the recipient's phone number.

                message (String): This parameter specifies the body of text that the recipient will receive on their
                mobile enabled device."""
        pass

    def sms_received(self, sender, message):
        """ This function will be used to alert the system that a user has sent (or replied to) a message to the system

            Args:
                sender (String): This parameter specifies the E.164 representation of the sender's phone number.

                message (String): This parameter specifies the body of text that the recipient has sent to the system"""
        pass

    def sms_status_update(self, sms_id, status):
        """ This function will be used either as the endpoint for the callback service or called directly by the
            service mentioned.

            Args:
                sms_id(String): This is the ID that used to uniquely identify the SMS which status is being updated.

                status(String): This is a status string that identifies the status of the message.
                For example (Queued, Sent, Delayed, Delivered) """
        pass
