import logging
log = logging.getLogger(__name__)

from cornice.resource import resource
from mfisheries.modules.fewer.cap.capsource import CAPSource
from mfisheries.models import DBSession
from mfisheries.core import BaseHandler


# noinspection SpellCheckingInspection,PyMethodMayBeStatic
@resource(collection_path='/api/capsources', path='/api/capsources/{id}', description='CAP Sources')
class CAPSourceHandler(BaseHandler):
    def _get_order_field(self):
        return CAPSource.name

    def __init__(self, request, context=None):
        BaseHandler.__init__(self, request, context)

    def _create_empty_model(self):
        return CAPSource(0,"", "", "", "")

    def _dict_to_model(self, data, rec_id=0):
        if rec_id == 0:  # If id is 0, we want to create a new entry
            src = self._create_empty_model()
        else:  # Retrieve record from database using id
            src = DBSession.query(self._target_class).get(rec_id)
        # Assign value to the fields of the model
        src.countryid = data['countryid']
        src.name = data['name']
        src.category = data['category']
        src.url = data['url']
        src.createdby = data['createdBy']
        src.scope = data['scope']
        if 'lastAlert' in data:
            src.lastAlert = data['lastAlert']
        return src

    def _get_fields(self):
        return ['countryid', 'name', 'category', 'url', 'createdBy', 'scope']

    def _get_target_class(self):
        return CAPSource

    def _handle_conditional_get(self, query):
        query = super(CAPSourceHandler, self)._handle_conditional_get(query)
        if "scope" in self.request.GET:
            query = query.filter(CAPSource.scope == self.request.GET['scope'])
        return query
    

# noinspection SpellCheckingInspection
@resource(path='/api/{countryid}/capsources', description='CAP Sources')
class CountryCAPSourceHandler(object):
    def __init__(self, request, context=None):
        self.request = request
        self.context = context
        self.cid = int(self.request.matchdict['countryid'])

    def get(self):
        res = DBSession.query(CAPSource).filter(CAPSource.countryid == self.cid).all()
        return map(lambda m: m.toJSON(), res)
