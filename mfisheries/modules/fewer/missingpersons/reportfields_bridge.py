from sqlalchemy import (
    Table,
    ForeignKey,
    Column,
    Integer,
    String
)
from mfisheries.models import Base

MSReportFieldsBridge = Table(
    'msreportfields_bridge',
    Base.metadata,
    Column('id', Integer, primary_key=True),
    Column('report_id', Integer, ForeignKey('msreports.id')),
    Column('field_id', Integer, ForeignKey('msreportfields.id')),
    Column('value', String(100))
)