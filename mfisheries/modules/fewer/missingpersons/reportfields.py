from datetime import datetime

from sqlalchemy import (
    Column,
    Integer,
    String,
    TIMESTAMP,
)

from mfisheries.models import Base

class MSReportField(Base):
    """The SQLAlchemy model class for adding fields to reports"""
    __tablename__ = 'msreportfields'
    id = Column(Integer, primary_key=True)
    name = Column(String(20), nullable=False, unique=True)
    timecreated = Column(TIMESTAMP, default=datetime.now)

    def __init__(self, name):
        self.name = name

    def getRequiredFields(self):
        return ["name"]

    def toJSON(self):
        rec = {
            "id" : self.id,
            "name": self.name,
            "timecreated": str(self.timecreated)
        }
        return rec

