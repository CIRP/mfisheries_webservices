from cornice.resource import resource
from mfisheries.core import BaseHandler
from mfisheries.models import DBSession
from sqlalchemy import or_, desc
import time

import logging
log = logging.getLogger(__name__)

from mfisheries.util.fileutil import store_media
from .report import Report
from .reportfields import MSReportField
from mfisheries.util.notificationManager import NotificationManager


# http://cornice.readthedocs.io/en/latest/resources.html
@resource(collection_path='/api/missingpersons', path='/api/missingpersons/{id}', description="Missing Persons")
class MSReportHandler(BaseHandler):
    def __init__(self, request, context=None):
        BaseHandler.__init__(self, request, context)
        self.order_func = desc

    def _get_order_field(self):
        return Report.timecreated

    def _create_empty_model(self):
        return Report(0,"")

    def _dict_to_model(self, data, rec_id=0):
        # If id is 0, we want to create a new entry
        if rec_id == 0:
            src = self._create_empty_model()
        else:
            src = DBSession.query(self._target_class).get(rec_id)
        # Assign value to the fields of the model
        src.title = data["title"]
        src.userid = data['userid']
        src.countryid = data['countryid']
        src.contact = data['contact']

        if "filepath" in data:
            src.filepath = data['filepath']

        if "description" in data:
            src.description = data['description']
        if "additional" in data:
            src.additional = data['additional']
        if "isFound" in data:
            src.isFound = data['isFound']

        if "latitude" in data:
            src.latitude = data["latitude"]
        if "longitude" in data:
            src.longitude = data["longitude"]

        if "foundBy" in data:
            src.foundBy = data['foundBy']

        return src

    def _get_fields(self):
        return self._create_empty_model().getRequiredFields()

    def _get_target_class(self):
        return Report

    def collection_post(self):
        res = super(MSReportHandler, self).collection_post()
        log.debug(res)
        if res:
            if "file" in self.request.POST:
                file_data = {
                    "filename": self.request.POST['file'].filename,
                    "file": self.request.POST['file'].value,
                    "type": 'image',
                    "userid": self.request.POST['userid']
                }
                location = store_media(file_data)
                # If file successfully
                if location is not "":
                    location_str = "static" + location.split('static')[1]
                    res['filepath'] = location_str
                    src = self._save(res, res['id'])
                    if src:
                        self.notify_of_report(res)
                        return src.toJSON()
                self.request.response.status = 400
                res['message'] = "Unable to save file resource"
        return res

    def _handle_conditional_get(self, query):
        query = super(MSReportHandler, self)._handle_conditional_get(query)
        if 'userid' in self.request.params:
            userid = self.request.params['userid']
            query = query.filter(Report.userid == userid)
        return query

    def put(self):
        res = super(MSReportHandler, self).put()
        log.debug("Printed: {0}".format(time.time()))
        if res:
            self.notify_of_report(res)
        return res

    def notify_of_report(self, res):
        data = self.retrieve_data()
        msg = "{0} was reported as missing".format(data['title'])
        try:

            # after building the basic message we attempt to put in additional details
            if "id" in data:  # record was updated
                log.debug("Missing Person report was updated, notifying users")
                if res['isFound'] == 1 or res['isFound'] == "":
                    msg = "{0} was found.".format(data['title'])
                    if "foundBy" in data and data['foundBy'] is not "":
                        msg = "{0} {1} was found by {2}".format(msg, data['title'], data['foundBy'])
                else:
                    msg = "Details for missing person {0} was updated".format(data['title'])
            else:
                log.debug("Missing person report was created, notifying users")
                msg = "{0} was reported as missing by {1}".format(data['title'], data['fullname'])
        except Exception as e :
            log.error("Unable to format notification report for missing persons:{0}".format(e))

        try:
            rec = {
                "title": "Missing Persons Report",
                "message": msg,
                "type": "missing_person",
                "alertId": res.id
            }
            # Send using Google Cloud Messaging (more accurately Firebase)
            notifier = NotificationManager()
            notify_res = notifier.notifyByTopic("missing_person_{0}".format(data['countryid']), rec)
            if notify_res:
                log.info("Notification for missing persons sent correctly")
            else:
                log.error("Unable to send missing persons notification")
        except Exception as e :
            log.error("Unable to send notification:{0}".format(e))


# http://cornice.readthedocs.io/en/latest/resources.html
@resource(collection_path='/api/missingpersons/fields', path='/api/missingpersons/fields/{id}', description="Damage Reporting")
class MSReportFieldHandler(BaseHandler):
    def __init__(self, request, context=None):
        BaseHandler.__init__(self, request, context)
    
    def _get_fields(self):
        return self._create_empty_model().getRequiredFields()

    def _dict_to_model(self, data, rec_id=0):
        # If id is 0, we want to create a new entry
        if rec_id == 0:
            src = self._create_empty_model()
        else:
            src = DBSession.query(self._target_class).get(rec_id)
        src.name = data['name']
        return src

    def _create_empty_model(self):
        return MSReportField("")

    def _get_order_field(self):
        return MSReportField.name

    def _get_target_class(self):
        return MSReportField