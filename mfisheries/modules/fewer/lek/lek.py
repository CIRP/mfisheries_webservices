from sqlalchemy import (
    Column,
    Integer,
    String,
    TIMESTAMP,
    ForeignKey,
    TEXT
)
from sqlalchemy.orm import relationship
from datetime import datetime
from mfisheries.models import Base


class LEK(Base):
    """SQLAlchemy declarative model class for a LEK object"""
    __tablename__ = "lek"
    id = Column(Integer, primary_key=True)
    text = Column(TEXT)
    userid = Column(Integer, ForeignKey('user.id'), nullable=False)
    countryid = Column(Integer, ForeignKey('country.id'))
    latitude = Column(String(20))
    longitude = Column(String(20))
    filepath = Column(String(200))
    filetype = Column(String(10))
    aDate = Column(TIMESTAMP)
    category = Column(String(30))
    isPublic = Column(Integer, default=1)
    timestamp = Column(TIMESTAMP, default=datetime.now)
    
    isSpecific = Column(String(5))

    country = relationship("Country", foreign_keys=[countryid])
    user = relationship("User", foreign_keys=[userid], lazy="subquery")

    def __init__(self, userid, filepath, filetype, text, latitude, longitude, isSpecific, aDate, category, countryid):
        self.userid = userid
        self.filepath = filepath
        self.filetype = filetype
        self.text = text
        self.category = category
        self.latitude = latitude
        self.longitude = longitude
        self.isSpecific = isSpecific
        self.aDate = aDate
        self.countryid = countryid

    def toJSON(self):
        rec = {
            'id': self.id,
            'userid': self.userid,
            'filepath': self.filepath,
            'filetype': self.filetype,
            'category': self.category,
            'text': self.text,
            'latitude': self.latitude,
            'longitude': self.longitude,
            'isSpecific': str(self.isSpecific),
            'aDate': str(self.aDate),
            'timestamp': str(self.timestamp),
            'countryid': self.countryid,
            'isPublic': str(self.isPublic)
        }

        if self.country:
            rec['country']= self.country.name

        if self.user:
            rec["fullname"] = "{0} {1}".format(self.user.fname, self.user.lname)
            rec["user"] = self.user.username

        return rec

    def getRequiredFields(self):
        return [
            "userid",
            # 'countryid',
            # 'filetype',
            # 'filepath',
            # "text",
            # 'category',
            # 'latitude',
            # 'longitude',
            # 'aDate'
        ]