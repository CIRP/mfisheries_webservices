from cornice.resource import resource
import shapefile
import simplekml
import os
import uuid
from datetime import datetime
from mfisheries.models import DBSession
from sqlalchemy import between

from .lek import LEK

import logging
log = logging.getLogger(__name__)

# Request to process the request. Parameters expected start=dd-mm-yyyy and end=dd-mm-yyyy and type=[shape|kml]
@resource(collection_path='/api/lek/data/export', path='/api/damagereport//data/export/{id}', description="Damage Reporting", cors_origins=('*',),
          cors_max_age=3600 )
class LEKReportExportHandler():

	def __init__(self, request, context=None):
		self.request = request
		self.context = context

	def collection_get(self):
		try:
			# retrieve data based on the dates requested from the user
			start_date = self.request.GET['start'] if'start' in self.request.GET else None
			end_date = self.request.GET['end'] if'end' in self.request.GET else None
			records = self.retrieve_dmgrpt_records(start_date, end_date)
			# Process data based on specification
			data_type = self.request.GET['type'] if'type' in self.request.GET else "shape"
			log.debug(data_type)
			res = None
			if data_type == "shape":
				res = self.format_as_shapefile(records)
			elif data_type == "kml":
				res = self.format_as_kml(records)

			if res:
				self.request.response.status = 200
			else:
				self.request.response.status = 400
			return res
		except Exception as e :
			self.request.response.status = 500
			log.error(e)
			return False

	# noinspection PyMethodMayBeStatic
	def retrieve_dmgrpt_records(self, start_date = None, end_date = None):
		start_date = datetime.strptime(start_date, "%d-%m-%Y") if start_date is not None else None
		end_date = datetime.strptime(end_date, "%d-%m-%Y") if end_date is not None else None
		log.debug("Start date was found at {0} and end date was found as {1}".format(start_date, end_date))
		query = DBSession.query().filter(LEK.isPublic == 1)
		if start_date and end_date is None:
			log.debug("Using only the start date")
			query = query.filter(LEK.timestamp > start_date)
		elif end_date and start_date is None:
			log.debug("Using only end date")
			query = query.filter(LEK.timestamp < end_date)
		elif start_date and end_date:
			log.debug("Using both start date and end date")
			query = query.filter(between(LEK.timestamp, start_date, end_date))
		# Retrieve the records based on the query specified from parameters
		records = query.all()
		log.debug("Found {0} damage reporting records".format(len(records)))
		return records

	# noinspection PyMethodMayBeStatic
	def format_as_shapefile(self, records):
		shapefile_path = "{0}/../../../static/export".format(os.path.dirname(os.path.realpath(__file__)))
		print("Location of path is: {0}".format(os.path.abspath(shapefile_path)))
		shapefile_path = os.path.abspath(shapefile_path)

		log.debug("Requesting information for the shapefile")
		# Generate a uuid for this request
		uuid_path = uuid.uuid4().hex
		path = os.path.join(shapefile_path, "{0}".format(uuid_path))

		for rec in records:
			# w = shapefile.Writer(shapeType=shapefile.POINT)
			if rec.latitude and rec.longitude:
				w = shapefile.Writer()
				w.field('name', 'C')
				# w.field('description', rec.description)
				log.debug("({0}, {1}".format(rec.longitude, rec.latitude))
				w.point(float(rec.longitude), float(rec.latitude)) # (X, Y)
				w.record(rec.name)
				w.save(os.path.join(path, "{0}".format(rec.id)))

		res = {
			"import": {
				"targetWorkshop": {
					"workspace": {"name": "FEWER-LEK"}
				},
				"data": {
					"type": "remote",
					"location": path
				}
			}
		}

		return res

	def format_as_kml(self, records):
		# log.debug("Requesting information for the kml")
		kml = simplekml.Kml()
		for rec in records:
			kml.newpoint(name=rec.name, description=rec.description, coords=[(rec.longitude, rec.latitude)])
		# log.debug("The KML produced the results:{0}".format(kml.kml()))
		self.request.response.content_type = "text/xml"
		return kml.kml()