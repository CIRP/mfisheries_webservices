import imp
import os
import shutil

import logging
log = logging.getLogger(__name__)

from cornice import Service  # allows the services to be exposed via a RESTFUL service
from pyramid.config import Configurator

from mfisheries import APP_ROOT
from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor
from mfisheries.util.secure import validate

weatherExtractor = Service(name='post', path='/api/weather/extractor', description='Adds a new extractor file')

ab_module_path = os.path.join(APP_ROOT, "")
rel_module_path = os.path.join("", "")


@weatherExtractor.post()
def upload_extractor(request):
    poolSvr = False
    if not validate(request):
        request.response.status = 401
        return {'status': 401, 'data': []}
    try: # check parameters
        filename = request.POST['file'].filename
        upfile = request.POST['file'].file
        country = request.POST['country']
        if 'pool' in request.POST:
            poolSvr = request.POST['pool']
        # Access the needed file to ensure we have the correct vale
        log.debug("Country name was received as :{0}".format(country))
    except Exception as e:
        log.error(e)
        request.response.status = 400
        return {'status': 400, "response": "Invalid request. Ensure all required fields are submitted"}

    try:
        log.debug("All parameters for uploading extractor received")
        # All parameters received
        config = Configurator()
        extension = filename.split(".")
        if len(extension) > 1:
            if extension[-1] == "py":

                code_path = os.path.join(ab_module_path, "modules/weather/parsers/extractors")
                local_path = os.path.join(rel_module_path, "modules/weather/parsers/extractors")
                log.debug("Code Path: {0}, Local Path: {1}".format(code_path, local_path))

                # Ensure country path exists
                if not os.path.exists(code_path):
                    os.makedirs(code_path)

                # Add country to the file name to prevent conflicts
                filename = "{0}_{1}".format(country.lower().replace(" ", "_"), filename.lower())
                log.debug("Extract file name is: {0}".format(filename))

                # Set full path for file
                file_path = os.path.join(code_path, filename)
                local_path = os.path.join(local_path, filename)
                log.debug("Local path {0}, Absolute path {1}".format(local_path, file_path))

                # Create a temporary location for uploading the file
                temp_file_path = file_path + '~'
                upfile.seek(0)

                # Upload file to temp location
                with open(temp_file_path, 'wb') as output_file:
                    shutil.copyfileobj(upfile, output_file)
                # Once upload complete then copy to desired location
                os.rename(temp_file_path, file_path)

                log.debug("Extractor saved at : {0}".format(file_path))
                # Dynamically importing a file using path -> https://stackoverflow.com/questions/67631/how-to-import-a-module-given-the-full-path
                config.scan('mfisheries.modules.fewer.weather.parsers.extractors')
                extractor = imp.load_source('mfisheries.modules.fewer.weather.parsers.{0}'.format(filename), file_path)

                # TODO - Weather - Check to ensure it has an extractor
                extractor_instance = extractor.Extractor()
                # Check to ensure that the uploaded file is a child of the desired class
                if isinstance(extractor_instance, WeatherSourceExtractor):
                    fields = extractor_instance.get_reading_types()
                    rec =  {
                        'status': 200,
                        "response": "File Upload Success",
                        "extractor_path": file_path,
                        "fields": fields
                    }

                    if poolSvr:
                        extractor_instance.extract()
                        readings = extractor_instance.toJSON()
                        rec["readings"] = readings

                    return rec
                else: # Invalid file submitted. Remove file and send appropriate message to user
                    os.remove(file_path)
                    config.scan('mfisheries.modules.fewer.weather.parsers.extractors')
                    request.response.status = 400
                    return {
                        'status': 400,
                        "response": "Incorrect Python Class upload in extractor file. Ensure file meets the required specifications and try again"
                    }
        config.scan('mfisheries.modules.fewer.weather.parsers.extractors')
        request.response.status = 400
        return {'status': 400, "response": "Incorrect file type"}
    except Exception as e:
        log.error(e)
        request.response.status = 400
        return {'status': 400, "response": "Invalid request. Server was unable to save extractor"}


@weatherExtractor.get()
def retrieve_extractor(request):
    if not validate(request):
        request.response.status = 401
        return {'status': 401, 'data': []}
    try:
        config = Configurator()
        codepath = request.GET['codepath']
        log.debug("File name is: {0}".format(codepath))
        filename = codepath.split('/').pop().split(".py")[0]
        log.debug("Filename {0}".format(filename))
        config.scan('mfisheries.modules.fewer.weather.parsers.extractors')
        extractor = imp.load_source('mfisheries.modules.fewer.weather.parsers.{0}'.format(filename), codepath)
        # TODO - Weather - Check to ensure it has an extractor
        extractor_instance = extractor.Extractor()
        # Check to ensure that the uploaded file is a child of the desired class
        if isinstance(extractor_instance, WeatherSourceExtractor):
            fields = extractor_instance.get_reading_types()
            rec = {"fields": fields}
            return rec

    except Exception as e:
        log.error("Unable to retrieve details of extractor: {0}".format(e))
    request.response.status = 400
    return {'status': 400, "response": "Invalid request. Server was unable to retrieve extractor"}


def delete_extractor_by_path(full_path):
    if os.path.exists(full_path):
        os.remove(full_path)
