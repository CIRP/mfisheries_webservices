import datetime
import logging
log = logging.getLogger(__name__)

from cornice.resource import resource  # allows the services to be exposed via a RESTFUL service
from mfisheries.models import DBSession
from mfisheries.modules.fewer.weather.weatheremail import WeatherEmail


@resource(collection_path='/api/weather/email', path='/api/weather/email/{id}', description="Weather email data")
class WeatherEmailsHandler(object):
    def __init__(self, request, context=None):
        self.request = request
        self.context = context

    def __save(self, data, ws_id=0):
        try:
            # If id is 0, we want to create a new entry
            if ws_id == 0:
                src = WeatherEmail()  # Create an empty WS
            # If id is not 0, then we want to update existing record
            else:
                src = DBSession.query(WeatherEmail).filter(WeatherEmail.id ==ws_id).first()
    
            # Add fields to the Weather Source object
            src.contactperson = data['contactperson']
            src.countryid = data['countryid']
            src.email = data['email']
            # Save the operation and return the object
            DBSession.add(src)
            DBSession.flush()
            return src
        except Exception as e:
            log.error("Unable to save Contact: " + str(e))
            return False

    def collection_get(self):
        # retrieve all weather data
        res = DBSession.query(WeatherEmail).all()
        if len(res) > 0:
            res = map(lambda m: m.toJSON(), res)
        return res

    def get(self):
        # Extract id passed as a parameter within the URL
        ws_id = int(self.request.matchdict['id'])
        # Attempt to Retrieve record from database
        res = DBSession.query(WeatherEmail).filter(WeatherEmail.id == ws_id).first()
        # If we successfully receive a record with the id specified
        if res:
            return res.toJSON()
        # Send a HTTP Status code to tell client id was not found
        self.request.response.status = 404
        return False

    def delete(self):
        # Extract id passed as a parameter within the URL
        ws_id = int(self.request.matchdict['id'])
        try:
            # Delete the weather
            DBSession.query(WeatherEmail).filter(WeatherEmail.id == ws_id).delete()
            # Retrieve the record so we can delete the corresponding extractor file
          
            return True
        except Exception as e:
            log.error(e.message)
            self.request.response.status = 400
            return False

    def put(self):
        # Extract data to be updated from the body of the put request
        data = self.request.json_body
        # Extract id passed as a parameter within the URL
        ws_id = int(self.request.matchdict['id'])
        src = self.__save(data, ws_id)
        if src:
            return src.toJSON()
       
        self.request.response.status = 400
        return False

    def collection_post(self):
        # Extract data to be updated from the body of the post request
        data = self.request.json_body

        src = self.__save(data)
        # If source was successfully saved
        if src:
            # Notify the client using the appropriate HTTP status code
            self.request.response.status = 201
            # Send the record
            return src.toJSON()
        

        self.request.response.status = 400
        return False
