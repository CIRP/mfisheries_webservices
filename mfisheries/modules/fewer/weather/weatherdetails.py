import datetime
import logging

from sqlalchemy import (
    Column,
    Integer,
    String,
    TIMESTAMP
)

from mfisheries.models import Base

log = logging.getLogger(__name__)


class WeatherDetails(Base):
    __tablename__ = "weatherdetails"
    id = Column(Integer, primary_key=True)
    source = Column(String(30))
    country = Column(String(30), nullable=False)
    infotype = Column(String(30))
    interval = Column(String(100), nullable=False)
    sourcetype = Column(String(1000))
    created = Column(TIMESTAMP, default=datetime.datetime.utcnow)

    def __init__(self, source, country, cat, url, code):
        self.source = source
        self.country = country
        self.infotype = cat
        self.interval = url
        self.sourcetype = code

    def toJSON(self):
        return {
            'id': self.id,
            'source': self.source,
            'country': self.country,
            'infotype': self.infotype,
            'interval': self.interval,
            'sourcetype': self.sourcetype,
            'created': str(self.created)
        }
