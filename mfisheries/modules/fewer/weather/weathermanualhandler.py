from cornice.resource import resource  # allows the services to be exposed via a RESTFUL service
import logging
log = logging.getLogger(__name__)
from mfisheries.modules.fewer.weather.parsers import factory
from pyramid.config import Configurator


@resource(collection_path='/api/entry/manualrun', path='/api/entry/manualrun/{object}', description="Weather entry data")
class WeatherManualHandler(object):
    def __init__(self, request, context=None):
        self.request = request
        self.context = context

    def collection_post(self):
        # Extract data to be updated from the body of the post request
        data = self.request.json_body
        try:
            config = Configurator()
            config.scan('mfisheries.modules.fewer.weather.parsers.extractors')
            # TODO Should probably not do this, but use record id instead
            filename = data['codepath'].split('/').pop().split(".py")[0]
            my_class = factory.extractor_factory(filename, code_path= data['codepath'])
            if my_class is None:
                raise Exception("Invalid factory specified: " + str(filename))
            if my_class.save(data['id']):
                return True
            return False
        except Exception as e:
            log.error(e)
            self.request.response.status = 400
        return False

