from mfisheries.modules.fewer.weather.weatheremail import WeatherEmail
from mfisheries.models import DBSession
import logging
import smtplib


log = logging.getLogger(__name__)
gmail_user = "testing1868@gmail.com"
gmail_pwd = "testing123"


def send_email(body, country,filename,recipient=None, subject="Extractor file error"):
    if recipient is None:
        recipient = []
    # Added for testing
    res = DBSession.query(WeatherEmail).all()
    if len(res) > 0:  # convert each record received to JSON format
            res = map(lambda m: m.toJSON(), res)

    for i in range(0,len(res)):
        if res[i]['country'].lower().replace(" ","") == country:
            recipient.append(res[i]['email'])

    fromEmail = gmail_user
    toEmails = recipient if type(recipient) is list else [recipient]
    messageBody ="Message from mfisheries automated emailing system\nError" + body

    # Prepare actual message
    message = """\From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (fromEmail, ", ".join(toEmails), subject, messageBody)
    message += "\nFilename: " + filename
    log.info("Attempting to send: " + message)

    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.login(gmail_user, gmail_pwd)
        server.sendmail(fromEmail, toEmails, message)
        server.close()
        # print('successfully sent the mail')
        log.info("Email {0} was successfully sent to {1} recipients".format(subject, len(recipient)))
    except Exception, e:
        log.error("Unable to send email: " + str(e))
