import logging
import sys
from pyramid.paster import get_appsettings, setup_logging
from sqlalchemy import engine_from_config

FREQUENCY = {
	"every 6 hours": 6,
	"every hour": 1,
	"once a day": 24
}


def run_weather_task(argv=sys.argv):
	"""
	This function will check all the weather sources to see which sources should be executed based on the last reading
	The function will:
	1. Retrieve the database record for the source
	2. Determine if time for update is required by the source
	3. If update is required:
		4. Initialize the extractor based on the file path specified
		5. If extractor exists and initialized successfully, attempts to retrieve readings
		5. If the readings retrieved, then it will attempt to use the JSON representation of the value and save to the database
	Implemented this way due to the repetition of the initialization of the services based on the number of threads the WSGI http server creates
	:return:
	"""
	# Retrieve parameters and setup environment
	if len(argv) < 2:
		print("Operation not yet supported, require the path to the ini configuration file")
		return

	from mfisheries.models import DBSession, Base
	config_uri = argv[1]
	# print("In run_weather_task received the config_uri as {0}".format(config_uri))
	setup_logging(config_uri)
	log = logging.getLogger(__name__)
	settings = get_appsettings(config_uri)

	engine = engine_from_config(settings, 'sqlalchemy.')
	DBSession.configure(bind=engine)
	Base.metadata.bind = engine
	log.info("Completed the configuration for Running mFisheries via CLI")

	# Attempting to initialise models to solve errors with relationships
	from mfisheries import load_local_modules, load_system_modules
	from pyramid.config import Configurator
	config = Configurator(settings=settings)
	config = load_system_modules(config)
	load_local_modules(config)

	import datetime
	# Start of primary Logic

	# 1 - Retrieve the database record for the source
	from mfisheries.modules.fewer.weather import WeatherSource
	sources = DBSession.query(WeatherSource).all()
	for entry in sources:
		try:
			log.info("Extractor {0} will be executed every {1}, current is {2}".format(entry.name, entry.frequency, entry.lastexecuted))
			# 2 - Determine if time for update is required by the source
			toBeUpdated = True
			loop = 24
			if entry.frequency.lower() in FREQUENCY:
				loop = FREQUENCY[entry.frequency.lower()]
			# log.info("Loop was detected as {0}".format(loop))

			if entry.lastexecuted:
				delta = datetime.datetime.utcnow() - entry.lastexecuted
				log.info("Now: {0} \t Last Executed: {1} \t Delta: {2}".format(datetime.datetime.utcnow(), entry.lastexecuted, delta))
				diff_hours = delta.total_seconds()//3600
				log.info("The time {0} is different from the current time by {1} hours and is greater than loop as {2}".format(entry.lastexecuted, diff_hours,  diff_hours > loop  ))
				toBeUpdated = diff_hours > loop
			log.info("To Be Updated was found to be: {0}".format(toBeUpdated))

			# 4 - Initialize the extractor based on the file path specified
			if toBeUpdated:
				from mfisheries.modules.fewer.weather.parsers import factory
				filename = entry.codepath.split('/').pop().split(".py")[0]
				my_class = factory.extractor_factory(filename)
				res = my_class.save(sourceid=entry.id)
				if res:
					log.debug("Updated source with a reading")
				else:
					log.error("Unable to update source")
		except Exception as e:
			log.error("Error occurred: {0}".format(e))
			DBSession.rollback()


def run_setup_weather_scheduled_task():
	from apscheduler.schedulers.background import BackgroundScheduler
	from mfisheries.models import DBSession
	from mfisheries.modules.fewer.weather import WeatherSource
	from mfisheries.modules.fewer.weather.parsers import factory
	log = logging.getLogger(__name__)

	sources = DBSession.query(WeatherSource).all()
	sched = BackgroundScheduler()
	for entry in sources:
		try:
			filename = entry.codepath.split('/').pop().split(".py")[0]
			loop = FREQUENCY[entry.frequency.lower()]
			src_id = entry.id
			# ("{0}-{1}-{2}".format(filename, loop, src_id))
			my_class = factory.extractor_factory(filename)
			# log.debug(my_class.extract())
			job = sched.add_job(my_class.save, 'interval', hours=loop, kwargs={'sourceid': src_id})
			log.info(job)
		except Exception, e:
			log.error(e)

	sched.start()
	log.info("Extractors Scheduled Successfully")