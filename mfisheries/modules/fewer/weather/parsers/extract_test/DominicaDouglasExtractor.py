try:
	import json
	import re
	
	import requests
	from bs4 import BeautifulSoup
	
	from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor
except ImportError, e:
	raise ImportError(e)


class Extractor(WeatherSourceExtractor):
	def __init__(self):
		WeatherSourceExtractor.__init__(
			self,
			"http://www.weather.gov.dm/current-conditions"
		)
	
	def get_poster_url(self):
		return self.post_url
	
	def get_extractor_url(self):
		return self.extractor_url
	
	def get_reading_types(self):
		return {
			"temperature": {
				"type": "numerical",
				"unit": "C"
			},  # TODO Need a cross platform way to represent degree symbol
			"wind": {
				"type": "text",
				"unit": "km/h"
			},
			"pressure": {
				"type": "numerical",
				"unit": "mb"
			},
			"humidity": {
				"type": "numerical",
				"unit": "%"
			}
		}
	
	def extract(self):
		self.readings = {}
		r = requests.get(self.extractor_url)
		soup = BeautifulSoup(r.content, "html.parser")
		temp = []
		for row in soup.find_all("span", {"class": "curr_temp"}):
			temp.append(row.text.split("oC")[0])
		data = []
		table = soup.find_all('div', attrs={'class': 'curr_cond_cf_no_bg'})
		for x in table:
			for a in x.find_all('p'):
				data.append(json.loads(json.dumps(a.text.split(":")[1])))
		winds = {}
		country = "Dominica - Douglas-Charles Airport"
		temperature = temp[1]
		humidity = data[5]
		pressure = data[6]
		winds['direction'] = data[4].split(" /")[0]
		winds['speed'] = data[4].split(" /")[1]
		
		self.readings = {
			"temperature": temperature,
			"humidity": humidity,
			"pressure": pressure,
			"wind": winds
		}
		# print data[4]
		
		return self.readings
	
	def toJSON(self):
		return json.dumps(self.readings)


if __name__ == "__main__":
	country = Extractor()
	print country.extract()
# print country.toJSON()
