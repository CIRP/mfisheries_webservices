import json

import requests

from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
	def __init__(self):
		WeatherSourceExtractor.__init__(
			self,
			"https://www.worldtides.info/api?extremes&lat=11.278979&lon=-60.577063&key=" + self.api_key
		)
	
	def get_poster_url(self):
		return self.post_url
	
	def get_extractor_url(self):
		return self.extractor_url
	
	def get_reading_types(self):
		return {
			
		}
	
	def extract(self):
		items = []
		forecast = []
		r = requests.get(self.extractor_url)
		self.readings['readings'] = r.json()['extremes']
		return self.readings
	
	def toJSON(self):
		return json.dumps(self.readings)


# Can be executed directly using the command python -m mfisheries.modules.fewer.weather.parsers.GrenadaCurrentExtractor
if __name__ == "__main__":
	country = Extractor()
	print country.extract()
	print country.toJSON()
# print country.get_reading_types()
# print country.save(3)
