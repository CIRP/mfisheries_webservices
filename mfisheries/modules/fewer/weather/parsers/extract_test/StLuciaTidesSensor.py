import json

import requests

from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
	def __init__(self):
		WeatherSourceExtractor.__init__(
			self,
			"https://tidesandcurrents.noaa.gov/api/datagetter?product=predictions&application=NOS.COOPS.TAC.WL&begin_date=20170714&end_date=20170715&datum=MLLW&station=TEC4775&time_zone=lst_ldt&units=english&interval=hilo&format=json"
		)
	
	def get_poster_url(self):
		return self.post_url
	
	def get_extractor_url(self):
		return self.extractor_url
	
	def get_reading_types(self):
		return {
			"High Tide": {
				"type": "numerical",
				"unit": "ft"
			},  # TODO Need a cross platform way to represent degree symbol
			"Low Tide": {
				"type": "numerical",
				"unit": "ft"
			}
			
		}
	
	def extract(self):
		self.readings['type'] = []
		self.readings['date'] = []
		self.readings['tide'] = []
		r = requests.get(self.extractor_url)
		for sensor in r.json()['predictions']:
			self.readings['type'].append(sensor['type'])
			self.readings['date'].append(sensor['t'])
			self.readings['tide'].append(sensor['v'])
		
		return self.readings
	
	def toJSON(self):
		return json.dumps(self.readings)


if __name__ == "__main__":
	country = Extractor()
	print country.extract()
	country.save(3)
