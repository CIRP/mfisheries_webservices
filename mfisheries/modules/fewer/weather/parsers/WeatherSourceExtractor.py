import datetime
import json
import logging
import transaction
import re
from abc import ABCMeta, abstractmethod

from mfisheries.models import DBSession
from mfisheries.modules.fewer.weather.threshold.threshold import Threshold
from mfisheries.modules.fewer.weather.threshold.weatheralert import WeatherAlert
from mfisheries.modules.fewer.weather.weatherreading import WeatherReadings
from mfisheries.modules.fewer.weather.weathersource import WeatherSource

log = logging.getLogger(__name__)


# https://www.toptal.com/python/python-class-attributes-an-overly-thorough-guide
class WeatherSourceExtractor(object):
	__metaclass__ = ABCMeta
	# Not needed anymore but have to remove method from all extractors
	post_url = ' '
	extractor_url = ""
	readings = {}
	# TODO change api_key from https://www.worldtides.info with account made for mfisheries
	# To manage api calls better for tides api
	api_key = "1139b6d8-e234-42d0-b93b-a61b647357fc"
	
	def __init__(self, extract=""):
		self.readings = {}
		self.extractor_url = extract
	
	@abstractmethod
	def get_extractor_url(self):
		pass
	
	@abstractmethod
	def toJSON(self):
		pass
	
	@abstractmethod
	def get_reading_types(self):
		pass
	
	@abstractmethod
	def extract(self):
		return {}
	
	def save(self, sourceid=0):
		log.info("Attempting to save an extracted weather reading for source: {0}".format(sourceid))
		try:
			data = self.extract()
			# If extension developer did not assign extracted values to class property
			if data and len(list(self.readings.keys())) < 1:
				self.readings = data

			log.debug("We retrieved the data as expected: Data has {0} dimensions of data".format(len(list(self.readings.keys()))))
			# Update the readings with the source that provided the data
			self.readings.update({"sourceid": sourceid}) # Update method of dict - https://www.tutorialspoint.com/python/dictionary_update.htm

			alert_res = self.weather_alert(sourceid) # Adds data to table to determine if notification to be sent
			status = self.__save(self.readings, sourceid)
			return status
		except Exception as e:
			log.error("Unable to save weather alert: {0}".format(e))
			return False

	def __save(self, data, w_src_id=0):
		try:
			# Check to see if
			weather_readings = DBSession.query(WeatherReadings).filter(WeatherReadings.sourceid == w_src_id).first()
			if weather_readings is None:
				log.debug("No sourceid specified. Creating a new Weather Reading")
				weather_readings = WeatherReadings("")  # Create an empty WS
			
			# Find the source corresponding to the record received
			weather_source = DBSession.query(WeatherSource).filter(WeatherSource.id == w_src_id).first()
			if weather_source:
				weather_source.lastexecuted = datetime.datetime.utcnow()
				DBSession.add(weather_source)
				DBSession.flush()
				log.debug("Session updated to time: {0}".format(weather_source.lastexecuted))
			else:
				raise Exception("Invalid source id supplied. No record found for: " + str(w_src_id))

			if weather_readings:
				weather_readings.timecreated = datetime.datetime.utcnow()
				# Add fields to the Weather Source object
				weather_readings.sourceid = data['sourceid']
				weather_readings.readings = str(data)
				DBSession.add(weather_readings)
				DBSession.flush()
			else:
				raise Exception("No Weather readings for source: " + str(data['sourceid']))

			transaction.commit()
			return weather_readings
		except Exception as e:
			DBSession.rollback()
			log.error("Unable to save Readings for source {0}. Generated error: {1}".format(w_src_id, e.message))
			return False

	def clean_text(self, data):
		try:
			data = data.replace('\n', ' ').replace('\r', ' ')
		except Exception as e:
			log.error(e)

		try:
			data = data.replace('\xa0', " ")
		except Exception as e:
			log.error(e)

		return data

	def findword(self, word,array):
		"""
		Accepts array of paragraphs and returns which paragraph text is found in
		"""
		items = []
		for data in array:
			if word in data.lower():
				items.append(data)
		return items

	def findwordList(self, word, array):
		save = ""
		for data in array:
			if word in data.lower():
				save = data
		return save

	def wordsize(self, stuff, word):
		string = ""
		pos = stuff.find(word) + len(word)
		while not (stuff[pos].isupper() and stuff[pos - 1].islower()):
			string += stuff[pos]
			pos += 1
		return string

	def comparison(self, newalert, oldalert):
		return set(newalert) == set(json.loads(oldalert))

	def weather_alert(self, sourceid):
		try:
			emergency = None
			warning = None
			# saving for the weather alerts
			query = DBSession.query(Threshold)
			res = query.filter(Threshold.weathersourceid == sourceid).first()
			if res:
				log.debug("Found record for sourceid: " + str(sourceid))
				thresholds = res.getThresholds()
				if thresholds or len(list(thresholds.keys())) > 2:
					log.debug("Threshold has values for emergency and warning")
					if "emergency" in thresholds:
						emergency = thresholds['emergency']
					if "warning" in thresholds:
						warning = thresholds['warnings']

					# If alert for source id exist
					count_alert = DBSession.query(WeatherAlert).filter(WeatherAlert.weathersourceid == sourceid).count()
					if count_alert:
						alert = DBSession.query(WeatherAlert).filter(WeatherAlert.weathersourceid == sourceid).first()
						log.debug("Alert for sourceid {0} previous existed with an id {1}".format(sourceid, alert.id))
					else:
						alert = WeatherAlert(sourceid, [])
						log.debug("Alter created for sourceid {0} with a record if of {1}".format(sourceid, alert.id))

					values = []

					# Finding all values triggered
					if emergency:
						for key in emergency:
							if emergency[key] < int(re.sub("[^0-9]", "", self.readings[key])):
								if key not in values:
									values.append(key)

					if warning:
						for key in warning:
							if warning[key] < int(re.sub("[^0-9]", "", self.readings[key])):
								if key not in values:
									values.append(key)


					equals = self.comparison(values, alert.readings)
					if len(values) > 0 and (alert.created.date() != datetime.date.today() or not equals):
						alert.readings = json.dumps(values)
						alert.sent = 0
						alert.created = datetime.datetime.utcnow()
						DBSession.add(alert)
						DBSession.flush()

					return alert

				else:
					log.debug("Record exist, but invalid data for threshold with sourceid: " + str(sourceid))
			else:
				log.debug("Unable to find record for source with id: " + str(sourceid))
		except Exception as e:
			log.error("Error occurred while attempting to determine if emergency or warning exists: " + str(e))
		return False