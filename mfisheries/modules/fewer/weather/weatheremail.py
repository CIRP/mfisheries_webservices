import logging
from mfisheries.models import Base
from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey
)
from sqlalchemy.orm import relationship

log = logging.getLogger(__name__)


class WeatherEmail(Base):
    __tablename__ = "weatheremail"
    id = Column(Integer, primary_key=True)
    email = Column(String(100),nullable=False)
    contactperson = Column(String(100),nullable=False)
    countryid = Column(Integer, ForeignKey('country.id'))
    country = relationship("Country", foreign_keys=[countryid], lazy='subquery')

    def __init__(self,cid="", email="", contactperson=""):
        self.countryid = cid
        self.email = email
        self.contactperson = contactperson

    def toJSON(self):
        rec = {
            'id': self.id,
            'countryid': self.countryid,
            'email': self.email,
            'contactperson': self.contactperson
        }
        if self.country:
            rec['country'] = self.country.name
        return rec
