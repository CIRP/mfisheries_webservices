import datetime
import logging

log = logging.getLogger(__name__)

from cornice.resource import resource  # allows the services to be exposed via a RESTFUL service
from mfisheries.models import DBSession

from mfisheries.modules.fewer.weather.weatherreading import WeatherReadings
from mfisheries.modules.fewer.weather.weathersource import WeatherSource
from mfisheries.modules.fewer.weather.weatherextractorhandler import delete_extractor_by_path
# from mfisheries.util import display_debug
from sqlalchemy import func
from mfisheries.util.notificationManager import NotificationManager

@resource(collection_path='/api/entry/weather', path='/api/entry/weather/{id}', description="Weather entry data")
class WeatherDetailsHandler(object):
    def __init__(self, request, context=None):
        self.request = request
        self.context = context

    def __save(self, data, ws_id=0):
        try:
            # If id is 0, we want to create a new entry
            if ws_id == 0:
                src = WeatherReadings("")  # Create an empty WS
            # If id is not 0, then we want to update existing record
            else:
                src = DBSession.query(WeatherReadings).filter(WeatherReadings.sourceid == data['sourceid']).first()

            source = DBSession.query(WeatherSource).filter(WeatherSource.id == data['sourceid']).first()
            source.lastexecuted = datetime.datetime.utcnow()
            src.timecreated = datetime.datetime.utcnow()
    
            # Add fields to the Weather Source object
            src.sourceid = data['sourceid']
            src.readings = str(data)

            # Save the operation and return the object
            DBSession.add(src)
            DBSession.add(source)
            DBSession.flush()
            return src
        except Exception as e:
            log.error("Unable to save Source: " + e.message)
            return False

    def collection_get(self):
        # retrieve all weather data
        res = []
        try:
            query = DBSession.query(WeatherReadings)
            if len(self.request.GET) > 0:
                query = query.join(WeatherSource, WeatherReadings.source)
                log.debug("Received additional data in get request ")

                if "countryid" in self.request.GET:
                    cid = self.request.GET['countryid']
                    log.debug("Received country as: " + cid)
                    query = query.filter(WeatherSource.countryid == cid)

                if "infotype" in self.request.GET:
                    infotype = self.request.GET['infotype']
                    log.debug("Received info type as: " + infotype)
                    query = query.filter(func.lower(WeatherSource.infotype) == func.lower(infotype))

                if "primary" in self.request.GET:
                    primary = self.request.GET['primary']
                    log.debug("Received primary as: " + primary)
                    query = query.filter(WeatherSource.isPrimary == primary)

            res = query.all()
            if len(res) > 0:
                res = map(lambda m: m.toJSON(), res)
        except Exception as e :
            log.error("Error Occurred trying to retrieve readings: {0}".format(e))
            self.request.response.status = 404
        return res

    def get(self):
        try:
            # Extract id passed as a parameter within the URL
            ws_id = int(self.request.matchdict['id'])
            # Attempt to Retrieve record from database
            res = DBSession.query(WeatherReadings).filter(WeatherReadings.sourceid == ws_id).first()
            # If we successfully receive a record with the id specified
            if res:
                return res.toJSON()
        except Exception as e :
            log.error("Error Occurred trying to retrieve readings: {0}".format(e))
        # Send a HTTP Status code to tell client id was not found
        self.request.response.status = 404
        return False

    def delete(self):
        # Extract id passed as a parameter within the URL
        ws_id = int(self.request.matchdict['id'])
        try:
            # Delete the weather
            DBSession.query(WeatherReadings).filter(WeatherReadings.sourceid == ws_id).delete()
            # Retrieve the record so we can delete the corresponding extractor file
            ws = DBSession.query(WeatherSource).filter(WeatherSource.id == ws_id).one()
            # Delete the file
            delete_extractor_by_path(ws.codepath)

            return True
        except Exception as e:
            log.error(e.message)
            self.request.response.status = 400
            return False

    def put(self):
        try:
            # Extract data to be updated from the body of the put request
            data = self.request.json_body
            # Extract id passed as a parameter within the URL
            ws_id = int(self.request.matchdict['id'])
            if self.__isvalid(data):
                src = self.__save(data, ws_id)
                if src:
                    return src.toJSON()


            source = DBSession.query(WeatherSource).filter(WeatherSource.id == data['sourceid']).first()
            source.status = "Fail"
            DBSession.add(source)
            DBSession.flush()
        except Exception as e:
            log.error(e.message)

        self.request.response.status = 400
        return False

    def collection_post(self):
        try:
            # Extract data to be updated from the body of the post request
            data = self.request.json_body
            # Ensure required fields are specified
            # if self.__isvalid(data):
            # Attempt to save the new source
            if DBSession.query(WeatherReadings).filter(WeatherReadings.sourceid == data['sourceid']).count():
                src = self.__save(data, data['sourceid'])
            else:
                src = self.__save(data)
            # If source was successfully saved
            if src:
                # Notify the client using the appropriate HTTP status code
                self.request.response.status = 201
                return src.toJSON()  # Send the record

            source = DBSession.query(WeatherSource).filter(WeatherSource.id == data['sourceid']).first()
            source.status = "Fail"
            DBSession.add(source)
            DBSession.flush()
        except Exception as e:
            log.error(e.message)

        self.request.response.status = 400
        return False