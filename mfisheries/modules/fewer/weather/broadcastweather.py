import time
import logging
log = logging.getLogger(__name__)
# Firebase related imports
from firebase import firebase
from pyfcm import FCMNotification
from pyramid.config import Configurator
from pyramid.paster import bootstrap

from mfisheries.core.config import Config
from mfisheries.models import DBSession
from mfisheries.modules.fewer.weather.threshold.weatheralert import WeatherAlert
from mfisheries.modules.fewer.weather.weathersource import WeatherSource

# Expected to be executed either from the main or source script
ini_full_path = "./development.ini"


# noinspection PyMethodMayBeStatic
class WeatherBroadcastManager(object):
	def __init__(self):
		self.fb = None
		self.api_key = None
		self.enable_debug = True
		self.config = Configurator()
	
	def getFBApp(self):
		"""
		This method will utilize the system configured firebase connection string to setup the connection
		between the mFisheries system and Firebase
		"""
		if not self.fb:  # If Firebase not previously initialized
			connection_string = DBSession.query(Config).filter(Config.key == "firebase_connection_string").one()
			fb_conn_str = connection_string.value
			self.fb = firebase.FirebaseApplication(fb_conn_str, None)
			DBSession.rollback()
		return self.fb
	
	def getPushService(self):
		"""
		This method will return an instantiated FCM Notification Object.
		It will fetch the api key from the database and perform the necessary setup for enabling FB push notification services
		"""
		if not self.api_key:
			api_key = DBSession.query(Config).filter(Config.key == "firebase_api_key").one()
			self.api_key = api_key.value
			DBSession.rollback()
		return FCMNotification(self.api_key)

	def notify(self, countryid, message = None):
		push_service = self.getPushService()
		if push_service:
			data = {
				"title": "Weather Notification",
				"message": "Weather information was updated",
				"type": "weather",
				"alertId": "weather"
			}

			if message is not None:
				data['message'] = message
			topic = "weather_" + countryid
			log.debug("Sending to {0} to subscribers of {1}".format(data, topic))
			result = push_service.notify_topic_subscribers(topic_name=topic, data_message=data)
			log.debug(str(result))
		else:
			log.debug("Unable to notify subscriber. Failure to configure push service", "error")

	def checkAllSourcesForAlerts(self):
		# Retrieve the notifications made
		fb = self.getFBApp()
		alerts = self.retrieveWeatherAlerts()
		# self.notify("dominica",["pressure"])
		for alert in alerts:
			try:
				weather_source = self.getWeatherSource(alert.weathersourceid)
				if weather_source:
					# Getting country to notify
					countryid = weather_source.countryid
					self.notify(countryid)
					self.setAsSent(alert.weathersourceid)
			except Exception as e:
				log.error("Unable to retrieve data: "+e.message)
	
	def setAsSent(self, src_id):
		try:
			query = DBSession.query(WeatherAlert)
			query = query.filter(WeatherAlert.weathersourceid == src_id)
			res = query.first()
			if res is not None:
				res.sent = 1

			DBSession.add(res)
			DBSession.flush()
			return res
		except Exception as e:
			err_msg = "Unable to set {0} as send: Error {1}".format(src_id, e.message)
			log.error(err_msg)
			DBSession.rollback()
			return None
		
	def getWeatherSource(self, id):
		try:
			res = DBSession.query(WeatherSource).filter(WeatherSource.id == id).first()
			return res
		except:
			log.error("Unable to get weather source from id")
			DBSession.rollback()
			return None
	
	def retrieveWeatherAlerts(self):
		try:
			res = DBSession.query(WeatherAlert).filter(WeatherAlert.sent == 0).all()
			return res
		except:
			log.error("Unable to get weather source from id")
			DBSession.rollback()
			return None
	
	def saveStatus(self, alert_fb_id, message, countryid):
		fb = self.getFBApp()
		fb_notify_base = '/weather_alerts/notif-status/' + alert_fb_id
		log.debug("Saving weather notification status")
		users = fb.get('/users', None)
		for user in users:
			if 'countryId' in users[user]:
				if users[user]['countryId'] == countryid:
					result = fb.put(fb_notify_base + '/users/' + user, False)
					log.debug(result)
			else:
				log.debug("Unable to retrieve country of account")
		millis = int(round(time.time() * 1000))
		fb.put(fb_notify_base + '/sent', millis)
		fb.put(fb_notify_base + '/message', message)


def run_weather_broadcast(path=None, use_bootstrap=False):
	log.info("Executing Broadcast Manager")
	# Ensures that System Modules are loaded even if executed from the command line or a service
	if use_bootstrap:
		if path:
			bootstrap(path)
		else:
			bootstrap(ini_full_path)
	bc = WeatherBroadcastManager()
	bc.checkAllSourcesForAlerts()


if __name__ == "__main__":
	log.debug("Running Broadcast Manager via command line")
	# Run this main using the command:  python -m mfisheries.modules.fewer.weather.broadcastweather
	run_weather_broadcast(use_bootstrap=True)
