import datetime
import json
import logging

from sqlalchemy import (
	String,
	Column,
	Integer,
	TIMESTAMP,
	ForeignKey
)
from sqlalchemy.orm import relationship

from mfisheries.models import Base

log = logging.getLogger(__name__)


class WeatherAlert(Base):
	__tablename__ = "weatheralerts"
	id = Column(Integer, primary_key=True)
	readings = Column(String(1000), default=None)
	weathersourceid = Column(Integer, ForeignKey('weathersources.id'))
	sent = Column(Integer, default=0)
	alerttype = Column(String(100), default=None)
	created = Column(TIMESTAMP, default=datetime.datetime.utcnow)

	weathersource = relationship("WeatherSource", foreign_keys=[weathersourceid], lazy='subquery')

	def __init__(self, wsourceid, readings=[]):
		self.weathersourceid = wsourceid
		self.readings = json.dumps(readings)
		self.created = datetime.datetime.utcnow()

	def toJSON(self):
		rec = {
			'id': self.id,
			'readings': self.readings,
			'weathersourceid': self.weathersourceid,
			'sent': self.sent,
			'alerttype': self.alerttype,
			'created': str(self.created)
		}

		if self.weathersource:
			rec['weathersource'] = self.weathersource.name

		return rec
