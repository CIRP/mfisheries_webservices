import datetime
import logging
import json

from sqlalchemy import (
	String,
	Column,
	Integer,
	Text,
	TIMESTAMP,
	ForeignKey
)
from sqlalchemy.orm import relationship
from mfisheries.models import Base

log = logging.getLogger(__name__)


class Threshold(Base):
	__tablename__ = "weatherthresholds"
	id = Column(Integer, primary_key=True)
	weathersourceid = Column(Integer, ForeignKey('weathersources.id'))
	thresholds = Column(String(1000))
	created = Column(TIMESTAMP, default=datetime.datetime.utcnow)
	
	weathersource = relationship("WeatherSource", foreign_keys=[weathersourceid], lazy='subquery')
	
	def __init__(self, wsourceid, thresholds):
		self.weathersourceid = wsourceid
		self.setThresholds(thresholds)
	
	def toJSON(self):
		rec = {
			'id': self.id,
			'weathersourceid': self.weathersourceid,
			'thresholds': self.thresholds,
			'created': str(self.created)
		}

		if self.weathersource:
			rec['weathersource'] = self.weathersource.name
		return rec
	
	def getThresholds(self):
		try:
			if self.thresholds and len(self.thresholds) > 2:
				return json.loads(self.thresholds)
		except Exception:
			pass
		return {}
	
	def setThresholds(self, thresholds):
		# if data is in a dictionary then convert to json string
		if type(thresholds) == dict:
			thresholds = json.dumps(thresholds)
		self.thresholds = thresholds
