import json
import logging

import transaction
import os
import time
import cgi
import shutil

from cornice import Service
from sqlalchemy import text

from mfisheries.models import DBSession
from mfisheries.util.fileutil import store_messaging_image
# TODO - Messaging - Easy - Use the same media storage utility as LEK
from mfisheries.util.secure import validate
from mfisheries import APP_ROOT

messaging_path = os.path.join(APP_ROOT, '')
messaging_image_path = os.path.join('', '')

messaging_image_service = Service(name='messagingimageservice', path='api/add/messagingimage', description='upload new image')

@messaging_image_service.get()
def test(request):
    return {'status': 201, 'data': []}


@messaging_image_service.post()
def upload_image(request):
    if not validate(request):
        return {'status': 401, 'data': []}

    #Check request parameters
    try:
        filename = request.POST['file'].filename
        image_file = request.POST['file'].file
        userid = request.POST['userid']
    except Exception as e:
        return {'status': 400, 'response': 'Invalid Request Paramenetrs'}

    try:
            extention = filename.split('.')
            timestamp = int(time.time())
            timestamp_string = str(timestamp)

            image_path = os.path.join(messaging_path, 'static/messaging')
            local_path = os.path.join(messaging_image_path, 'static/messaging')
            if not os.path.exists(image_path):
                os.makedirs(image_path)
            filename = "{0}_{1}".format(timestamp_string, filename.lower())
            file_path = os.path.join(image_path, filename)
            local_path = os.path.join(local_path, filename)
            # print("Local Path: " + str(local_path))
            # print("Absolute Path: " + str(file_path))

            temp_file_path = file_path + '~'
            image_file.seek(0)

            with open(temp_file_path, 'wb') as output_file:
                shutil.copyfileobj(image_file, output_file)

            os.rename(temp_file_path, file_path)
            return {'status': 200, 'response': 'success', 'filename': filename}
    except Exception as e:
            # print("Error: " + e.messaging)
            return {'status': 500, 'response': 'error'}
