from pyramid.response import FileResponse
from pyramid.view import view_config

from mfisheries import APP_ROOT
# Views for each service worker required
# functionality based on => https://docs.pylonsproject.org/projects/pyramid-cookbook/en/latest/static_assets/files.html
SW_DIR = "{0}/static/serviceworkers".format(APP_ROOT)


@view_config(route_name='home', renderer='templates/index.pt')
def my_view(request):
	return {'project': 'mFisheries'}

@view_config(route_name='push.js')
def push_js_sw(request):
	response = FileResponse(
		"{0}/serviceWorker.min.js".format(SW_DIR),
		request=request,
		content_type='application/javascript'
	)
	return response

@view_config(route_name='push.fcm.js')
def push_fcm_js_sw(request):
	response = FileResponse(
		"{0}/firebase-messaging-sw.js".format(SW_DIR),
		request=request,
		content_type='application/javascript'
	)
	return response

@view_config(route_name='sw.js')
def base_service_worker(request):
	response = FileResponse(
		"{0}/sw.js".format(SW_DIR),
		request=request,
		content_type='application/javascript'
	)
	return response

@view_config(route_name='manifest.json')
def base_manifest(request):
	response = FileResponse(
		"{0}/static/manifest.json".format(APP_ROOT),
		request=request,
		content_type='application/javascript'
	)
	return response

@view_config(route_name='fb-msg-sw.js')
def push_fcm_js_sw_alt(request):
	response = FileResponse(
		"{0}/firebase-messaging-sw.js".format(SW_DIR),
		request=request,
		content_type='application/javascript'
	)
	return response