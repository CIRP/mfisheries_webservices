
class AdminReportCtrl extends BaseController{

	constructor(Country, Locator, AuthService, LocalStorage, WeatherSource, Entries, Alerts, CAPSource, CapService, CountryLoc, $scope){
		super(Country, Locator, AuthService, LocalStorage);
		this.WeatherSource = WeatherSource;
		this.Entries = Entries;
		this.Alerts = Alerts;
		this.CAPSource = CAPSource;
		this.CapService =CapService;
		this.CountryLoc = CountryLoc;
		this.$scope = $scope;

		this.primaryTable = '';
		this.init();
	}

	setInfoName() {
		this.infoName = "Weather Dashboard";
	}

	init(){
		super.init();
		$(".mFish_menu").removeClass("active");
		$("#menu-weather").addClass("active");

		// Initialize attributes needed for functionality
		this.weatherSources = {};
		this.showMet = false;
		this.weather = {};
		this.alerts = {};
		this.countries = [];
		this.countryid = 0;
		this.countryname = "";
		this.image = "sunny";
		this.imageforecast = "sunny";
		this.temperature = "";
		this.openWeather = {
			"sys": {},
			"main": {},
			"weather": {}
		};
		this.forecast = [];
		this.tides = [];
		this.readings = {};
	}

	startView(){
		if (this.debug)console.log("Starting Weather Report Controller View Process");
		// this.retrieveCountries();
		this.getLocation();
		this.startScreen();
	}

	startAdmin(){
		if (this.debug)console.log("Starting Weather Report for admin process");
	}

	startScreen(){
		// display a loading message while we request country data
		this.displayLoading("Loading Countries").then(loading =>{
			// After Displaying notification
			this.retrieveCountries(true).then(countries =>{
				// if (this.debug)console.log("%s countries retrieved", countries.length);
				loading.close();
				this.locator.getLastLocationID().then(countryid => {
					if (this.debug)console.log("Location found from cache as: " + countryid);
					if (countryid && countryid !== "undefined")this.handleCountryChange(countryid);
				}, () => {
					if (this.debug)console.log("No country was previously selected");
				});
			}, () => loading.close());
		});
		// Setup
		this.fliper_id = setInterval(this.flipper, 20000);
		this.$scope.$on('$destroy', () => { clearInterval(this.fliper_id); });
	}

	flipper(){
		if(this.showMet === false){
			document.getElementById("MET-Data").style.display = "inline";
			document.getElementById("Open-Data").style.display = "none";
			this.showMet = true;
			// console.log("Met was false");
		}else{
			document.getElementById("MET-Data").style.display = "none";
			document.getElementById("Open-Data").style.display = "inline";
			this.showMet = false;
			// console.log("Met was true");
		}
		// metSide = metSide * -1;
	}

	/**
	 * This method will be
	 * @param lat latitude of the selected country
	 * @param long longitude of the selected country
	 * @param countryid the identifier of the selected country
	 */
	loadCountryWeatherAPI(lat, long, countryid) {
		if (this.debug)
			console.log("Attempting to load details for country %s was found to be (%s, %s)",countryid, lat, long);

		const weather_api_key = "3f3b5f31bc28cbcd8c7090057cfdb89f";
		const base_weather = "https://api.openweathermap.org/data/2.5/weather?APPID=" + weather_api_key;
		let open_weather_url = base_weather + "&units=metric&lat=" + lat + "&lon=" + long;

		const forecast_api_key = "ee27a1efb7ed4a886da6fd977dc282b2";
		const base_forecast = "https://api.openweathermap.org/data/2.5/forecast/daily?APPID=" + forecast_api_key;
		let open_weather_forecast = base_forecast + "&units=metric&lat=" + lat + "&lon=" + long + "&cnt=5" ;

		// console.log("Tide: %s , Weather: %s , Forecast: %s ", tide_url, open_weather_url, open_weather_forecast);
		// retrieve current weather
		this.apiCalls(open_weather_url, "current");
		// retrieve forecast weather
		this.apiCalls(open_weather_forecast, "forecast");

		// old retrieve tide data
		// const tide_api_key = "59a668e2-1d3e-4b5e-aeb7-2b63a5df065d";
		// const base_tide = "https://www.worldtides.info/api?key=" + tide_api_key;
		// let tide_url = base_tide + "&extremes&lat=" + lat + "&lon=" + long;
		// if (this.debug)console.log(tide_url);
		// this.apiCalls(tide_url, "tides");
	}

	// API calls for the external sources of information (Openweather and Worldtides)
	apiCalls(url, state) {
		// console.log("Loading data for %s from %s", state, url);
		fetch(url)
				.then(resp => resp.json())
				.then(data=> {

					if (state === "current") { // If current data is requested
						this.openWeather.main = data.main;
						this.openWeather.sys = data.sys;
						this.openWeather.sys.sunrise = this.timeConverter(data.sys.sunrise, "current");
						this.openWeather.sys.sunset = this.timeConverter(data.sys.sunset, "current");
						this.temperature = Math.round(data.main.temp);
						this.openWeather.weather = data.weather[0];

						if (data.weather[0].main === "Rain")
							this.image = "rainy";
						else if (data.weather[0].main === "Clouds")
							this.image = "cloudy";
						else if (data.weather[0].main === "Sunny" || data.weather[0].main === "Clear")
							this.image = "sunny";
					}
					else if (state === "forecast") { // If forecast data is requested
						this.forecast = data.list;
						this.forecast.pop();
						for (let i = 0; i < (this.forecast.length); i++) {
							this.forecast[i].dt = this.timeConverter(this.forecast[i].dt, "forecast");
							this.forecast[i].temp.max = Math.round(parseFloat(this.forecast[i].temp.max));
							this.forecast[i].temp.min = Math.round(parseFloat(this.forecast[i].temp.min));
							this.forecast[i].weather = this.forecast[i].weather[0];

							if (this.forecast[i].weather.main === "Rain")
								this.forecast[i].image = "rainy";
							else if (this.forecast[i].weather.main === "Clouds")
								this.forecast[i].image = "cloudy";
							else if (this.forecast[i].weather.main === "Sunny" || this.forecast[i].weather.main === "Clear")
								this.forecast[i].image = "sunny";
						}
					}

					this.$scope.$applyAsync();
				})
				.catch((error) => {
					if (this.debug) console.error(error);
				});
	}

	// Request Tide Data
	retrieveTideRecords(countryid){
		console.log("Attempting to retrieve tide data for country" + countryid);
		this.Entries.getMainTide(countryid).then(res => {
			// console.log("Received: " + JSON.stringify(res));
			const numRecs = 8 * 2; // 7 days in the future (plus today)
			this.tides = res.data[0].readings.tides.filter(rec => rec.event.includes("Tide")).slice(0, numRecs).map(rec =>{
				rec.date_time = rec.date + " " + rec.time;
				rec.height = rec.level_imperial;
				return rec;
			});
			console.log("Filtered %s tides", this.tides.length);
		}, err =>{
			if (this.debug)console.error(err);
		});
	}

	// Changes UNIX time stamp to readable time
	timeConverter(UNIX_timestamp, state) {
		const a = new Date(UNIX_timestamp * 1000);
		const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		const days = ['Sun', 'Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat'];
		const daysFull = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		let con = "a.m";
		let day = days[a.getDay()];
		const date = a.getDate();
		let hour = a.getHours();
		const min = a.getMinutes();

		if (state === "tides") {
			if (hour > 12) { // hour = hour - 4;
				hour = hour - 12;
				con = "p.m";
			}
			return day + ' ' + date + ' ' + hour + ':' + min + ' ' + con;
		}
		// It uses GMT time zone, restructured to local time
		if (state === "current") {
			if (hour > 12) { // hour = hour - 4;
				hour = hour - 12;
				con = "p.m";
			}
			return hour + ':' + min + ' ' + con;
		}
		day = daysFull[a.getDay()];
		if (this.debug)console.log("%s - %s %s", state, day, date);
		return day + " " + date;
	}

	handleCountryChange(countryid, action) {
		if (!isNaN(countryid))countryid = parseInt(countryid);
		// persist country for later use
		this.locator.saveLastLocationID(countryid).then(res => {
			if(this.debug)console.log("Country was successfully saved:" + JSON.stringify(res));
		});
		// Attempting to add country name to be displayed based on data
		let country = this.countries.filter(country => country.id === countryid)[0];
		if (country) this.countryname = country.name;
		this.contryid = countryid;
		this.loadCountryLocation(countryid).then(countryLoc =>{
			if (this.debug)console.log("Location for country was found to be (%s, %s)",countryLoc.latitude, countryLoc.longitude);
			this.loadCountryWeatherAPI(countryLoc.latitude, countryLoc.longitude, countryid);
		}, err =>{
			swal("Weather", "Unable to retrieve Country information", "error");
			if (this.debug)console.error(err);
		});

		this.getWeather(countryid);
		this.loadAlerts(countryid);
		// retrieve tide data
		this.retrieveTideRecords(countryid);
	}

	/**
	 * Retrieves latitude and longitude from modules.
	 * Coordinates are passed to request clients via a promise
	 * @param countryid
	 * @returns {Promise}
	 */
	loadCountryLocation(countryid) {
		if (!isNaN(countryid))countryid = parseInt(countryid);
		return new Promise((resolve, reject) => {
			console.log("Loading Country Location for: " + countryid);
			this.CountryLoc.get(countryid).then(res => {
				let rec = res.data.filter(el => el.countryid === countryid)[0];
				// console.log(rec);
				if (rec)resolve(rec);
				else reject("No record for country " + countryid + " found");
			}, reject);
		});
	}

	/**
	 *
	 */
	getSourceID() {
		for (let i = 0; i < (this.weatherSources.length); i++) {
			if (this.weatherSources[i].infotype === "Weather") {
				return this.weatherSources[i].id;
			}
		}
	}

	getWeather(countryid) {
		console.log("Attempting to retrieve main weather readings for : "+countryid);
		this.Entries.getMainWeather(countryid).then((res) => {
			// console.log("Retrieved readings: "+JSON.stringify(res.data[0].readings));
			const recs = res.data[0];
			if(recs && recs.readings){
				this.readings = recs.readings;
				delete this.readings.sourceid;
			}else{
				console.log("No readings");
			}
		});
	}

	loadAlerts(countryid) {
		this.cap_loading = true;
		this.alerts = [];
		this.communityAlerts = [];
		// Request each country (if countryid not specified, we will load all countries)
		this.Country.get(countryid).then(res => {
			// Ensure we retrieve country values
			// For each country, we receive the CAP source associated with that country
			res.data.forEach(country => {
				// We retrieve the compatible CAP Source //TODO Implement logic that can accommodate all sources
				this.CAPSource.get(country.id, true).then(srcRes => {
					// For each source, we extract its URL, Pass it to the CAP service
					srcRes.data.forEach(srcEl => {
						CapService.setBaseURL(srcEl.url);
						// CAP service receives the alert based on information passed
						this.CapService.getAlerts().then((servRes) => {
							this.alerts = this.alerts.concat(servRes.data.map(el => {
								// add the country
								el.country = country.name;
								return el;
							}));

							console.log(this.alerts);
							this.cap_loading = false; // Stop loading as soon as first is retrieved and displayed
						});
					});
				});
			});
		});
		// TODO Restructuring of alerts required
		this.Alerts.get().then((res) => {
			// console.log(res);
			this.communityAlerts = this.communityAlerts.concat(res.data);
		});
	}
}

AdminReportCtrl.$inject = [
	"Country",
	"Locator",
	"AuthService",
	'LocalStorage',
	"WeatherSource",
	'Entries',
	'Alerts',
	'CAPSource',
	'CapService',
	'CountryLoc',
		'$scope'
];

angular.module('mfisheries.Weather')
		.controller("AdminReportCtrl", AdminReportCtrl);