
class WeatherCtrl extends BaseController{

	constructor(Country, Locator, AuthService, LocalStorage, fileupload, WeatherSource, Threshold, Entries, $compile, $scope){
		super(Country, Locator, AuthService, LocalStorage);

		this.fileupload = fileupload;
		this.WeatherSource = WeatherSource;
		this.Threshold = Threshold;
		this.Entries = Entries;
		this.$compile =$compile;
		this.$scope = $scope;

		this.primaryTable = 'srcTbl';
		this.primaryModal = "#weatherModal";
		this.viewModal = "#viewModal";
		this.primaryForm = "newWeatherSource";

		this.init();
	}

	init(){
		super.init();
		// Initialise Menu
		$(".mFish_menu").removeClass("active");
		$("#menu-weather").addClass("active");
		$("#menu-fewer").addClass("active");
		$("#menu-weather-report").addClass("active");

		this.type = "";
		this.readings = [];
		this.thresholds = {};
	}

	setInfoName(){
		this.infoName = "Weather";
	}

	reset(){
		this.type = "Weather";
		this.readings = {};

		this.record = {
			"name": "",
			"countryid": this.currentUser.countryid,
			"infotype": "Weather",
			"url": "",
			"frequency": "Every Hour",
			"sourcetype": "Website",
			"duration": "Single Day",
			'createdby': this.currentUser.userid,
			'isPrimary': "0"
		};

		this.extractor = undefined;
	}

	startAdmin(){
		// Update source name with common defaults
		this.sourceNames = [
			this.currentUser.country + " - MET Office",
			this.currentUser.country + "- ODM",
			"NOAA",
			"CIMH",
			"OpenWeather",
			"Weather Underground",
			"AccuWeather"
		];
		this.reset();
		this.retrieveCountries();
		this.retrieveRecords();
	}

	startView(){

	}

	retrieveRecords(countryid) {
		this.is_loading = true;
		let country = (countryid) ? countryid : this.userCountry;
		console.log("Attempting to retrieve weather sources for: " + country);
		this.WeatherSource.get(country).then(res => {
			this.records = res.data;
			console.log("Retrieved " + this.records.length + "source records");
			this.is_loading = false;
		}, err => console.error(err));
	}

	deleteRec(source) {
		swal({
			title: "Delete Confirmation",
			text: "Are you sure you want to delete this record. You will not be able to undo this operation",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Delete"
		}).then(result => {
			console.log(result);
			if (result.value) {
				this.Entries.delete(source).then(() => {
					console.log("Entries deleted for source: " + source);
					this.Threshold.delete(source).then(() => {
						console.log("Successfully deleted thresholds");
						this.WeatherSource.delete(source).then( wsRes => {
							console.log(wsRes);
							swal("Deleted", "Record was successfully deleted", "success");
							this.refresh();
						}, (err) => {
							console.log(err);
							swal("Failed", "Unable to delete record", "error");
						});
					}, (err) => {
						console.log("Unable to delete threshold records" + err);
					});
				}, (err) => {
					console.log("Unable to delete weather records" + err);
				});
			}else console.log("Request to delete record was cancelled");
		});
	}

	displayErrorFeedback(title, message) {
		if (this.debug)console.log("s - %s", title, message);

		$("#spinning_upload").hide();
		$("#weather_source_next_btn").removeAttr('disable');

		swal({
			title: title,
			type: "error",
			text: message
		});
	}

	saveWeatherSource(){
		console.log("Attempting to save weather source with the following information");
		console.dir(this.record);
		return new Promise((resolve, reject)=>{
			let response;
			if (this.isEdit) response = this.WeatherSource.update(this.record);
			else response = this.WeatherSource.add(this.record);
			// Handle the response consistently
			response.then(res =>{
				// update the source data with the id of the saved record
				this.record = res.data;
				resolve(res.data);
			}, reject);
		});
	}

	uploadExtractor() {
		const uploadUrl = '/api/weather/extractor';
		// const sendData = {country: {
		// 	'name': this.currentUser.country, 'id': this.currentUser.country
		// }};
		const sendData = {country:this.currentUser.country};
		// TODO if is save, then file is required
		const operationFunc = (this.isEdit) ? this.WeatherSource.update : this.WeatherSource.add;
		console.log(operationFunc);

		if (this.extractor) { // Ensure File is Specified
			console.log(this.extractor);
			console.log(sendData);
			// Disable button while uploading extractor
			$("#weather_source_next_btn").attr('disable', 'true');
			$("#spinning_upload").show();

			// We have the file so we attempt to upload the extractor first
			this.fileupload.uploadFileToUrl(this.extractor, sendData, uploadUrl).then((data) => {
				console.log(data);
				$("#upload_extractor").val("");
				// If Successful, then we Add the Source and Entry to the database
				if (data.data && data.data.status === 200) {
					// Save the path of the uploaded extractor to the source (Allows for management related tasks)
					this.record.codepath = data.data.extractor_path;

					let configFieldsJSON;
					if (data.data.fields) {
						console.log("Received Data Fields from server for uploaded extractor");
						configFieldsJSON = data.data.fields;
						// Display Threshold configuration based on fields received from upload process
						this.resetThresholds(configFieldsJSON);
						this.fields = configFieldsJSON;
						this.field_names = Object.keys(configFieldsJSON);
						console.log(configFieldsJSON);
					} else { // If no configuration fields provided by extractor
						console.log("No Configurable fields for extractor was specified");
						//TODO Skip straight to the end for the source
					}


					this.saveWeatherSource().then(res => {
						console.log('success uploading weather source extractor ... moving to the threshold configuration');
						// Hide spinner
						$("#spinning_upload").hide();
						// close modal for weather details
						$(this.primaryModal).modal("hide");
						// open modal for thresholds
						$("#thresholdModal").modal("show");

					}, (err) => { // Error Callback if uploading weather source fails
						this.displayErrorFeedback("Adding New Weather Source", "Unable to add new weather source. If problem persists contact support to report error");
						console.error(err);
					});


				}
				else { // The upload extractor process failed
					this.displayErrorFeedback("Uploading Extractor", "Unable to Upload Extractor File. Ensure python file follows the implementation guidelines");
				}
			});
		} else {
			if (!this.isEdit) // File is not Specified so Display message to user
				this.displayErrorFeedback("Uploading Module", "No extractor file uploaded. Add extractor and resubmit.");
			else{ // New File upload not required to update value
				this.saveWeatherSource().then(() => {
					swal("Update", "Weather Source Updated Successfully", "success");
					$(this.primaryModal).modal("hide");
					this.refresh();
					}, (err) => {
						swal("Update", "Unable to update Weather Source", "error");
						console.error(err);
				});
			}
		}
	}

	// ****** Extractor Related Functionality *****

	/**
	 * Retrieve data via extractor and display results
	 * @param weather the weather record to receive data from extractor on
	 */
	loadData(weather) {
		this.reset();
		console.log(weather);
		this.type = weather.sourcetype;
		this.isTide = (weather.infotype === "Oceanic");
		console.log("Attempting to loading data of type: %s and isOceanic: %s", this.type, this.isTide );
		// Display an indicator to user that information is requested
		this.Entries.get(weather.countryid).then(res => {
			const data = res.data.filter(el => el.sourceid === weather.id)[0];
			if (data && data.readings) {
				$("#viewModal").modal("show");
				this.readings = data.readings;
				// console.dir(this.readings);
				delete this.readings.sourceid;
				// Special processing and formatting for sensor readings
				if (this.type.toLowerCase() === "sensor") this.sensorReadings(this.readings);
				console.log("Retrieved weather records as: " + JSON.stringify(this.readings));

			}else{
				swal("Weather Extractor", "Unable to display received readings", "error");
			}
		}, err => {
			console.log(err);
			swal("Weather Extractor", "No readings for the extractor received.", "error");
		});
		// Attempt to retrieve the threshold to display with data as well
	}

	showExtractor() {
		if (this.debug)console.log("Attempting to show extractor");
		$("#ExtractorUploadModal").modal('show');
	}

	testExtractor(weather) {
		// console.log(weather);

		const data = {
			"id": weather.id,
			"codepath": weather.codepath
		};

		this.displayLoading("Loading Weather Readings").then(loading => {
			this.Entries.test(data).then(res => {
				// console.dir(res);
				loading.close();
				swal("Record Added", "Record was successfully Added", "success");
				this.refresh();
			}, err => {
				loading.close();
				swal("Entries", "Unable to retrieve data from extractor", "error");
				console.error(err);
			});
		});
	}

	sensorReadings(readings) {
		if (this.debug)console.log(readings);
		let html = "";
		for (let i = 0; i < (readings.date.length); i++) {
			html += '<tr>';
			for (let key in readings) {
				if (readings.hasOwnProperty(key)) {
					if (this.debug) console.log(key);
					html += '<td>' + readings[key][i] + '</td>';
				}
			}
			html += '</tr>';
		}
		$('#sensorTable').html(html);
	}

	// ****** Threshold Related Functionality *****

	resetThresholds() {
		this.thresholds = {
			'warnings': {},
			'emergency': {}
		};
	}

	saveThresholds(threshold) {
		console.log("Attempting to save thresholds: " + JSON.stringify(threshold));
		let rec = {
			thresholds: threshold,
			weathersourceid: this.record.id
		};

		this.Threshold.add(rec).then(res => {
			console.log("Successfully added a new threshold for " + rec.weathersourceid);
			swal('Weather Source', 'Threshold Successfully Configure', 'success');
			$("#thresholdModal").modal("hide");
			this.refresh();
		}, err=> {
			console.error(err);
			swal('Weather Source', 'Unable to save thresholds', 'error');
		});
	}

	retrieveThreshold(weatherSource){
		console.log("attempting to retrieve weather: %s", weatherSource.name);
		return new Promise((resolve, reject) =>{
			this.Threshold.getByWeatherSource(weatherSource.id).then(res => {
				resolve(JSON.parse(res.data.thresholds));
			},reject);
		});

	}

	editThresholds(weather){
		this.record = weather;
		console.log("Attempting to display thresholds for weather: %s with id %s", weather.name, weather.id);
		// open modal for thresholds

		this.Threshold.getInitParameters(weather).then(paramsRes => {
			const configFieldsJSON = paramsRes.data.fields;
			console.log("ConfigFields: ");
			console.log(configFieldsJSON);
			this.retrieveThreshold(weather).then(thresholdRec => {
				console.log("Received:");
				console.log(thresholdRec);
				this.fields = configFieldsJSON;
				this.field_names = Object.keys(configFieldsJSON);

				this.thresholds = thresholdRec;

				$("#thresholdModal").modal("show");
				this.$scope.$apply();
			}, err =>{
				swal("Thresholds", "No thresholds for " + weather.name + " found", "error");
				console.error(err);
			});
		}, err =>{
			swal("Thresholds", "No thresholds for " + weather.name + " found", "error");
			console.error(err);
		});
	}
}
WeatherCtrl.$inject = [
	"Country",
	"Locator",
	"AuthService",
	'LocalStorage',
	'fileupload',
	'WeatherSource',
	'Threshold',
	'Entries',
	'$compile',
	"$scope"
];

angular.module('mfisheries.Weather', [])
		.controller("WeatherCtrl",WeatherCtrl);