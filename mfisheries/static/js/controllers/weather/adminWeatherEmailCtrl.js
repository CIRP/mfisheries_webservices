class AdminWeatherEmailCtrl extends BaseController {
	constructor(Country, Locator, AuthService, LocalStorage, Emailing) {
		super(Country, Locator, AuthService, LocalStorage);
		this.Emailing = Emailing;
		
		this.primaryTable = 'srcTbl';
		this.primaryModal = "#contactModal";
		this.viewModal = "#contactModal";
		this.primaryForm = "newWeatherSource";
		
		this.init();
	}
	
	init() {
		super.init();
		// Initialise Menu
		$(".mFish_menu").removeClass("active");
		$("#menu-fewer").addClass("active");
		$("#menu-weather-report").addClass("active");
		
	}
	
	setInfoName() {
		this.infoName = "Weather Admin Contacts";
	}
	
	reset() {
		this.record = {
			"name": "",
			"email": "",
			"countryid": this.currentUser.countryid,
			'createdby': this.currentUser.userid
		};
	}
	
	startAdmin() {
		this.reset();
		this.retrieveCountries();
		this.retrieveRecords();
	}
	
	retrieveRecords(countryid) {
		return new Promise((resolve, reject) => {
			this.is_loading = true;
			let country = (countryid) ? countryid : this.userCountry;
			console.log("Attempting to retrieve weather sources for: " + country);
			this.Emailing.get(country).then(res => {
				if (res.data) {
					this.records = res.data;
					console.log("Retrieved " + this.records.length + " source records");
					this.is_loading = false;
					resolve(this.records);
				}
			}, err => {
				console.error("Unable to load weather administrators:" + JSON.stringify(err));
				this.is_loading = false;
				reject(err);
			});
		});
	}
	
	deleteRec(rec) {
		console.log("Attempt to deleted: " + JSON.stringify(rec));
		swal({
			title: "Delete Confirmation",
			text: "Are you sure you want to delete this record. You will not be able to undo this operation",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Delete",
			closeOnConfirm: false
		}).then(res => {
			if (res.value) {
				this.Emailing.delete(rec).then(() => {
					swal("Deleted", "Record was successfully deleted", "success");
					this.retrieveRecords();
				});
			}
		});
	}
	
	save(rec) {
		return new Promise((resolve, reject) => {
			let response, isEdit = false, msg = "";
			if (rec.id && rec.id > 0) isEdit = true;
			if (isEdit) response = this.Emailing.update(rec);
			else response = this.Emailing.save(rec);
			// Message based on operation
			if (isEdit) msg = "Updating Contact";
			else msg = "Saving Contact";
			// Handle the operation
			response.then(res => {
				swal(msg, "Operation was completed successfully", "success");
				$(this.primaryModal).modal("hide");
				this.retrieveRecords();
				resolve(res.data);
			}, err => {
				// Notify user of failure
				swal(msg, "Operation failed. If error persist contact administrator for support.", "error");
				$(this.primaryModal).modal("hide");
				reject(err);
			});
		});
	}
}

AdminWeatherEmailCtrl.$inject = [
	"Country",
	"Locator",
	"AuthService",
	'LocalStorage',
	'Emailing'
];

angular.module('mfisheries.Weather')
	.controller("AdminWeatherEmailCtrl", AdminWeatherEmailCtrl);