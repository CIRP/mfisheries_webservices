
class FEWERCtrl extends BaseController{
	
	constructor(Country, Locator, AuthService, LocalStorage){
		super(Country, Locator, AuthService, LocalStorage);
		console.log("Initialized FEWER Controller");
		this.init();
	}
	
	init(){
		super.init();
		this.configureBottomBar();
	}
	
	setInfoName(){
		this.infoName = "FEWER";
	}
	
	configureBottomBar(){
		console.log("attempting to display bottom bar");
		$("#bottomMenu").show();
		
		$("#bottomCtrl").click(evt => {
			this.toggleRegister();
		});
	}
}

FEWERCtrl.$inject = [
	"Country",
	"Locator",
	"AuthService", "LocalStorage"
];

angular
	.module('mfisheries.FEWERModule', [])
	.controller("FEWERCtrl", FEWERCtrl);