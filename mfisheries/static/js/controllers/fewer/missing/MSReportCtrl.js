/**
 * Missing Person Report Controller
 *
 */
class MSReportCtrl extends MapBaseController{
	
	constructor(MSReportService, MSReportFieldService, AuthService, LocalStorage, Country, Locator, fileupload,CountryLoc, $scope){
		super(Country, Locator, AuthService, LocalStorage, CountryLoc);
		// Attach the needed services as attributes of the class
		this.MSReportService = MSReportService;
		this.MSReportFieldService = MSReportFieldService;
		this.$scope = $scope;
		this.fileupload = fileupload;
		this.primaryTable = "msgPersonTbl";
		this.primaryModal = "#modelModal";
		this.viewModal = "#viewModal";
		this.init();
	}
	
	/**
	 * Provides operations needed for running initialization operations
	 */
	init(){
		super.init();
		$(".mFish_menu").removeClass("active");
		$("#menu-fewer").addClass("active");
		$("#menu-msp-report").addClass("active");
		
		this.currLocation = ["", ""];
		this.tableId = "msgPersonTbl";

		this.is_loading = true;
		this.countryname = "";
		this.lastSeen = "";
	}

	setInfoName(){
		this.infoName = "Missing Persons";
	}
	setMapId(){
		this.mapId = "map";
	}

	/**
	 *
	 */
	startAdmin(){
		if (this.debug)console.log("Launching Administrative interface");
		this.getLocation();
		this.retrieveCountries(true).then(res => console.log(res), err => console.error(err));
		this.retrieveRecords();
		this.reset();

		this.all_countries = [];
		this.retrieveCountries(true).then(countries => this.all_countries = countries);
	}


	/**
	 *
	 */
	startView(){
		console.log("Launching View/Public interface for Missing persons");
		super.startView().then(res => console.log(res), err=> console.error(err));
		this.retrieveRecords();
	}

	/**
	 *
	 * @param countryid
	 */
	retrieveRecords(countryid){
		console.log("Hello MS report");
		let country  = (countryid) ? countryid : this.userCountry;
		this.is_loading = true;
		console.log("Attempting to retrieve missing person records for %s" , country);
		this.MSReportService.get(country).then(res => {
			this.is_loading = false;
			this.records = res.data.map(el =>{
				el.timecreated = moment(el.timecreated);
				el.timeupdated = moment(el.timeupdated);
				return el;
			});
			console.log("Received %s records from %s", this.records.length, country);
		});
	}

	/**
	 *
	 */
	reset(){
		if (!this.currLocation)
			this.currLocation = ["",""];
		
		this.record = {
			"id": 0,
			title: "",
			description: "",
			contact: "",
			countryid: this.currentUser.countryid,
			latitude: this.currLocation[0],
			longitude: this.currLocation[1],
			additional: "",
			userid: this.currentUser.userid,
			isFound: 0,
		};
		this.lastSeen = "";

		this.file = undefined;
	}

    displayAdd(){
        console.log("Adding new record");
        super.displayAdd();
    }
	
	save(report, file){
		// if update
		console.log(MSReportCtrl.lastSeen);
		console.log(report.additional);
		if(MSReportCtrl.lastSeen === "undefined" || report.additional === "undefined"){
            console.log("Empty");
            MSReportCtrl.lastSeen = "";
            report.additional = "";
		}
		if (report.id && report.id > 0){
            report.additional += MSReportCtrl.lastSeen;
            this.MSReportService.update(report).then(res =>{
				console.log(res);
				swal("Update Report", "Report was successfully Updated", "success");
				$(this.primaryModal).modal('hide');
				this.retrieveRecords();
			}, err => {
				console.log(err);
				swal("Update Report", "Unable to update report. If error persist contact administrator for support.", "error");
			});
		}else{ // we have a new record
			// TODO Implementation to check if record exists requires that it should be loaded. This request should be sent to the server

			if (MSReportCtrl.lastSeen && MSReportCtrl.lastSeen.length > 1)report.additional += MSReportCtrl.lastSeen;
			let sim = this.records.filter(el => el.title === report.title && el.countryid && report.countryid);
			if (sim.length < 1){
				this.MSReportService.add(report, file).then(res => {
					console.log(res);
					if (res.status === 201) {
						swal("Add Report", "Report was successfully Saved", "success");
						$(this.primaryModal).modal('hide');
						this.retrieveRecords();
					}else{
						swal("Add Report", "Unable to save report. If error persist contact administrator for support.", "error");
						console.log(res.status);
					}
				}, (err) => {
					swal("Add Report", "Unable to save report.  If error persist contact administrator for support.", "error");
					console.log(err);
				});
			}else{
				swal("Adding New Report", "Unable to add new Report. Report of a similar name exists.", "error");
			}
		}
	}
	
	deleteRec(report) {
		swal({
			title: "Delete Confirmation",
			text: "Are you sure you want to delete this record. You will not be able to undo this operation",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Delete",
			closeOnConfirm: false
		}).then(res => {
			console.log(res);
			if (res.value) {
				this.MSReportService.delete(report).then(res => {
					if (res && res.status === 200) {
						swal("Deleted", "Record was successfully deleted", "success");
						this.retrieveRecords();
					} else swal("Failed", "Unable to delete record", "error");
				}, err =>{
					swal("Failed", "Unable to delete record", "error");
					console.error(err);
				});
			}
		});
	}

    viewReport(report){
        this.report = report;
        console.log(this.report);
        console.log("DIsplaying report details for " + report.title);
        $('#viewModal').modal('show');
    }

    edit(report){
		console.log(report);
		this.report = report;
        const modelModal = $("#modelModal");
        modelModal.modal('show');
        modelModal.updatePolyfill();
	}
	
	markAsFound(report){
		if (this.debug)console.log("Marking person as found");
		swal({
			title: "Update Confirmation",
			text: "Confirm that you the person has been found?",
			type: "warning",
			showCancelButton: true,
			confirmButtonText: "Yes",
			closeOnConfirm: false
		}).then(res => {
			if (res.value){
				report.isFound = 1;
				this.save(report, true);
			}
		});
	}

	updateReportLocation(location){
		if (!location)location = this.currLocation;
		super.updateReportLocation(location);
		this.$scope.$apply();
	}
}

MSReportCtrl.$inject = [
	"MSReportService",
	"MSReportFieldService",
	"AuthService",
	"LocalStorage",
	"Country",
	"Locator",
	"fileupload",
	"CountryLoc",
	"$scope"
];

angular.module('mfisheries.MissingPersons', [])
		.controller("MSReportCtrl", MSReportCtrl);