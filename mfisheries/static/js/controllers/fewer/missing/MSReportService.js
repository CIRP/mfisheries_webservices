
class MSReportService extends FileBaseAPIService{
	constructor($http, fileupload){
		super($http, fileupload, "/api/missingpersons");
	}
}

MSReportService.$inject = [
		"$http",
	"fileupload"
];


class MSReportFieldService extends BaseAPIService{
	constructor($http){
		super($http,"/api/missingpersons/fields");

	}
}

angular.module("mfisheries.MissingPersons")
		.service("MSReportService",MSReportService)
		.service("MSReportFieldService",MSReportFieldService);