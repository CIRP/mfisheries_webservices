angular.module('mfisheries.FEWERModule')
	.controller('adminFEWERSrcCtrl', ['$scope', '$window', 'LocalStorage', 'CAPSource', 'AuthService', 'Country',
		function ($scope, $window, LocalStorage, CAPSource, AuthService, Country) {
			console.log("FEWER Admin Source Configuration Controller Loaded");
			
			$scope.sources = [];
			$scope.countries = [];
			
			$("#sourceModal").modal('hide');
            $(".mFish_menu").removeClass("active");
            $("#menu-fewer").addClass("active");

            AuthService.attachCurrUser($scope);
			const currUser = $scope.currentUser;
			
			
			
			Country.get($scope.userCountry).then(function (res) {
				$scope.countries = res.data;
			});
			
			function loadSources() {
				CAPSource.get($scope.userCountry).then(function (res) {
					console.log(res.data);
					if (res.data && res.data instanceof Array) {
						$scope.sources = res.data;
					}
				});
			}
			
			function reset() {
				$scope.newSource = {
					countryid: currUser.countryid,
					createdBy: currUser.userid,
					category: "CAP",
					scope: "national"
				};
			}
			
			reset();
			
			loadSources();
			
			const initAddSrcForm = function () {
				$("#country").val(0); // reset to first option
			};
			
			$scope.deleteSource = function (sourceId, idx) {
				console.log(idx);
				swal({
					title: "Delete Confirmation",
					text: "Are you sure you want to delete this record. You will not be able to undo this operation",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete",
					closeOnConfirm: false
				}).then( res => {
					if (res.value) {
						CAPSource.delete(sourceId).then(res => {
							swal("Delete Record", "Record was successfully deleted", "success");
							console.log(res);
							$scope.sources.splice(idx, 1);
						}, err => {
							swal("Delete Record", "Unable to delete record", "error");
							console.log(err);
						});
					}
				});
				
			};
			
			$scope.addSource = function () {
				$("#sourceModal").modal('show');
			};
			
			$scope.displayEdit = function(source){
				$scope.newSource = source;
				$("#sourceModal").modal('show');
			};
			
			$scope.manageSource = function(source){
				console.log(source);
				
			};
			
			$scope.saveSource = function (source) {
				const newSource = angular.copy(source);
				console.log(JSON.stringify(newSource));
				
				if (newSource.id){ // If id exists then we are updating previous record
					CAPSource.update(newSource).then(function(res){
						if (res.status === 200){
							swal("Updating Alert Source", "Alert Source Updating", "success");
							reset();
							$("#sourceModal").modal('hide');
						}else swal("Updating Alert Source", "Unable to Update Source. If problem persists contact administrator", "error");
						
					}, function(err){
						swal("Updating Alert Source", "Unable to Update Source. If problem persists contact administrator", "error");
						console.log(err);
					});
				}else{
					CAPSource.post(newSource).then(function (res) {
						if (res.status === 201) {
							swal("Add Alert Source", "Alert Source Created", "success");
							loadSources();
							reset();
							$("#sourceModal").modal('hide');
						}else swal("Add Alert Source", "Unable to Add Source. If problem persists contact administrator", "error");
					}, function (err) {
						swal("Add Alert Source", "Unable to Add Source. If problem persists contact administrator", "error");
						console.log(err);
					});
				}
				
				
				
			};
			
		}]);
