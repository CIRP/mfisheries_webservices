angular.module('mfisheries.CAP')
	.service("CAPSource", function ($http) {
		'use strict';
		var deferred = {};
		
		deferred.get = function (countryid, sendable) {
			const options = {};
			if (countryid && countryid !== 0) { // if country == 0 load all
				options.countryid = countryid;
			}
			if (sendable){
				options.scope = "mFisheries";
			}
			
			return $http.get('/api/capsources', { params: options });
		};
		
		deferred.get_one = function(srcid){
			return $http.get('/api/capsources/'+srcid);
		};
		
		deferred.post = function (source) {
			return $http.post('/api/capsources', source);
		};
		
		deferred.update = function (source) {
			return $http.put('/api/capsources/' + source.id, source);
		};
		
		deferred.delete = function (sourceId) {
			return $http.delete('/api/capsources/' + sourceId);
		};
		
		return deferred;
	});