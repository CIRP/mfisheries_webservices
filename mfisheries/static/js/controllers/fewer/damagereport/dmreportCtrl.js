// Class for controller based on https://coryrylan.com/blog/es2015-class-in-angularjs-controllers-and-services
/**
 * Class for DamageReport controller.
 * The same class is used for both viewing and administrative functionality
 */
class DMReportCtrl extends MapBaseController{
	/**
	 *
	 * @param DMReport
	 * @param DMCategory
	 * @param AuthService
	 * @param LocalStorage
	 * @param Country
	 * @param Locator
	 * @param fileupload
	 * @param CountryLoc
	 * @param $scope
	 */
	constructor(DMReport, DMCategory, AuthService, LocalStorage, Country, Locator, fileupload, CountryLoc, $scope) {
		super(Country, Locator, AuthService, LocalStorage, CountryLoc);
		
		this.DMReport = DMReport;
		this.DMCategory = DMCategory;

		this.$scope = $scope;
		
		this.file = {};
		this.record = {};
		this.is_loading = true;
		
		// Categories related attributes
		this.categories = [];
		this.category = {};

		
		this.is_cat_loading = true;
		this.fileupload = fileupload;

		this.primaryTable = 'dmgTableReports';
		this.primaryModal = "#modelModal";
		this.primaryForm = "damageReport";
		this.viewModal = "#viewModal";

		this.init();
	}
	
	/**
	 *
	 */
	init() {
		super.init();
		
		$(".mFish_menu").removeClass("active");
		$("#menu-fewer").addClass("active");
		$("#menu-dmg-report").addClass("active");
		
		this.getLocation();
		
		this.audience = [
			{'id': '', 'name': "Select Audience"},
			{'id': 1, 'name': "Public"},
			{'id': 0, 'name': "Private"}
		];
	}
	
	setInfoName(){
		this.infoName = "Damage Reports";
	}
	setMapId(){
		this.mapId = "map";
	}

	
	/**
	 *
	 */
	reset() {
		if (!this.currLocation)
			this.currLocation = ["",""];

		this.record = {
			"id": 0,
			"name": "",
			"description": "",
			"userid": this.currentUser.userid,
			"countryid": this.currentUser.countryid,
			"latitude": this.currLocation[0],
			"longitude": this.currLocation[1],
			"createdby": this.currentUser.userid,
			"filetype": "text",
			"filepath": "",
			"aDate": new Date(),
			"cost": 0.00,
			"category": "Environment",
			"isPublic": 1
		};
		this.file = undefined;
	}
	
	/**
	 *
	 */
	resetCategory() {
		this.category = {
			"id": 0,
			"name": "",
			"relatedCategoryId": "",
			"countryid": this.currentUser.countryid,
			"createdby" : this.currentUser.userid
		};
	}
	
	/**
	 *
	 */
	startAdmin() {
		// super.startAdmin();
		this.reset();
		this.resetCategory();
		this.retrieveRecords();
		this.retrieveCategories();
		this.retrieveCountries();


		this.all_countries = [];
		this.retrieveCountries(true).then(countries => this.all_countries = countries);
	}
	
	/**
	 *
	 */
	startView() {
		this.autoLoadRecords = false; // Set to False to control the loading screen for records
		// Start the parent process for setting up views
		super.startView().then(() =>{
			if (this.userCountry) {
				// If that process is completed successfully, then request records
				this.displayLoading("Loading Reports").then(loading => {
					this.retrieveRecords(this.userCountry).then(() => loading.close(), () => loading.close());
				});
			}else console.log("Country Selection is required to display records");
		}, err => console.log(err));
	}
	


	/**
	 *
	 * @param countryid
	 */
	retrieveRecords(countryid) {
		return new Promise((resolve, reject) => {
			let country = (countryid) ? countryid : this.userCountry;
			this.is_loading = true;
			console.log("Attempting to retrieve damage report for %s", country);
			this.DMReport.get(country).then(res => {
				this.is_loading = false;
				this.records = res.data.map(report => {
					report.aDate = moment(report.aDate).toDate();
					return report;
				});
				console.log("Received %s reports from %s", this.records.length, country);
				resolve(this.records);
			}, err => {console.error(err); reject(err); });
		});
	}
	
	retrieveCategories(countryid) {
		this.is_cat_loading = true;
		let country = (countryid) ? countryid : this.userCountry;

		this.DMCategory.get(country).then(res => {
			this.is_cat_loading = false;
			this.categories = res.data;
			if (this.debug)console.log("Received %s categories", res.data.length);
		});
	}
	
	addCategory() {
		if(this.newcategory)this.newcategory.reset();
		this.resetCategory();
		const modelModal = $("#categoryModal");
		modelModal.modal('show');
		modelModal.updatePolyfill();
	}

	displayAttachment(record){
		this.record = record;
        console.log(record);
        console.log(record.filepath);
		const modelModal = $("#contentModal");
		modelModal.modal('show');
        modelModal.updatePolyfill();
    }

	displayEdit(record) {
		console.log(typeof record.aDate);
		if (typeof(record.aDate) === 'string') {
			record.aDate = moment(report.aDate).toDate();
		}
		super.displayEdit(record);
	}

    displayAdd(){
        	console.log("Adding new record");
        	super.displayAdd();
        }
	
	deleteRec(report, idx) {
		const self = this;
		console.log("delete " + report.id + " at index " + idx);
		swal({
			title: "Delete Confirmation",
			text: "Are you sure you want to delete this record. You will not be able to undo this operation",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Delete",
			closeOnConfirm: false
		}).then(result => {
			if (result.value) {
				self.DMReport.delete(report).then(res => {
					swal("Delete Record", "Record was successfully deleted", "success");
					self.records.splice(idx, 1);
				}, err => {
					swal("Delete Record", "Unable to delete record", "error");
					console.log(err);
				});
			}
		});
	}
	
	uploadFile(file, report) {
		console.log("uploading media");
		const uploadUrl = '/api/damagereport/attachment';
		if (file) {
			$("#spinning_upload").show();
			this.fileupload.uploadFileToUrl(file, uploadUrl).then(function (data) {
				if (data.data && data.data.status === 200) {
					report.filetype = data.data.file_path;
					swal({
						title: "Damage Report",
						type: "success",
						text: "File was successfully uploaded"
					});
					return true;
				} else {
					swal({
						title: "Damage Report",
						type: "error",
						text: "Unable to Upload File"
					}).then(() => {
						console.log("Error Occurred when attempting to upload file: ");
						console.error(data);
						return false;
					});
				}
			});
		} else {
			swal("Uploading Attachment", "Please select File Before uploading", "error");
			return false;
		}
	}
	
	save(report, file) {
		console.log(report);
		report.aDate = (new Date(report.aDate)).getTime();
		// TODO Update cannot update a attachment
		// Update record
		if (report.id && report.id > 0) {
			this.DMReport.update(report, this.file).then(() => {
				swal("Report", "Report was successfully created", "success");
				$("#modelModal").modal('hide');
			}, (err) => {
				swal(
					"Update Report",
					"Unable to update report. If error persist contact administrator for support.",
					"error"
				);
				console.error(err);
			});
		}
		// Else we are attempting to save a new damage report
		else {
			// if we select another file type
			if (report.filetype !== "text") {
				if (!this.file || !this.file.name){ // if no file uploaded
					swal("Report", "Selection of attachment is required for "+ report.filetype, "error");
					return;
				}
				// Uploading record with attachment
				$("#spinning_upload").show();
				this.DMReport.add(report, this.file).then(res => {
					console.dir(res);
					swal("Report", "Report was successfully created", "success");
					$("#modelModal").modal('hide');
					this.retrieveRecords();
				}, err => {
					swal(
						"Report",
						"Unable to save report. If error persist contact administrator for support.",
						"error"
					);
					console.error(err);
				});
			}
			// Upload record if it was text
			else if (report.filetype === "text"){
				this.DMReport.add(report).then(res => {
						swal("Report", "Report was successfully created", "success");
						$("#modelModal").modal('hide');
						this.retrieveRecords();
				}, err => {
					swal(
						"Report",
						"Unable to save report. If error persist contact administrator for support.",
						"error"
					);
					console.error(err);
				});
			}
		}
	}
	
	saveCategory(category) {
		if (category.id > 0) {
			this.DMCategory.update(category).then(() => {
				swal("Update category", "Category was successfully created", "success");
				$("#categoryModal").modal('hide');
			}, (err) => {
				swal("Update category", "Unable to update report. If error persist contact administrator for support.", "error");
				console.log(err);
			});
		} else {
			this.DMCategory.add(category).then((res) => {
				if (res.status === 201) {
					swal("Saving Category", "Category was successfully created", "success");
					$("#categoryModal").modal('hide');
					this.retrieveCategories();
				} else swal("Saving Category", "Unable to save category", "error");
			}, (err) => {
				swal("Saving Category", "Unable to save category. If error persist contact administrator for support.", "error");
				console.log(err);
			});
		}
	}
	
	editCat(category) {
		console.log("edit - " + category.id);
		this.category = category;
		const modelModal = $("#categoryModal");
		modelModal.modal('show');
		modelModal.updatePolyfill();
	}
	
	deleteCat(category, idx) {
		const self = this;
		console.log("delete " + category.id + " at index " + idx);
		swal({
			title: "Delete Confirmation",
			text: "Are you sure you want to delete this record. You will not be able to undo this operation",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Delete",
			closeOnConfirm: false
		}).then(result => {
			if (result.value) {
				self.DMCategory.delete(category).then(() => {
					swal("Delete Category", "Category was successfully deleted", "success");
					self.categories.splice(idx, 1);
				}, err => {
					swal("Delete Category", "Unable to delete category", "error");
					console.log(err);
				});
			}
		});
	}
	
	handleCountryChange(countryid) {
		super.handleCountryChange(countryid);
		// Attempting to add country name to be displayed based on data
		if (!isNaN(countryid)) countryid = parseInt(countryid);
		let country = this.countries.filter(country => country.id === countryid)[0];
		if (country) this.countryname = country.name;
		// retrieve posts
		this.retrieveRecords(countryid);
		this.retrieveCategories(countryid);
	}



	updateReportLocation(location){
		if (!location)location = this.currLocation;
		super.updateReportLocation(location);
		this.$scope.$apply();
	}
}

DMReportCtrl.$inject = [
	"DMReport",
	"DMCategory",
	"AuthService",
	"LocalStorage",
	"Country",
	"Locator",
	"fileupload",
	"CountryLoc",
	"$scope"
];

angular.module('mfisheries.DMReport', [])
	.controller("DMReportCtrl", DMReportCtrl);