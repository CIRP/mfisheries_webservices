/**
 * Damage Report Data Service used to interact with the backend REST servcies
 */
class DMReport extends FileBaseAPIService{
	/**
	 *
	 * @param $http
	 */
	constructor($http, fileupload){
		super($http, fileupload, "/api/damagereport");
	}

}

DMReport.$inject = [
	"$http",
	"fileupload"
];
angular.module('mfisheries.DMReport').service("DMReport", DMReport);


class DMCategory extends BaseAPIService{
	/**
	 *
	 * @param $http
	 */
  constructor($http){
		super($http,  "/api/damagereport/category");
	}
}

DMCategory.$inject = [
  "$http",
];
angular.module('mfisheries.DMReport').service("DMCategory", DMCategory);