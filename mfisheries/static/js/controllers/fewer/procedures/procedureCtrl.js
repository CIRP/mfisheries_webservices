class ProcedureCtrl extends BaseController{
	
	constructor(Country, Locator, AuthService, LocalStorage){
		super(Country, Locator, AuthService, LocalStorage);
		this.init();
	}
	
	init(){
		$(".mFish_menu").removeClass("active");
		$("#menu-fewer").addClass("active");
		$("#menu-emg-pro").addClass("active");
		this.retrieveCountries();

		this.files = [
			"emergency_at_sea_and_sailing",
				"navigation_lights",
				"pre_sea_check",
				"search_and_rescure",
				"stability_and_trim",
				"vhf_radio"
		];

		this.primaryModal = "#procedureModal";
	}

	setInfoName() {
		this.infoName = "Emergency Procedures";
	}

	startAdmin() {
		this.retrieveCountries();
		this.retrieveRecords();
		this.setEventListeners();
	}

	startView() {
		this.retrieveCountries();
		this.retrieveRecords();
		this.setEventListeners();
	}

	retrieveRecords(countryid) {
		this.records = this.files.map(el =>{
			return {
				name: el.split("_").join(" "),
				type: "video",
				path: "http://test.mfisheries.cirp.org.tt/static/country_modules/tobago/emergencyProcedures/" + el +".mp4"
			};
		});
	}

	reset(){
		this.record = {};
	}
	displayEdit(rec){
		console.log(rec);
		super.displayEdit(rec);
	}

	setEventListeners(){
		$(".close").click(() => {
			console.log("close clicked");
			document.getElementById("videoPlayer").pause();
		});
	}
}

ProcedureCtrl.$inject = [
	"Country",
	"Locator",
	"AuthService",
	'LocalStorage',
];

angular.module('mfisheries.Procedures',[])
	.controller("ProcedureCtrl",ProcedureCtrl);