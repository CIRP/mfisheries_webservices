/**
 * Class for CAP listing controller.
 * The same class is used for both viewing and administrative functionality
 * Step 1 develop Retrieval for country dropdown list
 * Step 2 develop Retrieval for public group for country
 */
class AlertListCtrl extends BaseController {
	/**
	 *
	 * @param Country
	 * @param Locator
	 * @param AuthService
	 * @param LocalStorage
	 * @param $scope
	 * @param Alerts
	 * @param Group
	 */
	constructor(Country,Locator, AuthService, LocalStorage, $scope,  Alerts, Group) {
		super(Country, Locator, AuthService, LocalStorage);
		this.Group = Group;
		this.Alerts = Alerts;
		this.$scope = $scope;

		this.reports = [];
		this.userCountry = 0;
		this.is_loading = true;
        this.displayall = false;
        this.init();
	}

	init() {
		super.init();
		$(".mFish_menu").removeClass("active");
		$("#menu-fewer").addClass("active");
		$("#menu-alert").addClass("active");

		this.getLocation();
	}
	
	setInfoName(){
		this.infoName = "Alerts";
	}

	startView() {
		this.autoLoadRecords = true; // Set to False to control the loading screen for records
		super.startView().then(() =>{
			console.log(this.userCountry);
			this.$scope.$apply(); // Unsure why its needed here. But takes to long to load view configuration without it
			if (this.userCountry) {
				// If that process is completed successfully, then request records
				this.displayLoading("Loading Groups").then(loading => {
					this.retrieveRecords(this.userCountry).then(() => loading.close(), () => loading.close());
					this.displayall = true;
				});
			}else console.log("Country Selection is required to display records");
		}, err => console.log(err));
	}

	retrieveRecords(countryid){
		return new Promise((resolve, reject) => {
			this.Group.get(countryid).then(res => {
				this.records = res.data.map(el => {
					el.canSend = !el.oneWay;
					return el;
				});
				resolve(this.records);
				if (this.debug)console.log("Received %s groups for the country %s", this.records.length, this.userCountry);
			}, reject);
		});
	}

	handleGroupChange(group){
		if (this.debug)console.log("Attempting to run handle change groups");
		this.Alerts.getByGroup(group, this.displayall).then(reports => {
			if (this.debug)console.log("Retrieved %s alerts from group %s", reports.length, group.groupname);
            if (this.debug)console.log(reports.data);
            this.reports = reports.data;
		}, err => console.error(err));
	}

}

AlertListCtrl.$inject = [
	"Country",
	"Locator",
	"AuthService",
	"LocalStorage",
	"$scope",
	"Alerts",
	"Group"
];

angular.module('mfisheries.Alerts').controller("AlertListCtrl", AlertListCtrl);