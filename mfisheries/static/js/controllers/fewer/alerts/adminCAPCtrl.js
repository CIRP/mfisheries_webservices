"use strict";

angular.module('mfisheries.CAP', [])
	.controller('adminCAPCtrl', [
		'$scope',
		'LocalStorage',
		'$anchorScroll',
		'$location',
		'$routeParams',
		'$window',
		'CapService',
		'Alerts',
		'Country',
		'AuthService',
		'CAPSource',
		'CountryLoc',
		"PushNotify",
		function ($scope, LocalStorage, $anchorScroll, $location, $routeParams, $window, CapService, Alerts, Country, AuthService, CAPSource, CountryLoc, PushNotify) {

			let key;
			$(".mFish_menu").removeClass("active");
			$("#menu-fewer").addClass("active");
			$("#menu-alert").addClass("active");

			const config = CapService.getConfig();
			const COORDINATE_RADIUS = 15;
			
			let areaTemplates = [];
			let messageTemplates = [];
			let intervalId = -1;
			// const circles = [];
			// const polys = [];
			const features = [];
			const featureTypes = [];
			const poolTime = 10 * 60 * 1000; // minutes * seconds * milliseconds

			
			AuthService.attachCurrUser($scope).then(res => {
				if (!res){ $window.location.assign("/#/login");  }
			});

			function init() {
				console.log("Running the initialize function for CAP Ctrl");
				$scope.readOnly = false;
				$scope.cap_loading = true;
				$scope.comm_loading = true;
				$scope.alerts = [];
				$scope.communityAlerts = [];
				$scope.areaTemplateNames = [];
				$scope.messageTemplateNames = [];
				$scope.coords = [];
				$scope.alert = '';
				$scope.info = '';
				$scope.area = '';
				$scope.showAlerts = true;
				$scope.otherSelected = false;
				$scope.sending = false;
				$scope.mode = "None";
				$scope.action = "New";

				$scope.all_countries = [];

				$scope.isBroadcast = true;
			}
			
			function loadCountries(display) {
				$scope.country_loading = true;
				Country.get($scope.userCountry).then(function (res) {
					$scope.countries = res.data;
					// Attempting to Add a new Record at the beginning to give useful info for user
					const all_rec = {
						id: 0, name: "Display All Countries"
					};
					if (display)
						$scope.countries.unshift(all_rec);

					$scope.all_countries = res.data;
					// $scope.all_countries .unshift(all_rec);
					$scope.country_loading = false;
				});
			}
			
			function createAlert() {
				$scope.alert = new Alert();
				$scope.info = $scope.alert.addInfo();
				$scope.area = $scope.info.addArea();

				// Set Default and System Specified Values
				$scope.alert.sent = new Date().toISOString();
				$scope.alert.identifier = 'pending';
				$scope.alert.language = "en-us";

				$scope.alert.sender = 'FEWER';


				configureInfo($scope.info);
				$scope.expires = "360";
				$scope.category = "Met";
				// Initialize Bootstrap Tooltip Plugin
				$('[data-toggle="tooltip"]').tooltip();
			}

			function configureInfo(info){
				info.senderName = $scope.currentUser.fname + " " + $scope.currentUser.lname;
				info.audience = $scope.currentUser.country;
				info.web = "none";
				info.audience = $scope.currentUser.country;
				return info;
			}
			
			function loadAlerts(countryid) {
				$scope.cap_loading = true;
				$scope.comm_loading = true;

				$scope.alerts = [];
				$scope.communityAlerts = [];
				// Request each country (if countryid not specified, we will load all countries)
				Country.get(countryid).then(res => {
					// For each country, we receive the CAP source associated with that country
					res.data.forEach(country => {
						// TODO Implement logic that can accommodate all sources
						// We retrieve the compatible CAP Source
						CAPSource.get(country.id, true).then(srcRes => {
							// For each source, we extract its URL, Pass it to the CAP service
							srcRes.data.forEach(srcEl => {
								CapService.setBaseURL(srcEl.url);
								// CAP service receives the alert based on information passed
								CapService.getAlerts().then(function (servRes) {
									$scope.alerts = $scope.alerts.concat(servRes.data.map(el => {
										// add the country
										console.log(servRes);
										el.country = country.name;
										if (!el.references || el.references.length < 1)el.references = el.sender+","+el.alert_id+","+el.sent;
										return el;
									}));
									$scope.cap_loading = false; // Stop loading as soon as first is retrieved and displayed
									// TODO Place Updated time for retrieval of alerts
								});
							});
						});
						// TODO Restructuring of alerts required to allow for country-specific filtering
						Alerts.get(country.id).then(res => {
							console.log("Received from community alerts awaiting confirmation from country: %s", country.id);
							console.log(res);
							$scope.communityAlerts = $scope.communityAlerts.concat(res.data);
							$scope.comm_loading = false;
						}, err => console.log(err));
					});

				});

				// TODO Perform check if a group is selection in the community alerts section
			}
			
			function loadTemplates() {
				CapService.getMessageTemplates().then(function (res) {
					const templatesXML = res.data;
					const names = [];
					messageTemplates = [];
					for (let i = 0; i < templatesXML.length; i++) {
						names.push(templatesXML[i].title);
						const tmpl = parseTemplateToAlert(templatesXML[i].content);
						messageTemplates.push(tmpl);
					}
					$scope.messageTemplateNames = names;
				});
				
				CapService.getAreaTemplates().then(function (res) {
					const templatesXML = res.data;
					const names = [];
					areaTemplates = [];
					for (let i = 0; i < templatesXML.length; i++) {
						names.push(templatesXML[i].title);
						const tmpl = parseTemplateToAlert(templatesXML[i].content);
						areaTemplates.push(tmpl);
					}
					$scope.areaTemplateNames = names;
				});
			}
			
			function loadSendableSources(countryid) {
				console.log("Attempting to retrieve sendable sources");
				if (!countryid) countryid = $scope.userCountry;
				CAPSource.get(countryid, true).then(res => {
					if (res.data.length > 0){ // We have a source that can be used to send data
						CapService.setBaseURL(res.data[0].url);
						console.log(res.data[0].url);
						loadTemplates(); // After getting the URL We will update the area and message templates
					}else{
						swal("Source", "No Internal CAP Source Configured", "error");
					}
				});
			}

			$scope.handleCountryChange = function (countryid, action) {
				console.log("Attempting to change country to:" + countryid);
				$scope.userCountry = countryid;
				if ($scope.countryid !== countryid) $scope.countryid = countryid;
				if (action === "list") loadAlerts(countryid);
				else if (action === "add") loadSendableSources(countryid);
			};
			
			$scope.initializeListing = function () {
				console.log("Initializing Listing");
				init();
				loadCountries(true);
				createAlert();
				loadAlerts($scope.userCountry);

				intervalId = setInterval(res => {
					console.log(res);
					console.log("Running timed function for updating user interface");
					loadAlerts($scope.userCountry);
				}, poolTime);

			};
			
			$scope.refresh = function(){
				console.log("Refresh for CAP Alerts requested");
				loadAlerts($scope.userCountry);
			};
			
			$scope.initializeCreation = function () {
				init();
				loadCountries(false);
				createAlert();
				loadTemplates();
				loadSendableSources($scope.currentUser.countryid);
				$scope.countryid = $scope.currentUser.countryid;
				
				if ($routeParams.operation && $routeParams.alertid) {
					$scope.loadOperationOnAlert($routeParams.operation, $routeParams.alertid, $routeParams.reference);
				}
			};

			$scope.translateMobileAlert = function(mobileAlert){
				console.log("Attempting to translate the mobile alert to a CAP alert");
				console.log(mobileAlert);
				const severity = {
					"unspecified": "Unknown",
					"1-fair": "Minor",
					"2-bad": "Moderate",
					"3-severe": "Severe",
					"4-extreme": "Extreme"
				};

				const categories = {
					"bad weather": "Met",
					"rough seas": "Env"
					// else Other
				};

				const cat = mobileAlert.messagecontent.toLowerCase();
				if (cat in categories)$scope.category = categories[cat];
				else $scope.category = "Other";

				$scope.info.urgency = "Unknown";
				$scope.info.severity = severity[mobileAlert.severity.toLowerCase()];
				$scope.info.certainty = "Unknown";
				$scope.info.event = mobileAlert.messagecontent + " alert of"+ mobileAlert.severity + " severity";
				$scope.info.description = mobileAlert.messagecontent + " alert with a severity of"+ mobileAlert.severity +" was created by " + mobileAlert.fullname + " and approved by " + $scope.info.senderName;
				$scope.info.headline = mobileAlert.messagecontent;
				$scope.responseTypes = "Monitor";
				$scope.expires = mobileAlert.expires+"";
				// $scope.info.addParameter('userid', $scope.currentUser.userid);

				$scope.coords.push({"lat": mobileAlert.latitude, "lng": mobileAlert.longitude});
			};
			
			$scope.loadOperationOnAlert = function(operation, alertid, reference) {
				//TODO Set Display to load until receive details for existing CAP alert to be updated or deleted
				// console.log("Operation: %s - Alert ID: %s - Reference: %s", operation, alertid, reference);
				if (operation === "update")$scope.alert.msgType = "Update";
				if (operation === "cancel")$scope.alert.msgType = "Cancel";
				if (operation === "broadcast"){
					if (LocalStorage.hasKey("toBeBroadcastAlert")){
						const alert =   LocalStorage.getObject("toBeBroadcastAlert");
						$scope.isBroadcast = true;
						$scope.translateMobileAlert(alert);
					}else{ // Unable to retrieve alert from local storage, then redirect user to the list of alerts
						swal({
							title: "Broadcast Alert",
							text: "Unable to retrieve specified alert to be broadcasted.",
							timer: 1500,
							type: "error"
						}).then(res => {
							$window.location.assign("/#/admin/fewer/alerts");
						});
					}
				}
				$scope.alert.references = reference;
			};

			$scope.alertdata = {};
			
			$scope.displayRawAlert = function (alert) {
				$scope.alertdata =alert;
				$("#alertsModal").modal("show");
			};
			
			$scope.newAlert = function () {
				$scope.showAlerts = false;
				$scope.action = "New";
			};
			
			function alertToDisplay(alert) {
				$scope.info.certainty = alert.certainty;
				$scope.info.description = alert.description;
				$scope.info.event = alert.event;
				$scope.info.instruction = alert.instruction;
				$scope.info.urgency = alert.urgency;
				$scope.info.severity = alert.severity;
				$scope.info.headline = alert.title;
				$scope.info.web = alert.web;
				$scope.info.addResponseType(alert.response_type);
				alert.parameters.push({
					name: 'userid', value: $scope.currentUser.userid
				});
				
				$scope.info.parameters = $.map(alert.parameters, function (val, i) {
					return {"valueName": val.name, "value": val.value};
				});
				
				$scope.info.addCategory(alert.category);
				// $scope.sendAlert();
			}
			
			$scope.messageTemplateSelected = function (selected) {
				const idx = $scope.messageTemplateNames.indexOf(selected);
				$scope.alert = messageTemplates[idx];
				$scope.alert.identifier = 'pending';
				$scope.alert.lang = "en-us";
				
				$scope.info = $scope.alert.infos[0];
				
				$scope.info.pushArea($scope.area);
				$scope.alert.pushInfo($scope.info);
			};
			
			$scope.areaTemplateSelected = function (selected) {
				const index = $scope.areaTemplateNames.indexOf(selected);
				$scope.area = areaTemplates[index].infos[0].areas[0];
				
				for (let i = 0; i < $scope.area.polygons; i++) {
					addCapPolygonToMap($scope.area.polygons[i], $scope.area.areaDesc);
				}
				for (let i = 0; i < $scope.area.circles; i++) {
					addCapPolygonToMap($scope.area.circles[i], $scope.area.areaDesc);
				}
				
				// $($scope.area.polygons).each(function(index, value) {
				//   addCapPolygonToMap(value, area.areaDesc);
				// });
				// // clear and reload circles in map
				// $($scope.area.circles).each(function(index, value) {
				//   addCapCircleToMap(value, area.areaDesc);
				// });

				$scope.info.pushArea($scope.area);
				$scope.alert.identifier = 'pending';
				$scope.alert.lang = "en-us";
			};
			
			// submit alert JSON to server
			$scope.sendAlert = function () {
				getInfo();
				$scope.sending = true;

				if (!$scope.alert.language || $scope.alert.language.length < 2){
					$scope.alert.lang = "en-us";
					$scope.info.language = "en-us";
				}

				// if the scope is not specified, then set as
				if ($scope.alert.sender.length < 2) $scope.alert.sender = "FEWER";
				// $scope.info.addParameter('userid', $scope.currentUser.userid);


				const a_data = {
					'uid': $scope.uid,
					'password': $scope.password,
					'xml': $scope.alert.getCAP()
				};
				
				let result_message = "";
				swal({
					title: 'Confirm Alert',
					text: 'Alerts are broadcasted immediately after creation and cannot be deleted or subsequently edited. Proceed?',
					type: 'warning',
					showCancelButton: true,
					confirmButtonText: 'Yes',
					cancelButtonText: 'No',
					closeOnConfirm: true,
					allowOutsideClick: false
				}).then((confirmRes) => {
					if (confirmRes.value){
						CapService
								.sendAlert(a_data)
								.then(sendRes => {
											console.log(sendRes);
											$scope.sending = false;
											if (sendRes.status === 200) {
												if (sendRes.data.valid) $scope.processSuccessSentAlert(a_data, sendRes.data); // The Alert was sent Successfully
												else swal("Alert", "Invalid Message specified. Try again", "error") ;
											}
										},
										err => {
											console.log(err);
											$scope.sending = false;
											// Provide useful response about error for user to understand what is happening
											if (err.status === 400) result_message = 'Please enter valid login and password.';
											else if (err.status === 403) result_message = 'You are not authorized to release alerts. ';
											else result_message = 'POSSIBLE ERROR IN TRANSMISSION. ' + 'Check active alerts before resending.';
											$scope.responseStatus = result_message;
											swal("Alert", result_message, "error");
										});
					}
				});
			};
			
			$scope.convertCAPToMobile = function(alert){
			
			};

			$scope.processSuccessSentAlert = function(alert, capResponse){
				const result_message = "Alert was successfully created. Alert ID: " + capResponse.uuid;
				
				if ($scope.isBroadcast){
					if (LocalStorage.hasKey("toBeBroadcastAlert")){
						const broadcastAlert =   LocalStorage.getObject("toBeBroadcastAlert");
						broadcastAlert.isVerifies = true;
						broadcastAlert.isPublic = false;
						Alerts.update(broadcastAlert).then(res => console.log(res), err => console.error(err));
					}
					$scope.isBroadcast = false;
				}
				
				swal({
					title: "Alert",
					text: result_message,
					type: "success"
				}).then(successRes => {
					$window.location.assign("/#/admin/fewer/alerts");
				});

			};
			
			$scope.addGeocode = function () {
				$scope.area.geocodes.push({"valueName": "", "value": ""});
			};
			
			$scope.addParameter = function () {
				$scope.info.parameters.push({"valueName": "", "value": ""});
			};
			
			$scope.addCoord = function () {
				$scope.coords.push({"lat": "", "lng": ""});
			};
			
			$scope.expireChanged = function () {
				$scope.otherSelected = $scope.expires === "other";
			};

			// update model with values from screen
			const getInfo = function () {
				$scope.alert.sent = moment().format();
				$scope.sender = $scope.currentUser.fname + "_" + $scope.currentUser.lname;
				$scope.alert.source = escape_text($scope.sender);

				$scope.info.categories = [];
				$scope.info.addCategory($scope.category || 'Other');
				$scope.info.senderName = $scope.currentUser.fname + " " + $scope.currentUser.lname;
				$scope.info.responseTypes = [];
				// Set default response type if not set.
				$scope.info.addResponseType($scope.responseTypes || 'None');

				let expiresInMinutes = $scope.expires;

				if (expiresInMinutes === 'other') {
					expiresInMinutes = $scope.expiresOther;
				}

				$scope.info.expires = moment().add(expiresInMinutes, 'm').format();
				$scope.info.audience = $scope.currentUser.country;

				$scope.area.circles = coordsToCircles().concat(getCircles());
			};
			
			function coordsToCircles() {
				const circles = [];
				for (let i = 0; i < $scope.coords.length; i++) {
					circles.push($scope.coords[i].lat + "," + $scope.coords[i].lng + " " + COORDINATE_RADIUS);
				}
				return circles;
			}
			
			$scope.$watch('coords', function (newValue, oldValue) {
				$scope.area.circles = coordsToCircles().concat(getCircles());
			}, true);
			
			OpenLayers.ImgPath = config.OpenLayersImgPath;
			const Geographic = new OpenLayers.Projection('EPSG:4326');
			const Mercator = new OpenLayers.Projection('EPSG:3857');  // more modern (and official) version of 900913
			
			let map, drawControls, cap_area;
			// components of the map
			let osmLayer, circleLayer, polyLayer;

			
			if (!map) {  // only initialize the map once!
				
				if (!cap_area) {
					cap_area = new Area('Undesignated Area');  // constructor in caplib.js
				}
				
				map = new OpenLayers.Map({
					div: 'map',
					allOverlays: true,
					projection: Mercator,
					displayProjection: Geographic,
					autoUpdateSize: true,
					controls: [
						new OpenLayers.Control.Navigation({zoomWheelEnabled: false}),
						new OpenLayers.Control.PanPanel(),
						new OpenLayers.Control.ZoomPanel(),
						new OpenLayers.Control.ArgParser(),
						new OpenLayers.Control.Attribution()
					]
				});

				// create reference layers.
				osmLayer = new OpenLayers.Layer.OSM('OpenStreetMap');
				osmLayer.setVisibility(true);
				osmLayer.setIsBaseLayer(true);
				
				circleLayer = new OpenLayers.Layer.Vector("Circle Layer");
				circleLayer.events.on({
					"featureadded": function (e) {
						features.push(e.feature);
						featureTypes.push("circle");
						$scope.area.circles = $scope.area.circles.concat(getCircles());
					}
				});

				polyLayer = new OpenLayers.Layer.Vector('Drawing Layer');
				polyLayer.events.on({
					"featureadded": function (e) {
						features.push(e.feature);
						featureTypes.push("poly");
						$scope.area.polygons = getPolygons();
					}
				});
				
				// add all layers to map
				map.addLayers([osmLayer, polyLayer, circleLayer]);
				map.setBaseLayer(osmLayer);
				
				// create draw controls
				drawControls = {
					circle: new OpenLayers.Control.DrawFeature(
						circleLayer,
						OpenLayers.Handler.RegularPolygon,
						{
							handlerOptions: {
								sides: 40
							}
						}
					),
					polygon: new OpenLayers.Control.DrawFeature(
						polyLayer,
						OpenLayers.Handler.Polygon
					)
				};
				
				// prevent map from panning when in "circle" mode
				drawControls.circle.handler.stopDown = stop;
				drawControls.circle.handler.stopUp = stop;
				
				// add controls to the map
				// ...including the collection of (named) draw controls
				for (let key in drawControls) {
					if (drawControls.hasOwnProperty(key))
						map.addControl(drawControls[key]);
				}
				map.addControl(new OpenLayers.Control.LayerSwitcher());
				
				// Default map viewport.
				CountryLoc.get($scope.userCountry).then(res => {
					if (res.data.length === 1){
						let latitude =  parseFloat(res.data[0].latitude);
						let longitude = parseFloat(res.data[0].longitude);
						setView(latitude, longitude, 9);
					} else {
						setView(10.4, -61.2, 9);
					}
				}, (err) => {
					console.error(err);
					setView(10.4, -61.2, 9);
				});

			}

			// set the map view
			function setView(centerLat, centerLon, zoomLevel) {
				const centerCoords = new OpenLayers.LonLat(centerLon, centerLat);
				map.setCenter(centerCoords.transform(Geographic, Mercator), zoomLevel);
			}

			// handler for radio buttons, activates the corresponding OpenLayers control
			$scope.toggleControl = function (mode) {
				for (key in drawControls) {
					if (drawControls.hasOwnProperty(key)) {
						const control = drawControls[key];
						if (mode === key) {
							control.activate();
						} else {
							control.deactivate();
						}
					}
				}
			};

			// return an array of polygon strings
			function getPolygons() {
				const polygonsXML = [];
				if (polyLayer) {
					for (let i = 0; i < polyLayer.features.length; i++) {
						polygonsXML.push(polygonToCapXml(polyLayer.features[i]));
					}
				}
				return polygonsXML;
			}

			// return an array of circle strings
			function getCircles() {
				const circlesXML = [];
				if (circleLayer) {
					for (let i = 0; i < circleLayer.features.length; i++) {
						circlesXML.push(circleToCapXml(circleLayer.features[i]));
					}
				}
				return circlesXML;
			}

			// clear all features drawn to the draw layer
			$scope.clearAllDrawnAreas = function () {
				circleLayer.removeAllFeatures();
				polyLayer.removeAllFeatures();
				$scope.area.circles = coordsToCircles();
				$scope.area.polygons = [];
			};
			
			// remove the last feature added to the draw layer, skipping geocode previews
			$scope.clearLastDrawnArea = function () {
				if (features.length > 0) {
					const type = featureTypes.pop();
					if (type === "circle") {
						circleLayer.removeFeatures(features.pop());
						$scope.area.circles.pop();
					}
					else {
						polyLayer.removeFeatures(features.pop());
						$scope.area.polygons.pop();
					}
				}
			};
			
			// add a polygon in CAP string format as a feature on the drawing layer
			function addCapPolygonToMap(polygonString, source, opt_id) {
				const points = [];
				const pointStrings = polygonString.split(' ');
				// note swap of coordinate order 'twixt CAP and OpenLayers
				for (let i = 0; i < pointStrings.length; i++) {
					const coords = pointStrings[i].split(',');
					points.push(new OpenLayers.Geometry.Point(
						parseFloat(coords[1]),
						parseFloat(coords[0])).transform(Geographic, Mercator));
				}
				const ring = new OpenLayers.Geometry.LinearRing(points);
				const polygon = new OpenLayers.Geometry.Polygon(ring);
				const feature = new OpenLayers.Feature.Vector(polygon);
				feature.attributes = {polygon: true, source: source};
				if (opt_id) {
					feature.attributes.id = opt_id;
				}
				polyLayer.addFeatures([feature]);
			}

			//add a circle in CAP string format as a feature on the drawing layer
			function addCapCircleToMap(circleString, source) {
				const parts = circleString.split(' ');
				const radius = parseFloat(parts[1]) * 1000;
				const coords = parts[0].split(',');
				const centerPoint = new OpenLayers.Geometry.Point(
					parseFloat(coords[1]),
					parseFloat(coords[0])).transform(Geographic, Mercator);
				const circle = new OpenLayers.Geometry.Polygon.createRegularPolygon(
					centerPoint,
					radius,
					40);
				const feature = new OpenLayers.Feature.Vector(circle);
				feature.attributes = {circle: true, source: source};
				circleLayer.addFeatures([feature]);
			}
			
			/**
			 VARIOUS UTILITY FUNCTIONS
			 **/
			
			function polygonToCapXml(feature) {
				const vertices = feature.geometry.getVertices();
				if (vertices.length < 3) return null;  // need at least three vertices
				let polygonString = '';
				for (let i = 0; i < vertices.length; i++) {
					polygonString += pointToRoundedCAPString(vertices[i], 5) + ' ';
				}
				polygonString += pointToRoundedCAPString(vertices[0], 5);
				return polygonString;
			}
			
			function circleToCapXml(circle) {
				const centroid = circle.geometry.getCentroid();
				const radius = radiusOfCircle(circle.geometry);
				//geographicCentroid = centroid.transform(Mercator, Geographic);
				return pointToRoundedCAPString(centroid, 5) + ' ' + radius;
			}

			// note that this returns the coordinates in CAP (lat,lon) order
			function pointToRoundedCAPString(vertex, decimalPoints) {
				const geo_point = vertex.transform(Mercator, Geographic);
				const string = geo_point.y.toFixed(decimalPoints) + ',' +
					geo_point.x.toFixed(decimalPoints);
				vertex.transform(Geographic, Mercator);  // gotta undo the transform!
				return string;
			}
			
			function radiusOfCircle(circle) {
				const vertices = circle.getVertices();
				const centroid = circle.getCentroid();
				const edgePoint = vertices[0];
				return distanceBetweenPoints(centroid, edgePoint);
			}

			// both points in Mercator projection
			function distanceBetweenPoints(point1, point2) {
				return (point1.distanceTo(point2) / 1000).toFixed(5);  // in km, geodesic
			}
			
			function parseTemplateToAlert(template_xml) {
				const xml = $.parseXML(template_xml);
				const alert = parseCAP2Alert(xml);
				// Non-CAP-compliant fields:
				alert.expiresDurationMinutes = $(xml).find('expiresDurationMinutes').text();
				return alert;
			}
			
			function escape_text(rawText) {
				return $('<div/>').text(rawText).html();
			}


			// Insome cases (like when using templates) the reference may not be specified, we use the function to ensure that we always have the reference
			$scope.getAlertReferences = function (alert){
				if (alert.references.length > 5)return alert.references;
				alert.references = alert.sender + "," + alert.alert_id + "," + alert.sent;
				return alert.references;
			};

			// Operations for Community Alert to CAP Alerts Processing

			$scope.viewAlertDetails = function(alert, i){
				// console.log(alert);

				// Keys from the alert that are useful to be displayed
				const usefulKeysInOrder = [
						"fullname",
						"group",
						"severity",
						"messagecontent",
						"messageDescription",
						"deviceTimestamp",
						"latitude",
						"longitude",
						"dislikes",
						"likes"
				];

				// Map keys that provides a more human-readable description of the field
				const mapKeys = {
					"fullname" : "Name",
					"group": "Group",
					"severity": "Severity",
					"messagecontent": "Alert Type",
					"messageDescription": "Condition Type",
					"deviceTimestamp" : "Time Created",
					"latitude":"latitude",
					"longitude":"longitude",
					"dislikes": "Down Votes",
					"likes": "Up Votes"
				};

				const rec = {};
				usefulKeysInOrder.forEach(key => {
					rec[mapKeys[key]] = alert[key];
				});

				$scope.alertdata = rec;
				$("#alertsModal").modal("show");

			};


			$scope.broadcastAlert = function (alert, idx) {
				// console.log(alert);
				swal({
					title: "Broadcast Confirmation",
					text: "Are you sure you want to broadcast this Alert?",
					type: "warning",
					showCancelButton: true,
					confirmButtonText: "Broadcast"
				}).then(res => {
					if (res.value) { // User selected the confirmation Option
						const time = moment(alert.timestamp, 'YYYY-MM-DD hh:mm:ss').format();
						if (!alert.references)alert.references = alert.username + "," +alert.id + "," + time;
						LocalStorage.setObject("toBeBroadcastAlert",alert);
						const path = "/#/admin/fewer/alerts/broadcast/" + alert.id + "/" + alert.references;
						$window.location.assign(path);
					}
				});
			};

			$scope.ignoreAlert = function (alert, idx) {
				swal({
					title: "Broadcast Confirmation",
					text: "Are you sure you want to ignore this Alert request?",
					type: "warning",
					showCancelButton: true,
					confirmButtonText: "Ignore",
					closeOnConfirm: true
				}).then(res => {
					if (res.value) { // User selected the confirmation Option
						alert.isPublic = false;
						$scope.communityAlerts.splice(idx, 1);
						Alerts.update(alert).then(res => {
							swal("Broadcast Confirmation", "Alert request was successfully ignored.", "success");
							$scope.refresh();
						}, err => {
							swal("Broadcast Confirmation", "Unable to ignore alert. If problem persists contact administrator.", "success");
							console.error(err);
						});
					}
				});
			};
			
		}]);