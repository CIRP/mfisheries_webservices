
class CapListCtrl{

    constructor(Alerts, capServices, Country, AuthService, CAPSource, Locator){
        this.Alerts = Alerts;
        this.capServices = capServices;
        this.AuthService = AuthService;
        this.Country = Country;
        this.Locator = Locator;
        this.CAPSource = CAPSource;

        this.currLocation = ["", ""];
        this.countries = [];
        this.userCountry = 0;

        this.reports = [];
        this.report = {};
        this.is_loading = true;

        this.init();
    }

    init(){
        console.log('Running the initialization of CAP Alert Controller');
        $(".mFish_menu").removeClass("active");
        $("#menu-fewer").addClass("active");
        $("#menu-capalert").addClass("active");

        this.AuthService.attachCurrUser(this).then(currUser => {
            if (currUser)console.log("User is logged in");
            else console.log("User is not logged in");
            console.log(this.userCountry);
        });

        this.currLocation = ["", ""];
        this.countries = [];
    }

    startAdmin(){
        console.log("Launching Administrative interface");
        this.retrieveCountries(true);
        this.retrieveRecords();
    }

    startView(){
        console.log("Launching View/Public interface");
        this.retrieveCountries(true);
    }

    retrieveRecords(countryid){
        let country  = (countryid) ? countryid : this.userCountry;
        this.is_loading = true;
        console.log("Attempting to retrieve records for %s" , country);
        this.capServices.get(country).then(res => {
            this.is_loading = false;
            this.records = res.data;
            console.log("Received %s records from %s", this.records.length, country);
        });
    }

    retrieveCountries(display) {
        const self = this;
        this.Country.get(this.userCountry).then(res => {
            self.countries = res.data;
            console.log("Retrieved: %s countries", res.data.length);
            if (display){
                this.countries.unshift({
                    id: 0, name: "Select Country"
                });
            }
        });
    }

    handleCountryChange(countryid){
        console.log("Country was changed to:" + countryid);
        this.userCountry = countryid;
        this.Locator.saveLastLocationID(countryid).then(res => console.log(res));
        // Attempting to add country name to be displayed based on data
        let country = this.countries.filter(country => country.id === countryid)[0];
        if (country)this.countryname = country.name;
        // retrieve posts
        this.retrieveRecords(countryid);
    }
}

CapListCtrl.$inject = [
    "Alerts",
    "capServices",
    "Country",
    "AuthService",
    "CAPSource",
    "Locator"
];

angular.module('mfisheries.FEWERModule')
    .controller("CapListCtrl", CapListCtrl);