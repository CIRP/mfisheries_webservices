class   Alerts extends BaseAPIService{
	constructor($http){
		super($http, "/api/alerts");
	}

	get(countryid, allAlert){
		const options = { type:"public" };
		if (countryid && countryid !== 0)options.countryid = countryid;
		if (allAlert)delete options.type;
		return this.$http.get(this.base_url, { params: options });
	}
	
	getByGroup(groupid, displayAll){
		const options = {group: groupid};
		if (displayAll)options.all = true;
		return this.$http.get(this.base_url, {params: options });
	}
}

Alerts.$inject = [
	"$http"
];
angular.module('mfisheries.CAP').service("Alerts", Alerts);
