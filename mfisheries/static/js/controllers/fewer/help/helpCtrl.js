class HelpCtrl{
	constructor(){
		console.log("Help constructor was initialized");
		this.init();
	}

	init(){
		$(".mFish_menu").removeClass("active");
		$("#menu-fewer").addClass("active");
		$("#menu-help").addClass("active");
	}
}

HelpCtrl.$inject = [];

angular.module('mfisheries.controllers')
		.controller("HelpCtrl", HelpCtrl);