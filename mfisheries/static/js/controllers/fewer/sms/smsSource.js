
class Sms{
	
	constructor($http){
		this.$http = $http;
		this.base_url = "/api/getsms";
	}
	get (countryid) {
		const options = {};
		if (countryid)options.countryid = countryid;
		return this.$http.get(this.base_url, {params:options});
	}

	add(data){
		return this.$http.post(this.base_url, data);
	}
	update(source) {
		return this.$http.put(this.base_url + "/" + source.id, source);
	}
	delete(source){
		return this.$http.delete(this.base_url+ "/" + source.id);
	}
}

Sms.$inject = [
	"$http"
];
angular.module('mfisheries.sms').service("Sms", Sms);
