
class Twilio{

	constructor($http){
		this.$http = $http;
		this.base_url = "/api/addtwilio";
	}
	get (userid) {
		const options = {};
		if (userid)options.countryid = userid;
		return this.$http.get(this.base_url, {params:options});
	}

	add(data){
		return this.$http.post(this.base_url, data);
	}
	update(source) {
		return this.$http.put(this.base_url, source);
	}
	delete(source){
		source.delete = true;
		return this.$http.post(this.base_url,source);
	}
}

Twilio.$inject = [
	"$http"
];
angular.module('mfisheries.sms').service("Twilio", Twilio);
