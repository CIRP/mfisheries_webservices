"use strict";

angular.module('mfisheries.sms',[])
	.controller('SmsCtrl', [
		'$scope', 'LocalStorage', 'AuthService', 'Sms','Country','Twilio',
		function ($scope, LocalStorage, AuthService, Sms, Country,Twilio){

            $(".mFish_menu").removeClass("active");
            $("#sms-config").addClass("active");
            $("#menu-meta").addClass("active");
			
			AuthService.attachCurrUser($scope);
			const currUser = $scope.currentUser;
		
			$scope.configs = [];
			$scope.countries = [];
			$scope.countryConfigs = [];
			$scope.currConfig = {};

			reset();
			retrieveConfiguration();


			$scope.displayAdd = function () {
				reset();
				$('#newConfigModal').modal('show');
			};

			$scope.displayUpdate = function (config) {
				$scope.config = config;
				$('#newConfigModal').modal('show');
			};

			$scope.addCountryConfig = function(){
				$('#addSmsConfigModal').modal('show');
				$('#newConfigModal').modal('toggle');
				$scope.config.isnew = true;
			};

			$scope.addTwilioCountryConfig = function(data){
				if (data.isnew){
					Twilio.add(data).then(() => {
						swal("Add Configuration", "Configuration was successfully created", "success");
						$('#newConfigModal').modal('hide');
						$('#addSmsConfigModal').modal('hide');

						retrieveConfiguration();
					}, (err) => {
						swal("Update Configuration", "Unable to update configuration. If error persist contact administrator for support.", "error");
						console.log(err);
					});
				}
				else{
					Twilio.update(data).then(() => {
						swal("Update Configuration", "Configuration was successfully updated", "success");
						$('#newConfigModal').modal('hide');
						$('#addSmsConfigModal').modal('hide');
						retrieveConfiguration();
					}, (err) => {
						swal("Update Configuration", "Unable to update configuration. If error persist contact administrator for support.", "error");
						console.log(err);
					});
				}
				console.log(data);
			};

			$scope.editCountryConfig = function(rec){
				$scope.config = rec;
				$scope.config.isnew = false;

				$('#addSmsConfigModal').modal('show');
			};

			$scope.deleteCountryConfig = function (rec) {
				Twilio.delete(rec).then(res => {
					swal("Delete Configuration", "Configuration was successfully deleted", "success");
					reset();
					retrieveConfiguration();
					$('#addSmsConfigModal').modal('hide');
				}, err => {
					console.error("An error has occurred: " + err);
				});
            };


			function reset(){
				$scope.sms = {};
				$scope.config={};

				return $scope.sms;
			}
			
			function retrieveConfiguration(){
				Sms.get(currUser.countryid).then((res) => {
					$scope.configs = res.data.data;
                    console.log($scope.configs);
				}, (err) => {
					console.error(err);
				});
				Country.get($scope.userid).then(res => {
					$scope.countries = res.data;
				}, err => {
					console.error("Unable to retrieve config: " + err);
				});
				Twilio.get($scope.userid).then(res => {
					$scope.countryConfigs = res.data.data;
				}, err => {
					console.error("Unable to retrieve country config: " + err);
				});


			}

		}]);

