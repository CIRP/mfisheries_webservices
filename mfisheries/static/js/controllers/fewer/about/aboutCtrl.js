
class AboutCtrl{
	constructor(){
		console.log("About constructor was initialized");
		this.init();
	}
	
	init(){
		$(".mFish_menu").removeClass("active");
		$("#menu-fewer").addClass("active");
		$("#menu-about").addClass("active");
	}
}

AboutCtrl.$inject = [];

angular.module('mfisheries.controllers')
	.controller("AboutCtrl", AboutCtrl);