class EmergencyContactCtrl extends BaseController {

	constructor(Country, Locator, AuthService, LocalStorage, EmergencyContact, $location) {
		super(Country, Locator, AuthService, LocalStorage);
		if (this.debug) console.log("Initialized the Emergency Contacts controller");
		this.EmergencyContact = EmergencyContact;
		// URL location for default pictures
		this.default_pics = {
			'individual': $location.protocol() + "://" + window.location.host + "/static/img/emergency/personal_default_profile.png",
			'organization': $location.protocol() + "://" + window.location.host + "/static/img/emergency/organization_default_profile.png"
		};

		this.primaryTable = 'eContactTbl';
		this.init();

	}

	init() {
		super.init();
		$(".mFish_menu").removeClass("active");
		$("#menu-fewer").addClass("active");
		$("#menu-emg-contact").addClass("active");

		this.primaryModal = "#newEmergencyContactModal";
		this.primaryForm = "emergencyContactForm";

		this.phoneExpression = new RegExp('^([0-9]{3})([-. ]?([0-9]{4}))?$');
		this.isEdit = false;
	}

	setInfoName() {
		this.infoName = "Emergency Contacts";
	}

	startAdmin() {
		this.getLocation();
		this.retrieveCountries();
		this.retrieveRecords();
		this.reset();

		this.all_countries = [];
		this.retrieveCountries(true).then(countries => this.all_countries = countries);
	}

	startView() {
		this.autoLoadRecords = false; // Set to False to control the loading screen for records
		// Start the parent process for setting up views
		super.startView().then(() =>{
			if (this.userCountry) {
				// If that process is completed successfully, then request records
				this.displayLoading("Loading Reports").then(loading => {
					this.retrieveRecords(this.userCountry).then(() => loading.close(), () => loading.close());
				});
			}else{
				this.retrieveRecords(0);
			}
		}, err => console.log(err));
	}

	retrieveRecords(countryid) {
		return new Promise((resolve, reject) => {
			this.loading = true;
			let country = (countryid) ? countryid : this.userCountry;

			this.EmergencyContact.get(country).then(res => {
				console.log("Retrieved %s contacts", res.data.length);
				if (res.data.length > 0)
					this.records = res.data.map(el => {
						// if additional fields provided, convert to JSON and save
						el.additionalJSON = (el.additional && el.additional.length > 1) ? JSON.parse(el.additional) : [];
						return el;
					});
				else this.records = [];
				this.loading = false;
				resolve(this.records);
			}, err => {
				this.loading = false;
				console.error(err);
				reject(err);
			});
		});
	}

	reset() {
		this.isEdit = false;

		let countryid = this.currentUser.countryid || 0;
		let user_id = this.currentUser.userid || 0;

		this.record = {
			'name': '',
			'phone': '',
			'email': '',
			'type': 'organization',
			'additional': '',
			'additionalJSON': [],
			'countryid': countryid,
			'createdby': user_id,
			'image_url': this.default_pics.organization,
			'association': 'Unspecified'
		};

		this.resetAddField();
		return this.record;
	}

	resetAddField() {
		this.field = {
			key: '', value: ''
		};
		return this.field;
	}

	// noinspection JSMethodCanBeStatic
	getNumAddDetails(contact) {
		let numDetails = 0;
		if (contact.additionalJSON.length > 0) {
			numDetails = contact.additionalJSON.length;
		}
		return numDetails;
	}

	displayAddDetails(contact) {
		if (this.debug)console.log("Displaying additional details for: " + JSON.stringify(contact));
		this.record = contact;
		$('#addDetailsModal').modal('show');
	}

	saveAdditionalItem(field) {
		if (field.key.length >= 3 && field.value.length >= 3) {
			this.record.additionalJSON.push(field);
			resetAddField();
		} else {
			swal("Add Fields", "Keys and Values must be at least 3 characters long", "error");
		}
	}

	// noinspection JSMethodCanBeStatic
	showAdditionalItemForm() {
		$("#additional_contact").show('slow');
		$("#additional_contact_btn").show('fast');
	}
	
	removeAdditionalItem(field, i) {
		if (this.record.additionalJSON.length >= i) {
			this.record.additionalJSON.splice(i, 1);
		}
	}

	// noinspection JSMethodCanBeStatic
	translateAdditional(contact) {
		// Translate the additional fields to string
		if (contact.additionalJSON.length > 0) {
			contact.additional = JSON.stringify(contact.additionalJSON);
		} else {
			contact.additional = "";
		}
		return contact;
	}

	// noinspection JSMethodCanBeStatic
	filterNum(tel, countryid) {
		console.log(tel);
		if (!tel) {
			return '';
		}

		const value = tel.toString().replace(/[^0-9]/g, '');
		// var value = tel.toString().trim().replace(/^\+/, '');

		if (value.match(/[^0-9]/)) {
			return tel;
		}

		let number;
		// area = value.slice(0);
		// city = value.slice(1, 4);
		// number = value.slice(4);

		if (value) {
			if(value.length === 10){ //if area code is already in number leave it as it is
				number = value;
			}
			else if (value.length < 5 && value.length > 2) { ///if value is a 3 digit emergency number leave it as it is
				number = value;
			}
			else if (value.length === 7) { //if value is a 7 digit number add in the area code based on the selected country
				number = value;
				console.log(this.all_countries);
				console.log(countryid);

                // for(let country in this.countries)
                this.all_countries.forEach(function(country){
                    // console.log(country);
                    console.log(country.id);
                    console.log("Looking for id:" + countryid);
                    if (country.id === countryid){
                        console.log(number);
                        console.log(country);
                        number = country.areacode + number;
                        console.log(number);
                        return number;
                    }
                });
			}
		}
		if (number) {
			return number;
		} else {
			return '';
		}
	}

	save(record) {
		// TODO - EMGContacts - Evaluate how this is handled during during an update
		// if (this.record.phone.length === 8) {
		// 	let tempString = this.record.phone;
		// 	tempString = tempString.slice(0, 3) + tempString.slice(4);
		// 	this.record.phone = tempString;
		// }
		//
		// var number = this.record.phone;
		
		if (record)this.record = record;

		this.record.phone = this.filterNum(this.record.phone, this.record.countryid);

		this.record = this.translateAdditional(this.record);

		if (this.record.phone === '') {
			swal("Add New Contact", "Error invalid Number!", "error");
		}
		else if (this.record.id) { // Updating Record
			console.log("Updating the contact: " + JSON.stringify(this.record));
			this.EmergencyContact.update(this.record).then((res) =>{
				console.log('Updated Emergency Contact');
				if (res.status === 200) {
					$('#newEmergencyContactModal').modal('hide');
					swal('Updating Emergency Contact', 'Successfully Updated Emergency Contact Data', 'success');
				}
				else {
					swal('Updating Emergency Contact', 'Error Updating Emergency Contact', 'error');
				}
			}, err => {
				console.error(err);
				swal('Updating Emergency Contact', 'Error Updating Emergency Contact', 'error');
			});
		} else { // Saving record
			// Ensuring no duplicate Emergency Contact
			let sim = this.records.filter((el) => {
				return el.name.toLowerCase() === this.record.name.toLowerCase() &&
						el.countryid === this.record.countryid;
			});

			if (sim.length < 1) {
				console.log("Saving the contact: " + JSON.stringify(this.record));
				this.EmergencyContact.add(this.record).then((res) => {
					console.log("Adding the contact: " + JSON.stringify(this.record));
					if (res.status === 201) {
						console.log("Emergency Contact was saved successfully");
						// Add the record from the server to the list
						this.records.push(res.data);
						$('#newEmergencyContactModal').modal('hide');
						swal('Add New Emergency Contact', 'Emergency Contact Successfully Added', 'success');
						reset();
					}
					else {
						swal('Add New Emergency Contact', 'Unable to Save Contact. Ensure all fields are specified correctly', 'error');
					}
				}, (err) => {
					console.log(err);
					swal('Add New Emergency Contact', 'Unable to Save Contact. If problem persists, contact administrator', 'error');
				});
			} else {
				swal('Add New Emergency Contact', 'Unable to save contact, contact exists.', 'error');
			}
		}
	}

	deleteRec(contact) {
		console.log("Deleting the contact: " + JSON.stringify(contact));

		swal({
			title: "Delete Confirmation",
			text: "Are you sure you want to delete this record. You will not be able to undo this operation",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Delete",
			closeOnConfirm: false
		}).then(result => {
			console.log(result);
			if (result.value) {
				this.EmergencyContact.delete(contact).then((res) => {
					console.log('Deleting Emergency Contact');
					if (res.status === 200) {
						swal('Deleting Emergency Contact', 'Emergency Contact Successfully Deleted', 'success');
						this.retrieveRecords();
					}
					else {
						swal('Deleting Emergency Contact', 'Error Deleting Contact', 'error');
					}
				}, err => {
					console.error(err);
					swal('Deleting Emergency Contact', 'Error Deleting Contact', 'error');
				});
			}
		});
	}


}

EmergencyContactCtrl.$inject = [
	"Country",
	"Locator",
	"AuthService",
	'LocalStorage',
	"EmergencyContact",
	"$location"
];

angular.module('mfisheries.EmergencyModule')
		.controller("EmergencyContactCtrl", EmergencyContactCtrl);
