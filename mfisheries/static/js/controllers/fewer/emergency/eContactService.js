class EmergencyContact extends BaseAPIService{
	constructor($http){
		super($http, "api/emergencycontact");
	}
}
EmergencyContact.$inject = [
	"$http"
];
angular.module('mfisheries.EmergencyModule', [])
	.service("EmergencyContact", EmergencyContact);