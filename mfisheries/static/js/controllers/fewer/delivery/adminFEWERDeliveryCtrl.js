
class AdminAlertDeliveryCtrl extends BaseController{

	constructor(Country, Locator, AuthService, LocalStorage, $firebaseAuth, $firebaseArray){
		super(Country, Locator, AuthService, LocalStorage);
		this.$firebaseArray = $firebaseArray;
		this.auth = $firebaseAuth();

		this.viewModal = "#detailsModal";

		this.init();
	}

	init(){
		super.init();
		// Update menu
		$(".mFish_menu").removeClass("active");
		$("#menu-fewer").addClass("active");
		$("#menu-alert").addClass("active");

		// attempt to signing with firebase
		this.auth.$signInAnonymously();
	}

	setInfoName(){
		this.infoName = "Alert Delivery Report";
	}

	startAdmin(){
		this.retrieveRecords();
	}

	retrieveRecords() {
		// https://github.com/firebase/angularfire/blob/master/docs/reference.md#onauthstatechangedcallback-context
		// this.auth.$onAuthStateChanged(user => {
		// 	if (user){
		// 		const ref = firebase.database().ref().child('notif-status');
		// 		this.statuses = this.$firebaseArray(ref);
		// 	}
		// });
		this.records = [
			{
				id: "f1fc4190-0a0a-11e8-ba89-0ed5f89f718b",
				message: "Test Alert",
				fullname: "Kyle De Freitas",
				sent: Date.now(),
				medium: "App",
				status: "delivered"
			},
			{
				id: "f1fc4190-0a0a-11e8-ba89-0ed5f89f718b",
				message: "Test Alert",
				fullname: "Mikkel Hayes",
				sent: Date.now(),
				medium: "App",
				status: "delivered"
			},
			{
				id: "f1fc4190-0a0a-11e8-ba89-0ed5f89f718b",
				message: "Test Alert",
				fullname: "Will Steward",
				sent: Date.now(),
				medium: "SMS",
				status: "delivered"
			}
		];

	}
}

AdminAlertDeliveryCtrl.$inject = [
	'Country',
	'Locator',
	'AuthService',
	'LocalStorage',
	'$firebaseAuth',
	'$firebaseArray'
];
angular.module('mfisheries.FEWERModule')
		.controller('AdminAlertDeliveryCtrl', AdminAlertDeliveryCtrl);