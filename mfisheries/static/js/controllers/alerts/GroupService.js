class Group extends BaseAPIService{
	/**
	 *
	 * @param $http
	 */
	constructor($http){
		super($http, "/api/groups");
	}
	
	getPublicAdvisoryGroups(countryid){
		const options = { 'advisory': true };
		if (countryid && countryid !== 0)options.countryid = countryid;
		return this.$http.get(this.base_url, { params: options });
	}
}

Group.$inject = [
	"$http"
];
angular.module('mfisheries.Alerts').service("Group", Group);