class AlertGroupCtrl extends MapBaseController{

	constructor(Country, Locator, AuthService, LocalStorage, CountryLoc, Alerts, Group, $scope ){
		super(Country, Locator, AuthService, LocalStorage, CountryLoc);
		this.Group = Group;
		this.Alerts = Alerts;
		this.$scope = $scope;
		
		this.primaryTable = "commAlertTbl";
		this.primaryModal = 'msgModal';
		this.primaryForm = 'newgroup';

		this.init();
	}

	init(){
		super.init();
		// Update menu
		$(".mFish_menu").removeClass("active");
		$("#menu-fewer").addClass("active");
		$("#menu-alert").addClass("active");
		
		

		this.primaryModal = "#groupModal";

		this.alerts = [];
		this.alert = {};
		this.selectedGroup = "";
		this.displayall = false;
		this.poolTime = 10 * 60 * 1000; // minutes * seconds * milliseconds
		this.intervalId = -1;
		this.getLocation();
	}

	setInfoName(){
		this.infoName = "Alert Groups";
	}
	setMapId(){
		this.mapId = "mapGroup";
	}

	setupTimedRefresh(){
		this.intervalId = setInterval(() => {
			this.refresh();
		}, this.poolTime);
	}

	startAdmin(){
		console.log("Starting Alert Group Administration");
		
		this.retrieveCountries().then(res => console.log(res), err => console.error(err));
		this.retrieveRecords();
		this.reset();
		this.resetAlert();
		this.setupTimedRefresh();
	}

	startView(){
		if (this.debug)console.log("Starting Alert Group View Operation");
		this.retrieveRecords();
		this.setupTimedRefresh();
	}

    startPublicView(){
        console.log("Starting Public alerts Group View Operation");
        this.startView();
        this.retrieveCountries().then(res => console.log(res), err => console.error(err));
        this.displayall ="true";
    }

	retrieveRecords() {
		this.Group.get(this.userCountry).then(res => {
			this.records = res.data.map(el => {
				el.canSend = !el.oneWay;
				return el;
			});
			if (this.debug)console.log("Received %s groups for the country %s", this.records.length, this.userCountry);
		});
	}

	reset() {
		if (!this.currLocation)
			this.currLocation = ["",""];
		
		this.record = {
			'countryid': this.currentUser.countryid,
			'canSend': true,
			'oneWay': false // Set to allow users to post alerts to each other by default,
		};
		this.isEdit = false;
	}

	resetAlert(){
		if (!this.currLocation)
			this.currLocation = ["",""];
		
		this.alert = {
			'countryid': this.currentUser.countryid,
			'userid': this.currentUser.userid,
			"messagecontent": "Unspecified",
			"messageSeverity": "Unspecified",
			'isPublic': 0,
			"latitude": this.currLocation[0],
			"longitude": this.currLocation[1]
		};
		console.log("Reset alert to:"+ JSON.stringify(this.alert));

	}

	deleteRec(group) {
		swal({
			title: "Delete Confirmation",
			text: "Are you sure you want to delete this record. You will not be able to undo this operation",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Delete"
		}).then(res => {
			if (res.value) {
				this.Group.delete(group).then((res) => {
							console.log(res);
							swal("Deleted", "Record was successfully deleted", "success");
							// Remove group from array, when operation is successful
							this.retrieveRecords();
						},
						(err) => {
							swal("Failed", "Unable to delete record", "error");
							console.error(err);
						});
			}
		});
	}

    displayAdd(){
        console.log("Adding new record");
        super.displayAdd();
    }

	update(group) {
		swal({
			title: "Update Confirmation",
			text: "Are you sure you want to update this record. You will not be able to undo this operation",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3e8f3e",
			confirmButtonText: "Update",
			closeOnConfirm: false
		}).then(res => {
			if (res.value) {
				this.Group.update(group).then(res => {
					$(this.primaryModal).modal('hide');
					swal("Updated", "Record was successfully updated", "success");
					reset(); // Reset the model container for next operation
				}, err => {
					console.error(err);
					if (err.data && err.data.error_msg){
						swal("Update Group", err.data.error_msg, "error"); // Handles useful responses provided directly from server (for e.g. Duplicate records)
					}else{
						swal("Update Group", "Unable to update record.. If problem persists contact administrator.", "error");
					}
				});
			}
		});
	}

    // displayAddGroup(){
		// if(this.newgroup) this.newgroup.reset();
		// this.reset();
    //     $(this.primaryModal).modal('show');
    // }

	save(group) {
		// attempting to ensure that the value of oneWay is correct based on user behaviour
		this.record.oneWay = !this.record.canSend;

		if (this.record.id && this.record.id > 0) { // If we have an id then record can be updated
			this.update(this.record, group);
		} else { // Record is new and can be saved
			this.Group.add(this.record)
					.then(res => {
						$(this.primaryModal).modal('hide');
						swal("Create Group", "Group was successfully created", "success");
						this.retrieveRecords();
					}, err => {
						console.error(err);
						if (err.data && err.data.error_msg){
							swal("Create Group", err.data.error_msg, "error"); // Handles useful responses provided directly from server (for e.g. Duplicate records)
						}else{
							swal("Create Group", "Unable to create group. If problem persists contact administrator.", "error");
						}
					});
		}
	}

	view(alert){
		this.alert = alert;
		$("#viewModal").modal('show');
	}

	// Alert based functionality
	addCommAlert(){
		console.log("Select add community alert");
		if (this.newalert)this.newalert.reset();
		this.resetAlert();
		this.initMap();
		$("#msgModal").modal('show');
	}

	loadCommAlertListing(){
		if (this.debug)console.log("Request for retrieving community alerts executed");
		this.selectedGroup = {};
		this.alerts = [];
		this.records.forEach(group => {
			if (this.debug)console.log("Requesting information from group: " + group.groupname );
			this.Alerts.getByGroup(group.id).then(alerts => {
				if (this.debug)console.log("Retrieved %s alerts from group %s", alerts.length, group.groupname);
				if (alerts.data.length > 0)
					this.alerts = this.alerts.concat(alerts.data);
				if (this.debug)console.log(this.alerts);
			});
		});
	}

	refresh(){
		if (this.debug)console.log("Refresh for Community Alerts requested");
		if (this.debug)console.log("Selected Group was: " + this.selectedGroup);
		if (this.selectedGroup !== "" || this.selectedGroup !== "?")
			this.handleGroupChange(this.selectedGroup);
		else if (this.debug) console.log("No Group was selected");
	}

	handleGroupChange(group){
		if (this.debug)console.log("Attempting to run handle change groups");
		this.Alerts.getByGroup(group, this.displayall).then(alerts => {
			if (this.debug)console.log(alerts.data);
			this.alerts = alerts.data;
		}, err => console.error(err));
	}

	handleActiveChange(change){
		if (this.selectedGroup !== "" && this.selectedGroup !== "?")
			this.handleGroupChange(this.selectedGroup);
	}

	saveAlert(alert){
		alert.deviceTimestamp = moment().format('YYYY-MM-DD hh:mm:ss');
		if (this.debug)console.log(JSON.stringify(alert));
		this.Alerts.add(alert).then(res => {
			if (this.debug)console.log(res.data);
			this.resetAlert();
			swal("Alerts", "Successfully created alert", "success").then(res => {
				$("#msgModal").modal('hide');
			});
		}, err => {
			swal("Alerts", "Unable to send alert. If problem persists, contact administrator", "error");
			if (this.debug)console.error(err);
		});
	}

	deleteAlert(alert){
		swal({
			title: "Delete Confirmation",
			text: "Are you sure you want to delete this record. You will not be able to undo this operation",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Delete"
		}).then(res => {
			if (res.value) {
				this.Alerts.delete(alert).then((res) => {
					console.log(res);
					swal("Deleted", "Record was successfully deleted", "success");
					// Remove group from array, when operation is successful
					this.alerts.splice(
							_.indexOf(this.alerts, _.find(this.alerts, function (gp) {
								return gp.id === alert.id;
							})), 1);
				},
				(err) => {
					swal("Failed", "Unable to delete record", "error");
					console.error(err);
				});
			}
		});
	}

	// noinspection JSMethodCanBeStatic
	changeOneWay(group){
		if (this.debug)console.log(group);
		group.oneWay = !group.canSend;
	}

	retrieveAndUpdateCurrentLocation(){
		if (this.debug)console.log("Attempting to Update Form with current location");
		swal({
			type: 'info',
			title: 'Retrieving Location',
			text: 'Attempting to retrieve current location',
			onOpen: () => {
				swal.showLoading();
			}
		});

		// Attempting to force the system to refresh location
		this.getLocation(loc => {
			swal.close();
			if (loc){
				this.alert.latitude = loc[0];
				this.alert.longitude = loc[1];
			}else{
				swal("Location", "Unable to retrieve location. Ensure browser has permission to acquire your current location.","error");
			}
		}, true);
	}
	
	
	/**
	 *
	 */
	updateReportLocation(currLocation) {
		if (currLocation && currLocation.length > 0) {
			this.alert.latitude = currLocation[0];
			this.alert.longitude = currLocation[1];
			this.$scope.$apply();
		}
	}
}

AlertGroupCtrl.$inject = [
	"Country",
	"Locator",
	"AuthService",
	"LocalStorage",
	"CountryLoc",
	"Alerts",
	"Group",
	"$scope"
];

angular.module('mfisheries.Alerts', [])
		.controller("AlertGroupCtrl", AlertGroupCtrl);