/**
 * Missing Person Report Controller
 *
 */
class AlertGroupMembersCtrl extends BaseController{
	constructor(Country,Locator, AuthService, LocalStorage, Group, GroupMemberService, $routeParams) {
		super(Country, Locator, AuthService, LocalStorage);
		this.Group = Group;
		this.GroupMemberService = GroupMemberService;

		if ($routeParams.groupid)this.groupid = $routeParams.groupid;
		else {this.groupid = -1; console.log("Invalid group accessed");}

		this.init();
	}

	init() {
		super.init();
		this.members = [];
	}
	
	setInfoName(){
		this.infoName = "Alert Group Members";
	}

	startAdmin(){
		console.log("Launching administrative interface for group members");
		this.getLocation();
		this.retrieveCountries(true).then(res => console.log(res), err => console.error(err));
		this.retrieveRecords();
		this.reset();
	}

	retrieveRecords(countryid) {
		console.log("Attempting to retrieve records for group: " + this.groupid);
		this.GroupMemberService.getById(this.groupid).then(res => {
			this.members = res.data;
			console.log("Received %s members from group %s", this.members.length, this.groupid);
		});
	}

	reset(){
		if (!this.currLocation)this.currLocation = ["",""];
		this.member = { };
	}

	deleteRec(report){
		swal({
			title: "Delete Confirmation",
			text: "Are you sure you want to delete this record. You will not be able to undo this operation",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Delete",
			closeOnConfirm: false
		}).then(res => {
			console.log(res);
			if (res.value) {
				this.GroupMemberService.delete(report).then(res => {
					if (res && res.status === 200) {
						swal("Deleted", "Member was successfully removed from the group", "success");
						this.retrieveRecords();
					} else swal("Failed", "Unable to remove member", "error");
				}, err =>{
					swal("Failed", "Unable to  remove member", "error");
					console.error(err);
				});
			}
		});
	}
}

AlertGroupMembersCtrl.$inject = [
	"Country",
	"Locator",
	"AuthService",
	"LocalStorage",
	'Group',
	'GroupMemberService',
	'$routeParams'
];

angular.module('mfisheries.Alerts')
		.controller("AlertGroupMembersCtrl", AlertGroupMembersCtrl);