class GroupMemberService extends BaseAPIService{
	/**
	 *
	 * @param $http
	 */
	constructor($http){
		super($http, "/api/groups");
	}

	// @Override
	get(groupid){
		 const url = this.base_url + "/"+ groupid + "/members";
		 console.log("Attempting to retrieve group members for: " + url);
		 return this.$http.get(url);
	}

	// @Override
	getById(id){
		return this.get(id);
	}

	add(data){ throw "Operation Not Available"; }
	update(data){ throw "Operation Not Available"; }

	delete(data){
		const url = this.base_url + "/" + data.usergroupid + "/members/" + data.userid;
		return this.$http.delete(url);
	}
}

GroupMemberService.$inject = [
	"$http"
];
angular.module('mfisheries.Alerts').service("GroupMemberService", GroupMemberService);