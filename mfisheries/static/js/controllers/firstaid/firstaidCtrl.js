class FirstAidCtrl extends BaseController{

    constructor(Country, Locator, AuthService, LocalStorage){
        super(Country, Locator, AuthService, LocalStorage);
        this.init();
    }


    init(){
        super.init();
        $(".mFish_menu").removeClass("active");
        $("#menu-fewer").addClass("active");
        $("#menu-emg-pro").addClass("active");
        this.retrieveCountries();

        this.files = [
            "info"
        ];

        this.modules = {};

        this.folders = [];

        this.primaryModal= "#firstaidModal";

    }

    setInfoName(){
        this.infoName = "First Aid";
    }

    startAdmin(){
        this.retrieveCountries();
        this.retrieveRecords();
        this.setEventListeners();
    }

    retrieveRecords(countryid){
        console.log("Retrieve records");
        // $.getJSON( "https://test.mfisheries.cirp.org.tt/static/country_modules/tobago/firstAid/module.json", function( data ) {
        //     var items = [];
        //     console.log(data);
        // });
        $.ajax({
            type: 'GET',
            url: 'static/country_modules/tobago/FirstAid/module.json',
            dataType: "json",
            success: (data) => {
                console.log(data);
                this.modules = data;
                console.log("Module data");
                console.log(this.modules);
                this.ProcessData(this.modules);
                // return FirstAidCtrl.modules;
            },
            error: (xhr, ajaxOptions, thrownError) => {
                console.log(xhr.status);
                console.log(thrownError);
            }
        });

    }

    ProcessData(data){
        console.log(data);
        this.folders = data.options;
        console.log("Options: "+ this.folders);
        this.records = this.folders.map(el =>{
            return {
                name: JSON.stringify(el).split("_").join(" "),
            };
        });
        console.log("Records: "+this.records);
    }

    playFirstAid(record){
        console.log("hello");
        console.log(record);
        var name = record.name.split(" ").join("_");
        console.log(name);
        var path = record.path;
        console.log(path);
        // var infoJson = this.files.map(el =>{
        //    return{
        //        name:el,
        //        type: "json",
        //        path: path+"/info.json"
        //    } ;
        // });
        var infoJson = $.get(path+'/info.json?callback=insertReply').then(function(data) {
            var blob = new Blob([data], { type: 'json' });
            return blob;
        });
        console.log("Hello");
        console.log(infoJson);


    }

    reset(){
        this.record = {};
    }
    displayEdit(rec){
        console.log(rec);
        super.displayEdit(rec);
    }

    setEventListeners(){
        $(".close").click(() => {
            console.log("close clicked by user");
            document.getElementById("audioPlayer").pause();
        });
    }
}

FirstAidCtrl.$inject = [
    "Country",
    "Locator",
    "AuthService",
    'LocalStorage',
];

angular.module('mfisheries.FirstAidCtrl',[])
    .controller("FirstAidCtrl",FirstAidCtrl);