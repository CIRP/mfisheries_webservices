class LEKCategory extends BaseAPIService{
	constructor($http){
		super($http, "api/lek/categories");
	}
}
LEKCategory.$inject = [
	"$http"
];

class LEKService extends FileBaseAPIService{
	constructor($http, fileupload){
		super($http,fileupload, "/api/lek");
	}
}
LEKService.$inject = [
	"$http",
	"fileupload"
];

angular.module('mfisheries.LEK')
	.service("LEKCategory", LEKCategory)
	.service("LEKService",LEKService)
	.service("GetLEK", function ($http) {

		const deffered = {};

		deffered.get = function (userid, type, offset) {
			console.log('/api/get/lek/' + userid + '/' + type + '/' + offset);
			return $http.get('/api/get/lek/' + userid + '/' + type + '/' + offset);
		};
		
		return deffered;
	})
	
	.service("getLekByCategory", function ($http) {

		const deffered = {};

		deffered.get = function (userid, type, category) {
			console.log('/api/lek/' + userid + '/' + type + '/' + category);
			return $http.get('/api/lek/' + userid + '/' + type + '/' + category);
		};
		
		return deffered;
	})
	
	.service("getLekByRange", function ($http) {

		const deffered = {};

		deffered.get = function (userid, type, category, countryid, day1, month1, year1, day2, month2, year2) {
			console.log('/api/lek/' + userid + '/' + type + '/' + category + '/' + countryid + "/" + day1 + '/' + month1 + '/' + year1 + '/' + day2 + '/' + month2 + '/' + year2);
			return $http.get('/api/lek/' + userid + '/' + type + '/' + category + '/' + countryid + '/' + day1 + '/' + month1 + '/' + year1 + '/' + day2 + '/' + month2 + '/' + year2);
		};
		
		return deffered;
	})
	
	.service("getLekByDate", function ($http) {

		const deffered = {};

		deffered.get = function (userid, type, category, day, month, year) {
			console.log('/api/lek/' + userid + '/' + type + '/' + category + '/' + day + '/' + month + '/' + year);
			return $http.get('/api/lek/' + userid + '/' + type + '/' + category + '/' + day + '/' + month + '/' + year);
		};
		
		return deffered;
	});