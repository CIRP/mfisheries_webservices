class LEKCtrl extends MapBaseController{

	constructor(Country, Locator, AuthService, LocalStorage, LEKCategory, LEKService, CountryLoc, $scope){
		super(Country, Locator, AuthService, LocalStorage, CountryLoc);
		if (this.debug)console.log("Initialized the LEK controller");
		this.localStorage = LocalStorage;
		this.LEKCategory = LEKCategory;
		this.LEKService = LEKService;
		this.$scope = $scope;

		this.primaryModal ="#lekModal";
		this.primaryForm = "newlekpost";
		this.viewModal = "#viewLEKDetailsModal";
		this.primaryTable = "lekreports-tbl";

		this.init();
	}

	init(){
		super.init();
		// Update menu
		$(".mFish_menu").removeClass("active");
		$("#menu-fewer").addClass("active");
		$("#menu-lek").addClass("active");
        this.primaryForm = "newlekpost";


        this.categories = [];
		this.isEdit = false;
		this.lek_loading = false;
	}

	setInfoName(){
		this.infoName = "Local Ecological Knowledge";
	}
	setMapId(){
		this.mapId = "map";
	}

	startAdmin(){
		if (this.debug)console.log("Starting Administrative functionality");
		this.getLocation();
		this.retrieveCountries().then(res => console.log(res), err => console.error(err));
		this.retrieveRecords();
		this.retrieveCategories();
		this.reset();

		this.all_countries = [];
		this.retrieveCountries(true).then(countries => this.all_countries = countries);
	}

	startView(){
		this.getLocation();
		this.retrieveRecords();
		this.retrieveCategories();
		this.retrieveCountries(true);
	}

	retrieveRecords(countryid){
		return new Promise((resolve, reject) => {
			this.lek_loading = true;
			let country  = (countryid) ? countryid : this.userCountry;
			
			this.LEKService.get(country).then(res => {
				console.log("Received %s LEK records for country %s", res.data.length, this.userCountry);
				this.records = res.data;
				this.lek_loading = false;
				resolve(this.records);
			}, err => {
				console.error(err);
				this.lek_loading  = false;
				reject(err);
			});
			
			this.retrieveCategories(country);// TODO Should be managed by the agent that requested the data
		});
	}

	reset(){
		this.record = {
			'countryid': this.currentUser.countryid,
			'userid': this.currentUser.userid,
			"aDate": new Date(),
			"latitude": this.currLocation[0],
			"longitude": this.currLocation[1],
			"filetype": "text",
			"filepath": "",
			"isPublic": 1,
			"category": "Unspecified"
		};
	}

	deleteRec(lek){
		console.log("Attempt to deleted: " + JSON.stringify(lek));
		swal({
			title: "Delete Confirmation",
			text: "Are you sure you want to delete this record. You will not be able to undo this operation",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Delete",
			closeOnConfirm: false
		}).then( res => {
			if (res.value) {
				this.LEKService.delete(lek).then(() => {
					swal("Deleted", "Record was successfully deleted", "success");
					this.records.splice(
							_.indexOf(this.records, _.find(this.records, rec => {
								return rec.id === lek.id;
							})), 1);
				});
			}
		});
	}

	update(record) {
		swal({
			title: "Update Confirmation",
			text: "Are you sure you want to update this record. You will not be able to undo this operation",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3e8f3e",
			confirmButtonText: "Update",
			closeOnConfirm: false
		}).then(res => {
			if (res.value) {
				this.LEKService.update(record).then(res => {
					$(this.primaryModal).modal('hide');
					swal("Updated", "Record was successfully updated", "success");
					reset(); // Reset the model container for next operation
				}, err => {
					console.error(err);
					swal("Update Confirmation", "Unable to update record. If error persists notify administrator", "error");
				});
			}
		});
	}

	save(record, file){
		console.log("Attempt to save: " + JSON.stringify(record));
		console.log(file);

		record.aDate = (new Date(record.aDate)).getTime();

		// If record contains an id then Update record
		if (record.id && record.id > 0) {
			this.update(record); // TODO Update cannot update a attachment
		}
		// Else we are attempting to save a new damage report
		else {
			// if we select another file type
			if (record.filetype !== "text") {
				if (!this.file.name){ // if no file uploaded
					swal("LEK", "Selection of attachment is required for "+ record.filetype, "error");
					return;
				}
				// Uploading record with attachment
				$("#spinning_upload").show();
				this.LEKService.save(record, this.file).then(res => {
					swal("LEK", "LEK Report was successfully created", "success");
					$(this.primaryModal).modal('hide');
					$(this.primaryForm).reset();
					this.retrieveRecords();
					this.reset();
				}, err => {
					swal("LEK", "Unable to save LEK report. If error persist contact administrator for support.", "error");
					console.error(err);
				});
			}
			// Upload record if it was text
			else if (record.filetype === "text"){
				this.LEKService.save(record).then(res => {
					swal("Report", "LEK Report was successfully created", "success");
					$(this.primaryModal).modal('hide');
					this.retrieveRecords();
					this.reset();
				}, err => {
					swal("LEK", "Unable to save LEK report. If error persist contact administrator for support.",  "error");
					console.error(err);
				});
			}
		}

	}

	retrieveAndUpdateCurrentLocation(){
		if (this.debug)console.log("Attempting to Update Form with current location");
		swal({
			type: 'info',
			title: 'Retrieving Location',
			text: 'Attempting to retrieve current location',
			onOpen: () => {
				swal.showLoading();
			}
		});

		// Attempting to force the system to refresh location
		this.getLocation(loc => {
			swal.close();
			if (loc) {
				this.record.latitude = loc[0];
				this.record.longitude = loc[1];
			}else{
				swal("Location", "Unable to retrieve location. Ensure browser has permission to acquire your current location.","error");
			}

		}, true);

	}

	// Category Operations
	retrieveCategories(countryid){
		this.lek_cat_loading = true;
		let country  = (countryid) ? countryid : this.userCountry;

		this.LEKCategory.get(country).then(res => {
			console.log("Received %s LEK categories for country %s", res.data.length, this.userCountry);
			this.categories = res.data;
			this.lek_cat_loading = false;
		}, err => {
			console.error(err);
			this.lek_cat_loading = false;
		});
	}

	displayAttachment(record){
	  this.record = record;
	  console.log(record);
	  console.log(record.filepath);
	  const modelModal = $("#contentModal");
	  modelModal.modal('show');
	  modelModal.updatePolyfill();
	}

	resetCategory(){
		this.category = {
			'countryid': this.currentUser.countryid,
			'createdby': this.currentUser.userid
		};
	}

	addCategory(){
		this.resetCategory();
		$("#categoryModal").modal('show');
	}

	editCategory(category){
		this.isEdit = true;
		this.category = category;
		$("#categoryModal").modal('show');
	}

	deleteCategory(category){
		swal({
			title: "Delete Confirmation",
			text: "Are you sure you want to delete this record. You will not be able to undo this operation",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Delete",
			closeOnConfirm: false
		}).then( res => {
			if (res.value) {
				this.LEKCategory.delete(category).then(() => {
					swal("Deleted", "Record was successfully deleted", "success");
					this.categories.splice(
						_.indexOf(this.categories, _.find(this.categories, rec => {
							return rec.id === category.id;
						})), 1);
				});
			}
		});
	}

	saveCategory(){
		console.log("Attempting to create: %s", JSON.stringify(this.category));
		if (this.category.id){
			swal({
				title: "Update Confirmation",
				text: "Are you sure you want to update this record. You will not be able to undo this operation",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#3e8f3e",
				confirmButtonText: "Update",
				closeOnConfirm: false
			}).then(res => {
				if (res.value) {
					this.LEKCategory.update(this.category).then(res => {
						$(this.primaryModal).modal('hide');
						swal("Updated", "Record was successfully updated", "success");
						this.resetCategory(); // Reset the model container for next operation
					}, err => {
						console.error(err);
						swal("Update Confirmation", "Unable to update record. If error persists notify administrator", "error");
					});
				}
			});

		}else { // Record has no ID so we are creating a new Category
			let sim = this.categories.filter(el => {
				return el.countryid === this.category.countryid &&
						el.name.toLowerCase() === this.category.name.toLowerCase();
			});

			if (sim.length < 1) {
				this.LEKCategory
						.add(this.category)
						.then(() => {
							swal("Category", "Category Successfully Created", "success");
							$("#categoryModal").modal('hide');
							this.retrieveCategories();
							this.resetCategory();
						}, res => {
							swal("Category", "Error creating category!", "error");
							console.error(res);
						});
			} else {
				swal("Category", "Unable to create category. Category exists", "error");
			}
		}
	}

    displayAdd(){
        console.log("Adding new record");
        super.displayAdd();
    }

	updateReportLocation(location){
		if (!location)location = this.currLocation;
		super.updateReportLocation(location);
		this.$scope.$apply();
	}
}

LEKCtrl.$inject = [
	"Country",
	"Locator",
	"AuthService",
	"LocalStorage",
	"LEKCategory",
	"LEKService",
	"CountryLoc",
	"$scope"
];

angular.module('mfisheries.LEK', [])
		.controller("LEKCtrl", LEKCtrl);