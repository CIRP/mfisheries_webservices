class PodcastCtrl extends BaseController{

    constructor(Country, Locator, AuthService, LocalStorage){
        super(Country, Locator, AuthService, LocalStorage);
        this.init();
    }

    init(){
        $(".mFish_menu").removeClass("active");
        $("#menu-fewer").addClass("active");
        $("#menu-emg-pro").addClass("active");
        this.retrieveCountries();

        this.files = [
            "emergency_maintenance",
            "fishing_methods",
            "handling_of_fish",
            "preperation_for_sea",
            "rule_of_the_road",
            "survival_at_sea"
        ];

        this.primaryModal= "#podcastModal";

    }

    setInfoName(){
        this.infoName = "Podcasts";
    }

    startAdmin(){
        this.retrieveCountries();
        this.retrieveRecords();
        this.setEventListeners();
    }

    startView() {
        console.log("Starting View");
        this.retrieveCountries();
        this.retrieveRecords();
        this.setEventListeners();
    }

    retrieveRecords(countryid){
        this.records = this.files.map(el =>{
            return {
                name: el.split("_").join(" "),
                type: "audio",
                path: "http://test.mfisheries.cirp.org.tt/static/country_modules/tobago/Podcasts/" + el +".mp3"
            };
        });
    }

    reset(){
        this.record = {};
    }
    displayEdit(rec){
        console.log(rec);
        super.displayEdit(rec);
    }

    setEventListeners(){
        $(".close").click(() => {
            console.log("close clicked by user");
            document.getElementById("audioPlayer").pause();
        });
    }
}

PodcastCtrl.$inject = [
    "Country",
    "Locator",
    "AuthService",
    'LocalStorage',
];

angular.module('mfisheries.Podcasts',[])
    .controller("PodcastCtrl",PodcastCtrl);