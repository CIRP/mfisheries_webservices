
class DashboardCtrl{
	constructor(){
		console.log("Dashboard constructor was initialized");
		this.init();
	}
	
	init(){
		$(".mFish_menu").removeClass("active");
		$("#menu-fewer").addClass("active");
	}
}

DashboardCtrl.$inject = [];

angular.module('mfisheries.controllers')
	.controller("DashboardCtrl", DashboardCtrl);