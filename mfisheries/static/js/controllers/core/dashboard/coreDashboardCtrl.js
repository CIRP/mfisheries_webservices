
class coreDashboardCtrl{
    constructor(){
        console.log("Core Dashboard constructor was initialized");
        this.init();
    }

    init(){
        $(".mFish_menu").removeClass("active");
        $("#menu-fewer").addClass("active");
    }
}

coreDashboardCtrl.$inject = [];

angular.module('mfisheries.controllers')
    .controller("coreDashboardCtrl", coreDashboardCtrl);