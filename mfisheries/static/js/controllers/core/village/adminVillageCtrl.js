"use strict";

// TODO - Villages - Change Implementation to OOP
angular.module('mfisheries.Village', [])
	
	.controller('adminVillageCtrl', [
		"$scope", "LocalStorage", "Villages", "Country", 'fileupload', 'AuthService',
		function ($scope, LocalStorage, Villages, Country, fileupload, AuthService) {
			$(".mFish_menu").removeClass("active");
			$("#menu-villages").addClass("active");
			$("#menu-meta").addClass("active");
			
			$scope.villages = [];
			$scope.countries = [];
			$scope.readOnly = false;
			
			AuthService.attachCurrUser($scope);
			
			function reset() {
				$scope.village = {};
			}
			
			function retrieveVillages() {
				Villages.get($scope.userCountry).then(res => {
					$scope.villages = res.data;
				}, err => {
					console.log("Unable to retrieve records:" + err);
				});
			}
			
			// Retrieve Countries
			function retrieveCountries() {
				Country.get($scope.userCountry).then(res => {
					$scope.countries = res.data;
				}, err => {
					console.log("Unable to retrieve records:" + err);
				});
			}
			
			reset();
			retrieveVillages();
			retrieveCountries();
			
			$scope.edit = function (village) {
				$scope.village = village;
			};
			
			$scope.displayEdit = function (village) {
				$scope.village = village;
				$('#editvillageModal').modal('show');
			};
			
			$scope.delete = function (village) {
				swal({
					title: "Delete Confirmation",
					text: "Are you sure you want to delete this record. You will not be able to undo this operation",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete",
					closeOnConfirm: false
				}).then(res => {
					if (res.value) {
						Villages.delete(village).then(function (res) {
							if (res && res.status === 200) {
								swal("Deleted", "Record was successfully deleted", "success");
								retrieveVillages();
							}
							else
								swal("Failed", "Unable to delete record", "error");
						});
					}
				});
			};
			
			$scope.save = function (village) {
				console.log(village);
				if (village.id && village.id > 0) {
					swal({
						title: "Update Confirmation",
						text: "Are you sure you want to update this record. You will not be able to undo this operation",
						type: "warning",
						showCancelButton: true,
						confirmButtonText: "Update",
						closeOnConfirm: false
					}).then(res => {
						if (res.value) {
							Villages.update(village).then(function (res) {
								if (res && res.status === 200) {
									swal("Updated", "Record was successfully updated", "success");
									const c = _.find($scope.countries, function (country) {
										return country.id === parseInt(village.countryid);
									});
									village.country = c.name;
									$("#editvillageModal").modal('hide').on('hidden', $scope.reset);
								} else
									swal("Failed", "Unable to update record", "error");
							});
						}
					});
				}
			};
			
			$scope.add = function () {
				$("#villageModal").modal('show');
			};
			
			$scope.addNewVillage = function (village) {
				Villages.add(village).then(function (res) {
					console.log(res);
					if (res.data && res.data.status === 200) {
						swal({
							'title': "Created",
							'type': 'success',
							'text': "Village successfully created",
							closeOnConfirm: true
						}).then( res => {
							if (res.value) {
								retrieveVillages();
							}
						});
					} else {
						swal("Error", "Unable to save village", "error");
					}
				});
			};
			
			// gets the template to ng-include for a table row / item
			$scope.getTemplate = function (village) {
				if ($scope.village && $scope.village.id === village.id) return 'edit';
				return 'display';
			};
			
			$scope.showAddVillage = function (type) {
				if (type === 1) {
					$("#uploadVillage").hide();
					$("#addVillage").show('slow');
				} else if (type === 2) {
					$("#addVillage").hide();
					$("#uploadVillage").show('slow');
				}
			};
			

			$scope.submitUploadVillage = function (village) {
				const file = $scope.myFile;
				const uploadUrl = '/api/upload/villages';
				const data = {
					countryid: village.countryid
				};
				if (file) {
					$("#spinning_upload").show();
					fileupload.uploadFileToUrl(file, data, uploadUrl).then(function (data) {
						console.log(data);
						console.log(data.data);
						if (data.data.status === 200) {
							swal({
								title: "Uploaded Module",
								type: "success",
								text: "File was successfully uploaded",
								closeOnConfirm: true
							}, function () {
								window.location.reload();
							});
						} else {
							swal({
								title: "Uploading Module",
								type: "error",
								text: "Unable to Upload File"
							}).then(()=> {
								$("#countryModuleUploadModal").modal('hide');
							});
						}
						delete $scope.cmodule;
					});
				} else {
					swal("Uploading Module", "Please select File Before uploading", "error");
				}
			};
			
			
		}]);