angular.module('mfisheries.services',[])
	
	.service("fileupload", function($http){
		const deffered = {};
		
		deffered.uploadFileToUrl  = function(file, data,  uploadUrl, name_file){
			if (!name_file)name_file = "upload";
			const fd = new FormData();
			fd.append('file', file);
			for (let d in data){
				if (data.hasOwnProperty(d))
					fd.append(d, data[d]);
			}
			return $http.post(uploadUrl, fd, {
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined }
			});
		};

		deffered.saveRecordWithFile = function(file, data,  uploadUrl, name_file){
			console.log("Attempting to save record with file");
			return deffered.uploadFileToUrl(file, data,  uploadUrl, name_file);
		};

		deffered.updateRecordWithFile = function(file, data,  uploadUrl, name_file){
			console.log("Attempting to update record with file");
			if (!name_file)name_file = "upload";
			const fd = new FormData();
			fd.append('file', file);
			for (let d in data){
				if (data.hasOwnProperty(d))
					fd.append(d, data[d]);
			}
			return $http.put(uploadUrl, fd, {
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined }
			});
		};

		return deffered;
	});