'use strict';
// Adding Firebase authentication to the auth service

angular.module('mfisheries.factories', [])
	.factory("AuthService", function ($http, Session, USER_ROLES, LocalStorage, $firebaseAuth) {
		const authService = {};
		
		authService.login = function (credentials) {
			return new Promise((resolve, reject) => {
				$http
						.post('api/user/login', credentials)
						.then(res => {

							if (res.data.data.fb_token){
								let token = res.data.data.fb_token;

								$firebaseAuth().$signInWithCustomToken(token).then(fbUser => {
									console.log(fbUser);
								}).catch(err => console.log(err));
							}
							// Send the result
							resolve(
									Session.create(
										res.data.data.id,
										res.data.data.fname,
										res.data.data.lname,
										res.data.data._class,
										res.data.data.country,
										res.data.data.countryid
									)
							);
						}, reject);
			});
		};
		
		authService.addGoogleUser = function (data) {
			return new Promise((resolve, reject) => {
				$http
						.post('/api/add/google/user', data)
						.then(res => {
							if (res.data.status === 200) {
								Session.create(
										res.data.data.id,
										res.data.data.id,
										res.data.data.fname,
										res.data.data.lname,
										res.data.data._class,
										res.data.data.country);

								resolve(Session.user);
							} else {
								reject(res.data);
							}
						}, reject);
			});


		};
		
		authService.hasUser = function (user){
			return new Promise((resolve, reject) => {
				$http.get('api/user/username/' + user.username).then(res =>{
					resolve({
						'result': true,
						'data': res
					}, res);
				}, err => {
					resolve({
						"result": false,
						'data': err
					});
				});
			});
		};
		
		authService.isAuthenticated = function () {
			return Session.getSession();
		};
		
		authService.isAuthorized = function (authorizedRoles) {
			if (!angular.isArray(authorizedRoles)) {
				authorizedRoles = [authorizedRoles];
			}
			return (authService.isAuthenticated() &&
				authorizedRoles.indexOf(Session.user.userRole) !== -1);
		};
		
		authService.setupCtrlACL = function (currUser, controller) {
			
			// Retrieve role using role id from current user
			let roleKey = Object.keys(USER_ROLES).filter(key => {
				let r = USER_ROLES[key];
				return currUser.userRole === r.code;
			});
			let role = USER_ROLES[roleKey[0]];
			// console.log(role);
			
			// Set the role to the scope to give flexibility for more ng-if operations
			controller.role = role;
			
			// Enable Read Only config
			controller.readOnly = role.scope.privilege === "read-only";
			// console.log(role);
			// Set the Country ID based on user country id or 0 for global privileges
			controller.userCountry = role.scope.visibility === "global" ? 0 : currUser.countryid;
			currUser.userCountry = controller.userCountry;
			
			controller.currentUser = currUser;
		};
		
		/**
		 * Attempts to add the current user as an attribute of the controller
		 * @param controller
		 * @returns {Promise.<T>}
		 */
		authService.attachCurrUser = function (controller) {
			const currUser = LocalStorage.getObject('user');
			// TODO Check to ensure user is logged in
			if (currUser) {
				authService.setupCtrlACL(currUser, controller);
			}
			return Promise.resolve(currUser);
		};
		
		return authService;
	});
