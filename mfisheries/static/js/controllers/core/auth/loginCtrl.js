"use strict";
angular.module('mfisheries.controllers', [])
	.controller('LoginCtrl', [
		'$scope', '$rootScope', '$window', 'fileupload', 'AUTH_EVENTS', 'AuthService','Locator',
		function ($scope, $rootScope, $window, fileupload, AUTH_EVENTS, AuthService, Locator) {
			
			$(".mFish_menu").removeClass("active");
			$("#menu-country").addClass("active");
			
			$scope.user = {};
			
			$scope.checkStatus = function () {
				if (AuthService.isAuthenticated()) console.log("User is authenticated");
				else console.log("User is not authenticated");
			};
			
			$scope.login = function () {
				console.log("Attempting to log in");
				displayLoading("Logging In").then(loading =>{
					AuthService.login($scope.user).then(res => {
							// Using broadcast strategy for handling login. Check main.js 213 - 240
							$rootScope.$broadcast(AUTH_EVENTS.loginSuccess, {"userRole": res.userRole});
							$scope.setCurrentUser(res);
							if (loading)loading.close();
						}, error => {
							if (loading)loading.close();
							swal("Login", "Username or password invalid!", "error");
							$rootScope.$broadcast(AUTH_EVENTS.loginFailed);
							console.error(error);
						}
					);
				});
			};
			
			/**
			 * Displays the a control-less loading message dialog.
			 * Clients that use the function should use the extent and call close
			 * method on the response to dismiss message
			 * @param message
			 * @returns {Promise}
			 */
			function displayLoading(message){
				swal({
					type: "info",
					title: "Login",
					text: message,
					allowOutsideClick: false,
					allowEscapeKey: false,
					showConfirmButton: false,
					onOpen: function () {
						swal.showLoading();
					}
				});
				
				return Promise.resolve(swal);
			}
			
			/**
			 * Retrieve the users current location when the page is loaded.
			 * This is needed for first time registration through Google.
			 */
			function getCurrentLocation(){
				console.log("Attempting to Retrieve current location");
				return new Promise((resolve, reject) => {
					Locator.retrieveLastLocation().then(res => {
						// Used cached location
						resolve(res);
					}, err => { // No location previously cached
						console.log("No location previously cached. Attempting to retrieve location");
						// Retrieve current Location
						Locator.retrieveCurrentLocation().then(res => {
							// Current location received
							resolve(res);
						}, err => {
							// Error attempting to retrieve location
							console.log("We were unable to determine the user's location");
							reject(err);
						});
					});
				});
			}
			
			function resolveCountryNameToId(name){
			
			}
			
			// resolveCountryNameToId("Trinidad and Tobago");
			
			
			
			// Keeping Track of Attempts to connect to Google's Services
			let attempt = 0;
			const max_attempts = 2;
			
			function setUpGoogleSignIn() {
				attempt++;
				// If Google API service is available
				if ($window.gapi) {
					// Based on https://developers.google.com/identity/sign-in/web/incremental-auth
					$window.gapi.load('auth2', function () {
						// Retrieve the singleton for the GoogleAuth library and set up the client.
						$window.gapi.auth2.init({
							client_id: '542703593370-mj6tonokc8jv3igsjfaan5dtlf3sl2ml.apps.googleusercontent.com',
							cookiepolicy: 'single_host_origin'
							// scope: 'profile email'
						}).then(googleAuth => {
							console.log("Successfully initialized auth");
							attachSignIn(document.getElementById('customBtn'), googleAuth);
						}, err => console.error(err));
					});
				} else { // Google Service was not loaded into the browser
					console.log("Unable to Load Google API library");
					if (attempt < max_attempts) {
						console.log("Retrying");
						// Retry Loading Google Services
						$window.setTimeout(setUpGoogleSignIn, 1000);
					} else {
						console.log("Failed to load Google API library. Max Attempts exceeded");
						// swal("Login", "Google Login not available at this time. ", "error");
					}
				}
				
				function attachSignIn(btnElement, googleAuth) {
					// enable to button to allow the user to sign-in with Google
					// btnElement.disabled = false;
					
					// https://developers.google.com/identity/sign-in/web/reference#googleauthattachclickhandlercontainer-options--onsuccess-onfailure
					// parameters - (container, options, onsuccess, onfailure)
					googleAuth.attachClickHandler(btnElement, {}, googleUser => {
						// Useful data for your client-side scripts:
						const profile = googleUser.getBasicProfile();
						console.log(profile);
						const name = profile.getName().split(" ");
						// build data for the request to be sent to create user
						
						const data = {
							"username": profile.getEmail(),
							"password": profile.getId(),
							'fname': name.slice(0, name.length-1).join(" "),
							'lname': name[name.length -1 ],
							'email': profile.getEmail(),
							'login-type': 'google-web'
						};
						// convert data to the format expected (based on data submitted by mobile app)
						const obj = {
							"data": data,
							"modules": [],
						};
						
						// Check if we have a user with the current user name
						AuthService.hasUser(obj).then(res => {
							console.log(res);
							if (!res.result){ // No previous user with this email
								// Request user location
								
								// Send request to the Server to create user
								AuthService.addGoogleUser(obj).then(
									res => {
										console.log(res);
										
										if (res.status === 201) {
											$rootScope.$broadcast(AUTH_EVENTS.newgoogleUser, {"link": res.modules});
										} else {
											$rootScope.$broadcast(AUTH_EVENTS.loginSuccess, {"userRole": res.userRole});
										}
										$scope.setCurrentUser(res);
									},
									error => {
										swal("Login", "Unable to Sign In user", "error");
										$rootScope.$broadcast(AUTH_EVENTS.loginFailed);
										console.error(error);
									}
								);
							}else{
								$rootScope.$broadcast(AUTH_EVENTS.loginSuccess, {"userRole": res.userRole});
								$scope.setCurrentUser(res);
							}
						});
					},
					error => {
						console.error(error);
						swal("Login", "Unable to Log In with Google Account", "error");
					});
				}
			}
			
			// Wait for a second then Load Google Services
			$window.setTimeout(setUpGoogleSignIn, 1000);
			getCurrentLocation();
			
		}]);

