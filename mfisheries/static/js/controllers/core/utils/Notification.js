
class PushNotify{

	constructor(LocalStorage){
		this.LocalStorage = LocalStorage;
		this.fb_messaging = firebase.messaging();
		this.init();
	}

	init(){
		this.initNotify();
	}

	initNotify() {
		return new Promise((resolve, reject) => {
			this.fb_messaging.usePublicVapidKey(mFish.mf_fb_config.messagingWebPushKey);
			this.fb_messaging.requestPermission().then(res => {
				// Make initial request for messaging token
				this.fb_messaging.getToken().then(currentToken => {
					if (currentToken) {
						this.updateToken(currentToken);
						resolve(currentToken);
					}
					else {
						this.notifyWhyPermissionRequired();
						reject(currentToken);
					}
				}).catch(err => { // Unable to receive token from the server
					swal("Notification", "Unable to communicate with the firebase notification server. Contact administrator for assistance", "error");
					console.error(err);
					reject(err);
				});
			}).catch(err => {
				console.error("Unable to receive permission to notify user: " + err);
				reject(err);
			});
			this.initNotifyListener();
			this.initMessageListener();
		}) ;

	}

	updateToken(currentToken) {
		return this.LocalStorage.save("fb_msg_token", currentToken);
	}

	getToken(){
		return new Promise((resolve, reject) => {
			this.LocalStorage.get("fb_msg_token").then(res => {
				resolve(res);
			}, () => {
				this.initNotify(); // Attempt to request for permissions to retrieve notification token
			});
		});
	}


	notifyWhyPermissionRequired() {
		// TODO - Notification - Use swal to inform user and ask if to request again
	}

	initNotifyListener(){
		this.fb_messaging.onTokenRefresh(() => {
			this.fb_messaging.getToken().then(currentToken => {
				if (currentToken) this.updateToken(currentToken);
			}).catch(err => console.error(err));
		});
	}

	initMessageListener() {
		this.fb_messaging.onMessage(payload => {
			console.log("Received: ");
			console.log(payload);
			swal("Notification", payload, "info");
		});
	}
}

PushNotify.$inject = [
	"LocalStorage"
];
angular.module('mfisheries.services').service("PushNotify", PushNotify);