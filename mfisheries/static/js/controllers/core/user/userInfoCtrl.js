"use strict";
//userInfoCtrl.js
angular.module('mfisheries.User')
.controller('UserInfoCtrl',
	['$scope', '$routeParams', '$rootScope', 'LocalStorage', '$location', 'AUTH_EVENTS', 'Villages', 'Occupations', 'UserProfile', 'Country', 'Session',
		function($scope, $routeParams, $rootScope, LocalStorage, $location, AUTH_EVENTS, Villages, Occupations, UserProfile, Country, Session){

	console.log("User Info Controller");
	// Extract modules from the URL parameters
	// var modules = _.without($routeParams.modules.match(/[a-z'\-]+/gi),'modules'),
	// let modules = _.without($routeParams.modules.match(/[a-z'\-]+/gi), 'modules');
	// const userid = $routeParams.modules.split('userid')[1].split('_')[1];
	// let needOccupations = false,
	// 	needVillages = false;
	//
	// // console.log("UserId received: "+ userid);
	// // const tmp = $localstorage.getObject('user');
	// // if (tmp){
	// // 	if (tmp.userRole === 2 || tmp.userRole === 1){
	// // 		// $scope.readOnly = true;
	// // 		$location.path("/user/profile?userid="+userid);
	// // 	}else{ // redirect to user profile page
	// // 		$location.path("/user/profile");
	// // 	}
	// // }
	//
	//
	// $scope.occupations = [];
	// $scope.villages = [];
	// $scope.user = {};
	// $scope.vessel = "no";
	//
	// let user, countryID;
	//
	// UserProfile.get(userid).then(function(res){
	// 	if (res.data.status === 200) {
	// 		const rec = UserProfile.convert(res.data.data);
	// 		user = rec.user;
	// 		countryID = rec.selectedCountry;
	// 		// Retrieve Countries
	// 		Country.get().then(function (res) {
	// 			$scope.countries = res.data.data;
	// 			// When Country Completed Retrieve Occupations
	// 			Occupations.get(countryID).then(function (res) {
	// 				$scope.occupations = res.data.data;
	//
	// 				//When Occupation Completed Retrieve Villages
	// 				Villages.get(countryID).then(function (res) {
	// 					$scope.villages = res.data.data;
	//
	// 					//After Villages loaded we set the data in the scope
	// 					$scope.user = user;
	// 					$scope.selectedCountry = countryID;
	// 					if (user.standard && user.standard.vessel){
	// 						$scope.vessel = "yes";
	// 					}
	// 				});
	// 			});
	// 		});
	// 	}else{
	// 		console.log("Unable to find user with ID: " + userid);
	// 	}
	// });
	//
	//
	// modules = _.map(modules, function(el){
	// 	return el.toLowerCase();
	// });
	//
	// if($.inArray('photodiary', modules) > -1 || $.inArray('sos', modules) > -1 || $.inArray('navigation', modules) > -1 || $.inArray('alerts', modules) > -1 ){
	// 	needOccupations = true;
	// }
	//
	// if($.inArray('sos', modules) > -1 || $.inArray('alerts', modules) > -1 || $.inArray('navigation', modules) > -1){
	// 	needVillages = true;
	// }
	//
	// //http://underscorejs.org/#each
	// $scope.modules = {};
	// _.each(modules, function(m){
	// 	$scope.modules[m] = m;
	// });
	//
	// $scope.onCountryChange = function(countryid){
	// 	console.log(countryid);
	// 	if (needOccupations){
	// 		Occupations.get(countryid).then(function(res){
	// 			console.log(res.data.data);
	// 			$scope.occupations = res.data.data;
	// 		});
	// 	}
	// 	if (needVillages){
	// 		Villages.get(countryid).then(function(res){
	// 			$scope.villages = res.data.data;
	// 		});
	// 	}
	// };
	//
	// $scope.onVesselChange = function(){
	//
	// };
	// $scope.submit = function(){
	// 	$scope.submitted = true;
	// 	console.log("Retrieved User information from the form:");
	// 	console.log($scope.user);
	// 	let hasInfo = true;
	//
	// 	// Add additional logic for other modules that require occupation information
	// 	if($.inArray('photodiary', modules) > -1 || $.inArray('sos', modules) > -1 || $.inArray('navigation', modules) > -1 || $.inArray('alerts', modules) > -1 ){
	// 		if(typeof $scope.user.basic.occupations === 'undefined'){
	// 			swal("Missing Data!", "Looks like you forgot to enter your Occupation/s", "error");
	// 			hasInfo = false;
	// 		}
	// 	}
	// 	// Additional Information for standard profile
	// 	if($.inArray('alerts', modules) > -1 || $.inArray('sos', modules) > -1 ){
	// 		if(!$scope.user.standard){
	// 			hasInfo = false;
	// 			swal("Missing Data!", "Looks like you forgot to enter your Standard Profile Information", "error");
	// 		}
	// 	}
	// 	// Additional information that required for profile information
	// 	if($.inArray('sos', modules) > -1 || $.inArray('navigation', modules) > -1){
	// 		if(!$scope.user.full){
	// 			hasInfo = false;
	// 			swal("Missing Data!", "Looks like you forgot to enter your Full Profile Information", "error");
	// 		}
	// 	}
	//
	// 	// If the previous validation steps have proven to be successful
	// 	if(hasInfo){
	// 		$.each($scope.user, function(index, data){
	// 			console.log("Processing: " +index);
	//
	// 			if ($scope.user[index]){
	// 				// For Each of the items within the user object (retrieved from the form) we will add the user id
	// 				$scope.user[index].userid = userid;
	//
	// 				// Basic information
	// 				if(index === 'basic'){
	// 					console.log("Retrieving Basic Information");
	//
	// 					// Use jquery to get the selected items
	// 					const occ = [];
	// 					$(".occupation_options").filter(":checked").each(function(i, el){
	// 						console.log($(el).val());
	// 						occ.push($(el).val());
	// 					});
	// 					$scope.user[index].occupations = occ;
	//
	// 				}
	//
	// 				// Standard Information
	// 				if(index === 'standard'){
	// 					console.log("Retrieving Standard Information");
	// 					console.log($scope.vessel);
	// 					if($scope.vessel === true || $scope.vessel === "yes"){
	// 						$scope.user[index].hasvessel = "yes";
	// 					} else{
	// 						$scope.user[index].hasvessel = "no";
	// 					}
	// 				}
	// 			}
	//
	// 		});
	//
	// 		console.log("User information After Processing");
	// 		console.log($scope.user);
	// 		UserProfile.post($scope.user).then(function(res){
	// 			// Set current user
	// 			console.log("After sending information to the server we received: ");
	// 			console.log(res);
	// 			if (res.data.status === 200){
	// 				// Broadcast that login was successful
	// 				if (res.data){
   //          console.log(res.data);
	// 					Session.create(
	// 							res.data.data.id,
	// 							res.data.data.id,
	// 							res.data.data.fname,
	// 							res.data.data.lname,
	// 							res.data.data._class,
   //              res.data.country
	// 					);
	// 					$rootScope.$broadcast(AUTH_EVENTS.loginSuccess, {"userRole":res.data.data._class});
	// 				}
	// 				else
	// 					$rootScope.$broadcast(AUTH_EVENTS.loginSuccess, null);
	// 			}else{
	// 				swal("Registration", "Unable to successfully complete registration process", "error");
	// 			}
	// 			// console.log("Setting Current User to: " + res.userid);
	// 			// console.dir(res);
	// 			$scope.setCurrentUser(res);
	// 		});
	// 	}
	// };

}]);
