// angular.module('mfisheries.User')
// 	.service("User", ["$http", function($http){
// 		let user = {};
// 		user.get = function(countryid){
// 			if (countryid && countryid !== 0){
// 				return $http.get('/api/users/country/' + countryid);
// 			}else{
// 				return $http.get('/api/users');
// 			}
// 		};
// 		user.add = function(data){
// 			return $http.post('/api/users', data);
// 		};
// 		user.delete = function(userid){
// 			return $http.delete('/api/users/'+userid);
// 		};
// 		user.update = function(data){
// 			return $http.put('/api/users/'+data.id, data);
// 		};
// 		return user;
// 	}]);

class User extends BaseAPIService{
	/**
	 *
	 * @param $http
	 */
	constructor($http){
		super($http, "/api/users");
	}
}

User.$inject = [
	"$http"
];
angular.module('mfisheries.User').service("User", User);
