'use strict';

//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes
class BaseAPIService{
	/**
	 *
	 * @param $http
	 * @param url
	 */
	constructor($http, url){
		this.$http = $http;
		this.base_url = url;
		this.debug = true;
	}
	
	/**
	 *
	 * @param countryid
	 */
	get (countryid) {
		const options = {};
		if (countryid)options.countryid = countryid;
		return this.$http.get(this.base_url, {params:options});
	}
	
	/**
	 *
	 * @param id
	 */
	getById(id){
		return this.$http.get(this.base_url +"/"+id);
	}
	
	/**
	 *
	 * @param data
	 */
	add(data){
		return this.$http.post(this.base_url, data);
	}
	
	save(data){
		return this.add(data);
	}
	
	/**
	 *
	 * @param source
	 */
	update(source) {
		return this.$http.put(this.base_url + "/" + source.id, source);
	}
	
	// noinspection ReservedWordAsName
	/**
	 *
	 * @param source
	 */
	delete(source){
		return this.$http.delete(this.base_url+ "/" + source.id);
	}

	deleteRec(source){
		return this.delete(source);
	}
}

class FileBaseAPIService extends BaseAPIService{
	constructor($http, fileupload, url){
		super($http, url);
		this.fileupload = fileupload;
	}
	
	add(data, file, file_name){
		console.log(file);
		// If we have no file, perform traditional save operation
		if (!file) return super.add(data);
		// if no filename specified, we create one
		if (!file_name ) file_name = "file_userid_" + data.userid + "_" + file.name;
		// We attempt to save record using utility function
		return this.fileupload.saveRecordWithFile(
				file,
				data,
				this.base_url,
				file_name
		);
	}

	save(data, file, file_name) {
		return this.add(data, file, file_name);
	}
}

class BaseController{
	constructor(Country, Locator, AuthService, LocalStorage){
		this.Country = Country;
		this.locator = Locator;
		this.AuthService = AuthService;
		this.localStorage = LocalStorage;
		this.infoName = "BaseController";
		this.primaryModal = "#modelModal";
		this.debug = true;
		this.setInfoName();
		this.cacheTTL = 2 * 24 * 60 * 60 *1000; // day * hr * min * sec * milli
		this.primaryTable = "";
		this.viewModal = "";
		this.currentUser = undefined;
		this.primaryForm = "";
		this.autoLoadRecords = true;
	}
	
	init(){
		this.countries = [];
		this.currLocation = ["", ""];
		this.userCountry = 0;
		this.isEdit = false;
		
		this.records = [];
		this.record = {};
		this.AuthService.attachCurrUser(this);
	}
	
	handleUserLoggedIn(currUser){
		if (this.debug)console.log("User is logged in: " + JSON.stringify(currUser));
	}
	
	handleUserNotLoggedIn(){
		if (this.debug)console.log("User is not logged in");
		// window.location.hash = "#";
	}

	retrieveCountries(display, refresh) {
		if (refresh === undefined)refresh = false;

		return new Promise((resolve, reject) => {
			let clt_key = "country_last_time";
			let country_key = "countries";
			if (this.currentUser){
				clt_key += "_" + this.currentUser.id;
				country_key += "_" + this.currentUser.id;
			}
			if (this.userCountry){
				clt_key += "_" + this.userCountry;
				country_key += "_" + this.userCountry;
			}


			let last_time = this.localStorage.get(clt_key);
			if (!last_time)refresh = true;
			else if ((Date.now() - parseInt(last_time)) > this.cacheTTL)refresh = true;


			// if we can use cache
			if (this.localStorage.hasKey(country_key) && !refresh) {
				const countries = this.localStorage.getObject(country_key);
				console.log("received %s countries from cache ", countries.length);
				this.countries = countries;
				if (display) this.countries.unshift({ id: 0, name: "Select Country"});
				resolve(countries);
				return;
			}

			const self = this;
			this.Country.get(self.userCountry).then(res => {
				self.countries = res.data;
				if (self.debug)console.log("Retrieved %s countries from server", res.data.length);
				this.localStorage.setObject(country_key, self.countries);
				this.localStorage.set(clt_key, Date.now());

				if (display) self.countries.unshift({ id: 0, name: "Select Country"});
				resolve(self.countries);
			}, reject);
		});
	}

	startView(){
		return new Promise((resolve, reject) => {
			// display a loading message while we request country data
			this.displayLoading("Loading Countries").then(loading => {
				this.retrieveCountries(true).then(countries => {
					// After loading the country, check to see if there is a cached location
					this.locator.getLastLocationID().then(countryid => {
						console.log("Location found from cache as: " + countryid);
						// If we find a countryid, then we launch the steps involved in its configuration
						if (countryid && countryid !== "undefined")this.handleCountryChange(countryid);
						// After we complete the setup of the page, then we close the loading screen
						loading.close();
						// Then we send the list of countries to the requester
						resolve(countries);
					},  err => { loading.close(); reject(err); }); // If the requesting the last selected country fails
				}, err => { loading.close(); reject(err); }); // If the retrieving the countries fails
			});
		});
	}
	
	startAdmin(){
		// Check if user is signed in
		this.AuthService.attachCurrUser(this).then(currUser => {
			if (currUser)this.handleUserLoggedIn(currUser);
			else this.handleUserNotLoggedIn();
		});
	}

	/**
	 * Displays the a control-less loading message dialog.
	 * Clients that use the function should use the extent and call close
	 * method on the response to dismiss message
	 * @param message
	 * @returns {Promise}
	 */
	displayLoading(message){
		swal({
			type: "info",
			title: this.infoName ,
			text: message,
			allowOutsideClick: false,
			allowEscapeKey: false,
			showConfirmButton: false,
			onOpen: function () {
				swal.showLoading();
			}
		});

		return Promise.resolve(swal);
	}
	
	displayAdd(){
		if (this.debug)console.log("Launching modal to add new record");
		// Check if we have a primary form to update
		if (this.primaryForm && this.primaryForm.length > 2){ // Form was defined by the user
			if (this[this.primaryForm])this[this.primaryForm].reset();
			else if (this.debug)console.log("no primary form was detected");
		}
		this.reset();
		this.isEdit = false;
		const modelModal = $(this.primaryModal);
		modelModal.modal('show');
		// modelModal.updatePolyfill();
	}
	
	displayEdit(rec){
		if (this.debug)console.log("Launching model to edit: " + JSON.stringify(rec));
		this.reset();
		this.record = rec;
		this.isEdit = true;

		const modelModal = $(this.primaryModal);
		modelModal.modal('show');
		// modelModal.updatePolyfill();
	}

	displayView(record){
		this.record = record;
		this.isEdit = false;
		if (this.debug)console.log("Launching model to view: " + JSON.stringify(record));
		this.isEdit = false;
		if (this.viewModal.length > 1) {
			const modelModal = $(this.viewModal);
			modelModal.modal('show');
			// modelModal.updatePolyfill();
		}else{
			throw new Error('You have to specify the viewModal to use the displayView method!');
		}
	}

	// Abstract methods

	retrieveRecords(){
		throw new Error('You have to implement the method doSomething!');
	}

	setInfoName(){
		throw new Error('You have to implement the method doSomething!');
	}

	reset(){
		throw new Error('You have to implement the method doSomething!');
	}

	save(record){
		throw new Error('You have to implement the method doSomething!');
	}

	deleteRec(record){
		throw new Error('You have to implement the method doSomething!');
	}

	update(record){
		this.save(record);
	}
	/**
	 *
	 */
	getLocation(callback, refresh) {
		if (this.debug)console.log("Attempting to load the current location");
		const self = this;
		if (refresh === undefined)refresh = false;
		this.locator.retrieveCurrentLocation(refresh).then(location => {
			// if (self.debug)console.log("Received:" + JSON.stringify(location));
			self.currLocation[0] = location.coords.latitude;
			self.currLocation[1] = location.coords.longitude;
			self.updateReportLocation(self.currLocation);
			if (callback)callback(self.currLocation);
		}, err => {
			if (this.debug)console.log("unable to retrieve location");
			if (this.debug)console.error(err);
			if (callback)callback(null);
		});
	}

	/**
	 *
	 * @param countryid
	 */
	handleCountryChange(countryid){
		if (this.debug)console.log("Country was changed to:" + countryid);
		if (!isNaN(countryid)) countryid = parseInt(countryid);
		this.userCountry = countryid;
		this.locator.saveLastLocationID(countryid).then(res => console.log(res));
		// Attempting to add country name to be displayed based on data
		let country = this.countries.filter(country => country.id === countryid)[0];
		if (country)this.countryname = country.name;
		// retrieve posts
		if (this.autoLoadRecords)this.retrieveRecords(countryid);
	}
	
	/**
	 *
	 * @param currLocation
	 */
	updateReportLocation(currLocation) {

		if (currLocation && currLocation.length > 0) {
			this.record.latitude = currLocation[0];
			this.record.longitude = currLocation[1];
		}
	}
	
	/**
	 *
	 */
	generatePrimaryReport(){
		if (XLSX) {
			const workbook = XLSX.utils.table_to_book(document.getElementById(this.primaryTable));
			if (this.debug) console.log(workbook);
			const wopts = {bookType: 'xlsx', bookSST: false, type: 'array'};
			const wbout = XLSX.write(workbook, wopts);
			/* the saveAs call downloads a file on the local machine */
			const file_name = this.infoName + ".xlsx";
			saveAs(new Blob([wbout], {type: "application/octet-stream"}), file_name);
		}else{
			console.log("Export Library not available.");
			//TODO - base controller -notify the user that library not avialable
		}
	}

	refresh(){
		if (this.debug)console.log("Implementing refresh from the base controller");
		this.reset();
		this.retrieveRecords();
	}

	configureBottomBar(){
		if (this.debug)console.log("attempting to display bottom bar");
		$("#bottomMenu").show();

		$("#bottomCtrl").click(evt => {
			if (this.debug)console.log("Attempting to Log in");
		});
	}
}

class MapBaseController extends BaseController{
	constructor(Country, Locator, AuthService, LocalStorage, CountryLoc){
		super(Country, Locator, AuthService, LocalStorage);
		this.CountryLoc = CountryLoc;
		this.countryLocations = ['',''];
		this.mapId = "map";
	}

	init(){
		super.init();
		this.setMapId();
		this.retrieveCountryLocations();

		// set up listeners to improve rendering of google maps
		$(this.primaryModal).on("shown.bs.modal", res => {
			if (this.debug)console.log("Primary Modal launched");
			if (this.map) google.maps.event.trigger(this.map, "resize");
		});
	}

	retrieveCountryLocations(countryid){
		const country = (countryid) ? countryid : this.userCountry;
		console.log("Attempting  to load country locations for: " + country);

		this.CountryLoc.get(country).then(res => {
			this.countryLocations = res.data;
		}, err => { console.log(err);	});
	}

	displayAdd(){
		super.displayAdd();
		this.initMap();
	}

	displayEdit(record){
		super.displayEdit(record);
		const loc = [this.record.latitude, this.record.longitude];
		this.initMap(loc);
	}

	displayView(record){
		super.displayView(record);
		const loc = [this.record.latitude, this.record.longitude];
		this.initMap(loc);
	}

	initMap(currLocation, sectionId){
		if (!sectionId) sectionId = this.mapId;
		let latitude = 13.9636264, longitude = -62.3690426, zoomLvl = 10;
		if (!currLocation) { // If no location specified
			// attempt to retrieve the current location

			// If we have a location from the GPS
			if (this.currLocation[0] !== "") {
				latitude = this.currLocation[0];
				longitude = this.currLocation[1];
			} else { // We do not have a GPS coordinate, then attempt to use country locations
				if (this.userCountry === 0) { // We are using a global account, use the registered country location
					let rec = this.countryLocations.filter(el => el.countryid === this.currentUser.countryid);
					if (rec[0]) {
						latitude = rec[0].latitude;
						longitude = rec[0].longitude;
					}
				} else { // Use the location that is pulled in for the users account
					console.log("Attempting to use country Locations");
					if (this.countryLocations[0] !== '') {
						latitude = this.countryLocations[0].latitude;
						longitude = this.countryLocations[0].longitude;
					} else {
						console.error("Unable to retrieve current location");
						zoomLvl = 8;
					}
				}
			}
		}else{ // we recieved a location that the map should be displayed to
			latitude = parseFloat(currLocation[0]);
			longitude = parseFloat(currLocation[1]);
		}
		const myLatlng = {lat: latitude, lng: longitude};
		console.log(myLatlng);
		console.log(sectionId);
		console.log(document.getElementById(sectionId));
		this.map = new google.maps.Map(document.getElementById(sectionId), {
			zoom: zoomLvl,
			center: myLatlng
		});

		this.marker = new google.maps.Marker({
			position: myLatlng,
			map: this.map,
			title: 'Click to zoom'
		});

		google.maps.event.addListener(this.map, 'click', evt => {
			console.log("(%s, %s)", evt.latLng.lat(), evt.latLng.lng());
			const loc = [evt.latLng.lat(), evt.latLng.lng()];
			this.updateReportLocation(loc);
			this.updateMapLocation(loc);
		});

		google.maps.event.addListener(this.map, 'resize', evt => {
			this.map.setCenter(this.marker.getPosition());
		});

		// google.maps.event.trigger(this.map, 'resize');
	}

	updateMapLocation(loc){
		const latLngLiteral = {lat: loc[0], lng: loc[1]};
		if (this.marker && this.map) {
			this.marker.setPosition(latLngLiteral);
			this.map.setCenter(this.marker.getPosition());
		}
	}

	retrieveAndUpdateCurrentLocation(){
		if (this.debug)console.log("Attempting to Update Form with current location");
		swal({
			type: 'info',
			title: 'Retrieving Location',
			text: 'Attempting to retrieve current location',
			onOpen: () => {
				swal.showLoading();
			}
		});

		// Attempting to force the system to refresh location
		this.getLocation(loc => {
			swal.close();
			if (loc) {
				this.updateReportLocation(loc);
				this.updateMapLocation(loc);
			}else{
				swal("Location", "Unable to retrieve location. Ensure browser has permission to acquire your current location.","error");
			}
		}, true);
	}

	// Abstract Methods
	setMapId(){
		throw new Error('You have to implement the method doSomething!');
	}
} 