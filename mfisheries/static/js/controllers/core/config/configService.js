
class Config{
	
	constructor($http){
		this.$http = $http;
		this.base_url = "/api/config";
	}
	get () {
		return this.$http.get(this.base_url);
	}
	add(data){
		return this.$http.post(this.base_url, data);
	}
	update(source) {
		return this.$http.put(this.base_url + "/" + source.id, source);
	}
	delete(source){
		return this.$http.delete(this.base_url+ "/",source);
	}
}

Config.$inject = [
	"$http"
];
angular.module('mfisheries.Config').service("Config", Config);
