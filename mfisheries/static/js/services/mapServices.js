'use strict';

angular.module('mfisheries.MapServices', [])
	.service("$map", function (mapHelper, getUserTracks, LocalStorage, getTracksByDay, $window) {
		const self = this;
		this.map = {};
		this.conut = 0;
		this.markers = [];
		this.allMarkers = []; 	// marker array for all markers
		this.path = {}; 			  // track path
		this.polyLines = []; 		// polylines for track path
		
		this.tMarkers = []; 		// trip markers for ids
		this.tripPolyLines = [];// trip polylines
		this.tripMarkers = []; 	// trips markers
		this.tripPath = {}; 		// trip path
		
		this.timer = null; 			// timer for interval timer
		
		this.currentType = "";
		
		this.initMap = function (mapId, defaultLoc, defaultZoom) {
			
			if (!defaultLoc) {
				defaultLoc = [10.725212975635937,	-61.214232444763184];
			}
			if (!defaultZoom){
				defaultZoom = 8;
			}
			
			// gets the browsers  geo location and sets the map center using the coordinates
			const mapCenter = new google.maps.LatLng(defaultLoc[0], defaultLoc[1]); // map center object
			const mapOptions = { // map options object
				zoom: defaultZoom,
				minZoom: defaultZoom,
				center: mapCenter,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				streetViewControl: false,
				scaleControl: true,
				fullscreenControl: true
			};
			
			// mapId is a jquery selector [0] gives the first (should be only) DOM reference to the map
			self.map = new google.maps.Map(mapId[0], mapOptions);
			
			//sets the path for the polylines
			self.path = new google.maps.Polyline({
				strokeColor: '#FF0000',
				strokeOpacity: 1.0,
				strokeWeight: 2,
				icons: [{
					icon: {
						path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
						strokeColor: '#0000ff',
						fillColor: '#0000ff',
						fillOpacity: 1
					},
					repeat: '100px',
					path: []
				}]
			});
			
			//sets the path for the polylines
			self.tripPath = new google.maps.Polyline({
				strokeColor: '#FF0000',
				strokeOpacity: 1.0,
				strokeWeight: 2
			});
			
			//sets the map for the polylines path
			self.lineOptions(self.map);
		};
		
		// Add Image as Icon to location
		this.addLEKMarker = function (_markers) {
			if (self.currentType !== "LEK")
				return;
			
			self.clearOverlays(self.markers);
			self.setAllMap(null, self.markers);
			self.setAllMap(null, self.polyLines);
			self.markers = [];
			const contentString = "",								// pick one of the three icons in the sprite
				offset = Math.floor(Math.random() * 3) * 16,				// Calculate desired pixel-size of the marker
				size = Math.floor(60),
				scaleFactor = size / 16;
			
			$.each(_markers, function (index, data) {
				
				const infowindow = new google.maps.InfoWindow({
					content: generateLEKInfoWindow(data)
				});
				
				const markerId = self.getMarkerUniqueId(data.latitude, data.longitude),
					marker = new google.maps.Marker({
						position: new google.maps.LatLng(data.latitude, data.longitude),
						map: self.map,
						title: data.text,
						id: 'marker_' + markerId,
						animation: google.maps.Animation.DROP,
						draggable: false
					});
				
				marker.addListener('click', function () {
					infowindow.open(self.map, marker);
				});
				
				self.markers.push(marker);
			});
		};
		
		function generateLEKInfoWindow(data) {
			let contentString = "";
			
			if (data.filetype.includes("image")) {
				contentString = "<img src='" + data.location + "' width='300px' height='300px'/>";
			}
			else if (data.filetype.includes("video")) {
				contentString = '<video width="320" height="240" controls><source src="' + data.location + '" type="' + data.filetype + '">Your browser does not support the video tag.</video>';
			}
			else if (data.filetype.includes("audio")) {
				contentString = '<audio controls><source src="' + data.location + '" type="' + data.filetype + '">Your browser does not support the audio element.</audio>';
			}
			
			contentString += '<div class="media-body"><p><b>' + data.timestamp + '</b></p><p>' + data.text + '</p>' +
				'<p><i>By ' + data.username + '</i></p><p>Category: ' + data.category + '</p></div>';
			
			return contentString;
		}
		
		// Add Image as Icon to location
		this.addImageMarker = function (_markers) {
			if (self.currentType !== "image")
				return;
			self.clearOverlays(self.markers);
			self.setAllMap(null, self.markers);
			self.setAllMap(null, self.polyLines);
			self.markers = [];
			let contentString = "";
			const offset = Math.floor(Math.random() * 3) * 16,				// Calculate desired pixel-size of the marker
				size = Math.floor(60),
				scaleFactor = size / 16;
			
			$.each(_markers.location, function (index, data) {
				
				data.img = new google.maps.MarkerImage(
					data.imglocation, // my 16x48 sprite with 3 circular icons
					new google.maps.Size(16 * scaleFactor, 16 * scaleFactor), // desired size
					new google.maps.Point(0, offset * scaleFactor), // offset within the scaled sprite
					new google.maps.Point(size / 2, size / 2), // anchor point is half of the desired size
					new google.maps.Size(16 * scaleFactor, 48 * scaleFactor) // scaled size of the entire sprite
				);
				// data.img = '.'+data.img_location;
				contentString = mapHelper.imageWindow(_markers.images[data.id]);
				const markerId = self.getMarkerUniqueId(data.latitude, data.longitude),
					marker = new google.maps.Marker({
						position: new google.maps.LatLng(data.latitude, data.longitude),
						map: self.map,
						title: "Date: " + data.date,
						icon: data.img,
						id: 'marker_' + markerId,
						animation: google.maps.Animation.DROP,
						draggable: false
					});
				
				self.markers.push(marker);
				
				self.addInfoWindow(contentString.get(0), marker);
			});
		};
		
		// adding track markers to the map
		this.addMarker = function (_markers, all, starttrackid) {
			if (self.currentType !== "tracks")
				return;
			// clears the map of all usual markers
			self.clearMarkers();
			// clears the map of all trip markers
			self.clearTripMarkers();
			let contentString = "",
				show = true;
			
			// for each track we will create a marker
			$.each(_markers, function (index, data) {
				// console.log(data);
				// if the type is not sos and this is the current track markers i.e. not showing all markers of a user's track
				if (data.type.toLowerCase() !== 'sos' && !all) {
					data.img = './static/img/tracks.png';
					contentString = mapHelper.trackWindow(data);
				}
				// if the type is sos and this is the current track markers i.e. not showing all markers of a user's track
				else if (data.type.toLowerCase() === 'sos' && !all) {
					data.img = './static/img/sos.png';
					contentString = mapHelper.sosWindow(data);
				}
				// if the type is not sos and this is all the track markers i.e. showing all markers of a user's track
				else if (data.type.toLowerCase() !== 'sos' && all) {
					// if this is the first or last marker we want it to have some special actions
					if (data.type === 'start' || data.type === 'end') {
						data.img = './static/img/blue.png';
						data.starttrackid = starttrackid;
						contentString = mapHelper.endTrackWindow(data);
					} else {
						// if the type is not sos and this is the current track markers i.e. not showing all markers of a user's track
						data.img = './static/img/ctrack.png';
						contentString = mapHelper.allTrackWindow(data);
					}
				}
				// if the type is sos and this is not the current track markers i.e. showing all markers of a user's track
				else if (data.type.toLowerCase() === 'sos' && all) {
					data.img = './static/img/sos.png';
					contentString = mapHelper.allSosWindow(data);
				}
				
				// create a marker id
				const markerId = self.getMarkerUniqueId(data.latitude, data.longitude),	// give the marker some properties
					marker = new google.maps.Marker({
						position: new google.maps.LatLng(data.latitude, data.longitude),
						map: self.map,
						title: "User: " + data.userid,
						icon: data.img,
						id: 'marker_' + markerId,
						animation: (data.type.toLowerCase() === "sos" ? google.maps.Animation.BOUNCE : undefined),
						draggable: false
					});
				
				// add the marker id to the array of marker ids
				self.markers[markerId] = marker; // cache marker in markers object
				// add the marker to the array for renderign to the map
				self.allMarkers.push(marker);
				setTimeout(function () {
					self.markers[markerId].setAnimation(null);
				}, 1000);
				
				if (!all) {
					// if this is not all the markers we want to give ther first
					// marker button's its functionality
					$(contentString).find('#' + data.userid).click(function () {
						
						self.getTracks({
							userid: this.id,
							day: this.dataset.day,
							month: this.dataset.month,
							year: this.dataset.year,
							starttrackid: this.dataset.starttrackid
						});
						// console.log({day:this.dataset.day,month:this.dataset.month, starttrackid:this.dataset.starttrackid});
					});
				} else {
					// we want to get the trips for the markers
					$(contentString).find('#' + data.userid).click(function () {
						
						self.getTrips({userid: this.id, starttrackid: this.dataset.starttrackid, date: this.dataset.timestamp},
							function (hasTrips) {
								if (hasTrips) {
									// if ther corresponding track has a trip
									// asspicated with it
									$('#' + data.userid).hide();
									$('#' + data.userid + '_r').show();
								} else {
									// the track has no associated trips
									$('#' + data.userid).hide();
									$('.no-trip').show();
								}
							});
						// console.log({userid:this.id, starttrackid:this.dataset.starttrackid});
					});
					
					// toggling the trip markers to show and hide
					$(contentString).find('#' + data.userid + '_r').click(function () {
						
						// console.log(self.tripMarkers.length);
						if (show) {
							self.clearOverlays(self.tripMarkers);
							show = false;
						} else if (!show) {
							self.showOverlays(self.tripMarkers);
							show = true;
						}
						
					});
					//pushes paths into the polyline array
					self.polyLines.push(new google.maps.LatLng(data.latitude, data.longitude));
					clearInterval(self.timer);
					
				}
				// add the info window to each marker
				self.addInfoWindow(contentString.get(0), marker);
			});
			
			
			self.path.setPath(self.polyLines);	//sets the path
		};
		
		// Adding track markers for the trip
		this.addTripMarker = function (_markers) {
			// clears the map of all trip markers
			self.clearTripMarkers();
			let contentString = "";
			
			// for each trip marker
			$.each(_markers, function (index, data) {
				data.img = './static/img/magenta.png';
				contentString = mapHelper.tripWindow(data);
				
				const markerId = self.getMarkerUniqueId(data.latitude, data.longitude),
					
					marker = new google.maps.Marker({
						position: new google.maps.LatLng(data.latitude, data.longitude),
						map: self.map,
						title: "User: " + data.userid,
						icon: data.img,
						id: 'marker_' + markerId,
						// animation 	: google.maps.Animation.DROP,
						draggable: false
					});
				
				self.tMarkers[markerId] = marker; // cache marker in markers object
				self.tripMarkers.push(marker);
				
				//pushes paths into the polyline array
				self.tripPolyLines.push(new google.maps.LatLng(data.latitude, data.longitude));
				
				self.addInfoWindow(contentString.get(0), marker);
			});
			// console.log(self.tripPolyLines);
			// self.tripPath.setPath(self.tripPolyLines);	//sets the path
			// console.log(self.tripPolyLines);
		};
		
		this._handleData = function (res) {
			const tracks = res.data.data;
			console.log("Retrieved " + tracks.length + " tracks");
			self.lineOptions(self.map);
			self.polyLines = [];
			self.addMarker(tracks, false);
		};
		
		// getting all current markers
		this.getMarkers = function (d, keepCurrent) {
			if (keepCurrent) { //if true then we will attempt to keep map up to date by polling the server every 30 seconds
				console.log('Displaying markers for ' + d.day + "/" + d.month + "/" + d.year);
				
				$window.setTimeout(function () {
					// Retrieve Tracks for the specified day to be displayed on the map
					getTracksByDay
						.get(d.day, d.month, d.year)
						.then(self._handleData);
				}, 1000); // The delay is used to allow the map tiles to load before the points is placed on map
				
				self.timer = setInterval(function () {
					console.log('polling at: ' + Date.now());
					getTracksByDay
						.get(d.day, d.month, d.year)
						.then(self._handleData);
				}, 30000);
			} else {
				getTracksByDay
					.get(d.day, d.month, d.year)
					.then(self._handleData);
			}
		};
		
		// getting the trips for a user
		this.getTrips = function (data, _callback) {
			const trips = new window.CG.Collections.TripsLocationCollection(); //TODO Check if this works the declaration of CG is not global
			trips.fetch({
				url: 'api/index.php/get/user/current/trips/' + data.starttrackid + '/' + data.userid + '/' + data.date,
				success: function (model, response) {
					// console.log(JSON.stringify(model.toJSON()));
					
					self.addTripMarker(model.toJSON());
					if (typeof _callback === 'function') {
						_callback(model.toJSON().length > 0);
					}
				},
				error: function (model, response) {
					console.log(response.responseText);
				}
			});
		};
		
		
		// getting all the tracks for a given user, using the start track id
		this.getTracks = function (data) {
			const tracks = getUserTracks.get(data.day,
				data.month, data.year,
				data.userid,
				data.starttrackid);
			tracks.then(function (res) {
				console.log(res.data.data);
				if (res.data.data.length > 0) {
					self.addMarker(res.data.data, true, data.starttrackid);
					const m = res.data.data[0];
					self.zoomIn(new google.maps.LatLng(m.latitude, m.longitude));
				} else {
					swal("Info!", "No other tracks associated with this track", "info");
				}
			});
			
		};
		
		this.addInfoWindow = function (contentString, marker) {
			const infowindow = new google.maps.InfoWindow();
			
			google.maps.event.addListener(marker, 'click', (function (contentString, marker) {
				return function () {
					infowindow.setContent(contentString);
					infowindow.open(self.map, marker);
				};
			})(contentString, marker));
		};
		
		this.getMarkerUniqueId = function (lat, lng) {
			return lat + "_" + lng;
		};
		
		this.zoomOut = function () {
			$window.setTimeout(function () {
				const center = self.map.getCenter();
				self.map.setZoom(8);
				self.map.setCenter(center);
			}, 200);
		};
		
		this.zoomIn = function (center) {
			$window.setTimeout(function () {
				self.map.setZoom(13);
				self.map.setCenter(center);
			}, 200);
		};
		
		this.setMapCenter = function (lat, lng, zoom) {
			window.setTimeout(function () {
				const center = new google.maps.LatLng(lat, lng);
				self.map.setZoom(zoom);
				self.map.setCenter(center);
			}, 200);
		};
		
		//TODO Use the getLocation to load the current location of the browser
		this.getLocation = function (_callback) {
			if ("geolocation" in navigator) {
				navigator.geolocation.getCurrentPosition(
					function (location) { //retrieved location
						if (typeof _callback === 'function') {
							_callback(location);
						}
					},
					function () { //geolocation request failed
						console.log("Unable to retrieve location");
						_callback(undefined);
					}
				);
			} else {
				console.log("Browser does not support geolocation");
				_callback(undefined);
			}
			
		};
		
		
		this.removeMarker = function (lat, lng) {
			const markerId = self.getMarkerUniqueId(lat, lng);
			const marker = self.markers[markerId];
			if (marker)
				marker.setMap(null); // set markers setMap to null to remove it from map
			else {
				console.log("Unable to find marker for : " + lat + " " + lng);
				$window.location.reload();
			}
			
			delete self.markers[markerId]; // delete marker instance from markers object
		};
		
		// Sets the map on all markers in the array.
		this.setAllMap = function (map, array) {
			for (let i = 0; i < array.length; i++) {
				array[i].setMap(map);
			}
		};
		
		// Removes the overlays from the map, but keeps them in the array.
		this.clearOverlays = function (array) {
			self.setAllMap(null, array);
		};
		
		// Shows any overlays currently in the array.
		this.showOverlays = function (array) {
			self.setAllMap(self.map, array);
		};
		
		this.lineOptions = function (map) {
			self.path.setMap(map);
		};
		
		this.clearMarkers = function () {
			self.clearOverlays(self.allMarkers);
			self.setAllMap(null, self.allMarkers);
			self.setAllMap(null, self.polyLines);
			self.allMarkers = [];
		};
		
		this.clearTripMarkers = function () {
			self.clearOverlays(self.tripMarkers);
			self.setAllMap(null, self.tripMarkers);
			// self.setAllMap(null,self.tripPolyLines);
			self.tripMarkers = [];
		};
		
		this.setType = function (type) {
			self.currentType = type;
		};
		
	});
