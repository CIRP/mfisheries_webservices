'use strict';

angular.module('mfisheries.services')
	.service("login", function ($http) {
		var deffered = {};
		
		deffered.post = function (data) {
			
			return $http.post('api/user/login', data);
		};
		return deffered;
	});