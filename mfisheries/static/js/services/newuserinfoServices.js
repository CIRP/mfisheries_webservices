'use strict';


/**
*	services group name: newUserInfoSErvices
*	name services: mfisheries
*/

angular.module('mfisheries.neUserInfoServices',[])

// a service name
.service("getNewUserInfo", function($http){
	// javascript object used to retrieve service
	const deffered = {};

	// get request which accepts a userid as a parameter
	deffered.get = function(userid){	
		return $http.get('/api/get/new/info/info/'+userid);
	};

	return deffered;
});