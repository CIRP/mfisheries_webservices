// Maps.js
(function (document, window, $, _) {
	const CG = {};
	
	CG.Map = {
		count: 0, // count for timer
		map: {}, // map object
		markers: [], // general markers array to store id
		allMarkers: [], // marker array for all markers
		path: {}, // track path
		polyLines: [], // polylines for track path
		
		tMarkers: [], // trip markers for ids
		tripPolyLines: [], // trip polylines
		tripMarkers: [], // trips markers
		tripPath: {}, // trip path
		
		timer: null, // timer for interval timer
		
		initMap: function (mapId, callback, defaultLoc, defaultZoom) {
			console.log("Running initMap inside the maps,js file");
			if (!defaultLoc) {
				defaultLoc = [10.725212975635937, -61.214232444763184];
			}
			if (!defaultZoom){
				defaultZoom = 8;
			}
			
			// gets the browsers  geo location and sets the map center using the coordinates
			const mapCenter = new google.maps.LatLng(defaultLoc[0], defaultLoc[1]);
			const mapOptions = {
				zoom: defaultZoom,
				center: mapCenter,
				mapTypeId: google.maps.MapTypeId.HYBRID,
			};
			
			CG.Map.map = new google.maps.Map(mapId[0], mapOptions);
			
			//sets the path for the polylines
			CG.Map.path = new google.maps.Polyline({
				strokeColor: '#FF0000',
				strokeOpacity: 1.0,
				strokeWeight: 2,
				icons: [{
					icon: {
						path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
						strokeColor: '#0000ff',
						fillColor: '#0000ff',
						fillOpacity: 1
					},
					repeat: '100px',
					path: []
				}]
			});
			
			//sets the path for the polylines
			CG.Map.tripPath = new google.maps.Polyline({
				strokeColor: '#FF0000',
				strokeOpacity: 1.0,
				strokeWeight: 2
			});
			
			//sets the map for the polylines path
			CG.Map.lineOptions(CG.Map.map);
			// call back function when the map is finished
			if (typeof callback === 'function') {
				callback();
			}
		},
		
		// adding track markers to the map
		addMarker: function (_markers, all, starttrackid) {
			// clears the map of all usual markers
			CG.Map.clearMarkers();
			// clears the map of all trip markers
			CG.Map.clearTripMarkers();
			let contentString = "",
				show = true;
			
			// for each marker
			$.each(_markers, function (index, data) {
				
				if (data.type !== 'sos' && !all) {
					// if the type is not sos and this is 
					// the current track markers 
					// i.e. not showing all markers of a user's track	
					data.img = './img/tracks.png';
					contentString = CG.Services.trackWindow(data);
					
				} else if (data.type === 'sos' && !all) {
					// if the type is sos and this is
					// the current track markers 
					// i.e. not showing all markers of a user's track
					data.img = './img/sos.png';
					contentString = CG.Services.sosWindow(data);
					
				} else if (data.type !== 'sos' && all) {
					// if the type is not sos and this is
					// all the track markers 
					// i.e. showing all markers of a user's track
					if (data.type === 'start') {// || data.type === 'end'){
						// if this is the first marker we want it to have some
						// special actions
						data.img = './img/blue.png';
						data.starttrackid = starttrackid;
						contentString = CG.Services.endTrackWindow(data);
					} else {
						// if the type is not sos and this is
						// the current track markers 
						// i.e. not showing all markers of a user's track
						data.img = './img/ctrack.png';
						contentString = CG.Services.allTrackWindow(data);
					}
				}
				else if (data.type === 'sos' && all) {
					// if the type is sos and this is
					// not the current track markers 
					// i.e. showing all markers of a user's track
					data.img = './img/sos.png';
					contentString = CG.Services.allSosWindow(data);
				}
				
				// create a marker id
				const markerId = CG.Map.getMarkerUniqueId(data.latitude, data.longitude),										// give the marker some properties
					marker = new google.maps.Marker({
						position: new google.maps.LatLng(data.latitude, data.longitude),
						map: CG.Map.map,
						title: "User: " + data.userid,
						icon: data.img,
						id: 'marker_' + markerId,
						// animation 	: google.maps.Animation.DROP,
						draggable: false
					});
				
				// add the marker id to the array of marker ids
				CG.Map.markers[markerId] = marker; // cache marker in markers object
				// add the marker to the array for renderign to the map
				CG.Map.allMarkers.push(marker);
				
				if (!all) {
					// if this is not all the markers we want to give ther first
					// marker button's its functionality
					$(contentString).find('#' + data.userid).click(function () {
						
						CG.Map.getTracks({
							userid: this.id,
							day: this.dataset.day,
							month: this.dataset.month,
							starttrackid: this.dataset.starttrackid
						});
						// console.log({userid:this.id,day:this.dataset.day,month:this.dataset.month, starttrackid:this.dataset.starttrackid});
					});
				} else {
					// we want to get the trips for the markers
					$(contentString).find('#' + data.userid).click(function () {
						
						CG.Map.getTrips({userid: this.id, starttrackid: this.dataset.starttrackid, date: this.dataset.timestamp},
							function (hasTrips) {
								if (hasTrips) {
									// if ther corresponding track has a trip 
									// asspicated with it 
									$('#' + data.userid).hide();
									$('#' + data.userid + '_r').show();
								} else {
									// the track has no associated trips								
									$('#' + data.userid).hide();
									$('.no-trip').show();
								}
							});
						// console.log({userid:this.id, starttrackid:this.dataset.starttrackid});
					});
					
					// toggling the trip markers to show and hide
					$(contentString).find('#' + data.userid + '_r').click(function () {
						
						// console.log(CG.Map.tripMarkers.length);
						if (show) {
							CG.Map.clearOverlays(CG.Map.tripMarkers);
							show = false;
						} else if (!show) {
							CG.Map.showOverlays(CG.Map.tripMarkers);
							show = true;
						}
						
					});
					//pushes paths into the polyline array
					CG.Map.polyLines.push(new google.maps.LatLng(data.latitude, data.longitude));
					clearInterval(CG.Map.timer);
					
				}
				// add the info window to each marker
				CG.Map.addInfoWindow(contentString.get(0), marker);
			});
			CG.Map.path.setPath(CG.Map.polyLines);	//sets the path						
		},
		
		
		addTripMarker: function (_markers) {
			// clears the map of all trip markers
			CG.Map.clearTripMarkers();
			let contentString = "";
			
			// for each trip marker
			$.each(_markers, function (index, data) {
				data.img = './img/magenta.png';
				contentString = CG.Services.tripWindow(data);
				
				const markerId = CG.Map.getMarkerUniqueId(data.latitude, data.longitude),
					
					marker = new google.maps.Marker({
						position: new google.maps.LatLng(data.latitude, data.longitude),
						map: CG.Map.map,
						title: "User: " + data.userid,
						icon: data.img,
						id: 'marker_' + markerId,
						// animation 	: google.maps.Animation.DROP,
						draggable: false
					});
				
				CG.Map.tMarkers[markerId] = marker; // cache marker in markers object
				CG.Map.tripMarkers.push(marker);
				
				//pushes paths into the polyline array
				CG.Map.tripPolyLines.push(new google.maps.LatLng(data.latitude, data.longitude));
				
				CG.Map.addInfoWindow(contentString.get(0), marker);
			});
			// console.log(CG.Map.tripPolyLines);
			// CG.Map.tripPath.setPath(CG.Map.tripPolyLines);	//sets the path						
			// console.log(CG.Map.tripPolyLines);
		},
		
		// getting all current markers
		getMarkers: function (data, current, callback) {
			const markers = new CG.Collections.MapMarkerCollection();
			
			if (current) {
				// if we want to get the current markers for the day
				window.setTimeout(function () {
					markers.fetch({
						url: 'api/index.php/markersbyday/' + data.day + '/' + data.month,
						success: function (model, response) {
							// console.log(JSON.stringify(model.toJSON()));
							//sets the polylines path to null and clear the polylines array
							CG.Map.lineOptions(CG.Map.map);
							CG.Map.polyLines = [];
							CG.Map.addMarker(model.toJSON(), false);
							// clearInterval(CG.Map.timer);
							// console.log('fisrt poll');
						},
						error: function (model, response) {
							console.log(response.responseText);
						}
					});
				}, 1000);
				// after the initial marker pull from the database
				// keep polling after every x seconds to get new tracks
				CG.Map.timer = setInterval(function () {
					markers.fetch({
						url: 'api/index.php/markersbyday/' + data.day + '/' + data.month,
						success: function (model, response) {
							//sets the polylines path to null and clear the polylines array
							CG.Map.lineOptions(CG.Map.map);
							CG.Map.polyLines = [];
							CG.Map.addMarker(model.toJSON(), false);
							if (typeof callback === 'function') {
								callback();
							}
						},
						error: function (model, response) {
							console.log(response.responseText);
						}
					});
				}, 30000); // polling every 30 seconds
			} else {
				// just get the track for a given day
				markers.fetch({
					url: 'api/index.php/markersbyday/' + data.day + '/' + data.month,
					success: function (model, response) {
						//sets the polylines path to null and clear the polylines array
						CG.Map.lineOptions(CG.Map.map);
						CG.Map.polyLines = [];
						CG.Map.addMarker(model.toJSON(), false);
						clearInterval(CG.Map.timer);
						// console.log('static');
					},
					error: function (model, response) {
						console.log(response.responseText);
					}
				});
			}
		},
		
		// getting all the tracks for a given user, using the start track id
		getTracks: function (data) {
			
			const tracks = new CG.Collections.UserTrackCollection();
			tracks.fetch({
				url: 'api/index.php/usertracks/' + data.day + '/' + data.month + '/' + data.userid + '/' + data.starttrackid,
				success: function (model, response) {
					if (model.toJSON().length > 0) {
						// console.log(JSON.stringify(model.toJSON()));
						CG.Map.addMarker(model.toJSON(), true, data.starttrackid);
						const m = model.at(0).toJSON();
						CG.Map.zoomIn(new google.maps.LatLng(m.latitude, m.longitude));
					} else {
						alert("No more tracks");
					}
				},
				error: function (model, response) {
					console.log(response.responseText);
				}
			});
		},
		
		// getting the trips for a user
		getTrips: function (data, _callback) {
			const trips = new CG.Collections.TripsLocationCollection();
			trips.fetch({
				url: 'api/index.php/get/user/current/trips/' + data.starttrackid + '/' + data.userid + '/' + data.date,
				success: function (model, response) {
					// console.log(JSON.stringify(model.toJSON()));
					
					CG.Map.addTripMarker(model.toJSON());
					if (typeof _callback === 'function') {
						_callback(model.toJSON().length > 0);
					}
				},
				error: function (model, response) {
					console.log(response.responseText);
				}
			});
		},
		
		
		// function to add infor window given the content string and marker
		addInfoWindow: function (contentString, marker) {
			const infowindow = new google.maps.InfoWindow();
			
			google.maps.event.addListener(marker, 'click', (function (contentString, marker) {
				return function () {
					infowindow.setContent(contentString);
					infowindow.open(CG.Map.map, marker);
				};
			})(contentString, marker));
		},
		
		getMarkerUniqueId: function (lat, lng) {
			return lat + '_' + lng;
		},
		
		zoomOut: function () {
			
			window.setTimeout(function () {
				const center = CG.Map.map.getCenter();
				CG.Map.map.setZoom(8);
				CG.Map.map.setCenter(center);
			}, 200);
		},
		
		zoomIn: function (center) {
			
			window.setTimeout(function () {
				CG.Map.map.setZoom(13);
				CG.Map.map.setCenter(center);
			}, 200);
		},
		
		setMapCenter: function (lat, lng, zoom) {
			
			window.setTimeout(function () {
				const center = new google.maps.LatLng(lat, lng);
				CG.Map.map.setZoom(zoom);
				CG.Map.map.setCenter(center);
			}, 200);
		},
		
		getLocation: function (_callback) {
			navigator.geolocation.getCurrentPosition(
				function (location) {
					if (typeof _callback === 'function') {
						_callback(location);
					}
				},
				function () {
					console.log("No browser support");
				}
			);
		},
		
		/**
		 * Removes given marker from map.
		 * @param {!google.maps.Marker} marker A google.maps.Marker instance that will be removed.
		 * @param {!string} markerId Id of marker.
		 */
		removeMarker: function (marker, markerId) {
			marker.setMap(null); // set markers setMap to null to remove it from map
			delete CG.Map.markers[markerId]; // delete marker instance from markers object
		},
		
		// Sets the map on all markers in the array.
		setAllMap: function (map, array) {
			for (let i = 0; i < array.length; i++) {
				array[i].setMap(map);
			}
		},
		
		// Removes the overlays from the map, but keeps them in the array.
		clearOverlays: function (array) {
			CG.Map.setAllMap(null, array);
		},
		
		// Shows any overlays currently in the array.
		showOverlays: function (array) {
			CG.Map.setAllMap(CG.Map.map, array);
		},
		
		lineOptions: function (map) {
			CG.Map.path.setMap(map);
		},
		
		clearMarkers: function () {
			CG.Map.clearOverlays(CG.Map.allMarkers);
			CG.Map.setAllMap(null, CG.Map.allMarkers);
			CG.Map.setAllMap(null, CG.Map.polyLines);
			CG.Map.allMarkers = [];
		},
		
		clearTripMarkers: function () {
			
			CG.Map.clearOverlays(CG.Map.tripMarkers);
			CG.Map.setAllMap(null, CG.Map.tripMarkers);
			// CG.Map.setAllMap(null,CG.Map.tripPolyLines);
			CG.Map.tripMarkers = [];
			
		}
	};
	
	
}(document, this, jQuery, _));