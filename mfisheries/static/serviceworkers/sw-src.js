importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.0.0/workbox-sw.js');

// Using our version check to see if the code cached by the service worker was updated
const self_version_check = "1.2";
console.log("Serviceworker is now at version:" + self_version_check);

workbox.setConfig({debug: false});
workbox.core.setLogLevel(workbox.core.LOG_LEVELS.debug);

// Enable Offline Google Analytics (disabled until bug resolved)
// workbox.googleAnalytics.initialize({
// 	parameterOverrides: {
// 		network_status: 'offline',
// 	},
// });

// Pre-cache resource list is generated automatically when command 'gulp service-worker' is executed
workbox.precaching.precacheAndRoute([]);

// *** Additional Strategies based on Google's Common Recipes ***
// Caching Images - https://developers.google.com/web/tools/workbox/guides/common-recipes#caching_images
// From specific sub directory - https://developers.google.com/web/tools/workbox/guides/common-recipes#cache_resources_from_a_specific_subdirectory
workbox.routing.registerRoute(
		new RegExp("/static/userimages/"),
		workbox.strategies.staleWhileRevalidate({
			cacheName: 'user-resources',
			plugins: [
				new workbox.expiration.Plugin({
					maxEntries: 60,
					maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
				}),
			],
		})
);

// Works but does it override the previous strategies
workbox.routing.registerRoute(
		new RegExp("/"),
		workbox.strategies.networkFirst({
			cacheName: 'base_cache',
		})
);


// Add the Google API calls and resources to the cache: https://developers.google.com/web/tools/workbox/guides/common-recipes#caching_content_from_multiple_origins
workbox.routing.registerRoute(
		/.*(?:googleapis|gstatic)\.com.*$/,
		workbox.strategies.staleWhileRevalidate({
			cacheName: 'googleapis',
		})
);

// Add JS and CSS files if not previously cached - https://developers.google.com/web/tools/workbox/guides/common-recipes#cache_css_and_javascript_files
workbox.routing.registerRoute(
		/\.(?:js|css)$/,
		workbox.strategies.staleWhileRevalidate({
			cacheName: 'static-resources',
		})
);