importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.0.0/workbox-sw.js');

// Using our version check to see if the code cached by the service worker was updated
const self_version_check = "1.2";
console.log("Serviceworker is now at version:" + self_version_check);

workbox.setConfig({debug: false});
workbox.core.setLogLevel(workbox.core.LOG_LEVELS.debug);

// Enable Offline Google Analytics (disabled until bug resolved)
// workbox.googleAnalytics.initialize({
// 	parameterOverrides: {
// 		network_status: 'offline',
// 	},
// });

// Pre-cache resource list is generated automatically when command 'gulp service-worker' is executed
workbox.precaching.precacheAndRoute([
  {
    "url": "static/dist/js/app.js",
    "revision": "c7f9016ba913fe854ba622681f8ccba1"
  },
  {
    "url": "static/dist/js/app.min.js",
    "revision": "f0afc8f13b0fa80c842ff828211b7c03"
  },
  {
    "url": "static/dist/vendor/bundle.js",
    "revision": "a63677ad04c08691f3aea6782adb00eb"
  },
  {
    "url": "static/dist/vendor/bundle.min.js",
    "revision": "a28f6e4b0a845d5b9907578f6c08726e"
  },
  {
    "url": "static/css/bootstrap-fullcalendar.css",
    "revision": "187fdc3c140105567f23a6d73f177f10"
  },
  {
    "url": "static/css/core.css",
    "revision": "47b2433ad39325ae87ba703deebc0d9e"
  },
  {
    "url": "static/css/images/ajax-loader.gif",
    "revision": "5ad4ee485daa3123bd791f2d48c37de6"
  },
  {
    "url": "static/css/images/icons-18-white.png",
    "revision": "bcc3797b5ee26595de904538fe84e289"
  },
  {
    "url": "static/css/main.css",
    "revision": "4b87b1614aa1dc55bc60fc33a92aaed3"
  },
  {
    "url": "static/img/banner_design.jpg",
    "revision": "67a975d28eec6bbd9a37d13cc6cc7fa9"
  },
  {
    "url": "static/img/blue.png",
    "revision": "e2eb9459ef92a35ce6ca7740c6f12112"
  },
  {
    "url": "static/img/boat_background.jpg",
    "revision": "d76b00555f911ca79bc71112291a0230"
  },
  {
    "url": "static/img/btn_google_signin_dark_normal_web.png",
    "revision": "696c081ae56a5d4674d5da24e7810ab7"
  },
  {
    "url": "static/img/ctrack.png",
    "revision": "55846ef81f7dbcd4f0637674b6eba8e0"
  },
  {
    "url": "static/img/emergency/organization_default_profile.png",
    "revision": "77865803ce96fd333abae65b6655f7ac"
  },
  {
    "url": "static/img/emergency/personal_default_profile.png",
    "revision": "a6acb56149b43f0194fbeaf7705b1ba5"
  },
  {
    "url": "static/img/favicon.png",
    "revision": "bfac09e27a46667f817df9a4c2b1c3e7"
  },
  {
    "url": "static/img/fewer_banner.png",
    "revision": "a2e8eedee7933087d3819f011dbb8414"
  },
  {
    "url": "static/img/fewer_Logo.png",
    "revision": "015bcd717700bfaf91b29a85d85080d8"
  },
  {
    "url": "static/img/Fisherman Tracks Marker 200 by 200.png",
    "revision": "024dc2a94764bf047eec72e25af5b007"
  },
  {
    "url": "static/img/icons/audio_default.png",
    "revision": "52101b66ffc3c252b19300f3b77ad015"
  },
  {
    "url": "static/img/icons/fewer/icon_dmg_report.png",
    "revision": "222fcb66acd108395f59e6eb6db048be"
  },
  {
    "url": "static/img/icons/fewer/icon_emergency_contacts.png",
    "revision": "d039e885237065ad04ee0744ac444257"
  },
  {
    "url": "static/img/icons/fewer/icon_emg_proc.png",
    "revision": "f4fd9b7a2e5491e1b2a60b9e534af26f"
  },
  {
    "url": "static/img/icons/fewer/icon_fewer_alert.png",
    "revision": "aeca9c644877ae85894cd16e2bb6622f"
  },
  {
    "url": "static/img/icons/fewer/icon_fewer_weather.png",
    "revision": "b0d85b2ccf35fd6524b0cb0f1afd71b9"
  },
  {
    "url": "static/img/icons/fewer/icon_lek.png",
    "revision": "1b8d87bac1c19e014c3240282bd748ce"
  },
  {
    "url": "static/img/icons/fewer/icon_messaging.png",
    "revision": "8737d80503aadf4993fee86925c1feaf"
  },
  {
    "url": "static/img/icons/fewer/icon_missing_person.png",
    "revision": "33212101377e660d8f5c2a91cc7aa678"
  },
  {
    "url": "static/img/icons/ic_email_black_36dp.png",
    "revision": "256587781178fbd711832e1d5e6dfe53"
  },
  {
    "url": "static/img/icons/ic_info_outline_black_36dp.png",
    "revision": "4541a80013d6f648627cfe960082786d"
  },
  {
    "url": "static/img/icons/ic_phone_black_36dp.png",
    "revision": "9fc3ec07f2c8771a3b6af1bd9020bed6"
  },
  {
    "url": "static/img/icons/ic_share.png",
    "revision": "21b1b7190a10f9bad2b890326fa3e0e4"
  },
  {
    "url": "static/img/icons/icon_alert.png",
    "revision": "985c064d25f2a1ab2f36bb2889d42e6b"
  },
  {
    "url": "static/img/icons/icon_camera.png",
    "revision": "a123ab75df55e0721b7b5f1d4f21575c"
  },
  {
    "url": "static/img/icons/icon_dmg_report.png",
    "revision": "7ec64c20a6dfadf22c6e85a3c44b6339"
  },
  {
    "url": "static/img/icons/icon_emergency_contacts.png",
    "revision": "aee324c652ba61386f158c39e759697f"
  },
  {
    "url": "static/img/icons/icon_emg_proc.png",
    "revision": "8c8c6980a6b1cfd6017ed944c21d742e"
  },
  {
    "url": "static/img/icons/icon_fewer_about.png",
    "revision": "1a76f514a3a950c89a08d1d03f23ed01"
  },
  {
    "url": "static/img/icons/icon_fewer.png",
    "revision": "7dd1d148b61b7b0d57037e9fa4e0ebc4"
  },
  {
    "url": "static/img/icons/icon_firstaid.png",
    "revision": "706cee983d93b70cd51ef28b885d9d80"
  },
  {
    "url": "static/img/icons/icon_help.png",
    "revision": "dada681390c2b897ed05573d44756e34"
  },
  {
    "url": "static/img/icons/icon_lek.png",
    "revision": "981696cc0a2fb9e01f844b777092f7e9"
  },
  {
    "url": "static/img/icons/icon_messaging.png",
    "revision": "cf1efa3a991d6fb71e815855c6e3fa8d"
  },
  {
    "url": "static/img/icons/icon_missing_person.png",
    "revision": "aa4a01aaba7d5908c6b3b885b4782e8a"
  },
  {
    "url": "static/img/icons/icon_nav.png",
    "revision": "b3ea4d24583bfb6ed26a5ea1894d06d9"
  },
  {
    "url": "static/img/icons/icon_photos.png",
    "revision": "92c556decad81ac2d230db47e69719bd"
  },
  {
    "url": "static/img/icons/icon_placeholder.png",
    "revision": "7a7286a317c65a32776896e1c2618529"
  },
  {
    "url": "static/img/icons/icon_podcast.png",
    "revision": "776460515193c394d201a7f5d2866c26"
  },
  {
    "url": "static/img/icons/icon_sos.png",
    "revision": "7be596117caacb21b82292eaa2e34899"
  },
  {
    "url": "static/img/icons/icon_weather.png",
    "revision": "434362b87fa085646b1f04734edcc2fe"
  },
  {
    "url": "static/img/icons/img_default.png",
    "revision": "92fb3d831dc6bdae3ce4c908d4783e35"
  },
  {
    "url": "static/img/icons/text_default.png",
    "revision": "cce1133bbe821d306cc63ee0fc591ca3"
  },
  {
    "url": "static/img/icons/video_default.png",
    "revision": "52ae515dd3848ecf43a234a1c2b467e1"
  },
  {
    "url": "static/img/layer-switcher-maximize.png",
    "revision": "2dfeaf84b8c5fc7163167f488415690b"
  },
  {
    "url": "static/img/layer-switcher-minimize.png",
    "revision": "d731188d4944d50fed84229ff0c9dc86"
  },
  {
    "url": "static/img/logo/android-icon-144x144.png",
    "revision": "b578ea77e84c631aa5f66e502deabce9"
  },
  {
    "url": "static/img/logo/android-icon-192x192.png",
    "revision": "b3dc826e239d3b26a01cbd9a9dca5f55"
  },
  {
    "url": "static/img/logo/android-icon-36x36.png",
    "revision": "4d17eb8067c201386070fca09346f25d"
  },
  {
    "url": "static/img/logo/android-icon-48x48.png",
    "revision": "903e72b36e2ee55346eeb7ae51ec389c"
  },
  {
    "url": "static/img/logo/android-icon-72x72.png",
    "revision": "7eaf04acb304fa820bab1a8ec11ba10d"
  },
  {
    "url": "static/img/logo/android-icon-96x96.png",
    "revision": "2dfc24d79ee960b2f1c0b8e8736c7ac8"
  },
  {
    "url": "static/img/logo/apple-icon-114x114.png",
    "revision": "a1abacff25719dfc0bd8c9ab1174cbf4"
  },
  {
    "url": "static/img/logo/apple-icon-120x120.png",
    "revision": "82519b348291b527f19cc20cf29d55fb"
  },
  {
    "url": "static/img/logo/apple-icon-144x144.png",
    "revision": "b578ea77e84c631aa5f66e502deabce9"
  },
  {
    "url": "static/img/logo/apple-icon-152x152.png",
    "revision": "5c9185bbba4cada15672600f878d5ed4"
  },
  {
    "url": "static/img/logo/apple-icon-180x180.png",
    "revision": "6a4c8f06216293a2da7976681f298930"
  },
  {
    "url": "static/img/logo/apple-icon-57x57.png",
    "revision": "c38122302595cad67de5b50a1d834195"
  },
  {
    "url": "static/img/logo/apple-icon-60x60.png",
    "revision": "38b6a207a871c1f289034fce6d0dc610"
  },
  {
    "url": "static/img/logo/apple-icon-72x72.png",
    "revision": "7eaf04acb304fa820bab1a8ec11ba10d"
  },
  {
    "url": "static/img/logo/apple-icon-76x76.png",
    "revision": "0a6306eb383b90ac2dcf8106b22271fa"
  },
  {
    "url": "static/img/logo/apple-icon-precomposed.png",
    "revision": "31091e51c79244eb56e258b5136bb973"
  },
  {
    "url": "static/img/logo/apple-icon.png",
    "revision": "31091e51c79244eb56e258b5136bb973"
  },
  {
    "url": "static/img/logo/favicon-16x16.png",
    "revision": "71003e94b33e5978a17342efef5240a6"
  },
  {
    "url": "static/img/logo/favicon-32x32.png",
    "revision": "08754d227f76f1602497600559555353"
  },
  {
    "url": "static/img/logo/favicon-96x96.png",
    "revision": "2dfc24d79ee960b2f1c0b8e8736c7ac8"
  },
  {
    "url": "static/img/logo/ms-icon-144x144.png",
    "revision": "b578ea77e84c631aa5f66e502deabce9"
  },
  {
    "url": "static/img/logo/ms-icon-150x150.png",
    "revision": "e517bd7407e2d315137c830e066f2090"
  },
  {
    "url": "static/img/logo/ms-icon-310x310.png",
    "revision": "171fd08ddb6928472d73f19826794261"
  },
  {
    "url": "static/img/logo/ms-icon-70x70.png",
    "revision": "b5b8433d1c5382fb0b61850e97843127"
  },
  {
    "url": "static/img/magenta.png",
    "revision": "5ed6342a2fe97c87820ec6df87556616"
  },
  {
    "url": "static/img/mF-Logo.png",
    "revision": "72f5ccf12463ee1b4e2b01a8d0b922b9"
  },
  {
    "url": "static/img/mobile-loc.png",
    "revision": "e2633f7a2e01c8b6d9c67afd751952ca"
  },
  {
    "url": "static/img/net1.jpg",
    "revision": "7b1fe5644e88ca61a43be7566e44d152"
  },
  {
    "url": "static/img/orange button 20px.png",
    "revision": "b1edd67a7a290cb81006418947528387"
  },
  {
    "url": "static/img/red button 20px.png",
    "revision": "9e045fb7ffa251bb4eefcf1b1c23fe5f"
  },
  {
    "url": "static/img/sos.png",
    "revision": "351e3b992cd26c770a4384d52719944c"
  },
  {
    "url": "static/img/tracks.png",
    "revision": "af7713e8bb88bb1100743707240ad208"
  },
  {
    "url": "static/img/transfer.gif",
    "revision": "e86360ff6b3e5069bc33b53a87b5326f"
  },
  {
    "url": "static/img/weather/cloudy.gif",
    "revision": "75671ab493831f9d0f5357a13362bf14"
  },
  {
    "url": "static/img/weather/fewer_banner.jpg",
    "revision": "79abe4a826a797ab0af8a44f9867b282"
  },
  {
    "url": "static/img/weather/humidity.png",
    "revision": "df034ff243e071bfd0fdcead8d5111f9"
  },
  {
    "url": "static/img/weather/pressure.png",
    "revision": "fe442ebf958819652c5366352a88a58c"
  },
  {
    "url": "static/img/weather/rainy.gif",
    "revision": "c1d51764d33e79d9a60812abd04f8625"
  },
  {
    "url": "static/img/weather/sunny.gif",
    "revision": "04c18773815848fadc3e981ee7cd5533"
  },
  {
    "url": "static/img/weather/sunrise.png",
    "revision": "105453704fe96822a650cfc96f1dcc0c"
  },
  {
    "url": "static/img/weather/sunset.png",
    "revision": "7014dc664a50648de008ea8bdced51d8"
  },
  {
    "url": "static/img/weather/temperature.png",
    "revision": "4c13bacfa00b6130676ec812538573a9"
  },
  {
    "url": "static/img/weather/thunderstorm.gif",
    "revision": "3d11c7e2c55ef2e86da0110a4e404d35"
  },
  {
    "url": "static/img/weather/visibility.png",
    "revision": "3dbb488d44c6d9d72ed8872ae4c845c7"
  },
  {
    "url": "static/img/weather/wind.png",
    "revision": "c5119ae62f54ac2edf207489631665f3"
  },
  {
    "url": "static/js/controllers/alerts/alertGroupMembers.pt",
    "revision": "8991cdce36bfbd8dbf618b943c208b82"
  },
  {
    "url": "static/js/controllers/alerts/alertGroups.pt",
    "revision": "aebb9de37a0b00047eaf5986b43c6276"
  },
  {
    "url": "static/js/controllers/core/config/config.pt",
    "revision": "98351d0bfd4fbd762c8249d7f729e9c5"
  },
  {
    "url": "static/js/controllers/core/dashboard/coreAdminDashboard.pt",
    "revision": "f58198b7d5f3db6afa20bab0bcb54534"
  },
  {
    "url": "static/js/controllers/core/dashboard/coreDashboard.pt",
    "revision": "f5b67d50bf69fa12ee27fcebf66e85f4"
  },
  {
    "url": "static/js/controllers/core/dashboard/countryAdminDashboard.pt",
    "revision": "29a043d53fd24f5ff5f5262f24f8613f"
  },
  {
    "url": "static/js/controllers/core/occupation/adminoccupation.pt",
    "revision": "60b3f03c794f2dbf04ba06a1c78b7986"
  },
  {
    "url": "static/js/controllers/core/user/adminusers.pt",
    "revision": "2235fd99b4548fbc221630a84b76bf51"
  },
  {
    "url": "static/js/controllers/core/village/adminvillage.pt",
    "revision": "246f4ae58fce7e67407cb066d5c9375f"
  },
  {
    "url": "static/js/controllers/fewer/about/about.pt",
    "revision": "912196bc3fad3018b7982e45b1892e03"
  },
  {
    "url": "static/js/controllers/fewer/adminFewer.pt",
    "revision": "d41d8cd98f00b204e9800998ecf8427e"
  },
  {
    "url": "static/js/controllers/fewer/alerts/addcap.pt",
    "revision": "8cb45d22d6970f0534ccedab6f4391d3"
  },
  {
    "url": "static/js/controllers/fewer/alerts/admincap.pt",
    "revision": "d93db36d06906d44a0a77c9ffafe1ad9"
  },
  {
    "url": "static/js/controllers/fewer/alerts/listcap.pt",
    "revision": "a2858ee81dfb38ebe11dd67eeb05856f"
  },
  {
    "url": "static/js/controllers/fewer/damagereport/adminDmReport.pt",
    "revision": "85e29ff2511aea2a49bfb7e041f5281d"
  },
  {
    "url": "static/js/controllers/fewer/damagereport/dmreport.pt",
    "revision": "1f4d9eab9a65ce6d0f6b1fbf6da2b017"
  },
  {
    "url": "static/js/controllers/fewer/delivery/delivery.pt",
    "revision": "5cbf02553293368e9c72d48bb04da49e"
  },
  {
    "url": "static/js/controllers/fewer/emergency/emergencyContact.pt",
    "revision": "e82943aac1c807dd7a021bd62e0cdb38"
  },
  {
    "url": "static/js/controllers/fewer/emergency/listEmergencyContact.pt",
    "revision": "c7aad14b341295a2779f04f9d9883677"
  },
  {
    "url": "static/js/controllers/fewer/fewer.pt",
    "revision": "33c0fff48bb8fc693e414745b46c8184"
  },
  {
    "url": "static/js/controllers/fewer/help/help.pt",
    "revision": "63d65232ded74565bc4fd6d75ee4aa34"
  },
  {
    "url": "static/js/controllers/fewer/missing/adminMissingReport.pt",
    "revision": "ebf322e275283e7dd43673e5d415670e"
  },
  {
    "url": "static/js/controllers/fewer/missing/missingReport.pt",
    "revision": "83310532a57a3736399993fdc772d7f5"
  },
  {
    "url": "static/js/controllers/fewer/procedures/adminProcedures.pt",
    "revision": "5722dfbb0a594c58274af8d166cb297e"
  },
  {
    "url": "static/js/controllers/fewer/procedures/procedures.pt",
    "revision": "dcfa229cf1cb36cd24a1ac0bfb0c1988"
  },
  {
    "url": "static/js/controllers/fewer/sms/adminsms.pt",
    "revision": "42fc6bfaaf7f8c6d37d126b5cf26e424"
  },
  {
    "url": "static/js/controllers/fewer/sources/manage.pt",
    "revision": "5ec7c06267e0b7ed7559ebd4f1058f66"
  },
  {
    "url": "static/js/controllers/fewer/sources/sources.pt",
    "revision": "7d7f1e7531ea5bc7773aef1a7870aeb0"
  },
  {
    "url": "static/js/controllers/firstaid/adminFirstAid.pt",
    "revision": "5ca0f13c6c9ab8729a3a4f1768fd9c2d"
  },
  {
    "url": "static/js/controllers/firstaid/firstaid.pt",
    "revision": "d41d8cd98f00b204e9800998ecf8427e"
  },
  {
    "url": "static/js/controllers/firstaid/test.pt",
    "revision": "cf73ebbefc062c7aeb479df2296558a8"
  },
  {
    "url": "static/js/controllers/lek/adminlek.pt",
    "revision": "3c4309deb4fe63d3a7290ebfbb29e9f1"
  },
  {
    "url": "static/js/controllers/lek/lek.pt",
    "revision": "04b8c783ad3e8eea108ef55a14432b66"
  },
  {
    "url": "static/js/controllers/podcast/adminPodcast.pt",
    "revision": "0dda88edaeba8226263a8bb17b8f7297"
  },
  {
    "url": "static/js/controllers/podcast/podcast.pt",
    "revision": "50661ec4673b62693d4f7060a9ee2a37"
  },
  {
    "url": "static/js/controllers/weather/adminemail.pt",
    "revision": "aadb0ed3e0db60a79b45ea94a516b8d4"
  },
  {
    "url": "static/js/controllers/weather/adminweather.pt",
    "revision": "240fd54993a2c46dd97f5e05adf46ad6"
  },
  {
    "url": "static/js/controllers/weather/report/adminreport.pt",
    "revision": "e7d3f56e1043c1e50f977e8d40aa0da7"
  },
  {
    "url": "static/partials/admincountry.pt",
    "revision": "b80f0ace414489045cec3c64ec12d992"
  },
  {
    "url": "static/partials/coastguard.pt",
    "revision": "e4787636451bf0e383045dc87ce37d19"
  },
  {
    "url": "static/partials/fewer/alerts.pt",
    "revision": "abe3977c47f670916d4b4bb4328304b3"
  },
  {
    "url": "static/partials/fewer/notify.pt",
    "revision": "d6e1dcda5a19d162a6cf0f1e1fea7847"
  },
  {
    "url": "static/partials/gallery.pt",
    "revision": "27135f4fd805ef9dd1319c1ea40897d3"
  },
  {
    "url": "static/partials/home.pt",
    "revision": "a3c9b07b58ef29f2bf50232c2a56582c"
  },
  {
    "url": "static/partials/imagediary.pt",
    "revision": "3c59ab0452f576069849a17804006b4c"
  },
  {
    "url": "static/partials/lightbox.pt",
    "revision": "a0ed166337fc7547749299cb47e94d4b"
  },
  {
    "url": "static/partials/login.pt",
    "revision": "2e237c3a348e780ef56989796154118b"
  },
  {
    "url": "static/partials/miscellaneous.pt",
    "revision": "0ead08f00daed45aa5045dec6a6abe45"
  },
  {
    "url": "static/partials/newuserinfo.pt",
    "revision": "fbf1ba1a6c3c0e72216894c372cd53df"
  },
  {
    "url": "static/partials/triphistory.pt",
    "revision": "e8ca342aa89718abeb2fbb126313900b"
  },
  {
    "url": "static/partials/userinfo.pt",
    "revision": "b143f1e4d899f4c5643cb4c11cfcaafb"
  },
  {
    "url": "static/partials/userprofile.pt",
    "revision": "3002394c4f8b40fff3a99bf0e919772d"
  }
]);

// *** Additional Strategies based on Google's Common Recipes ***
// Caching Images - https://developers.google.com/web/tools/workbox/guides/common-recipes#caching_images
// From specific sub directory - https://developers.google.com/web/tools/workbox/guides/common-recipes#cache_resources_from_a_specific_subdirectory
workbox.routing.registerRoute(
		new RegExp("/static/userimages/"),
		workbox.strategies.staleWhileRevalidate({
			cacheName: 'user-resources',
			plugins: [
				new workbox.expiration.Plugin({
					maxEntries: 60,
					maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
				}),
			],
		})
);

// Works but does it override the previous strategies
workbox.routing.registerRoute(
		new RegExp("/"),
		workbox.strategies.networkFirst({
			cacheName: 'base_cache',
		})
);


// Add the Google API calls and resources to the cache: https://developers.google.com/web/tools/workbox/guides/common-recipes#caching_content_from_multiple_origins
workbox.routing.registerRoute(
		/.*(?:googleapis|gstatic)\.com.*$/,
		workbox.strategies.staleWhileRevalidate({
			cacheName: 'googleapis',
		})
);

// Add JS and CSS files if not previously cached - https://developers.google.com/web/tools/workbox/guides/common-recipes#cache_css_and_javascript_files
workbox.routing.registerRoute(
		/\.(?:js|css)$/,
		workbox.strategies.staleWhileRevalidate({
			cacheName: 'static-resources',
		})
);