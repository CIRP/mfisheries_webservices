# from mfisheries.util import display_debug


class NotifyError(object):
	def __init__(self, module_name, error_msg):
		self.module_name = module_name
		self.error_msg = error_msg
		print("NotifyError event successfully created")
		print("received: {0}:{1}".format(module_name, error_msg))
		# TODO Send information for error
