from sqlalchemy import engine_from_config
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker
from zope.sqlalchemy import ZopeTransactionExtension
import zope.sqlalchemy
import logging
import os
log = logging.getLogger(__name__)

# Inportant to understand the thread-local scope of the global variable
# https://docs.sqlalchemy.org/en/13/orm/contextual.html#thread-local-scope
DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension(), expire_on_commit=False))
Base = declarative_base()


def load_local_modules(config):
    # Contains the code to initialize the models and database configuration
    # Points to the util folder (however as of 02/17 does nothing)
    config.scan('mfisheries.util')
    config.scan('mfisheries.events')

    # Core Modules
    config.scan('mfisheries.core.config')
    config.scan('mfisheries.core.country')
    config.scan('mfisheries.core.user')
    config.scan('mfisheries.core')

    # Catch details, trips and hull information
    config.scan('mfisheries.models')

    # Additional Modules
    config.scan('mfisheries.modules.alerts')
    config.scan('mfisheries.modules.photos')
    config.scan('mfisheries.modules.tracks')
    config.scan('mfisheries.modules.sos')
    config.scan('mfisheries.modules.messaging')

    config.scan('mfisheries.modules.fewer')
    # config.scan('mfisheries.modules.fewer.lek')
    # config.scan('mfisheries.modules.fewer.sms')
    # config.scan('mfisheries.modules.fewer.weather')
    # config.scan('mfisheries.modules.fewer.emergencyContacts')

    # UI Specification
    config.scan('mfisheries.views')
    return config


def initialize_sql(engine):
    print("Running the initialize_sql of the models package")
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    Base.metadata.create_all(engine)
    return engine


def get_tm_session(session_factory, transaction_manager):
    """
    Get a ``sqlalchemy.orm.Session`` instance backed by a transaction.
    This function will hook the session to the transaction manager which
    will take care of committing any changes.
    - When using pyramid_tm it will automatically be committed or aborted
      depending on whether an exception is raised.
    - When using scripts you should wrap the session in a manager yourself.
      For example::
          import transaction
          engine = get_engine(settings)
          session_factory = get_session_factory(engine)
          with transaction.manager:
              dbsession = get_tm_session(session_factory, transaction.manager)
    """
    dbsession = session_factory()
    zope.sqlalchemy.register(dbsession, transaction_manager=transaction_manager)
    return dbsession


def includeme(config):
    """
    Initialize the model for a Pyramid app.

    Activate this setup using ``config.include('pyramid_scaffold.models')``.

    """
    print('Running the includeme from the models init.py file')
    settings = config.get_settings()

    # use pyramid_tm to hook the transaction lifecycle to the request
    config.include('pyramid_tm')
    settings['tm.manager_hook'] = 'pyramid_tm.explicit_manager'

    # use pyramid_retry to retry a request when transient exceptions occur
    # config.include('pyramid_retry')

    session_factory = get_session_factory(get_engine(settings))
    config.registry['dbsession_factory'] = session_factory

    # make request.dbsession available for use in Pyramid
    config.add_request_method(
        # r.tm is the transaction manager used by pyramid_tm
        lambda r: get_tm_session(session_factory, r.tm),
        'dbsession',
        reify=True
    )
    return config


def get_engine(settings, prefix='sqlalchemy.'):
    return engine_from_config(settings, prefix)


def get_session_factory(engine):
    factory = sessionmaker(expire_on_commit=False)
    factory.configure(bind=engine)
    return factory

def add_session_config(config, settings, engine):
    log.info('Running Session')
    # engine = get_engine(settings)
    factory = get_session_factory(engine)
    config.registry.db_session_factory = factory
    return config
