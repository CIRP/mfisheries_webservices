from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey
)

from mfisheries.models import Base


class TripPoints(Base):
    """The SQLAlchemy model class for a TripPoints object"""
    __tablename__ = 'trippoints'
    id = Column(Integer, primary_key=True)
    userid = Column(Integer, ForeignKey('user.id'), nullable=False)
    tripid = Column(Integer, ForeignKey('tripheaders.id'), nullable=False)
    latitude = Column(String(20))
    longitude = Column(String(20))

    def __init__(self):
        super(TripPoints, self).__init__()
