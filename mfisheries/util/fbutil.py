from pyramid.events import subscriber
from mfisheries.events import NotifyError
import logging
log = logging.getLogger(__name__)


@subscriber(NotifyError)
def executeOnError(event):
	log.error("Received - {0} {1}".format(event.module_name, event.error_msg))