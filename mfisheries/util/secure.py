import json
import logging
import os
import random
import string

from mfisheries import APP_ROOT

log = logging.getLogger(__name__)
# path = os.path.dirname(os.path.realpath(__file__))
path = APP_ROOT

FILE_NAME = '{0}/API_KEY.json'.format(path)
# print("API key found at {0}".format(FILE_NAME))


def validate(req):
    # log.info("Attempting to verify API key submitted with request")
    key = req.headers.get('apikey', 'blank')
    keys = []
    try:
        if os.path.isfile(FILE_NAME):
            with open(FILE_NAME) as data_file:
                data = json.load(data_file)
                keys = data
        else:
            # log.info("Attempting to generate an API keys")
            keys.append(new_key())
    except Exception as e:
        log.error(str(e))

    return key in keys


def gen_key():
    return ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(32))


def new_key():
    return update_key(FILE_NAME)


def update_key(filename):
    with open(filename) as data_file:
        data = json.load(data_file)
        keys = data
        data_file.close()

    keys.append(gen_key())
    with open(filename, 'wb') as outfile:
        json.dump(keys, outfile)
        outfile.close()
