import logging
# Firebase related imports
from firebase import firebase
from pyfcm import FCMNotification
from mfisheries.core.config import Config
from mfisheries.models import DBSession

log = logging.getLogger(__name__)


class NotificationManager(object):

	def __init__(self):
		self.fb = None
		self.api_key = None
		self.enable_debug = True

	def getFBApp(self):
		"""
		This method will utilize the system configured firebase connection string to setup the connection
		between the mFisheries system and Firebase
		"""
		try:
			if not self.fb:  # If Firebase not previously initialized
				c = DBSession.query(Config).filter(Config.key == "firebase_connection_string").one()
				fb_conn_str = c.value
				self.fb = firebase.FirebaseApplication(fb_conn_str)
			return self.fb
		except Exception as e:
			log.error("Error attempting to generate Firebase Application: {0}".format(e))
			DBSession.rollback()
			return False

	def getPushService(self):
		"""
		This method will return an instantiated FCM Notification Object.
		It will fetch the api key from the database and perform the necessary setup for enabling FB push notification services
		"""
		try:
			if not self.api_key:
				c = DBSession.query(Config).filter(Config.key == "firebase_api_key").one()
				self.api_key = c.value
			return FCMNotification(self.api_key)
		except Exception as e:
			log.error("Error attempting to intialise Push service: {0}".format(e))
			DBSession.rollback()
			return False

	def notifyByTopic(self, topic, msg):
		"""
		The system will provide notification to all the users subscribed to a topic
		:param topic: The topic that users will be subscribed to
		:param msg: The message that will be delivered to the subscribers
		:return: The result of the push notification service
		"""
		result = False
		try:
			push_service = self.getPushService()
			result = push_service.notify_topic_subscribers(topic_name=topic, data_message=msg)
		except Exception as e:
			log.error("Unable to send notification: {0}".format(e))
		return result

