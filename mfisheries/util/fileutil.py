import cgi
import codecs
import json
import logging
import os
import time
from base64 import decodestring

from mfisheries import APP_ROOT

log = logging.getLogger(__name__)

# TODO Redo implementation to have a single and extensible implementation

duplicates_path = os.path.join(APP_ROOT, 'duplicates')
duplicates = "duplicates.json"
duplicate_file_path = os.path.join(duplicates_path, duplicates)

res_path = os.path.join(APP_ROOT, 'static/userimages')
audio_path = os.path.join(res_path, 'audio')
video_path = os.path.join(res_path, 'video')
image_path = os.path.join(res_path, 'image')
user_image_path = os.path.join(APP_ROOT, 'static/userimages')

mesaging_path = os.path.join(APP_ROOT, 'static/userimages')
messaging_image_path = os.path.join(mesaging_path, 'image')


def store_image(userid, filename, imgdata):
	"""Deprecated"""

	log.info("FileUtil: User {0} is attempting to store image at: {1}".format(userid, filename))
	timestamp = int(time.time())
	filename = str(userid) + "_" + str(timestamp) + "_" + filename
	log.info("FileUtil: Rename the file to {0}".format(filename))
	path = os.path.join(user_image_path, filename)
	log.info("FileUtil: attempting for store file at {0}".format(path))
	try:
		with open(path, "wb") as f:
			f.write(decodestring(imgdata))
			f.close()
			log.info("FileUtil: Write operation was completed successfully")
		return path
	except Exception as e:
		log.error("FileUtil: Unable to store image: {0}".format(e))
		return None


def add_data(data):
	if not os.path.exists(duplicate_file_path):
		_list = [data]
		return write(_list)
	else:
		_list = read()
		_list.append(data)
		return write(_list)


def write(data):
	try:
		with codecs.open(duplicate_file_path, 'wb', 'utf8') as f:
			f.write(json.dumps(data, sort_keys=True, ensure_ascii=False))
		return True
	except Exception as e:
		log.error("Error: {0}".format(e.message))
		return True


def read():
	with open(duplicate_file_path) as f:
		data = json.load(f)
		return data


def store_messaging_image(data):
	try:
		log.info("Attempting to store messaging image")
		timestamp = int(time.time())
		file_name = str(data['userid']) + "_" + str(timestamp) + "_" + data['filename']
		log.info("File name {0}".format(file_name))
		
		file_path = os.path.join(messaging_image_path, file_name)
		log.info("File path {0}".format(file_path))
		
		file_data = cgi.FieldStorage()
		file_data = data['file']
		fp = open(file_path, 'wb')
		fp.write(file_data)
		fp.close()
		return file_path
	except Exception as e:
		log.error("Error occurred when storing image " + str(e))
		return ""


def store_media(data):
	try:
		log.info("Attempting to store Media")
		timestamp = int(time.time())
		file_name = str(data['userid']) + "_" + str(timestamp) + "_" + data['filename']
		log.info("File name {0}".format(file_name))
		
		if "audio" in data['type']:
			file_path = os.path.join(audio_path, file_name)
		elif 'video' in data['type']:
			file_path = os.path.join(video_path, file_name)
		elif 'image' in data['type']:
			file_path = os.path.join(image_path, file_name)
		else:
			file_path = os.path.join(res_path, file_name)
		
		log.info("File path: {0}".format(file_path))
		
		# file_data = cgi.FieldStorage()
		file_data = data['file']
		fp = open(file_path, 'wb')
		fp.write(file_data)
		fp.close()

		if 'video' in data['type']:
			if compress_video(file_path):
				log.info("Video {0} compressed".format(file_path))
			else:
				log.error("Unable to compress video: {0}".format(file_path))

		return file_path
	except Exception as e:
		log.error("Error occurred when storing media files: " + str(e))
		return ""

def compress_video(file_path):
	try:
		# Based on https://github.com/google/python-subprocess32
		import sys
		if os.name == 'posix' and sys.version_info[0] < 3:
			import subprocess32 as subprocess
		else:
			import subprocess
		# Based on https://stackoverflow.com/questions/36373795/lossy-compression-for-video-files-before-being-uploaded-on-server-django-web-ap
		subprocess.check_call(
			['ffmpeg', '-v', '-8', '-i', file_path, '-vf', 'scale=-2:480', '-preset', 'slow',
			 '-c:v', 'libx264', '-strict', 'experimental', '-c:a', 'aac', '-crf', '20', '-maxrate', '500k',
			 '-bufsize', '500k', '-r', '25', '-f', 'mp4', file_path, '-y'])
		return True
	except Exception as e:
		log.error("Error occurred when attemptign to compress stored media files: {0}".format(e))
		return False


