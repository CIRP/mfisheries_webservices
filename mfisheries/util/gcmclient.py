import json
import requests
import logging
log = logging.getLogger(__name__)

from pyfcm import FCMNotification

DEFAULT_API = "AAAAy23oC3E:APA91bHG8Qm1q9t31IJeHowvP-HnfV39WuCRwBibj7tum-kh0oFvkBrUQPrtKH_FbAJ0soh-EbtqUewN654iu49g617PnfL50VyraDbvnGZdaTsR2lvKzLGaopQnNGvpKtvRz3kW-rd9M9w6Ub6tH_06XFQELU2TcA"
gcm = "key=AIzaSyAcnj3kIrypDTXN1wjIMT-vBHpKDpeDzCA"


def getPushNotificationService():
    push_service = None
    try:
        # Attempt to load the FCM details from the database
        from mfisheries.models import DBSession
        from mfisheries.core.config import Config
        # To retrieve data from database
        api_key = DBSession.query(Config).filter(Config.key == "firebase_api_key").one()
        api_key = api_key.value
        # DBSession.rollback()
        # Attempt to load the FCM notification with key. If the key empty, the statement will fail and fall to default
        push_service = FCMNotification(api_key)
    except Exception as dbe:
        log.info("Unable to retrieve API for service from database. Attempting defaults: {0}".format(dbe))
        try:
            push_service = FCMNotification(api_key=DEFAULT_API)
        except Exception as e:
            log.error("All strategies to load notification service failed: {0}".format(e))
    return push_service


def notifyAlertGroup(groupid, data):
    """
    The function will be used to provide notification to alert groups based on the parameters.
    The function is a convenient way to send notification to alert groups.
    It utilises the notifyByTopic function
    :param groupid: sends a notification to groups in the format "alert-group-<groupid>
    :param data: The string message data that will be sent to the subscribed clients
    :return: Either the result from the notification service or False if unsuccessful
    """
    topic_str = "alert-group-{0}".format(groupid)
    return notifyByTopic(topic_str, data)


def notifyByTopic(topic, data):
    """
    The function will be used to provide notification to subscribers based on the parameters provided.
    :param topic: The name of the group to send notification to
    :param data: The string message data that will be sent to the subscribed clients
    :return: Either the result from the notification service or False if unsuccessful
    """
    try:
        push_service = getPushNotificationService()
        log.info("Attempting to send notification to subscribers of topic: {0}".format(topic))
        result = push_service.notify_topic_subscribers(topic_name=topic, data_message=data)
        log.info("Notification Service provided {0} as a response".format(result))
        return result
    except Exception as e:
        log.error("Unable to send notification: {0}".format(e))
        return False


def sendMsg(message, subscribers):
    msg = {
        "data": {"message": message['msg']},
        "registration_ids": subscribers
    }

    res = requests.post(
        'https://gcm-http.googleapis.com/gcm/send',
        data=json.dumps(msg),  # this is the original line
        headers={'Authorization': gcm, 'Content-Type': 'application/json'}
    )

    print(("Status Code: {0} Text: {1}".format(res.status_code,res.text)))
    if res.status_code == 200:
        resp = json.loads(res.text)
        if resp['success'] == 0:
            return False
        else:
            return True
    else:
        return False


def testNotify():
    # Must be executed from the root of projects
    # from pyramid.paster import get_appsettings
    # from sqlalchemy import engine_from_config
    print("Testing the ability to send notifications")
    # ini_full_path = "./development.ini"
    # settings = get_appsettings(ini_full_path, name='main')
    # engine = engine_from_config(settings, 'sqlalchemy.')

    # userRegistrationID = "c-6w2WyCNEk:APA91bEMzHtZdZTCGicKw_evp5SH4gYZ3U7O33i2XnK6C2p7MzJtHct8_JfxXr5S6zI_9zute2lozdDGhHUR8AKsEtUuxZh8rFiEtijd_6hPOoFHdOXh4xfmX53byP-fqH7w9dI4oapv"
    push_service = getPushNotificationService()
    userRegistrationID = "f9zyEBq50yo:APA91bHMt-ViVhJOGAKrjp0bp8pKXSTyicw0aZWTyCcLS00nCcb9zlbrzS55O_DnUc2-LGB2jOq74uf-KNICQtR_oSezQ_q1O1NoHoXMpVHF2Zj4cjCRaaCBnGCcBwR_EVGZaXMgF3vI"
    res = push_service.notify_single_device(userRegistrationID, "Testing the Ability to send messages to individual if app in background", "Test Message")
    print(dict(res))


if __name__ == "__main__":
    testNotify()