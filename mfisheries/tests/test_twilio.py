import unittest

from mfisheries.modules.fewer.sms.twilioSms import TwilioSms
from mfisheries.modules.fewer.sms.smsSubscriptions import SmsSubscription
import json
import os
import unittest

import transaction
from pyramid.paster import get_appsettings
from webtest import TestApp
from zope.sqlalchemy import register

from mfisheries import APP_ROOT
from mfisheries import main
from zope.sqlalchemy import register
from mfisheries.models import DBSession

dir_path = os.path.dirname(os.path.realpath(__file__))


class TwilioTest(unittest.TestCase):

    def setUp(self):

        register(DBSession, transaction_manager=transaction.manager)

    def test_send_sms(self):
        message = "This is a simple sms test message"
        recipient = "+18687777479"
        # TODO - Kwasi  - Use the Testing capability of Twilio - https://www.twilio.com/docs/iam/test-credentials
        # Also note that it is strongly advised to avoid having the account information in the repository
        # message_id = TwilioSms().send_sms(recipient, message)
        # self.assertIsNotNone(message_id)

