import json
import unittest

import transaction
from pyramid.paster import get_appsettings
from webtest import TestApp
from zope.sqlalchemy import register

from mfisheries import APP_ROOT
from mfisheries import main
from ..models import DBSession


class APITests(unittest.TestCase):
	def setUp(self):
		settings = get_appsettings('mfisheries/tests/test.ini', name='main')
		self.testapp = TestApp(main(global_config=None, **settings))

		fp = open('{0}/API_KEY.json'.format(APP_ROOT))
		data = json.load(fp)
		self.header = {"apikey": str(data[0])}
		self.countries = ['Trinidad', 'Tobago']
		from mfisheries.scripts.initializedb import install_database_tables
		engine, res, e = install_database_tables(settings={
			'sqlalchemy.url': 'sqlite:///test.db'
		})
		register(DBSession, transaction_manager=transaction.manager)

	def test_root(self):
		print("Running First Test")
		res = self.testapp.get("/", status=200)
		self.assertTrue(b'mFisheries' in res.body)

	def test_countries(self):
		self.testapp.get('/api/countries',headers=self.header, status=200)

	def test_modules(self):
		self.testapp.get('/api/modules',headers=self.header, status=200)

	def test_villages(self):
		self.testapp.get('/api/villages',headers=self.header, status=200)

	def test_users(self):
		self.testapp.get('/api/users',headers=self.header, status=200)

	def test_module_files(self):
		countries = [
			'atb',
			'dominica',
			'grenada',
			'skn',
			'slu',
			'svg',
			'tobago',
			'trinidad'
		]
		modules = [
			'Podcast.zip',
			# 'Navigation.zip',
			'FirstAid.zip'
		]
		for x in range(0, len(countries)):
			base_url = '/static/country_modules/' + countries[x]
			print("Base url is: " + base_url)
			for mod in modules:
				url = base_url + '/' + mod
				print("Requesting: " + url)

				self.testapp.get(url, status=200)

			url = base_url + '/FirstAid.zip'
			print("Requesting: " + url)
			self.testapp.get(url, status=200)

	def test_unexisting_page(self):
		self.testapp.get('/SomePage', status=404)
