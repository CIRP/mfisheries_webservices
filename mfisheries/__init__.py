import os
import threading
import json
import logging
import firebase_admin

from apscheduler.schedulers.background import BackgroundScheduler
from pyramid.config import Configurator
from pyramid.settings import asbool
from sqlalchemy import engine_from_config
from mfisheries.models import initialize_sql, DBSession, Base, load_local_modules
from mfisheries.modules.fewer.weather.parsers import scheduler
from firebase_admin import credentials
from scripts.broadcast import trigger_notification # Need to come after APP_ROOT

APP_ROOT = os.path.dirname(os.path.realpath(__file__))
log = logging.getLogger(__name__)


def load_system_modules(config):
	config.include('pyramid_chameleon')
	config.include('cornice')
	return config


def setup_background_scheduler():
	from mfisheries.modules.fewer.broadcastmanager import run_bcast_mgmtr_default
	from mfisheries.modules.fewer.weather.broadcastweather import run_weather_broadcast
	from mfisheries.modules.alerts.scheduler import run_alert_scheduled_task
	from mfisheries.core.config.config import Config

	# Change Logging Level due to frequency of pooling
	# logging.getLogger("urllib3").setLevel(logging.ERROR)
	# logging.getLogger("apscheduler").setLevel(logging.ERROR)
	# logging.getLogger("mfisheries").setLevel(logging.ERROR)

	background_scheduler = BackgroundScheduler()
	try:
		interval = DBSession.query(Config).filter(Config.key == "alert_source_interval").one()
		alert_interval = int(interval.value)
		# Background tasks for Broadcasting the Alerts
		broadcast_job = background_scheduler.add_job(run_bcast_mgmtr_default, 'interval', minutes=alert_interval)
		# The Threading task for Weather source retrieval
		broadcast_weather = background_scheduler.add_job(run_weather_broadcast, 'interval', minutes=alert_interval)
		# Update to ensure that the system clears expired alerts
		alert_update = background_scheduler.add_job(run_alert_scheduled_task, 'interval', minutes=alert_interval)
		# run the process for setting up
		threading.Timer(10, scheduler.run_setup_weather_scheduled_task).start()

		background_scheduler.start()
		# Printing for debugging purposes
		log.info(broadcast_job)
		log.info(broadcast_weather)
		log.info(alert_update)
	except Exception as e:
		log.error("Error occurred when starting scheduler: {0}".format(e))


def setup_views(config):
	config.add_static_view('static', 'static', cache_max_age=3600)
	config.add_route('home', '/')
	# Load Service workers
	config.add_route('manifest.json', '/manifest.json')
	config.add_route('sw.js', '/sw.js')
	config.add_route('push.js', '/serviceWorker.min.js')
	config.add_route('push.fcm.js', '/firebase-messaging-sw.js')
	config.add_route('fb-msg-sw.js', '/firebase-messaging-sw.js')


	return config


def setup_firebase_admin():
	try:
		from mfisheries.core.config.config import Config
		admin_skd_key_str = DBSession.query(Config).filter(Config.key == "firebase_admin_sdk_key").one_or_none()
		if admin_skd_key_str and len(admin_skd_key_str) > 2:
			admin_skd_key = json.loads(admin_skd_key_str.value)
			# Setup the Firebase Admin
			cred = credentials.Certificate(admin_skd_key)
			firebase_admin.initialize_app(cred)
	except Exception as e:
		log.error(e)


def main(global_config, **settings):
	""" This function returns a Pyramid WSGI application."""
	print("Running the __init__ of the mFisheries project")
	engine = engine_from_config(settings, 'sqlalchemy.')
	
	DBSession.configure(bind=engine)
	Base.metadata.bind = engine
	
	config = Configurator(settings=settings)
	config = load_system_modules(config)
	config = load_local_modules(config)
	config = setup_views(config)

	setup_firebase_admin()
	
	run_scheduler = asbool(settings.get('mfisheries.run_scheduler', 'true'))
	if run_scheduler:
		setup_background_scheduler()
	
	return config.make_wsgi_app()