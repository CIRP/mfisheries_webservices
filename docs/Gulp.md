# JavaScript/Front-end Development
The following section highlights the use of the gulp system to test and build the front-end JavaScript code.

The gulp system takes the code written for the mFisheries angular web app within the js folders and subfolders (controller, services, etc).

The "gulp watch" command will look for changes in the JS and CSS files. When a change (upon saving) is detected, the system will lint the JS code and compile the JS code to a single app.js file.


## lint
- Lint all the developed JS file using jshint

## pack-js
- Compile all JS file into the app.js file

## watch-js
- watch for changes in JS files

## pack-js-deploy
- Compile, minify and obstrucate JS file

## pack-libs
- Compile the Bower Components into a bundle.js file
- NB: Not functional as of 25-05-17

## pack-css
- Compile all the css files with the css and vendor folders into the stylesheet.cs file

## watch-css
- Listen for changes the developed CSS files and run the package file if detected

## serve
- Run the pyramid command to lauch the pyramid app
- NB: Not displaying needed debugging information as of 25-05-17

## deploy
- Runs all the specific commands needed for launch the application
- NB: as of 25-05-17 only packages JS files

## watch
- Listen for changes in JS and CSS files. Lints and Compiles files in the app.js and stylesheet.cs files

## default (i.e. gulp)
- Runs the watching, and serving