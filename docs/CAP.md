run_bcast_mgmtr_default
* Is executed in the main __init__.py file of the project
* Is defined in the broadcastmanager.py file in the fewer module pacakge
* Utilizes the BroadcastManager also defined in the same file
* Executes the checkallSourcesForAlert which checks all the CAP sources for new alerts



BroadcastManager::checkAllSourcesForAlerts
*  



run_alert_scheduled_task
* Is executed in the main __init__.py file of the project
* Is defined in the scheduler.py file in the alerts module package
* The tasks includes:
    * Marking all alerts older than the time-to-live are marked as inactive
    * Ensure that all alerts to be broadcasted were distributed to the appropriate channels
