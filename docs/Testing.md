mFisheries Web and API Services Platform
==================
##Testing
### 1. Install software dependencies
Installing Pytest and other requirements
```bash
pip install pytest
pip install -e ".[testing]"
```

### 2. Running General Test 
- This run all the test within the mfisheries/test folder 
```bash
python setup.py test
```
or
```bash
pip install -e ".[testing]"
py.test --cov -q
```

### 3. Testing a specific module

- The Remove the existing database models
```bash
rm test.db
```
- Test Models
```bash
py.test -q -s mfisheries/tests/test_models.py::ModelsTests
```

- Test API calls
```bash
py.test -q -s mfisheries/tests/test_api.py::APITests
```
- Test API generated with Cornice Resources
```bash
py.test -q -s mfisheries/tests/test_resources.py::APIResources
```

or specific test in resources
```bash
py.test -q -s mfisheries/tests/test_resources.py::APIResources::test_country_loc
```

- Test Extractors
```bash
py.test -q -s mfisheries/tests/test_extractors.py::ExtractorTests
```

or specific test in extractors
```bash
py.test -q -s mfisheries/tests/test_extractors.py::ExtractorTests::test_get_extractor_files
```
- Test Twilio
```bash
py.test -q -s mfisheries/tests/test_twilio.py::TwilioTest
```

or specific test in twilio
```bash
py.test -q -s mfisheries/tests/test_twilio.py::TwilioTest::test_send_sms
```

Currently we remove the test.db file before running the test to ensure that the insertion test
