mFisheries Web and API Services Platform - Serviceworkers
==================

## Introduction

## Installation
We will use the guide from [Google Workbox](https://developers.google.com/web/tools/workbox/guides/get-started)
The following guide will only highlight cases where the installation is different from the recommended installation

1. Add the sw.js as a view in the file "mfisheries/views.py"
```python
@view_config(route_name='sw.js')
def push_fcm_js_sw(request):
	response = FileResponse(
		"{0}/sw.js".format(SW_DIR),
		request=request,
		content_type='application/javascript'
	)
	return response
```

2. We will use the Guide built for gulp https://developers.google.com/web/tools/workbox/guides/precache-files/workbox-build

3. We use only the injectManifest option because we want to add other functionality to the sw-src.js file

4. We configured the gulp command to run the appropriate command to generate new precache files list
```js
const workbox_config = require("./workbox-config");
const workboxBuild = require('workbox-build');
gulp.task('service-worker', function(){
	return workboxBuild.injectManifest(workbox_config);
});
```


## Browser guides for serviceworkers during development
#### Mozilla
1. https://hacks.mozilla.org/2016/03/debugging-service-workers-and-push-with-firefox-devtools/
2. Use about:serviceworkers in the browser bar to update URL

#### Chrome
1. https://developers.google.com/web/tools/workbox/guides/troubleshoot-and-debug