import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.md')) as f:
    README = f.read()
with open(os.path.join("{0}/docs".format(here), 'CHANGES.txt')) as f:
    CHANGES = f.read()


requires = [
    # Pyramid specific packages
    'pyramid',
    'pyramid_chameleon',
    'pyramid_debugtoolbar',
    'pyramid_mako',
    'pyramid_tm',
    'plaster_pastedeploy',
    'zope.interface',
    'zope.sqlalchemy',
    'waitress',
    'cornice == 2.4.0',
    # MySQL related packages
    'mysql-python',
    'SQLAlchemy',
    'transaction',
    'sqlalchemy-pagination',
    # For processing KML files
    'fastkml',
    'lxml',
    'pygeoif',
    'simplekml',

    'pyOpenSSL',
    'python-crontab',
    'python-dateutil',
    'alembic',
    'apscheduler',
    'beautifulsoup4',
    'cryptography',
    'requests',
    'simplejson',

    'twilio', # Used for the sending of SMS-based alerts

    'subprocess32', # Used for processing video compression in separate process
    # Firebase Related
    'pyfcm',
    'python-firebase',
    'firebase-admin',
    'grpcio',
    # GeoNode Shapefiles
    'pyshp',
    # Sentry for Crash Monitoring
    "raven"
]

test_requires = [
    'WebTest >= 1.3.1', 
    'pytest == 2.9.2',
    'pytest-cov',
]


setup(
    name='mfisheries',
    version='3.7',
    description='mfisheries',
    long_description=README + '\n\n' + CHANGES,
    classifiers=[
        "mFisheries", "CIRP", "UWI", "STA"
    ],
    author='Caribbean ICT Research Programme',
    author_email='mfisheries2011@gmail.com',
    url='http://www.cirp.org.tt',
    keywords='mfisheries',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    test_suite='mfisheries',
    # tests_require=test_requires,
    extras_require={
      'testing': test_requires,
    },
    install_requires=requires,
    entry_points="""\
        [paste.app_factory]
        main = mfisheries:main
        [console_scripts]
        initialize_mfisheries_db = mfisheries.scripts.initializedb:main
        update_alerts = mfisheries.modules.alerts.scheduler:run_alert_scheduled_task
        run_event_test = mfisheries.scripts.broadcast:trigger_notification
        show_settings = mfisheries.scripts.manage_settings:show_settings
        fetch_weather = mfisheries.modules.fewer.weather.parsers.scheduler:run_weather_task
        run_alert_broadcast = mfisheries.modules.fewer.broadcastmanager:run_bcast_mgmtr_default
    """,
)
