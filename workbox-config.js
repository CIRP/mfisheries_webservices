module.exports = {
  "globDirectory": "mfisheries/static/",
  "globPatterns": [
	  "dist/**/*.{json,html,css,gif,png,js,jpg}",
	  "css/**/*.{json,html,css,gif,png,js,jpg}",
	  "img/**/*.{json,html,css,gif,png,js,jpg}",
	  "js/**/*.{html,pt}",
	  "partials/**/*.{html,pt}"
  ],
  "swDest": "mfisheries/static/serviceworkers/sw.js",
  "swSrc":"mfisheries/static/serviceworkers/sw-src.js",
  "globIgnores": [
    "src/sw-src.js"
  ],
	modifyUrlPrefix: {
		'dist/': 'static/dist/',
		'css/': 'static/css/',
		'js/': 'static/js/',
		'img/': 'static/img/',
		'partials/': 'static/partials/'
	}
};